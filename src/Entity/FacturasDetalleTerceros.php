<?php

namespace App\Entity;

use App\Repository\FacturasDetalleTercerosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturasDetalleTercerosRepository::class)
 */
class FacturasDetalleTerceros
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=FacturasDetalle::class, inversedBy="facturasDetalleTerceros")
     */
    private $detalle;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="facturasDetalleTerceros")
     */
    private $facturas;

    /**
     * @ORM\Column(type="string", length=13)
     */
    private $rfc;

    /**
     * @ORM\Column(type="string", length=140)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $regimen_fiscal;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $domicilio;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDetalle(): ?FacturasDetalle
    {
        return $this->detalle;
    }

    public function setDetalle(?FacturasDetalle $detalle): self
    {
        $this->detalle = $detalle;

        return $this;
    }

    public function getFacturas(): ?Facturas
    {
        return $this->facturas;
    }

    public function setFacturas(?Facturas $facturas): self
    {
        $this->facturas = $facturas;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRegimenFiscal(): ?string
    {
        return $this->regimen_fiscal;
    }

    public function setRegimenFiscal(string $regimen_fiscal): self
    {
        $this->regimen_fiscal = $regimen_fiscal;

        return $this;
    }

    public function getDomicilio(): ?string
    {
        return $this->domicilio;
    }

    public function setDomicilio(string $domicilio): self
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
