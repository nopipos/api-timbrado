<?php

namespace App\Entity\Tuweb;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Entity\Sucursales;
use App\Repository\Tuweb\TicketsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TicketsRepository::class)
 */
class Tickets
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Facturas::class, mappedBy="factura")
     */
    private $factura;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $ticket;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="tickets")

     */
    private $empresa;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="boolean")
     */
    private $impreso;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\Column(type="float")
     */
    private $subtotal;

    /**
     * @ORM\Column(type="float")
     */
    private $descuento;

    /**
     * @ORM\Column(type="float")
     */
    private $iva;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $metodo_pago;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $forma_pago;

    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     */
    private $rfc;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=TicketsDetalle::class, mappedBy="ticket")
     */
    private $ticketsDetalles;

    /**
     * @ORM\OneToOne(targetEntity=Sucursales::class, cascade={"persist", "remove"})
     */
    private $sucursal;

    public function __construct()
    {
        $this->factura = new ArrayCollection();
        $this->ticketsDetalles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Facturas[]
     */
    public function getFactura(): Collection
    {
        return $this->factura;
    }

    public function addFactura(Facturas $factura): self
    {
        if (!$this->factura->contains($factura)) {
            $this->factura[] = $factura;
            $factura->setFactura($this);
        }

        return $this;
    }

    public function removeFactura(Facturas $factura): self
    {
        if ($this->factura->removeElement($factura)) {
            // set the owning side to null (unless already changed)
            if ($factura->getFactura() === $this) {
                $factura->setFactura(null);
            }
        }

        return $this;
    }

    public function getTicket(): ?string
    {
        return $this->ticket;
    }

    public function setTicket(string $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getImpreso(): ?bool
    {
        return $this->impreso;
    }

    public function setImpreso(bool $impreso): self
    {
        $this->impreso = $impreso;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(float $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getDescuento(): ?float
    {
        return $this->descuento;
    }

    public function setDescuento(float $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getIva(): ?float
    {
        return $this->iva;
    }

    public function setIva(float $iva): self
    {
        $this->iva = $iva;

        return $this;
    }

    public function getMetodoPago(): ?string
    {
        return $this->metodo_pago;
    }

    public function setMetodoPago(?string $metodo_pago): self
    {
        $this->metodo_pago = $metodo_pago;

        return $this;
    }

    public function getFormaPago(): ?string
    {
        return $this->forma_pago;
    }

    public function setFormaPago(string $forma_pago): self
    {
        $this->forma_pago = $forma_pago;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(?string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|TicketsDetalle[]
     */
    public function getTicketsDetalles(): Collection
    {
        return $this->ticketsDetalles;
    }

    public function addTicketsDetalle(TicketsDetalle $ticketsDetalle): self
    {
        if (!$this->ticketsDetalles->contains($ticketsDetalle)) {
            $this->ticketsDetalles[] = $ticketsDetalle;
            $ticketsDetalle->setTicket($this);
        }

        return $this;
    }

    public function removeTicketsDetalle(TicketsDetalle $ticketsDetalle): self
    {
        if ($this->ticketsDetalles->removeElement($ticketsDetalle)) {
            // set the owning side to null (unless already changed)
            if ($ticketsDetalle->getTicket() === $this) {
                $ticketsDetalle->setTicket(null);
            }
        }

        return $this;
    }

    public function getSucursal(): ?Sucursales
    {
        return $this->sucursal;
    }

    public function setSucursal(?Sucursales $sucursal): self
    {
        $this->sucursal = $sucursal;

        return $this;
    }
}
