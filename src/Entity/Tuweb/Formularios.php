<?php

namespace App\Entity\Tuweb;

use App\Entity\Empresas;
use App\Repository\Tuweb\FormulariosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FormulariosRepository::class)
 */
class Formularios
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="formularios")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $campo;

    /**
     * @ORM\Column(name="tipo", type="string", length=25, nullable=true, options={"default":"texto"})
     */
    private $tipo;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\Column(type="integer")
     */
    private $requerido;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCampo(): ?string
    {
        return $this->campo;
    }

    public function setCampo(string $campo): self
    {
        $this->campo = $campo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo): self
    {
        $this->tipo = $tipo;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getRequerido(): ?int
    {
        return $this->requerido;
    }

    public function setRequerido(int $requerido): self
    {
        $this->requerido = $requerido;

        return $this;
    }

}
