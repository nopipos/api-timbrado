<?php

namespace App\Entity;

use App\Repository\ClienteDireccionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClienteDireccionRepository::class)
 * @ORM\Table (name="cliente_direccion")
 */
class ClienteDireccion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Clientes::class, inversedBy="direcciones")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cliente;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $calle;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $no_ext;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $no_int;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $colonia;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $municipio;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $estado;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $pais;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $referencia;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true,"default" = 1} )
     */
    private $estatus;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $coordenadas;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCliente(): ?Clientes
    {
        return $this->cliente;
    }

    public function setCliente(?Clientes $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCalle(): ?string
    {
        return $this->calle;
    }

    public function setCalle(?string $calle): self
    {
        $this->calle = $calle;

        return $this;
    }

    public function getNoExt(): ?string
    {
        return $this->no_ext;
    }

    public function setNoExt(?string $no_ext): self
    {
        $this->no_ext = $no_ext;

        return $this;
    }

    public function getNoInt(): ?string
    {
        return $this->no_int;
    }

    public function setNoInt(?string $no_int): self
    {
        $this->no_int = $no_int;

        return $this;
    }

    public function getColonia(): ?string
    {
        return $this->colonia;
    }

    public function setColonia(?string $colonia): self
    {
        $this->colonia = $colonia;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    public function setMunicipio(?string $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getPais(): ?string
    {
        return $this->pais;
    }

    public function setPais(?string $pais): self
    {
        $this->pais = $pais;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getReferencia(): ?string
    {
        return $this->referencia;
    }

    public function setReferencia(string $referencia): self
    {
        $this->referencia = $referencia;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getCoordenadas(): ?string
    {
        return $this->coordenadas;
    }

    public function setCoordenadas(?string $coordenadas): self
    {
        $this->coordenadas = $coordenadas;

        return $this;
    }
}
