<?php

namespace App\Entity;

use App\Repository\FacturasTimbreRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturasTimbreRepository::class)
 */
class FacturasTimbre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="facturasTimbres")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="timbre")
     */
    private $factura;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $codigo;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $mensaje;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha_peticion;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $ruta_timbre;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha_cancelacion;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $motivo_cancelacion;

    /**
     * @ORM\Column(type="string", length=39, nullable=true)
     */
    private $uuid_relacion;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $ruta_cancelacion;

    /**
     * @ORM\ManyToOne(targetEntity=Pac::class, inversedBy="timbre")
     */
    private $pac;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha_timbrado;

    /**
     * @ORM\Column(type="string", length=39, nullable=true)
     */
    private $folio_fiscal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(?string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getMensaje(): ?string
    {
        return $this->mensaje;
    }

    public function setMensaje(?string $mensaje): self
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    public function getFechaPeticion(): ?\DateTimeInterface
    {
        return $this->fecha_peticion;
    }

    public function setFechaPeticion(\DateTimeInterface $fecha_peticion): self
    {
        $this->fecha_peticion = $fecha_peticion;

        return $this;
    }

    public function getRutaTimbre(): ?string
    {
        return $this->ruta_timbre;
    }

    public function setRutaTimbre(?string $ruta_timbre): self
    {
        $this->ruta_timbre = $ruta_timbre;

        return $this;
    }

    public function getFechaCancelacion(): ?\DateTimeInterface
    {
        return $this->fecha_cancelacion;
    }

    public function setFechaCancelacion(?\DateTimeInterface $fecha_cancelacion): self
    {
        $this->fecha_cancelacion = $fecha_cancelacion;

        return $this;
    }

    public function getMotivoCancelacion(): ?string
    {
        return $this->motivo_cancelacion;
    }

    public function setMotivoCancelacion(?string $motivo_cancelacion): self
    {
        $this->motivo_cancelacion = $motivo_cancelacion;

        return $this;
    }

    public function getUuidRelacion(): ?string
    {
        return $this->uuid_relacion;
    }

    public function setUuidRelacion(?string $uuid_relacion): self
    {
        $this->uuid_relacion = $uuid_relacion;

        return $this;
    }

    public function getRutaCancelacion(): ?string
    {
        return $this->ruta_cancelacion;
    }

    public function setRutaCancelacion(?string $ruta_cancelacion): self
    {
        $this->ruta_cancelacion = $ruta_cancelacion;

        return $this;
    }

    public function getPac(): ?Pac
    {
        return $this->pac;
    }

    public function setPac(?Pac $pac): self
    {
        $this->pac = $pac;

        return $this;
    }

    public function getFechaTimbrado(): ?\DateTimeInterface
    {
        return $this->fecha_timbrado;
    }

    public function setFechaTimbrado(\DateTimeInterface $fecha_timbrado): self
    {
        $this->fecha_timbrado = $fecha_timbrado;

        return $this;
    }

    public function getFolioFiscal(): ?string
    {
        return $this->folio_fiscal;
    }

    public function setFolioFiscal(?string $folio_fiscal): self
    {
        $this->folio_fiscal = $folio_fiscal;

        return $this;
    }

    public function existeUUID(ManagerRegistry $repo,$uuid){

        $peticion = $repo->getRepository(self::class);
        $modelUsuario = $peticion->findOneBy(['folio_fiscal'=>$uuid]);

        if($modelUsuario == null)
            return false;
        else
            return true;
            
    }
}
