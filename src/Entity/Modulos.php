<?php

namespace App\Entity;

use App\Repository\ModulosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ModulosRepository::class)
 * @ORM\Table (name="modulos")
 */
class Modulos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nombre;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=EmpresasModulos::class, mappedBy="modulo")
     */
    private $modulo;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $interno;

    public function __construct()
    {
        $this->modulo = new ArrayCollection();
        $this->modulos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|EmpresasModulos[]
     */
    public function getModulo(): Collection
    {
        return $this->modulo;
    }

    public function addModulo(EmpresasModulos $modulo): self
    {
        if (!$this->modulo->contains($modulo)) {
            $this->modulo[] = $modulo;
            $modulo->setModulo($this);
        }

        return $this;
    }

    public function removeModulo(EmpresasModulos $modulo): self
    {
        if ($this->modulo->removeElement($modulo)) {
            // set the owning side to null (unless already changed)
            if ($modulo->getModulo() === $this) {
                $modulo->setModulo(null);
            }
        }

        return $this;
    }

    public function getInterno(): ?int
    {
        return $this->interno;
    }

    public function setInterno(?int $interno): self
    {
        $this->interno = $interno;

        return $this;
    }

}
