<?php

namespace App\Entity;

use App\Repository\ComplementosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComplementosRepository::class)
 */
class Complementos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $icono;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=EmpresasComplementos::class, mappedBy="complemento")
     */
    private $empresasComplementos;

    public function __construct()
    {
        $this->empresasComplementos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getIcono(): ?string
    {
        return $this->icono;
    }

    public function setIcono(string $icono): self
    {
        $this->icono = $icono;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|EmpresasComplementos[]
     */
    public function getEmpresasComplementos(): Collection
    {
        return $this->empresasComplementos;
    }

    public function addEmpresasComplemento(EmpresasComplementos $empresasComplemento): self
    {
        if (!$this->empresasComplementos->contains($empresasComplemento)) {
            $this->empresasComplementos[] = $empresasComplemento;
            $empresasComplemento->setComplemento($this);
        }

        return $this;
    }

    public function removeEmpresasComplemento(EmpresasComplementos $empresasComplemento): self
    {
        if ($this->empresasComplementos->removeElement($empresasComplemento)) {
            // set the owning side to null (unless already changed)
            if ($empresasComplemento->getComplemento() === $this) {
                $empresasComplemento->setComplemento(null);
            }
        }

        return $this;
    }
}
