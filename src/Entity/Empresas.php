<?php

namespace App\Entity;

use App\Entity\Addendas\AddendaDatos;
use App\Entity\Addendas\AddendaEmpresas;
use App\Entity\Catalogos\empresasDocumentos;
use App\Entity\Complementos\CCE\cce;
use App\Entity\Complementos\CCE\cceDestino;
use App\Entity\Complementos\CCE\cceMercancias;
use App\Entity\Complementos\CCE\cceMercanciasDetalle;
use App\Entity\Complementos\CCE\ccePropietario;
use App\Entity\Complementos\CP\cMercancias;
use App\Entity\Complementos\CP\cPersonas;
use App\Entity\Complementos\CP\cUbicaciones;
use App\Entity\Complementos\CP\cp;
use App\Entity\Complementos\CP\cpFigura;
use App\Entity\Complementos\CP\cpFiguraParte;
use App\Entity\Complementos\CP\cpMercancia;
use App\Entity\Complementos\CP\cpMercanciaAutoFed;
use App\Entity\Complementos\CP\cpMercanciaAutoFedRemo;
use App\Entity\Complementos\CP\cpMercanciaDetalle;
use App\Entity\Complementos\CP\cpMercanciaDetalleCantidad;
use App\Entity\Complementos\CP\cpMercanciaDetalleGuia;
use App\Entity\Complementos\CP\cpMercanciaDetallePedi;
use App\Entity\Complementos\CP\cpMercanciaDetalles;
use App\Entity\Complementos\CP\cpUbicacion;
use App\Entity\Complementos\Donat\donatarias;
use App\Entity\Nomina\Conceptos;
use App\Entity\Customers;
use App\Entity\FormasPago;

use App\Entity\Nomina\EmpleadoConcepto;
use App\Entity\Nomina\EmpleadoDeducciones;
use App\Entity\Nomina\EmpleadoHorasExtra;
use App\Entity\Nomina\EmpleadoIncapacidades;
use App\Entity\Nomina\EmpleadoJubilacion;
use App\Entity\Nomina\EmpleadoOtrosPagos;
use App\Entity\Nomina\EmpleadoPercepciones;
use App\Entity\Nomina\EmpleadoSeparacion;
use App\Entity\Nomina\Empleados;
use App\Entity\Nomina\Nomina;
use App\Entity\SAT\SatDescargas;
use App\Entity\Tuweb\Formularios;
use App\Entity\Tuweb\Tickets;
use App\Repository\EmpresasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpresasRepository::class)
 * @ORM\Table(indexes={@ORM\Index(name="rfc_em", columns={"rfc"})})
 */
class Empresas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer" , options={"unsigned"=true})
     *
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Customers::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity=FormasPago::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $pago_default;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=13)
     */
    private $rfc;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $curp;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $regimen;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $cer;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $key_csd;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $password;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $pais;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $estado;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $ciudad;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $municipio;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $colonia;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $num_ext;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $num_int;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $cp;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true,"default" = 1})
     */
    private $estatus;


    /**
     * @ORM\OneToMany(targetEntity=Clientes::class, mappedBy="empresa")
     */
    private $clientes;

    /**
     * @ORM\OneToMany(targetEntity=Productos::class, mappedBy="empresa")
     */
    private $productos;

    /**
     * @ORM\OneToMany(targetEntity=Series::class, mappedBy="empresa")
     */
    private $series;

    /**
     * @ORM\OneToMany(targetEntity=ClientesMail::class, mappedBy="empresa")
     */
    private $mails;

    /**
     * @ORM\OneToMany(targetEntity=Facturas::class, mappedBy="empresa")
     */
    private $facturas;

    /**
     * @ORM\OneToMany(targetEntity=Sucursales::class, mappedBy="empresa")
     */
    private $sucursales;

    /**
     * @ORM\OneToMany(targetEntity=FacturaDetalleImpuesto::class, mappedBy="empresa")
     */
    private $empresas;

    /**
     * @ORM\OneToMany(targetEntity=ComplementosPago::class, mappedBy="empresa")
     */
    private $pagos;

    /**
     * @ORM\OneToMany(targetEntity=ComplementosPagoDocumento::class, mappedBy="empresa")
     */
    private $documentos;

    /**
     * @ORM\OneToMany(targetEntity=Usuarios::class, mappedBy="empresa")
     */
    private $usuarios2;

    /**
     * @ORM\OneToMany(targetEntity=EmpresasModulos::class, mappedBy="empresa")
     */
    private $modulos;

    /**
     * @ORM\OneToMany(targetEntity=UsuariosEmpresas::class, mappedBy="empresa")
     */
    private $usuarios;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $imagen;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $cer_fiel;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $key_fiel;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $password_fiel;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $calle;

    /**
     * @ORM\OneToMany(targetEntity=Unidades::class, mappedBy="empresa")
     */
    private $unidades;

    /**
     * @ORM\OneToMany(targetEntity=Formularios::class, mappedBy="empresa")
     */
    private $formularios;

    /**
     * @ORM\OneToMany(targetEntity=SatDescargas::class, mappedBy="empresa")
     */
    private $empresa;

    /**
     * @ORM\Column(type="integer")
     */
    private $Pac1;

    /**
     * @ORM\Column(type="integer")
     */
    private $PAc2;

    /**
     * @ORM\Column(type="integer")
     */
    private $Pac3;

    /**
     * @ORM\Column(type="integer")
     */
    private $RevisionFolios;

    /**
     * @ORM\ManyToOne(targetEntity=Tickets::class, inversedBy="empresa")
     */
    private $tickets;

    /**
     * @ORM\OneToMany(targetEntity=Empleados::class, mappedBy="empresa")
     */
    private $empleados;

    /**
     * @ORM\OneToMany(targetEntity=Nomina::class, mappedBy="empresa")
     */
    private $nominas;

    /**
     * @ORM\OneToMany(targetEntity=Conceptos::class, mappedBy="empresa")
     */
    private $conceptos;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoConcepto::class, mappedBy="empresa")
     */
    private $empleadoConceptos;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoPercepciones::class, mappedBy="empresa")
     */
    private $empleadoPercepciones;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoDeducciones::class, mappedBy="empresa")
     */
    private $empleadoDeducciones;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoIncapacidades::class, mappedBy="empresa")
     */
    private $empleadoIncapacidades;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoHorasExtra::class, mappedBy="empresa")
     */
    private $empleadoHorasExtras;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoJubilacion::class, mappedBy="empresa")
     */
    private $empleadoJubilacions;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoSeparacion::class, mappedBy="empresa")
     */
    private $empleadoSeparacions;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoOtrosPagos::class, mappedBy="empresa")
     */
    private $empleadoOtrosPagos;

    /**
     * @ORM\OneToMany(targetEntity=FacturasInfoBlobal::class, mappedBy="empresa")
     */
    private $facturasInfoBlobals;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     */
    private $fac_atr_adquiriente;

    /**
     * @ORM\OneToMany(targetEntity=EmpresasComplementos::class, mappedBy="empresa")
     */
    private $empresasComplementos;

    /**
     * @ORM\OneToMany(targetEntity=cUbicaciones::class, mappedBy="empresa")
     */
    private $cUbicaciones;

    /**
     * @ORM\OneToMany(targetEntity=cPersonas::class, mappedBy="empresa")
     */
    private $cPersonas;

    /**
     * @ORM\OneToMany(targetEntity=cMercancias::class, mappedBy="empresas")
     */
    private $cMercancias;

    /**
     * @ORM\OneToMany(targetEntity=cp::class, mappedBy="empresa")
     */
    private $cps;

    /**
     * @ORM\OneToMany(targetEntity=cpUbicacion::class, mappedBy="empresa")
     */
    private $cpUbicacions;

    /**
     * @ORM\OneToMany(targetEntity=cpFigura::class, mappedBy="empresa")
     */
    private $cpFiguras;

    /**
     * @ORM\OneToMany(targetEntity=cpFiguraParte::class, mappedBy="empresa")
     */
    private $cpFiguraPartes;

    /**
     * @ORM\OneToMany(targetEntity=cpMercancia::class, mappedBy="empresa")
     */
    private $cpMercancias;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalle::class, mappedBy="empresa")
     */
    private $cpMercanciaDetalles;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetallePedi::class, mappedBy="empresa")
     */
    private $cpMercanciaDetallePedis;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalleGuia::class, mappedBy="empresa")
     */
    private $cpMercanciaDetalleGuias;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalleCantidad::class, mappedBy="empresa")
     */
    private $cpMercanciaDetalleCantidads;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaAutoFed::class, mappedBy="empresa")
     */
    private $cpMercanciaAutoFeds;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaAutoFedRemo::class, mappedBy="empresa")
     */
    private $cpMercanciaAutoFedRemos;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalles::class, mappedBy="empresa")
     */
    private $cpMercanciaDetallesdet;

    /**
     * @ORM\OneToMany(targetEntity=cce::class, mappedBy="empresa")
     */
    private $cces;

    /**
     * @ORM\OneToMany(targetEntity=cceDestino::class, mappedBy="empresa")
     */
    private $cceDestinos;

    /**
     * @ORM\OneToMany(targetEntity=ccePropietario::class, mappedBy="empresa")
     */
    private $ccePropietarios;

    /**
     * @ORM\OneToMany(targetEntity=cceMercancias::class, mappedBy="empresa")
     */
    private $cceMercancias;

    /**
     * @ORM\OneToMany(targetEntity=cceMercanciasDetalle::class, mappedBy="empresa")
     */
    private $cceMercanciasDetalles;

    /**
     * @ORM\OneToMany(targetEntity=AddendaEmpresas::class, mappedBy="empresa")
     */
    private $addendaEmpresas;

    /**
     * @ORM\OneToMany(targetEntity=AddendaDatos::class, mappedBy="empresa")
     */
    private $addendaDatos;

    /**
     * @ORM\OneToMany(targetEntity=donatarias::class, mappedBy="empresa")
     */
    private $donatarias;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $autorizacion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $leyenda;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_donat;

    /**
     * @ORM\OneToMany(targetEntity=empresasDocumentos::class, mappedBy="empresa")
     */
    private $empresasDocumentos;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_fin_csd;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_fin_fiel;

    /**
     * @ORM\OneToMany(targetEntity=ConfiguracionEmpresas::class, mappedBy="empresa")
     */
    private $configuracionEmpresas;

    /**
     * @ORM\OneToMany(targetEntity=FacturasTimbre::class, mappedBy="empresa")
     */
    private $facturasTimbres;

    /**
     * @ORM\OneToMany(targetEntity=FacturasSat::class, mappedBy="empresa")
     */
    private $facturasSats;

    public function __construct()
    {
        $this->clientes = new ArrayCollection();
        $this->productos = new ArrayCollection();
        $this->series = new ArrayCollection();
        $this->mails = new ArrayCollection();
        $this->facturas = new ArrayCollection();
        $this->sucursales = new ArrayCollection();
        $this->empresas = new ArrayCollection();
        $this->pagos = new ArrayCollection();
        $this->documentos = new ArrayCollection();
        $this->usuarios2 = new ArrayCollection();
        $this->modulos = new ArrayCollection();
        $this->usuarios = new ArrayCollection();
        $this->unidades = new ArrayCollection();
        $this->formularios = new ArrayCollection();
        $this->empresa = new ArrayCollection();
        $this->empleados = new ArrayCollection();
        $this->nominas = new ArrayCollection();
        $this->conceptos = new ArrayCollection();
        $this->empleadoConceptos = new ArrayCollection();
        $this->empleadoPercepciones = new ArrayCollection();
        $this->empleadoDeducciones = new ArrayCollection();
        $this->empleadoIncapacidades = new ArrayCollection();
        $this->empleadoHorasExtras = new ArrayCollection();
        $this->empleadoJubilacions = new ArrayCollection();
        $this->empleadoSeparacions = new ArrayCollection();
        $this->empleadoOtrosPagos = new ArrayCollection();
        $this->facturasInfoBlobals = new ArrayCollection();
        $this->empresasComplementos = new ArrayCollection();
        $this->cUbicaciones = new ArrayCollection();
        $this->cPersonas = new ArrayCollection();
        $this->cMercancias = new ArrayCollection();
        $this->cps = new ArrayCollection();
        $this->cpUbicacions = new ArrayCollection();
        $this->cpFiguras = new ArrayCollection();
        $this->cpFiguraPartes = new ArrayCollection();
        $this->cpMercancias = new ArrayCollection();
        $this->cpMercanciaDetalles = new ArrayCollection();
        $this->cpMercanciaDetallePedis = new ArrayCollection();
        $this->cpMercanciaDetalleGuias = new ArrayCollection();
        $this->cpMercanciaDetalleCantidads = new ArrayCollection();
        $this->cpMercanciaAutoFeds = new ArrayCollection();
        $this->cpMercanciaAutoFedRemos = new ArrayCollection();
        $this->cpMercanciaDetallesdet = new ArrayCollection();
        $this->cces = new ArrayCollection();
        $this->cceDestinos = new ArrayCollection();
        $this->ccePropietarios = new ArrayCollection();
        $this->cceMercancias = new ArrayCollection();
        $this->cceMercanciasDetalles = new ArrayCollection();
        $this->addendaEmpresas = new ArrayCollection();
        $this->addendaDatos = new ArrayCollection();
        $this->donatarias = new ArrayCollection();
        $this->empresasDocumentos = new ArrayCollection();
        $this->configuracionEmpresas = new ArrayCollection();
        $this->facturasTimbres = new ArrayCollection();
        $this->facturasSats = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(?string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function getCurp()
    {
        return $this->curp;
    }


    public function setCurp($curp): void
    {
        $this->curp = $curp;
    }


    public function setRfc(string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRegimen(): ?string
    {
        return $this->regimen;
    }

    public function setRegimen(string $regimen): self
    {
        $this->regimen = $regimen;

        return $this;
    }

    public function getCer(): ?string
    {
        return $this->cer;
    }

    public function setCer(?string $cer): self
    {
        $this->cer = $cer;

        return $this;
    }

    public function getKeyCsd(): ?string
    {
        return $this->key_csd;
    }

    public function setKeyCsd(?string $key_csd): self
    {
        $this->key_csd = $key_csd;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getPais(): ?string
    {
        return $this->pais;
    }

    public function setPais(?string $pais): self
    {
        $this->pais = $pais;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getCiudad()
    {
        return $this->ciudad;
    }

    public function setCiudad($ciudad): void
    {
        $this->ciudad = $ciudad;
    }


    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    public function setMunicipio(?string $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    public function getColonia(): ?string
    {
        return $this->colonia;
    }

    public function setColonia(?string $colonia): self
    {
        $this->colonia = $colonia;

        return $this;
    }

    public function getNumExt(): ?string
    {
        return $this->num_ext;
    }

    public function setNumExt(?string $num_ext): self
    {
        $this->num_ext = $num_ext;

        return $this;
    }

    public function getNumInt(): ?string
    {
        return $this->num_int;
    }

    public function setNumInt(?string $num_int): self
    {
        $this->num_int = $num_int;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|Clientes[]
     */
    public function getClientes(): Collection
    {
        return $this->clientes;
    }

    public function addCliente(Clientes $cliente): self
    {
        if (!$this->clientes->contains($cliente)) {
            $this->clientes[] = $cliente;
            $cliente->setEmpresa($this);
        }

        return $this;
    }

    public function removeCliente(Clientes $cliente): self
    {
        if ($this->clientes->removeElement($cliente)) {
            // set the owning side to null (unless already changed)
            if ($cliente->getEmpresa() === $this) {
                $cliente->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Productos[]
     */
    public function getProductos(): Collection
    {
        return $this->productos;
    }

    public function addProducto(Productos $producto): self
    {
        if (!$this->productos->contains($producto)) {
            $this->productos[] = $producto;
            $producto->setEmpresa($this);
        }

        return $this;
    }

    public function removeProducto(Productos $producto): self
    {
        if ($this->productos->removeElement($producto)) {
            // set the owning side to null (unless already changed)
            if ($producto->getEmpresa() === $this) {
                $producto->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Series[]
     */
    public function getSeries(): Collection
    {
        return $this->series;
    }

    public function addSeries(Series $series): self
    {
        if (!$this->series->contains($series)) {
            $this->series[] = $series;
            $series->setEmpresa($this);
        }

        return $this;
    }

    public function removeSeries(Series $series): self
    {
        if ($this->series->removeElement($series)) {
            // set the owning side to null (unless already changed)
            if ($series->getEmpresa() === $this) {
                $series->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClientesMail[]
     */
    public function getMails(): Collection
    {
        return $this->mails;
    }

    public function addMail(ClientesMail $mail): self
    {
        if (!$this->mails->contains($mail)) {
            $this->mails[] = $mail;
            $mail->setEmpresa($this);
        }

        return $this;
    }

    public function removeMail(ClientesMail $mail): self
    {
        if ($this->mails->removeElement($mail)) {
            // set the owning side to null (unless already changed)
            if ($mail->getEmpresa() === $this) {
                $mail->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Facturas[]
     */
    public function getFacturas(): Collection
    {
        return $this->facturas;
    }

    public function addFactura(Facturas $factura): self
    {
        if (!$this->facturas->contains($factura)) {
            $this->facturas[] = $factura;
            $factura->setEmpresa($this);
        }

        return $this;
    }

    public function removeFactura(Facturas $factura): self
    {
        if ($this->facturas->removeElement($factura)) {
            // set the owning side to null (unless already changed)
            if ($factura->getEmpresa() === $this) {
                $factura->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Sucursales[]
     */
    public function getSucursales(): Collection
    {
        return $this->sucursales;
    }

    public function addSucursale(Sucursales $sucursale): self
    {
        if (!$this->sucursales->contains($sucursale)) {
            $this->sucursales[] = $sucursale;
            $sucursale->setEmpresa($this);
        }

        return $this;
    }

    public function removeSucursale(Sucursales $sucursale): self
    {
        if ($this->sucursales->removeElement($sucursale)) {
            // set the owning side to null (unless already changed)
            if ($sucursale->getEmpresa() === $this) {
                $sucursale->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FacturaDetalleImpuesto[]
     */
    public function getEmpresas(): Collection
    {
        return $this->empresas;
    }

    public function addEmpresa(FacturaDetalleImpuesto $empresa): self
    {
        if (!$this->empresas->contains($empresa)) {
            $this->empresas[] = $empresa;
            $empresa->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpresa(FacturaDetalleImpuesto $empresa): self
    {
        if ($this->empresas->removeElement($empresa)) {
            // set the owning side to null (unless already changed)
            if ($empresa->getEmpresa() === $this) {
                $empresa->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ComplementosPago[]
     */
    public function getPagos(): Collection
    {
        return $this->pagos;
    }

    public function addPago(ComplementosPago $pago): self
    {
        if (!$this->pagos->contains($pago)) {
            $this->pagos[] = $pago;
            $pago->setEmpresa($this);
        }

        return $this;
    }

    public function removePago(ComplementosPago $pago): self
    {
        if ($this->pagos->removeElement($pago)) {
            // set the owning side to null (unless already changed)
            if ($pago->getEmpresa() === $this) {
                $pago->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ComplementosPagoDocumento[]
     */
    public function getDocumentos(): Collection
    {
        return $this->documentos;
    }

    public function addDocumento(ComplementosPagoDocumento $documento): self
    {
        if (!$this->documentos->contains($documento)) {
            $this->documentos[] = $documento;
            $documento->setEmpresa($this);
        }

        return $this;
    }

    public function removeDocumento(ComplementosPagoDocumento $documento): self
    {
        if ($this->documentos->removeElement($documento)) {
            // set the owning side to null (unless already changed)
            if ($documento->getEmpresa() === $this) {
                $documento->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Usuarios[]
     */
    public function getUsuarios2(): Collection
    {
        return $this->usuarios2;
    }

    public function addUsuarios2(Usuarios $usuarios2): self
    {
        if (!$this->usuarios2->contains($usuarios2)) {
            $this->usuarios2[] = $usuarios2;
            $usuarios2->setEmpresa($this);
        }

        return $this;
    }

    public function removeUsuarios2(Usuarios $usuarios2): self
    {
        if ($this->usuarios2->removeElement($usuarios2)) {
            // set the owning side to null (unless already changed)
            if ($usuarios2->getEmpresa() === $this) {
                $usuarios2->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpresasModulos[]
     */
    public function getModulos(): Collection
    {
        return $this->modulos;
    }

    public function addModulo(EmpresasModulos $modulo): self
    {
        if (!$this->modulos->contains($modulo)) {
            $this->modulos[] = $modulo;
            $modulo->setEmpresa($this);
        }

        return $this;
    }

    public function removeModulo(EmpresasModulos $modulo): self
    {
        if ($this->modulos->removeElement($modulo)) {
            // set the owning side to null (unless already changed)
            if ($modulo->getEmpresa() === $this) {
                $modulo->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UsuariosEmpresas[]
     */
    public function getUsuarios(): Collection
    {
        return $this->usuarios;
    }

    public function addUsuario(UsuariosEmpresas $usuario): self
    {
        if (!$this->usuarios->contains($usuario)) {
            $this->usuarios[] = $usuario;
            $usuario->setEmpresa($this);
        }

        return $this;
    }

    public function removeUsuario(UsuariosEmpresas $usuario): self
    {
        if ($this->usuarios->removeElement($usuario)) {
            // set the owning side to null (unless already changed)
            if ($usuario->getEmpresa() === $this) {
                $usuario->setEmpresa(null);
            }
        }

        return $this;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setImagen(?string $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getCerFiel(): ?string
    {
        return $this->cer_fiel;
    }

    public function setCerFiel(?string $cer_fiel): self
    {
        $this->cer_fiel = $cer_fiel;

        return $this;
    }

    public function getKeyFiel(): ?string
    {
        return $this->key_fiel;
    }

    public function setKeyFiel(?string $key_fiel): self
    {
        $this->key_fiel = $key_fiel;

        return $this;
    }

    public function getPasswordFiel(): ?string
    {
        return $this->password_fiel;
    }

    public function setPasswordFiel(?string $password_fiel): self
    {
        $this->password_fiel = $password_fiel;

        return $this;
    }

    public function getCalle(): ?string
    {
        return $this->calle;
    }

    public function setCalle(?string $calle): self
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * @return Collection|Unidades[]
     */
    public function getUnidades(): Collection
    {
        return $this->unidades;
    }

    public function addUnidade(Unidades $unidade): self
    {
        if (!$this->unidades->contains($unidade)) {
            $this->unidades[] = $unidade;
            $unidade->setEmpresa($this);
        }

        return $this;
    }

    public function removeUnidade(Unidades $unidade): self
    {
        if ($this->unidades->removeElement($unidade)) {
            // set the owning side to null (unless already changed)
            if ($unidade->getEmpresa() === $this) {
                $unidade->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Formularios[]
     */
    public function getFormularios(): Collection
    {
        return $this->formularios;
    }

    public function addFormulario(Formularios $formulario): self
    {
        if (!$this->formularios->contains($formulario)) {
            $this->formularios[] = $formulario;
            $formulario->setEmpresa($this);
        }

        return $this;
    }

    public function removeFormulario(Formularios $formulario): self
    {
        if ($this->formularios->removeElement($formulario)) {
            // set the owning side to null (unless already changed)
            if ($formulario->getEmpresa() === $this) {
                $formulario->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SatDescargas[]
     */
    public function getEmpresa(): Collection
    {
        return $this->empresa;
    }

    public function getPac1(): ?int
    {
        return $this->Pac1;
    }

    public function setPac1(int $Pac1): self
    {
        $this->Pac1 = $Pac1;

        return $this;
    }

    public function getPAc2(): ?int
    {
        return $this->PAc2;
    }

    public function setPAc2(int $PAc2): self
    {
        $this->PAc2 = $PAc2;

        return $this;
    }

    public function getPac3(): ?int
    {
        return $this->Pac3;
    }

    public function setPac3(int $Pac3): self
    {
        $this->Pac3 = $Pac3;

        return $this;
    }

    public function getRevisionFolios(): ?int
    {
        return $this->RevisionFolios;
    }

    public function setRevisionFolios(int $RevisionFolios): self
    {
        $this->RevisionFolios = $RevisionFolios;

        return $this;
    }

    public function getTickets(): ?Tickets
    {
        return $this->tickets;
    }

    public function setTickets(?Tickets $tickets): self
    {
        $this->tickets = $tickets;

        return $this;
    }

    /**
     * @return Collection|Empleados[]
     */
    public function getEmpleados(): Collection
    {
        return $this->empleados;
    }

    public function addEmpleado(Empleados $empleado): self
    {
        if (!$this->empleados->contains($empleado)) {
            $this->empleados[] = $empleado;
            $empleado->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpleado(Empleados $empleado): self
    {
        if ($this->empleados->removeElement($empleado)) {
            // set the owning side to null (unless already changed)
            if ($empleado->getEmpresa() === $this) {
                $empleado->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Nomina[]
     */
    public function getNominas(): Collection
    {
        return $this->nominas;
    }

    public function addNomina(Nomina $nomina): self
    {
        if (!$this->nominas->contains($nomina)) {
            $this->nominas[] = $nomina;
            $nomina->setEmpresa($this);
        }

        return $this;
    }

    public function removeNomina(Nomina $nomina): self
    {
        if ($this->nominas->removeElement($nomina)) {
            // set the owning side to null (unless already changed)
            if ($nomina->getEmpresa() === $this) {
                $nomina->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Conceptos[]
     */
    public function getConceptos(): Collection
    {
        return $this->conceptos;
    }

    public function addConcepto(Conceptos $concepto): self
    {
        if (!$this->conceptos->contains($concepto)) {
            $this->conceptos[] = $concepto;
            $concepto->setEmpresa($this);
        }

        return $this;
    }

    public function removeConcepto(Conceptos $concepto): self
    {
        if ($this->conceptos->removeElement($concepto)) {
            // set the owning side to null (unless already changed)
            if ($concepto->getEmpresa() === $this) {
                $concepto->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoConcepto[]
     */
    public function getEmpleadoConceptos(): Collection
    {
        return $this->empleadoConceptos;
    }

    public function addEmpleadoConcepto(EmpleadoConcepto $empleadoConcepto): self
    {
        if (!$this->empleadoConceptos->contains($empleadoConcepto)) {
            $this->empleadoConceptos[] = $empleadoConcepto;
            $empleadoConcepto->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpleadoConcepto(EmpleadoConcepto $empleadoConcepto): self
    {
        if ($this->empleadoConceptos->removeElement($empleadoConcepto)) {
            // set the owning side to null (unless already changed)
            if ($empleadoConcepto->getEmpresa() === $this) {
                $empleadoConcepto->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoPercepciones[]
     */
    public function getEmpleadoPercepciones(): Collection
    {
        return $this->empleadoPercepciones;
    }

    public function addEmpleadoPercepcione(EmpleadoPercepciones $empleadoPercepcione): self
    {
        if (!$this->empleadoPercepciones->contains($empleadoPercepcione)) {
            $this->empleadoPercepciones[] = $empleadoPercepcione;
            $empleadoPercepcione->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpleadoPercepcione(EmpleadoPercepciones $empleadoPercepcione): self
    {
        if ($this->empleadoPercepciones->removeElement($empleadoPercepcione)) {
            // set the owning side to null (unless already changed)
            if ($empleadoPercepcione->getEmpresa() === $this) {
                $empleadoPercepcione->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoDeducciones[]
     */
    public function getEmpleadoDeducciones(): Collection
    {
        return $this->empleadoDeducciones;
    }

    public function addEmpleadoDeduccione(EmpleadoDeducciones $empleadoDeduccione): self
    {
        if (!$this->empleadoDeducciones->contains($empleadoDeduccione)) {
            $this->empleadoDeducciones[] = $empleadoDeduccione;
            $empleadoDeduccione->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpleadoDeduccione(EmpleadoDeducciones $empleadoDeduccione): self
    {
        if ($this->empleadoDeducciones->removeElement($empleadoDeduccione)) {
            // set the owning side to null (unless already changed)
            if ($empleadoDeduccione->getEmpresa() === $this) {
                $empleadoDeduccione->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoIncapacidades[]
     */
    public function getEmpleadoIncapacidades(): Collection
    {
        return $this->empleadoIncapacidades;
    }

    public function addEmpleadoIncapacidade(EmpleadoIncapacidades $empleadoIncapacidade): self
    {
        if (!$this->empleadoIncapacidades->contains($empleadoIncapacidade)) {
            $this->empleadoIncapacidades[] = $empleadoIncapacidade;
            $empleadoIncapacidade->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpleadoIncapacidade(EmpleadoIncapacidades $empleadoIncapacidade): self
    {
        if ($this->empleadoIncapacidades->removeElement($empleadoIncapacidade)) {
            // set the owning side to null (unless already changed)
            if ($empleadoIncapacidade->getEmpresa() === $this) {
                $empleadoIncapacidade->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoHorasExtra[]
     */
    public function getEmpleadoHorasExtras(): Collection
    {
        return $this->empleadoHorasExtras;
    }

    public function addEmpleadoHorasExtra(EmpleadoHorasExtra $empleadoHorasExtra): self
    {
        if (!$this->empleadoHorasExtras->contains($empleadoHorasExtra)) {
            $this->empleadoHorasExtras[] = $empleadoHorasExtra;
            $empleadoHorasExtra->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpleadoHorasExtra(EmpleadoHorasExtra $empleadoHorasExtra): self
    {
        if ($this->empleadoHorasExtras->removeElement($empleadoHorasExtra)) {
            // set the owning side to null (unless already changed)
            if ($empleadoHorasExtra->getEmpresa() === $this) {
                $empleadoHorasExtra->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoJubilacion[]
     */
    public function getEmpleadoJubilacions(): Collection
    {
        return $this->empleadoJubilacions;
    }

    public function addEmpleadoJubilacion(EmpleadoJubilacion $empleadoJubilacion): self
    {
        if (!$this->empleadoJubilacions->contains($empleadoJubilacion)) {
            $this->empleadoJubilacions[] = $empleadoJubilacion;
            $empleadoJubilacion->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpleadoJubilacion(EmpleadoJubilacion $empleadoJubilacion): self
    {
        if ($this->empleadoJubilacions->removeElement($empleadoJubilacion)) {
            // set the owning side to null (unless already changed)
            if ($empleadoJubilacion->getEmpresa() === $this) {
                $empleadoJubilacion->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoSeparacion[]
     */
    public function getEmpleadoSeparacions(): Collection
    {
        return $this->empleadoSeparacions;
    }

    public function addEmpleadoSeparacion(EmpleadoSeparacion $empleadoSeparacion): self
    {
        if (!$this->empleadoSeparacions->contains($empleadoSeparacion)) {
            $this->empleadoSeparacions[] = $empleadoSeparacion;
            $empleadoSeparacion->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpleadoSeparacion(EmpleadoSeparacion $empleadoSeparacion): self
    {
        if ($this->empleadoSeparacions->removeElement($empleadoSeparacion)) {
            // set the owning side to null (unless already changed)
            if ($empleadoSeparacion->getEmpresa() === $this) {
                $empleadoSeparacion->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoOtrosPagos[]
     */
    public function getEmpleadoOtrosPagos(): Collection
    {
        return $this->empleadoOtrosPagos;
    }

    public function addEmpleadoOtrosPago(EmpleadoOtrosPagos $empleadoOtrosPago): self
    {
        if (!$this->empleadoOtrosPagos->contains($empleadoOtrosPago)) {
            $this->empleadoOtrosPagos[] = $empleadoOtrosPago;
            $empleadoOtrosPago->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpleadoOtrosPago(EmpleadoOtrosPagos $empleadoOtrosPago): self
    {
        if ($this->empleadoOtrosPagos->removeElement($empleadoOtrosPago)) {
            // set the owning side to null (unless already changed)
            if ($empleadoOtrosPago->getEmpresa() === $this) {
                $empleadoOtrosPago->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FacturasInfoBlobal[]
     */
    public function getFacturasInfoBlobals(): Collection
    {
        return $this->facturasInfoBlobals;
    }

    public function addFacturasInfoBlobal(FacturasInfoBlobal $facturasInfoBlobal): self
    {
        if (!$this->facturasInfoBlobals->contains($facturasInfoBlobal)) {
            $this->facturasInfoBlobals[] = $facturasInfoBlobal;
            $facturasInfoBlobal->setEmpresa($this);
        }

        return $this;
    }

    public function removeFacturasInfoBlobal(FacturasInfoBlobal $facturasInfoBlobal): self
    {
        if ($this->facturasInfoBlobals->removeElement($facturasInfoBlobal)) {
            // set the owning side to null (unless already changed)
            if ($facturasInfoBlobal->getEmpresa() === $this) {
                $facturasInfoBlobal->setEmpresa(null);
            }
        }

        return $this;
    }

    public function getFacAtrAdquiriente(): ?string
    {
        return $this->fac_atr_adquiriente;
    }

    public function setFacAtrAdquiriente(?string $fac_atr_adquiriente): self
    {
        $this->fac_atr_adquiriente = $fac_atr_adquiriente;

        return $this;
    }

    /**
     * @return Collection|EmpresasComplementos[]
     */
    public function getEmpresasComplementos(): Collection
    {
        return $this->empresasComplementos;
    }

    public function addEmpresasComplemento(EmpresasComplementos $empresasComplemento): self
    {
        if (!$this->empresasComplementos->contains($empresasComplemento)) {
            $this->empresasComplementos[] = $empresasComplemento;
            $empresasComplemento->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpresasComplemento(EmpresasComplementos $empresasComplemento): self
    {
        if ($this->empresasComplementos->removeElement($empresasComplemento)) {
            // set the owning side to null (unless already changed)
            if ($empresasComplemento->getEmpresa() === $this) {
                $empresasComplemento->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|cUbicaciones[]
     */
    public function getCUbicaciones(): Collection
    {
        return $this->cUbicaciones;
    }

    public function addCUbicacione(cUbicaciones $cUbicacione): self
    {
        if (!$this->cUbicaciones->contains($cUbicacione)) {
            $this->cUbicaciones[] = $cUbicacione;
            $cUbicacione->setEmpresa($this);
        }

        return $this;
    }

    public function removeCUbicacione(cUbicaciones $cUbicacione): self
    {
        if ($this->cUbicaciones->removeElement($cUbicacione)) {
            // set the owning side to null (unless already changed)
            if ($cUbicacione->getEmpresa() === $this) {
                $cUbicacione->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|cPersonas[]
     */
    public function getCPersonas(): Collection
    {
        return $this->cPersonas;
    }

    public function addCPersona(cPersonas $cPersona): self
    {
        if (!$this->cPersonas->contains($cPersona)) {
            $this->cPersonas[] = $cPersona;
            $cPersona->setEmpresa($this);
        }

        return $this;
    }

    public function removeCPersona(cPersonas $cPersona): self
    {
        if ($this->cPersonas->removeElement($cPersona)) {
            // set the owning side to null (unless already changed)
            if ($cPersona->getEmpresa() === $this) {
                $cPersona->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|cMercancias[]
     */
    public function getCMercancias(): Collection
    {
        return $this->cMercancias;
    }

    public function addCMercancia(cMercancias $cMercancia): self
    {
        if (!$this->cMercancias->contains($cMercancia)) {
            $this->cMercancias[] = $cMercancia;
            $cMercancia->setEmpresas($this);
        }

        return $this;
    }

    public function removeCMercancia(cMercancias $cMercancia): self
    {
        if ($this->cMercancias->removeElement($cMercancia)) {
            // set the owning side to null (unless already changed)
            if ($cMercancia->getEmpresas() === $this) {
                $cMercancia->setEmpresas(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cp>
     */
    public function getCps(): Collection
    {
        return $this->cps;
    }

    public function addCp(cp $cp): self
    {
        if (!$this->cps->contains($cp)) {
            $this->cps[] = $cp;
            $cp->setEmpresa($this);
        }

        return $this;
    }

    public function removeCp(cp $cp): self
    {
        if ($this->cps->removeElement($cp)) {
            // set the owning side to null (unless already changed)
            if ($cp->getEmpresa() === $this) {
                $cp->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpUbicacion>
     */
    public function getCpUbicacions(): Collection
    {
        return $this->cpUbicacions;
    }

    public function addCpUbicacion(cpUbicacion $cpUbicacion): self
    {
        if (!$this->cpUbicacions->contains($cpUbicacion)) {
            $this->cpUbicacions[] = $cpUbicacion;
            $cpUbicacion->setEmpresa($this);
        }

        return $this;
    }

    public function removeCpUbicacion(cpUbicacion $cpUbicacion): self
    {
        if ($this->cpUbicacions->removeElement($cpUbicacion)) {
            // set the owning side to null (unless already changed)
            if ($cpUbicacion->getEmpresa() === $this) {
                $cpUbicacion->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpFigura>
     */
    public function getCpFiguras(): Collection
    {
        return $this->cpFiguras;
    }

    public function addCpFigura(cpFigura $cpFigura): self
    {
        if (!$this->cpFiguras->contains($cpFigura)) {
            $this->cpFiguras[] = $cpFigura;
            $cpFigura->setEmpresa($this);
        }

        return $this;
    }

    public function removeCpFigura(cpFigura $cpFigura): self
    {
        if ($this->cpFiguras->removeElement($cpFigura)) {
            // set the owning side to null (unless already changed)
            if ($cpFigura->getEmpresa() === $this) {
                $cpFigura->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpFiguraParte>
     */
    public function getCpFiguraPartes(): Collection
    {
        return $this->cpFiguraPartes;
    }

    public function addCpFiguraParte(cpFiguraParte $cpFiguraParte): self
    {
        if (!$this->cpFiguraPartes->contains($cpFiguraParte)) {
            $this->cpFiguraPartes[] = $cpFiguraParte;
            $cpFiguraParte->setEmpresa($this);
        }

        return $this;
    }

    public function removeCpFiguraParte(cpFiguraParte $cpFiguraParte): self
    {
        if ($this->cpFiguraPartes->removeElement($cpFiguraParte)) {
            // set the owning side to null (unless already changed)
            if ($cpFiguraParte->getEmpresa() === $this) {
                $cpFiguraParte->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercancia>
     */
    public function getCpMercancias(): Collection
    {
        return $this->cpMercancias;
    }

    public function addCpMercancia(cpMercancia $cpMercancia): self
    {
        if (!$this->cpMercancias->contains($cpMercancia)) {
            $this->cpMercancias[] = $cpMercancia;
            $cpMercancia->setEmpresa($this);
        }

        return $this;
    }

    public function removeCpMercancia(cpMercancia $cpMercancia): self
    {
        if ($this->cpMercancias->removeElement($cpMercancia)) {
            // set the owning side to null (unless already changed)
            if ($cpMercancia->getEmpresa() === $this) {
                $cpMercancia->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalle>
     */
    public function getCpMercanciaDetalles(): Collection
    {
        return $this->cpMercanciaDetalles;
    }

    public function addCpMercanciaDetalle(cpMercanciaDetalle $cpMercanciaDetalle): self
    {
        if (!$this->cpMercanciaDetalles->contains($cpMercanciaDetalle)) {
            $this->cpMercanciaDetalles[] = $cpMercanciaDetalle;
            $cpMercanciaDetalle->setEmpresa($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetalle(cpMercanciaDetalle $cpMercanciaDetalle): self
    {
        if ($this->cpMercanciaDetalles->removeElement($cpMercanciaDetalle)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetalle->getEmpresa() === $this) {
                $cpMercanciaDetalle->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetallePedi>
     */
    public function getCpMercanciaDetallePedis(): Collection
    {
        return $this->cpMercanciaDetallePedis;
    }

    public function addCpMercanciaDetallePedi(cpMercanciaDetallePedi $cpMercanciaDetallePedi): self
    {
        if (!$this->cpMercanciaDetallePedis->contains($cpMercanciaDetallePedi)) {
            $this->cpMercanciaDetallePedis[] = $cpMercanciaDetallePedi;
            $cpMercanciaDetallePedi->setEmpresa($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetallePedi(cpMercanciaDetallePedi $cpMercanciaDetallePedi): self
    {
        if ($this->cpMercanciaDetallePedis->removeElement($cpMercanciaDetallePedi)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetallePedi->getEmpresa() === $this) {
                $cpMercanciaDetallePedi->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalleGuia>
     */
    public function getCpMercanciaDetalleGuias(): Collection
    {
        return $this->cpMercanciaDetalleGuias;
    }

    public function addCpMercanciaDetalleGuia(cpMercanciaDetalleGuia $cpMercanciaDetalleGuia): self
    {
        if (!$this->cpMercanciaDetalleGuias->contains($cpMercanciaDetalleGuia)) {
            $this->cpMercanciaDetalleGuias[] = $cpMercanciaDetalleGuia;
            $cpMercanciaDetalleGuia->setEmpresa($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetalleGuia(cpMercanciaDetalleGuia $cpMercanciaDetalleGuia): self
    {
        if ($this->cpMercanciaDetalleGuias->removeElement($cpMercanciaDetalleGuia)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetalleGuia->getEmpresa() === $this) {
                $cpMercanciaDetalleGuia->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalleCantidad>
     */
    public function getCpMercanciaDetalleCantidads(): Collection
    {
        return $this->cpMercanciaDetalleCantidads;
    }

    public function addCpMercanciaDetalleCantidad(cpMercanciaDetalleCantidad $cpMercanciaDetalleCantidad): self
    {
        if (!$this->cpMercanciaDetalleCantidads->contains($cpMercanciaDetalleCantidad)) {
            $this->cpMercanciaDetalleCantidads[] = $cpMercanciaDetalleCantidad;
            $cpMercanciaDetalleCantidad->setEmpresa($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetalleCantidad(cpMercanciaDetalleCantidad $cpMercanciaDetalleCantidad): self
    {
        if ($this->cpMercanciaDetalleCantidads->removeElement($cpMercanciaDetalleCantidad)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetalleCantidad->getEmpresa() === $this) {
                $cpMercanciaDetalleCantidad->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaAutoFed>
     */
    public function getCpMercanciaAutoFeds(): Collection
    {
        return $this->cpMercanciaAutoFeds;
    }

    public function addCpMercanciaAutoFed(cpMercanciaAutoFed $cpMercanciaAutoFed): self
    {
        if (!$this->cpMercanciaAutoFeds->contains($cpMercanciaAutoFed)) {
            $this->cpMercanciaAutoFeds[] = $cpMercanciaAutoFed;
            $cpMercanciaAutoFed->setEmpresa($this);
        }

        return $this;
    }

    public function removeCpMercanciaAutoFed(cpMercanciaAutoFed $cpMercanciaAutoFed): self
    {
        if ($this->cpMercanciaAutoFeds->removeElement($cpMercanciaAutoFed)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaAutoFed->getEmpresa() === $this) {
                $cpMercanciaAutoFed->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaAutoFedRemo>
     */
    public function getCpMercanciaAutoFedRemos(): Collection
    {
        return $this->cpMercanciaAutoFedRemos;
    }

    public function addCpMercanciaAutoFedRemo(cpMercanciaAutoFedRemo $cpMercanciaAutoFedRemo): self
    {
        if (!$this->cpMercanciaAutoFedRemos->contains($cpMercanciaAutoFedRemo)) {
            $this->cpMercanciaAutoFedRemos[] = $cpMercanciaAutoFedRemo;
            $cpMercanciaAutoFedRemo->setEmpresa($this);
        }

        return $this;
    }

    public function removeCpMercanciaAutoFedRemo(cpMercanciaAutoFedRemo $cpMercanciaAutoFedRemo): self
    {
        if ($this->cpMercanciaAutoFedRemos->removeElement($cpMercanciaAutoFedRemo)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaAutoFedRemo->getEmpresa() === $this) {
                $cpMercanciaAutoFedRemo->setEmpresa(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, cpMercanciaDetalles>
     */
    public function getCpMercanciaDetallesdet(): Collection
    {
        return $this->cpMercanciaDetallesdet;
    }

    public function addCpMercanciaDetallesdet(cpMercanciaDetalles $cpMercanciaDetallesdet): self
    {
        if (!$this->cpMercanciaDetallesdet->contains($cpMercanciaDetallesdet)) {
            $this->cpMercanciaDetallesdet[] = $cpMercanciaDetallesdet;
            $cpMercanciaDetallesdet->setEmpresa($this);
        }
    }
    public function getCustomer(): ?Customers
    {
        return $this->customer;
    }

    public function setCustomer(?Customers $customer): self
    {
        $this->customer = $customer;

        return $this;
    }


    public function removeCpMercanciaDetallesdet(cpMercanciaDetalles $cpMercanciaDetallesdet): self
    {
        if ($this->cpMercanciaDetallesdet->removeElement($cpMercanciaDetallesdet)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetallesdet->getEmpresa() === $this) {
                $cpMercanciaDetallesdet->setEmpresa(null);
            }
        }
    }
    public function getPagoDefault(): ?FormasPago
    {
        return $this->pago_default;
    }

    
    public function setPagoDefault(?FormasPago $pago_default): self
    {
        $this->pago_default = $pago_default;
        return $this;
    }

    /**
     * @return Collection<int, cce>
     */
    public function getCces(): Collection
    {
        return $this->cces;
    }

    public function addCce(cce $cce): self
    {
        if (!$this->cces->contains($cce)) {
            $this->cces[] = $cce;
            $cce->setEmpresa($this);
        }

        return $this;
    }

    public function removeCce(cce $cce): self
    {
        if ($this->cces->removeElement($cce)) {
            // set the owning side to null (unless already changed)
            if ($cce->getEmpresa() === $this) {
                $cce->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cceDestino>
     */
    public function getCceDestinos(): Collection
    {
        return $this->cceDestinos;
    }

    public function addCceDestino(cceDestino $cceDestino): self
    {
        if (!$this->cceDestinos->contains($cceDestino)) {
            $this->cceDestinos[] = $cceDestino;
            $cceDestino->setEmpresa($this);
        }

        return $this;
    }

    public function removeCceDestino(cceDestino $cceDestino): self
    {
        if ($this->cceDestinos->removeElement($cceDestino)) {
            // set the owning side to null (unless already changed)
            if ($cceDestino->getEmpresa() === $this) {
                $cceDestino->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ccePropietario>
     */
    public function getCcePropietarios(): Collection
    {
        return $this->ccePropietarios;
    }

    public function addCcePropietario(ccePropietario $ccePropietario): self
    {
        if (!$this->ccePropietarios->contains($ccePropietario)) {
            $this->ccePropietarios[] = $ccePropietario;
            $ccePropietario->setEmpresa($this);
        }

        return $this;
    }

    public function removeCcePropietario(ccePropietario $ccePropietario): self
    {
        if ($this->ccePropietarios->removeElement($ccePropietario)) {
            // set the owning side to null (unless already changed)
            if ($ccePropietario->getEmpresa() === $this) {
                $ccePropietario->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cceMercancias>
     */
    public function getCceMercancias(): Collection
    {
        return $this->cceMercancias;
    }

    public function addCceMercancia(cceMercancias $cceMercancia): self
    {
        if (!$this->cceMercancias->contains($cceMercancia)) {
            $this->cceMercancias[] = $cceMercancia;
            $cceMercancia->setEmpresa($this);
        }

        return $this;
    }

    public function removeCceMercancia(cceMercancias $cceMercancia): self
    {
        if ($this->cceMercancias->removeElement($cceMercancia)) {
            // set the owning side to null (unless already changed)
            if ($cceMercancia->getEmpresa() === $this) {
                $cceMercancia->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cceMercanciasDetalle>
     */
    public function getCceMercanciasDetalles(): Collection
    {
        return $this->cceMercanciasDetalles;
    }

    public function addCceMercanciasDetalle(cceMercanciasDetalle $cceMercanciasDetalle): self
    {
        if (!$this->cceMercanciasDetalles->contains($cceMercanciasDetalle)) {
            $this->cceMercanciasDetalles[] = $cceMercanciasDetalle;
            $cceMercanciasDetalle->setEmpresa($this);
        }

        return $this;
    }

    public function removeCceMercanciasDetalle(cceMercanciasDetalle $cceMercanciasDetalle): self
    {
        if ($this->cceMercanciasDetalles->removeElement($cceMercanciasDetalle)) {
            // set the owning side to null (unless already changed)
            if ($cceMercanciasDetalle->getEmpresa() === $this) {
                $cceMercanciasDetalle->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AddendaEmpresas>
     */
    public function getAddendaEmpresas(): Collection
    {
        return $this->addendaEmpresas;
    }

    public function addAddendaEmpresa(AddendaEmpresas $addendaEmpresa): self
    {
        if (!$this->addendaEmpresas->contains($addendaEmpresa)) {
            $this->addendaEmpresas[] = $addendaEmpresa;
            $addendaEmpresa->setEmpresa($this);
        }

        return $this;
    }

    public function removeAddendaEmpresa(AddendaEmpresas $addendaEmpresa): self
    {
        if ($this->addendaEmpresas->removeElement($addendaEmpresa)) {
            // set the owning side to null (unless already changed)
            if ($addendaEmpresa->getEmpresa() === $this) {
                $addendaEmpresa->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AddendaDatos>
     */
    public function getAddendaDatos(): Collection
    {
        return $this->addendaDatos;
    }

    public function addAddendaDato(AddendaDatos $addendaDato): self
    {
        if (!$this->addendaDatos->contains($addendaDato)) {
            $this->addendaDatos[] = $addendaDato;
            $addendaDato->setEmpresa($this);
        }

        return $this;
    }

    public function removeAddendaDato(AddendaDatos $addendaDato): self
    {
        if ($this->addendaDatos->removeElement($addendaDato)) {
            // set the owning side to null (unless already changed)
            if ($addendaDato->getEmpresa() === $this) {
                $addendaDato->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, donatarias>
     */
    public function getDonatarias(): Collection
    {
        return $this->donatarias;
    }

    public function addDonataria(donatarias $donataria): self
    {
        if (!$this->donatarias->contains($donataria)) {
            $this->donatarias[] = $donataria;
            $donataria->setEmpresa($this);
        }

        return $this;
    }

    public function removeDonataria(donatarias $donataria): self
    {
        if ($this->donatarias->removeElement($donataria)) {
            // set the owning side to null (unless already changed)
            if ($donataria->getEmpresa() === $this) {
                $donataria->setEmpresa(null);
            }
        }

        return $this;
    }

    public function getAutorizacion(): ?string
    {
        return $this->autorizacion;
    }

    public function setAutorizacion(?string $autorizacion): self
    {
        $this->autorizacion = $autorizacion;

        return $this;
    }

    public function getLeyenda(): ?string
    {
        return $this->leyenda;
    }

    public function setLeyenda(?string $leyenda): self
    {
        $this->leyenda = $leyenda;

        return $this;
    }

    public function getFechaDonat(): ?\DateTimeInterface
    {
        return $this->fecha_donat;
    }

    public function setFechaDonat(?\DateTimeInterface $fecha_donat): self
    {
        $this->fecha_donat = $fecha_donat;

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'curp'=>$this->getCurp(),
            'calle'=>$this->getCalle(),
            'ciudad'=>$this->getCiudad(),
            'num_ext'=>$this->getNumExt(),
            'num_int'=>$this->getNumInt(),
            'colonia'=>$this->getColonia(),
            'municipio'=>$this->getMunicipio(),
            'cp'=>$this->getCp(),
            'estado' => $this->getEstado(),
            'pais' => $this->getPais()
        ];

    }

    /**
     * @return Collection<int, empresasDocumentos>
     */
    public function getEmpresasDocumentos(): Collection
    {
        return $this->empresasDocumentos;
    }

    public function addEmpresasDocumento(empresasDocumentos $empresasDocumento): self
    {
        if (!$this->empresasDocumentos->contains($empresasDocumento)) {
            $this->empresasDocumentos[] = $empresasDocumento;
            $empresasDocumento->setEmpresa($this);
        }

        return $this;
    }

    public function removeEmpresasDocumento(empresasDocumentos $empresasDocumento): self
    {
        if ($this->empresasDocumentos->removeElement($empresasDocumento)) {
            // set the owning side to null (unless already changed)
            if ($empresasDocumento->getEmpresa() === $this) {
                $empresasDocumento->setEmpresa(null);
            }
        }

        return $this;
    }

    public function getFechaFinCsd(): ?\DateTimeInterface
    {
        return $this->fecha_fin_csd;
    }

    public function setFechaFinCsd(?\DateTimeInterface $fecha_fin_csd): self
    {
        $this->fecha_fin_csd = $fecha_fin_csd;

        return $this;
    }

    public function getFechaFinFiel(): ?\DateTimeInterface
    {
        return $this->fecha_fin_fiel;
    }

    public function setFechaFinFiel(?\DateTimeInterface $fecha_fin_fiel): self
    {
        $this->fecha_fin_fiel = $fecha_fin_fiel;

        return $this;
    }

    /**
     * @return Collection<int, ConfiguracionEmpresas>
     */
    public function getConfiguracionEmpresas(): Collection
    {
        return $this->configuracionEmpresas;
    }

    public function addConfiguracionEmpresa(ConfiguracionEmpresas $configuracionEmpresa): self
    {
        if (!$this->configuracionEmpresas->contains($configuracionEmpresa)) {
            $this->configuracionEmpresas[] = $configuracionEmpresa;
            $configuracionEmpresa->setEmpresa($this);
        }

        return $this;
    }

    public function removeConfiguracionEmpresa(ConfiguracionEmpresas $configuracionEmpresa): self
    {
        if ($this->configuracionEmpresas->removeElement($configuracionEmpresa)) {
            // set the owning side to null (unless already changed)
            if ($configuracionEmpresa->getEmpresa() === $this) {
                $configuracionEmpresa->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, FacturasTimbre>
     */
    public function getFacturasTimbres(): Collection
    {
        return $this->facturasTimbres;
    }

    public function addFacturasTimbre(FacturasTimbre $facturasTimbre): self
    {
        if (!$this->facturasTimbres->contains($facturasTimbre)) {
            $this->facturasTimbres[] = $facturasTimbre;
            $facturasTimbre->setEmpresa($this);
        }

        return $this;
    }

    public function removeFacturasTimbre(FacturasTimbre $facturasTimbre): self
    {
        if ($this->facturasTimbres->removeElement($facturasTimbre)) {
            // set the owning side to null (unless already changed)
            if ($facturasTimbre->getEmpresa() === $this) {
                $facturasTimbre->setEmpresa(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, FacturasSat>
     */
    public function getFacturasSats(): Collection
    {
        return $this->facturasSats;
    }

    public function addFacturasSat(FacturasSat $facturasSat): self
    {
        if (!$this->facturasSats->contains($facturasSat)) {
            $this->facturasSats[] = $facturasSat;
            $facturasSat->setEmpresa($this);
        }

        return $this;
    }

    public function removeFacturasSat(FacturasSat $facturasSat): self
    {
        if ($this->facturasSats->removeElement($facturasSat)) {
            // set the owning side to null (unless already changed)
            if ($facturasSat->getEmpresa() === $this) {
                $facturasSat->setEmpresa(null);
            }
        }

        return $this;
    }


}
