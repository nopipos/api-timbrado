<?php

namespace App\Entity;

use App\Repository\FacturasDetalleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturasDetalleRepository::class)
 * @ORM\Table (name="facturas_detalle")
 */
class FacturasDetalle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="detalle")
     * @ORM\JoinColumn(nullable=false)
     */
    private $facturas;

    /**
     * @ORM\Column(type="integer")
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $unidad;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $clave_sat;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $unidad_sat;

    /**
     * @ORM\Column(type="text")
     */
    private $descripcion;

    /**
     * @ORM\Column(type="float")
     */
    private $cantidad;

    /**
     * @ORM\Column(type="float")
     */
    private $precio_unitario;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $descuento;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $fraccion_arancelaria;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true,"default" = 1})
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=FacturaDetalleImpuesto::class, mappedBy="detalle")
     */
    private $impuestos;

    public function __construct()
    {
        $this->impuestos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFacturas(): ?Facturas
    {
        return $this->facturas;
    }

    public function setFacturas(?Facturas $facturas): self
    {
        $this->facturas = $facturas;

        return $this;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(?string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getUnidad(): ?string
    {
        return $this->unidad;
    }

    public function setUnidad(?string $unidad): self
    {
        $this->unidad = $unidad;

        return $this;
    }

    public function getClaveSat(): ?string
    {
        return $this->clave_sat;
    }

    public function setClaveSat(string $clave_sat): self
    {
        $this->clave_sat = $clave_sat;

        return $this;
    }

    public function getUnidadSat(): ?string
    {
        return $this->unidad_sat;
    }

    public function setUnidadSat(string $unidad_sat): self
    {
        $this->unidad_sat = $unidad_sat;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getCantidad(): ?float
    {
        return $this->cantidad;
    }

    public function setCantidad(float $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getPrecioUnitario(): ?float
    {
        return $this->precio_unitario;
    }

    public function setPrecioUnitario(float $precio_unitario): self
    {
        $this->precio_unitario = $precio_unitario;

        return $this;
    }

    public function getDescuento(): ?float
    {
        return $this->descuento;
    }

    public function setDescuento(?float $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getFraccionArancelaria(): ?string
    {
        return $this->fraccion_arancelaria;
    }

    public function setFraccionArancelaria(?string $fraccion_arancelaria): self
    {
        $this->fraccion_arancelaria = $fraccion_arancelaria;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|FacturaDetalleImpuesto[]
     */
    public function getImpuestos(): Collection
    {
        return $this->impuestos;
    }

    public function addImpuesto(FacturaDetalleImpuesto $impuesto): self
    {
        if (!$this->impuestos->contains($impuesto)) {
            $this->impuestos[] = $impuesto;
            $impuesto->setDetalle($this);
        }

        return $this;
    }

    public function removeImpuesto(FacturaDetalleImpuesto $impuesto): self
    {
        if ($this->impuestos->removeElement($impuesto)) {
            // set the owning side to null (unless already changed)
            if ($impuesto->getDetalle() === $this) {
                $impuesto->setDetalle(null);
            }
        }

        return $this;
    }
}
