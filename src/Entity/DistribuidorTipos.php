<?php

namespace App\Entity;

use App\Repository\DistribuidorTiposRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DistribuidorTiposRepository::class)
 */
class DistribuidorTipos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $tipo;

    /**
     * @ORM\Column(type="float")
     */
    private $precio_timbrado;

    /**
     * @ORM\Column(type="float")
     */
    private $precio_producto;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getPrecioTimbrado(): ?float
    {
        return $this->precio_timbrado;
    }

    public function setPrecioTimbrado(float $precio_timbrado): self
    {
        $this->precio_timbrado = $precio_timbrado;

        return $this;
    }

    public function getPrecioProducto(): ?float
    {
        return $this->precio_producto;
    }

    public function setPrecioProducto(float $precio_producto): self
    {
        $this->precio_producto = $precio_producto;

        return $this;
    }



}
