<?php

namespace App\Entity;

use App\Repository\EmpresasComplementosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpresasComplementosRepository::class)
 */
class EmpresasComplementos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empresasComplementos")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Complementos::class, inversedBy="empresasComplementos")
     */
    private $complemento;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getComplemento(): ?Complementos
    {
        return $this->complemento;
    }

    public function setComplemento(?Complementos $complemento): self
    {
        $this->complemento = $complemento;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
