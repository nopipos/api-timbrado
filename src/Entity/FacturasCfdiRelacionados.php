<?php

namespace App\Entity;

use App\Repository\FacturasCfdiRelacionadosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturasCfdiRelacionadosRepository::class)
 */
class FacturasCfdiRelacionados
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="facturasCfdiRelacionados")
     */
    private $facturas;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $tipo_relacion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=FacturasCfdiRelacionadosUuid::class, mappedBy="tipo_relacion")
     */
    private $facturasCfdiRelacionadosUuids;

    public function __construct()
    {
        $this->facturasCfdiRelacionadosUuids = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFacturas(): ?Facturas
    {
        return $this->facturas;
    }

    public function setFacturas(?Facturas $facturas): self
    {
        $this->facturas = $facturas;

        return $this;
    }

    public function getTipoRelacion(): ?string
    {
        return $this->tipo_relacion;
    }

    public function setTipoRelacion(string $tipo_relacion): self
    {
        $this->tipo_relacion = $tipo_relacion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|FacturasCfdiRelacionadosUuid[]
     */
    public function getFacturasCfdiRelacionadosUuids(): Collection
    {
        return $this->facturasCfdiRelacionadosUuids;
    }

    public function addFacturasCfdiRelacionadosUuid(FacturasCfdiRelacionadosUuid $facturasCfdiRelacionadosUuid): self
    {
        if (!$this->facturasCfdiRelacionadosUuids->contains($facturasCfdiRelacionadosUuid)) {
            $this->facturasCfdiRelacionadosUuids[] = $facturasCfdiRelacionadosUuid;
            $facturasCfdiRelacionadosUuid->setTipoRelacion($this);
        }

        return $this;
    }

    public function removeFacturasCfdiRelacionadosUuid(FacturasCfdiRelacionadosUuid $facturasCfdiRelacionadosUuid): self
    {
        if ($this->facturasCfdiRelacionadosUuids->removeElement($facturasCfdiRelacionadosUuid)) {
            // set the owning side to null (unless already changed)
            if ($facturasCfdiRelacionadosUuid->getTipoRelacion() === $this) {
                $facturasCfdiRelacionadosUuid->setTipoRelacion(null);
            }
        }

        return $this;
    }
}
