<?php

namespace App\Entity\Timbre;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\Mapping as ORM;

/**
 * ValPeticiones
 *
 * @ORM\Table(name="val_peticiones")
 * @ORM\Entity
 */
class ValPeticiones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fecha_xml", type="datetime", nullable=true)
     */
    private $fechaXml;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=true)
     */
    private $codigo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo", type="string", length=10, nullable=true)
     */
    private $tipo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="serie", type="string", length=30, nullable=true)
     */
    private $serie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="folio", type="string", length=40, nullable=true)
     */
    private $folio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="emisor", type="string", length=13, nullable=true)
     */
    private $emisor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="receptor", type="string", length=13, nullable=true)
     */
    private $receptor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="uuid", type="string", length=38, nullable=true)
     */
    private $uuid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ruta", type="string", length=140, nullable=true)
     */
    private $ruta;

    /**
     * @var float|null
     *
     * @ORM\Column(name="tiempo", type="float", precision=10, scale=0, nullable=true)
     */
    private $tiempo;

    /**
     * @var bool
     *
     * @ORM\Column(name="estatus", type="boolean", nullable=false)
     */
    private $estatus;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string",length=140, nullable=false)
     */
    private $usuario;



    /**
     * Get the value of estatus
     *
     * @return  bool
     */ 
    public function getEstatus()
    {
        return $this->estatus;
    }

    /**
     * Set the value of estatus
     *
     * @param  bool  $estatus
     *
     * @return  self
     */ 
    public function setEstatus(bool $estatus)
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * Get the value of tiempo
     *
     * @return  float|null
     */ 
    public function getTiempo()
    {
        return $this->tiempo;
    }

    /**
     * Set the value of tiempo
     *
     * @param  float|null  $tiempo
     *
     * @return  self
     */ 
    public function setTiempo($tiempo)
    {
        $this->tiempo = $tiempo;

        return $this;
    }

    /**
     * Get the value of ruta
     *
     * @return  string|null
     */ 
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set the value of ruta
     *
     * @param  string|null  $ruta
     *
     * @return  self
     */ 
    public function setRuta($ruta)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get the value of uuid
     *
     * @return  string|null
     */ 
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set the value of uuid
     *
     * @param  string|null  $uuid
     *
     * @return  self
     */ 
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get the value of receptor
     *
     * @return  string|null
     */ 
    public function getReceptor()
    {
        return $this->receptor;
    }

    /**
     * Set the value of receptor
     *
     * @param  string|null  $receptor
     *
     * @return  self
     */ 
    public function setReceptor($receptor)
    {
        $this->receptor = $receptor;

        return $this;
    }

    /**
     * Get the value of emisor
     *
     * @return  string|null
     */ 
    public function getEmisor()
    {
        return $this->emisor;
    }

    /**
     * Set the value of emisor
     *
     * @param  string|null  $emisor
     *
     * @return  self
     */ 
    public function setEmisor($emisor)
    {
        $this->emisor = $emisor;

        return $this;
    }

    /**
     * Get the value of folio
     *
     * @return  string|null
     */ 
    public function getFolio()
    {
        return $this->folio;
    }

    /**
     * Set the value of folio
     *
     * @param  string|null  $folio
     *
     * @return  self
     */ 
    public function setFolio($folio)
    {
        $this->folio = $folio;

        return $this;
    }

    /**
     * Get the value of serie
     *
     * @return  string|null
     */ 
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * Set the value of serie
     *
     * @param  string|null  $serie
     *
     * @return  self
     */ 
    public function setSerie($serie)
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * Get the value of tipo
     *
     * @return  string|null
     */ 
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set the value of tipo
     *
     * @param  string|null  $tipo
     *
     * @return  self
     */ 
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get the value of codigo
     *
     * @return  string|null
     */ 
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set the value of codigo
     *
     * @param  string|null  $codigo
     *
     * @return  self
     */ 
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get the value of fechaXml
     *
     * @return  \DateTime|null
     */ 
    public function getFechaXml()
    {
        return $this->fechaXml;
    }

    /**
     * Set the value of fechaXml
     *
     * @param  \DateTime|null  $fechaXml
     *
     * @return  self
     */ 
    public function setFechaXml($fechaXml)
    {
        $this->fechaXml = $fechaXml;

        return $this;
    }

    /**
     * Get the value of fecha
     *
     * @return  \DateTime
     */ 
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set the value of fecha
     *
     * @param  \DateTime  $fecha
     *
     * @return  self
     */ 
    public function setFecha(\DateTime $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }
    public function existeUUID(ManagerRegistry $repo,$uuid){

        $peticion = $repo->getRepository(self::class);
        $modelUsuario = $peticion->findOneBy(['uuid'=>$uuid,'estatus'=>'1']);

        if($modelUsuario == null)
            return false;
        else
            return true;
            
    }

    /**
     * Get the value of usuario
     *
     * @return  string
     */ 
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set the value of usuario
     *
     * @param  string  $usuario
     *
     * @return  self
     */ 
    public function setUsuario(string $usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }
}
