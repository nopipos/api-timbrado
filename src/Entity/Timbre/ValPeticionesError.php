<?php

namespace App\Entity\Timbre;

use Doctrine\ORM\Mapping as ORM;

/**
 * ValPeticionesError
 *
 * @ORM\Table(name="val_peticiones_error")
 * @ORM\Entity
 */
class ValPeticionesError
{
    /**
     * @var int
     *
     * @ORM\Column(name="validacion_id", type="integer", nullable=false)
     */
    private $validacionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="error", type="text", length=65535, nullable=true)
     */
    private $error;

    /**
     * @var \ValPeticiones
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ValPeticiones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id")
     * })
     */
    private $id;



    /**
     * Get the value of id
     *
     * @return  \ValPeticiones
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  \ValPeticiones  $id
     *
     * @return  self
     */ 
    public function setId(ValPeticiones $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of error
     *
     * @return  string|null
     */ 
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set the value of error
     *
     * @param  string|null  $error
     *
     * @return  self
     */ 
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get the value of validacionId
     *
     * @return  int
     */ 
    public function getValidacionId()
    {
        return $this->validacionId;
    }

    /**
     * Set the value of validacionId
     *
     * @param  int  $validacionId
     *
     * @return  self
     */ 
    public function setValidacionId(int $validacionId)
    {
        $this->validacionId = $validacionId;

        return $this;
    }
}
