<?php

namespace App\Entity\Timbre;

use Doctrine\ORM\Mapping as ORM;

/**
 * ValPeticionesSat
 *
 * @ORM\Table(name="val_peticiones_sat", indexes={@ORM\Index(name="val_peticiones_sat_FK", columns={"peticion_id"})})
 * @ORM\Entity
 */
class ValPeticionesSat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado_sat", type="string", length=30, nullable=true)
     */
    private $estadoSat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="respuesta", type="string", length=255, nullable=true)
     */
    private $respuesta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado_cancelacion", type="string", length=40, nullable=true)
     */
    private $estadoCancelacion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="estatus", type="integer", nullable=true)
     */
    private $estatus;

    /**
     * @var \ValPeticiones
     *
     * @ORM\ManyToOne(targetEntity="ValPeticiones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="peticion_id", referencedColumnName="id")
     * })
     */
    private $peticion;



    /**
     * Get the value of peticion
     *
     * @return  \ValPeticiones
     */ 
    public function getPeticion()
    {
        return $this->peticion;
    }

    /**
     * Set the value of peticion
     *
     * @param  \ValPeticiones  $peticion
     *
     * @return  self
     */ 
    public function setPeticion(ValPeticiones $peticion)
    {
        $this->peticion = $peticion;

        return $this;
    }

    /**
     * Get the value of estatus
     *
     * @return  int|null
     */ 
    public function getEstatus()
    {
        return $this->estatus;
    }

    /**
     * Set the value of estatus
     *
     * @param  int|null  $estatus
     *
     * @return  self
     */ 
    public function setEstatus($estatus)
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * Get the value of estadoCancelacion
     *
     * @return  string|null
     */ 
    public function getEstadoCancelacion()
    {
        return $this->estadoCancelacion;
    }

    /**
     * Set the value of estadoCancelacion
     *
     * @param  string|null  $estadoCancelacion
     *
     * @return  self
     */ 
    public function setEstadoCancelacion($estadoCancelacion)
    {
        $this->estadoCancelacion = $estadoCancelacion;

        return $this;
    }

    /**
     * Get the value of respuesta
     *
     * @return  string|null
     */ 
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set the value of respuesta
     *
     * @param  string|null  $respuesta
     *
     * @return  self
     */ 
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get the value of estadoSat
     *
     * @return  string|null
     */ 
    public function getEstadoSat()
    {
        return $this->estadoSat;
    }

    /**
     * Set the value of estadoSat
     *
     * @param  string|null  $estadoSat
     *
     * @return  self
     */ 
    public function setEstadoSat($estadoSat)
    {
        $this->estadoSat = $estadoSat;

        return $this;
    }

    /**
     * Get the value of id
     *
     * @return  int
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */ 
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }
}
