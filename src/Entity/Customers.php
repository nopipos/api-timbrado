<?php

namespace App\Entity;

use App\Repository\CustomersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CustomersRepository::class)
 * @ORM\Table(indexes={@ORM\Index(name="name_em2", columns={"rfc"})})

 */
class Customers
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=13)
     */
    private $rfc;

    /**
     * @ORM\Column(type="string", length=13)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Distribuidores::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $distribuidor;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true,"default" = 1})
     */
    private $estatus;

    public function __construct()
    {
        $this->estatus = 1;
        $this->type = 'debit';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getDistribuidor(): ?Distribuidores
    {
        return $this->distribuidor;
    }

    public function setDistribuidor(?Distribuidores $distribuidor): self
    {
        $this->distribuidor = $distribuidor;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }


}
