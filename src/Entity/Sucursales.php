<?php

namespace App\Entity;

use App\Repository\SucursalesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SucursalesRepository::class)
 * @ORM\Table (name="sucursales")
 */
class Sucursales
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="sucursales")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $pais;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $estado;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $municipio;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $colonia;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $num_ext;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $num_int;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true,"default" = 1})
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=Facturas::class, mappedBy="sucursal")
     */
    private $sucursales;

    /**
     * @ORM\OneToMany(targetEntity=Series::class, mappedBy="sucursal")
     */
    private $series;

    public function __construct()
    {
        $this->sucursales = new ArrayCollection();
        $this->series = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(?string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getPais(): ?string
    {
        return $this->pais;
    }

    public function setPais(?string $pais): self
    {
        $this->pais = $pais;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    public function setMunicipio(?string $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    public function getColonia(): ?string
    {
        return $this->colonia;
    }

    public function setColonia(?string $colonia): self
    {
        $this->colonia = $colonia;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getNumExt(): ?string
    {
        return $this->num_ext;
    }

    public function setNumExt(?string $num_ext): self
    {
        $this->num_ext = $num_ext;

        return $this;
    }

    public function getNumInt(): ?string
    {
        return $this->num_int;
    }

    public function setNumInt(?string $num_int): self
    {
        $this->num_int = $num_int;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|Facturas[]
     */
    public function getSucursales(): Collection
    {
        return $this->sucursales;
    }

    public function addSucursale(Facturas $sucursale): self
    {
        if (!$this->sucursales->contains($sucursale)) {
            $this->sucursales[] = $sucursale;
            $sucursale->setSucursal($this);
        }

        return $this;
    }

    public function removeSucursale(Facturas $sucursale): self
    {
        if ($this->sucursales->removeElement($sucursale)) {
            // set the owning side to null (unless already changed)
            if ($sucursale->getSucursal() === $this) {
                $sucursale->setSucursal(null);
            }
        }

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'clave'=>$this->getClave(),
            'nombre'=>$this->getNombre(),
            'pais'=>$this->getPais(),
            'estado'=>$this->getEstado(),
            'municipio'=>$this->getMunicipio(),
            'colonia'=>$this->getColonia(),
            'cp'=>$this->getCp(),
            'numExt'=>$this->getNumExt(),
            'numInt'=>$this->getNumInt(),
        ];

    }

    /**
     * @return Collection|Series[]
     */
    public function getSeries(): Collection
    {
        return $this->series;
    }

    public function addSeries(Series $series): self
    {
        if (!$this->series->contains($series)) {
            $this->series[] = $series;
            $series->setSucursal($this);
        }

        return $this;
    }

    public function removeSeries(Series $series): self
    {
        if ($this->series->removeElement($series)) {
            // set the owning side to null (unless already changed)
            if ($series->getSucursal() === $this) {
                $series->setSucursal(null);
            }
        }

        return $this;
    }
}
