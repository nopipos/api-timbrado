<?php

namespace App\Entity;

use App\Entity\Addendas\AddendaDatos;
use App\Entity\Complementos\CCE\cce;
use App\Entity\Complementos\CCE\cceDestino;
use App\Entity\Complementos\CCE\cceMercancias;
use App\Entity\Complementos\CCE\cceMercanciasDetalle;
use App\Entity\Complementos\CCE\ccePropietario;
use App\Entity\Complementos\CP\cp;
use App\Entity\Complementos\CP\cpFigura;
use App\Entity\Complementos\CP\cpMercancia;
use App\Entity\Complementos\CP\cpMercanciaAutoFed;
use App\Entity\Complementos\CP\cpMercanciaDetalle;
use App\Entity\Complementos\CP\cpUbicacion;
use App\Entity\Complementos\Donat\donatarias;
use App\Entity\Tuweb\Tickets;
use App\Repository\FacturasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturasRepository::class)
 * @ORM\Table(indexes={
 * @ORM\Index(name="serie_folio", columns={"serie","folio"}),
 * @ORM\Index(name="tipo", columns={"empresa_id","tipo"}),
 * })
 */
class Facturas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="facturas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Usuarios::class, inversedBy="usuarios")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity=Sucursales::class, inversedBy="sucursales")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sucursal;

    /**
     * @ORM\ManyToOne(targetEntity=Clientes::class, inversedBy="clientes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cliente;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $serie;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $folio;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $forma_pago;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $metodo_pago;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $timbrado;

    /**
     * @ORM\Column(type="integer", nullable=true,options={"comment":"o = vigente, 1 = cancelado 2 = en proceso"})
     */
    private $cancelado;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\Column(type="float")
     */
    private $subtotal;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $iva;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $descuento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $porcentaje_descuento;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $moneda;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tipo_cambio;


    /**
     * @ORM\Column(type="string", length=20)
     */
    private $condiciones_pago;

    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     */
    private $nota;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true,"default" = 1})
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=FacturasDetalle::class, mappedBy="facturas")
     */
    private $detalle;

    /**
     * @ORM\OneToMany(targetEntity=FActurasCliente::class, mappedBy="factura")
     */
    private $clientes;

    /**
     * @ORM\OneToMany(targetEntity=FacturaDetalleImpuesto::class, mappedBy="factura")
     */
    private $facturas;

    /**
     * @ORM\OneToMany(targetEntity=ComplementosPago::class, mappedBy="factura")
     */
    private $pagos;

    /**
     * @ORM\OneToMany(targetEntity=ComplementosPagoDocumento::class, mappedBy="factura")
     */
    private $documentos;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $tipoComprobante;

    /**
     * @ORM\ManyToOne(targetEntity=Tickets::class, inversedBy="factura")
     */
    private $factura;

    /**
     * @ORM\OneToMany(targetEntity=FacturasCfdiRelacionados::class, mappedBy="facturas")
     */
    private $facturasCfdiRelacionados;

    /**
     * @ORM\OneToMany(targetEntity=FacturasInfoBlobal::class, mappedBy="factura")
     */
    private $facturasInfoBlobals;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $confirmacion;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $exportacion;

    /**
     * @ORM\OneToMany(targetEntity=FacturasDetalleTerceros::class, mappedBy="facturas")
     */
    private $facturasDetalleTerceros;

    /**
     * @ORM\OneToMany(targetEntity=cp::class, mappedBy="factura")
     */
    private $cps;

    /**
     * @ORM\OneToMany(targetEntity=cpUbicacion::class, mappedBy="factura")
     */
    private $cpUbicacions;

    /**
     * @ORM\OneToMany(targetEntity=cpFigura::class, mappedBy="factura")
     */
    private $cpFiguras;

    /**
     * @ORM\OneToMany(targetEntity=cpMercancia::class, mappedBy="factura")
     */
    private $cpMercancias;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalle::class, mappedBy="factura")
     */
    private $cpMercanciaDetalles;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaAutoFed::class, mappedBy="factura")
     */
    private $cpMercanciaAutoFeds;

    /**
     * @ORM\OneToMany(targetEntity=cce::class, mappedBy="factura")
     */
    private $cces;

    /**
     * @ORM\OneToMany(targetEntity=cceDestino::class, mappedBy="factura")
     */
    private $cceDestinos;

    /**
     * @ORM\OneToMany(targetEntity=ccePropietario::class, mappedBy="factura")
     */
    private $ccePropietarios;

    /**
     * @ORM\OneToMany(targetEntity=cceMercancias::class, mappedBy="factura")
     */
    private $cceMercancias;

    /**
     * @ORM\OneToMany(targetEntity=cceMercanciasDetalle::class, mappedBy="factura")
     */
    private $cceMercanciasDetalles;

    /**
     * @ORM\OneToMany(targetEntity=AddendaDatos::class, mappedBy="factura")
     */
    private $addendaDatos;

    /**
     * @ORM\OneToMany(targetEntity=donatarias::class, mappedBy="factura")
     */
    private $donatarias;

    /**
     * @ORM\Column(type="integer",options={"comment":"1 = emision, 2 = recepcion 3 = nomina","default" : 1})
     */
    private $tipo;

    /**
     * @ORM\OneToMany(targetEntity=FacturasTimbre::class, mappedBy="factura")
     */
    private $timbre;

    /**
     * @ORM\OneToMany(targetEntity=FacturasSat::class, mappedBy="factura")
     */
    private $facturasSats;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $version;

    public function __construct()
    {
        $this->detalle = new ArrayCollection();
        $this->clientes = new ArrayCollection();
        $this->facturas = new ArrayCollection();
        $this->pagos = new ArrayCollection();
        $this->documentos = new ArrayCollection();
        $this->facturasCfdiRelacionados = new ArrayCollection();
        $this->facturasInfoBlobals = new ArrayCollection();
        $this->facturasDetalleTerceros = new ArrayCollection();
        $this->cps = new ArrayCollection();
        $this->cpUbicacions = new ArrayCollection();
        $this->cpFiguras = new ArrayCollection();
        $this->cpMercancias = new ArrayCollection();
        $this->cpMercanciaDetalles = new ArrayCollection();
        $this->cpMercanciaAutoFeds = new ArrayCollection();
        $this->cces = new ArrayCollection();
        $this->cceDestinos = new ArrayCollection();
        $this->ccePropietarios = new ArrayCollection();
        $this->cceMercancias = new ArrayCollection();
        $this->cceMercanciasDetalles = new ArrayCollection();
        $this->addendaDatos = new ArrayCollection();
        $this->donatarias = new ArrayCollection();
        $this->facturasSats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getUsuario(): ?Usuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?Usuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getSucursal(): ?Sucursales
    {
        return $this->sucursal;
    }

    public function setSucursal(?Sucursales $sucursal): self
    {
        $this->sucursal = $sucursal;

        return $this;
    }

    public function getCliente(): ?Clientes
    {
        return $this->cliente;
    }

    public function setCliente(?Clientes $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(?string $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getFolio(): ?string
    {
        return $this->folio;
    }

    public function setFolio(?string $folio): self
    {
        $this->folio = $folio;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getFormaPago(): ?string
    {
        return $this->forma_pago;
    }

    public function setFormaPago(?string $forma_pago): self
    {
        $this->forma_pago = $forma_pago;

        return $this;
    }

    public function getMetodoPago(): ?string
    {
        return $this->metodo_pago;
    }

    public function setMetodoPago(?string $metodo_pago): self
    {
        $this->metodo_pago = $metodo_pago;

        return $this;
    }

    public function getTimbrado(): ?int
    {
        return $this->timbrado;
    }

    public function setTimbrado(?int $timbrado): self
    {
        $this->timbrado = $timbrado;

        return $this;
    }

    public function getCancelado(): ?int
    {
        return $this->cancelado;
    }

    public function setCancelado(?int $cancelado): self
    {
        $this->cancelado = $cancelado;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(float $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getIva(): ?float
    {
        return $this->iva;
    }

    public function setIva(?float $iva): self
    {
        $this->iva = $iva;

        return $this;
    }

    public function getDescuento(): ?float
    {
        return $this->descuento;
    }

    public function setDescuento(?float $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getPorcentajeDescuento(): ?float
    {
        return $this->porcentaje_descuento;
    }

    public function setPorcentajeDescuento(?float $porcentaje_descuento): self
    {
        $this->porcentaje_descuento = $porcentaje_descuento;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getTipoCambio(): ?float
    {
        return $this->tipo_cambio;
    }

    public function setTipoCambio(?float $tipo_cambio): self
    {
        $this->tipo_cambio = $tipo_cambio;

        return $this;
    }

    public function getFechaTimbrado(): ?\DateTimeInterface
    {
        return $this->fecha_timbrado;
    }

    public function setFechaTimbrado(?\DateTimeInterface $fecha_timbrado): self
    {
        $this->fecha_timbrado = $fecha_timbrado;

        return $this;
    }

    public function getFechaCancelado(): ?\DateTimeInterface
    {
        return $this->fecha_cancelado;
    }

    public function setFechaCancelado(?\DateTimeInterface $fecha_cancelado): self
    {
        $this->fecha_cancelado = $fecha_cancelado;

        return $this;
    }

    public function getCondicionesPago(): ?string
    {
        return $this->condiciones_pago;
    }

    public function setCondicionesPago(string $condiciones_pago): self
    {
        $this->condiciones_pago = $condiciones_pago;

        return $this;
    }

    public function getNota(): ?string
    {
        return $this->nota;
    }

    public function setNota(?string $nota): self
    {
        $this->nota = $nota;

        return $this;
    }

    public function getTipoRelacion(): ?string
    {
        return $this->tipo_relacion;
    }

    public function setTipoRelacion(?string $tipo_relacion): self
    {
        $this->tipo_relacion = $tipo_relacion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|FacturasDetalle[]
     */
    public function getDetalle(): Collection
    {
        return $this->detalle;
    }

    public function addDetalle(FacturasDetalle $detalle): self
    {
        if (!$this->detalle->contains($detalle)) {
            $this->detalle[] = $detalle;
            $detalle->setFacturas($this);
        }

        return $this;
    }

    public function removeDetalle(FacturasDetalle $detalle): self
    {
        if ($this->detalle->removeElement($detalle)) {
            // set the owning side to null (unless already changed)
            if ($detalle->getFacturas() === $this) {
                $detalle->setFacturas(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FActurasCliente[]
     */
    public function getClientes(): Collection
    {
        return $this->clientes;
    }

    public function addCliente(FActurasCliente $cliente): self
    {
        if (!$this->clientes->contains($cliente)) {
            $this->clientes[] = $cliente;
            $cliente->setFactura($this);
        }

        return $this;
    }

    public function removeCliente(FActurasCliente $cliente): self
    {
        if ($this->clientes->removeElement($cliente)) {
            // set the owning side to null (unless already changed)
            if ($cliente->getFactura() === $this) {
                $cliente->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FacturaDetalleImpuesto[]
     */
    public function getFacturas(): Collection
    {
        return $this->facturas;
    }

    public function addFactura(FacturaDetalleImpuesto $factura): self
    {
        if (!$this->facturas->contains($factura)) {
            $this->facturas[] = $factura;
            $factura->setFactura($this);
        }

        return $this;
    }

    public function removeFactura(FacturaDetalleImpuesto $factura): self
    {
        if ($this->facturas->removeElement($factura)) {
            // set the owning side to null (unless already changed)
            if ($factura->getFactura() === $this) {
                $factura->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ComplementosPago[]
     */
    public function getPagos(): Collection
    {
        return $this->pagos;
    }

    public function addPago(ComplementosPago $pago): self
    {
        if (!$this->pagos->contains($pago)) {
            $this->pagos[] = $pago;
            $pago->setFactura($this);
        }

        return $this;
    }

    public function removePago(ComplementosPago $pago): self
    {
        if ($this->pagos->removeElement($pago)) {
            // set the owning side to null (unless already changed)
            if ($pago->getFactura() === $this) {
                $pago->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ComplementosPagoDocumento[]
     */
    public function getDocumentos(): Collection
    {
        return $this->documentos;
    }

    public function addDocumento(ComplementosPagoDocumento $documento): self
    {
        if (!$this->documentos->contains($documento)) {
            $this->documentos[] = $documento;
            $documento->setFactura($this);
        }

        return $this;
    }

    public function removeDocumento(ComplementosPagoDocumento $documento): self
    {
        if ($this->documentos->removeElement($documento)) {
            // set the owning side to null (unless already changed)
            if ($documento->getFactura() === $this) {
                $documento->setFactura(null);
            }
        }

        return $this;
    }

    public function getFolioFiscal(): ?string
    {
        return $this->folio_fiscal;
    }

    public function setFolioFiscal(?string $folio_fiscal): self
    {
        $this->folio_fiscal = $folio_fiscal;

        return $this;
    }

    public function getTipoComprobante(): ?string
    {
        return $this->tipoComprobante;
    }

    public function setTipoComprobante(string $tipoComprobante): self
    {
        $this->tipoComprobante = $tipoComprobante;

        return $this;
    }

    public function getFactura(): ?Tickets
    {
        return $this->factura;
    }

    public function setFactura(?Tickets $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    /**
     * @return Collection|FacturasCfdiRelacionados[]
     */
    public function getFacturasCfdiRelacionados(): Collection
    {
        return $this->facturasCfdiRelacionados;
    }

    public function addFacturasCfdiRelacionado(FacturasCfdiRelacionados $facturasCfdiRelacionado): self
    {
        if (!$this->facturasCfdiRelacionados->contains($facturasCfdiRelacionado)) {
            $this->facturasCfdiRelacionados[] = $facturasCfdiRelacionado;
            $facturasCfdiRelacionado->setFacturas($this);
        }

        return $this;
    }

    public function removeFacturasCfdiRelacionado(FacturasCfdiRelacionados $facturasCfdiRelacionado): self
    {
        if ($this->facturasCfdiRelacionados->removeElement($facturasCfdiRelacionado)) {
            // set the owning side to null (unless already changed)
            if ($facturasCfdiRelacionado->getFacturas() === $this) {
                $facturasCfdiRelacionado->setFacturas(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FacturasInfoBlobal[]
     */
    public function getFacturasInfoBlobals(): Collection
    {
        return $this->facturasInfoBlobals;
    }

    public function addFacturasInfoBlobal(FacturasInfoBlobal $facturasInfoBlobal): self
    {
        if (!$this->facturasInfoBlobals->contains($facturasInfoBlobal)) {
            $this->facturasInfoBlobals[] = $facturasInfoBlobal;
            $facturasInfoBlobal->setFactura($this);
        }

        return $this;
    }

    public function removeFacturasInfoBlobal(FacturasInfoBlobal $facturasInfoBlobal): self
    {
        if ($this->facturasInfoBlobals->removeElement($facturasInfoBlobal)) {
            // set the owning side to null (unless already changed)
            if ($facturasInfoBlobal->getFactura() === $this) {
                $facturasInfoBlobal->setFactura(null);
            }
        }

        return $this;
    }

    public function getConfirmacion(): ?string
    {
        return $this->confirmacion;
    }

    public function setConfirmacion(?string $confirmacion): self
    {
        $this->confirmacion = $confirmacion;

        return $this;
    }

    public function getExportacion(): ?string
    {
        return $this->exportacion;
    }

    public function setExportacion(string $exportacion): self
    {
        $this->exportacion = $exportacion;

        return $this;
    }

    /**
     * @return Collection|FacturasDetalleTerceros[]
     */
    public function getFacturasDetalleTerceros(): Collection
    {
        return $this->facturasDetalleTerceros;
    }

    public function addFacturasDetalleTercero(FacturasDetalleTerceros $facturasDetalleTercero): self
    {
        if (!$this->facturasDetalleTerceros->contains($facturasDetalleTercero)) {
            $this->facturasDetalleTerceros[] = $facturasDetalleTercero;
            $facturasDetalleTercero->setFacturas($this);
        }

        return $this;
    }

    public function removeFacturasDetalleTercero(FacturasDetalleTerceros $facturasDetalleTercero): self
    {
        if ($this->facturasDetalleTerceros->removeElement($facturasDetalleTercero)) {
            // set the owning side to null (unless already changed)
            if ($facturasDetalleTercero->getFacturas() === $this) {
                $facturasDetalleTercero->setFacturas(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cp>
     */
    public function getCps(): Collection
    {
        return $this->cps;
    }

    public function addCp(cp $cp): self
    {
        if (!$this->cps->contains($cp)) {
            $this->cps[] = $cp;
            $cp->setFactura($this);
        }

        return $this;
    }

    public function removeCp(cp $cp): self
    {
        if ($this->cps->removeElement($cp)) {
            // set the owning side to null (unless already changed)
            if ($cp->getFactura() === $this) {
                $cp->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpUbicacion>
     */
    public function getCpUbicacions(): Collection
    {
        return $this->cpUbicacions;
    }

    public function addCpUbicacion(cpUbicacion $cpUbicacion): self
    {
        if (!$this->cpUbicacions->contains($cpUbicacion)) {
            $this->cpUbicacions[] = $cpUbicacion;
            $cpUbicacion->setFactura($this);
        }

        return $this;
    }

    public function removeCpUbicacion(cpUbicacion $cpUbicacion): self
    {
        if ($this->cpUbicacions->removeElement($cpUbicacion)) {
            // set the owning side to null (unless already changed)
            if ($cpUbicacion->getFactura() === $this) {
                $cpUbicacion->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpFigura>
     */
    public function getCpFiguras(): Collection
    {
        return $this->cpFiguras;
    }

    public function addCpFigura(cpFigura $cpFigura): self
    {
        if (!$this->cpFiguras->contains($cpFigura)) {
            $this->cpFiguras[] = $cpFigura;
            $cpFigura->setFactura($this);
        }

        return $this;
    }

    public function removeCpFigura(cpFigura $cpFigura): self
    {
        if ($this->cpFiguras->removeElement($cpFigura)) {
            // set the owning side to null (unless already changed)
            if ($cpFigura->getFactura() === $this) {
                $cpFigura->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercancia>
     */
    public function getCpMercancias(): Collection
    {
        return $this->cpMercancias;
    }

    public function addCpMercancia(cpMercancia $cpMercancia): self
    {
        if (!$this->cpMercancias->contains($cpMercancia)) {
            $this->cpMercancias[] = $cpMercancia;
            $cpMercancia->setFactura($this);
        }

        return $this;
    }

    public function removeCpMercancia(cpMercancia $cpMercancia): self
    {
        if ($this->cpMercancias->removeElement($cpMercancia)) {
            // set the owning side to null (unless already changed)
            if ($cpMercancia->getFactura() === $this) {
                $cpMercancia->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalle>
     */
    public function getCpMercanciaDetalles(): Collection
    {
        return $this->cpMercanciaDetalles;
    }

    public function addCpMercanciaDetalle(cpMercanciaDetalle $cpMercanciaDetalle): self
    {
        if (!$this->cpMercanciaDetalles->contains($cpMercanciaDetalle)) {
            $this->cpMercanciaDetalles[] = $cpMercanciaDetalle;
            $cpMercanciaDetalle->setFactura($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetalle(cpMercanciaDetalle $cpMercanciaDetalle): self
    {
        if ($this->cpMercanciaDetalles->removeElement($cpMercanciaDetalle)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetalle->getFactura() === $this) {
                $cpMercanciaDetalle->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaAutoFed>
     */
    public function getCpMercanciaAutoFeds(): Collection
    {
        return $this->cpMercanciaAutoFeds;
    }

    public function addCpMercanciaAutoFed(cpMercanciaAutoFed $cpMercanciaAutoFed): self
    {
        if (!$this->cpMercanciaAutoFeds->contains($cpMercanciaAutoFed)) {
            $this->cpMercanciaAutoFeds[] = $cpMercanciaAutoFed;
            $cpMercanciaAutoFed->setFactura($this);
        }

        return $this;
    }

    public function removeCpMercanciaAutoFed(cpMercanciaAutoFed $cpMercanciaAutoFed): self
    {
        if ($this->cpMercanciaAutoFeds->removeElement($cpMercanciaAutoFed)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaAutoFed->getFactura() === $this) {
                $cpMercanciaAutoFed->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cce>
     */
    public function getCces(): Collection
    {
        return $this->cces;
    }

    public function addCce(cce $cce): self
    {
        if (!$this->cces->contains($cce)) {
            $this->cces[] = $cce;
            $cce->setFactura($this);
        }

        return $this;
    }

    public function removeCce(cce $cce): self
    {
        if ($this->cces->removeElement($cce)) {
            // set the owning side to null (unless already changed)
            if ($cce->getFactura() === $this) {
                $cce->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cceDestino>
     */
    public function getCceDestinos(): Collection
    {
        return $this->cceDestinos;
    }

    public function addCceDestino(cceDestino $cceDestino): self
    {
        if (!$this->cceDestinos->contains($cceDestino)) {
            $this->cceDestinos[] = $cceDestino;
            $cceDestino->setFactura($this);
        }

        return $this;
    }

    public function removeCceDestino(cceDestino $cceDestino): self
    {
        if ($this->cceDestinos->removeElement($cceDestino)) {
            // set the owning side to null (unless already changed)
            if ($cceDestino->getFactura() === $this) {
                $cceDestino->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ccePropietario>
     */
    public function getCcePropietarios(): Collection
    {
        return $this->ccePropietarios;
    }

    public function addCcePropietario(ccePropietario $ccePropietario): self
    {
        if (!$this->ccePropietarios->contains($ccePropietario)) {
            $this->ccePropietarios[] = $ccePropietario;
            $ccePropietario->setFactura($this);
        }

        return $this;
    }

    public function removeCcePropietario(ccePropietario $ccePropietario): self
    {
        if ($this->ccePropietarios->removeElement($ccePropietario)) {
            // set the owning side to null (unless already changed)
            if ($ccePropietario->getFactura() === $this) {
                $ccePropietario->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cceMercancias>
     */
    public function getCceMercancias(): Collection
    {
        return $this->cceMercancias;
    }

    public function addCceMercancia(cceMercancias $cceMercancia): self
    {
        if (!$this->cceMercancias->contains($cceMercancia)) {
            $this->cceMercancias[] = $cceMercancia;
            $cceMercancia->setFactura($this);
        }

        return $this;
    }

    public function removeCceMercancia(cceMercancias $cceMercancia): self
    {
        if ($this->cceMercancias->removeElement($cceMercancia)) {
            // set the owning side to null (unless already changed)
            if ($cceMercancia->getFactura() === $this) {
                $cceMercancia->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cceMercanciasDetalle>
     */
    public function getCceMercanciasDetalles(): Collection
    {
        return $this->cceMercanciasDetalles;
    }

    public function addCceMercanciasDetalle(cceMercanciasDetalle $cceMercanciasDetalle): self
    {
        if (!$this->cceMercanciasDetalles->contains($cceMercanciasDetalle)) {
            $this->cceMercanciasDetalles[] = $cceMercanciasDetalle;
            $cceMercanciasDetalle->setFactura($this);
        }

        return $this;
    }

    public function removeCceMercanciasDetalle(cceMercanciasDetalle $cceMercanciasDetalle): self
    {
        if ($this->cceMercanciasDetalles->removeElement($cceMercanciasDetalle)) {
            // set the owning side to null (unless already changed)
            if ($cceMercanciasDetalle->getFactura() === $this) {
                $cceMercanciasDetalle->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AddendaDatos>
     */
    public function getAddendaDatos(): Collection
    {
        return $this->addendaDatos;
    }

    public function addAddendaDato(AddendaDatos $addendaDato): self
    {
        if (!$this->addendaDatos->contains($addendaDato)) {
            $this->addendaDatos[] = $addendaDato;
            $addendaDato->setFactura($this);
        }

        return $this;
    }

    public function removeAddendaDato(AddendaDatos $addendaDato): self
    {
        if ($this->addendaDatos->removeElement($addendaDato)) {
            // set the owning side to null (unless already changed)
            if ($addendaDato->getFactura() === $this) {
                $addendaDato->setFactura(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, donatarias>
     */
    public function getDonatarias(): Collection
    {
        return $this->donatarias;
    }

    public function addDonataria(donatarias $donataria): self
    {
        if (!$this->donatarias->contains($donataria)) {
            $this->donatarias[] = $donataria;
            $donataria->setFactura($this);
        }

        return $this;
    }

    public function removeDonataria(donatarias $donataria): self
    {
        if ($this->donatarias->removeElement($donataria)) {
            // set the owning side to null (unless already changed)
            if ($donataria->getFactura() === $this) {
                $donataria->setFactura(null);
            }
        }

        return $this;
    }

    public function getTipo(): ?int
    {
        return $this->tipo;
    }

    public function setTipo(int $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getTimbre(): ?FacturasTimbre
    {
        return $this->timbre;
    }

    public function setTimbre(?FacturasTimbre $timbre): self
    {
        $this->timbre = $timbre;

        return $this;
    }

    /**
     * @return Collection<int, FacturasSat>
     */
    public function getFacturasSats(): Collection
    {
        return $this->facturasSats;
    }

    public function addFacturasSat(FacturasSat $facturasSat): self
    {
        if (!$this->facturasSats->contains($facturasSat)) {
            $this->facturasSats[] = $facturasSat;
            $facturasSat->setFactura($this);
        }

        return $this;
    }

    public function removeFacturasSat(FacturasSat $facturasSat): self
    {
        if ($this->facturasSats->removeElement($facturasSat)) {
            // set the owning side to null (unless already changed)
            if ($facturasSat->getFactura() === $this) {
                $facturasSat->setFactura(null);
            }
        }

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }
}
