<?php

namespace App\Entity\Nomina;

use App\Entity\Empresas;
use App\Repository\Nomina\EmpleadoConceptoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpleadoConceptoRepository::class)
 */
class EmpleadoConcepto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empleadoConceptos")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Empleados::class, inversedBy="empleadoConceptos")
     */
    private $empleado;

    /**
     * @ORM\Column(type="integer")
     */
    private $tipo_deduccion;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $concepto;

    /**
     * @ORM\Column(type="float")
     */
    private $importe_gravado;

    /**
     * @ORM\Column(type="float")
     */
    private $importe_excento;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\Column(type="float")
     */
    private $importe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getEmpleado(): ?Empleados
    {
        return $this->empleado;
    }

    public function setEmpleado(?Empleados $empleado): self
    {
        $this->empleado = $empleado;

        return $this;
    }

    public function getTipoDeduccion(): ?int
    {
        return $this->tipo_deduccion;
    }

    public function setTipoDeduccion(int $tipo_deduccion): self
    {
        $this->tipo_deduccion = $tipo_deduccion;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    public function setConcepto(string $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }

    public function getImporteGravado(): ?float
    {
        return $this->importe_gravado;
    }

    public function setImporteGravado(float $importe_gravado): self
    {
        $this->importe_gravado = $importe_gravado;

        return $this;
    }

    public function getImporteExcento(): ?float
    {
        return $this->importe_excento;
    }

    public function setImporteExcento(float $importe_excento): self
    {
        $this->importe_excento = $importe_excento;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }
}
