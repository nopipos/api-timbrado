<?php

namespace App\Entity\Nomina;

use App\Entity\Empresas;
use App\Repository\Nomina\EmpleadoHorasExtraRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpleadoHorasExtraRepository::class)
 */
class EmpleadoHorasExtra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empleadoHorasExtras")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Nomina::class, inversedBy="empleadoHorasExtras")
     */
    private $nomina;

    /**
     * @ORM\ManyToOne(targetEntity=Empleados::class, inversedBy="empleadoHorasExtras")
     */
    private $empleado;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dias;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hora;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $tipo;

    /**
     * @ORM\Column(type="float")
     */
    private $importe_pago;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $importe_pagado;

    /**
     * @ORM\Column(type="integer")
     */
    private $estus;

    /**
     * @ORM\ManyToOne(targetEntity=EmpleadoPercepciones::class, inversedBy="empleadoHorasExtras")
     */
    private $percepcion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getNomina(): ?Nomina
    {
        return $this->nomina;
    }

    public function setNomina(?Nomina $nomina): self
    {
        $this->nomina = $nomina;

        return $this;
    }

    public function getEmpleado(): ?Empleados
    {
        return $this->empleado;
    }

    public function setEmpleado(?Empleados $empleado): self
    {
        $this->empleado = $empleado;

        return $this;
    }

    public function getDias(): ?int
    {
        return $this->dias;
    }

    public function setDias(?int $dias): self
    {
        $this->dias = $dias;

        return $this;
    }

    public function getHora(): ?int
    {
        return $this->hora;
    }

    public function setHora(?int $hora): self
    {
        $this->hora = $hora;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getImportePago(): ?float
    {
        return $this->importe_pago;
    }

    public function setImportePago(float $importe_pago): self
    {
        $this->importe_pago = $importe_pago;

        return $this;
    }

    public function getImportePagado(): ?float
    {
        return $this->importe_pagado;
    }

    public function setImportePagado(?float $importe_pagado): self
    {
        $this->importe_pagado = $importe_pagado;

        return $this;
    }

    public function getEstus(): ?int
    {
        return $this->estus;
    }

    public function setEstus(int $estus): self
    {
        $this->estus = $estus;

        return $this;
    }

    public function getPercepcion(): ?EmpleadoPercepciones
    {
        return $this->percepcion;
    }

    public function setPercepcion(?EmpleadoPercepciones $percepcion): self
    {
        $this->percepcion = $percepcion;

        return $this;
    }
}
