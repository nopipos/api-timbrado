<?php

namespace App\Entity\Nomina;

use App\Entity\Empresas;
use App\Repository\Nomina\ConceptosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConceptosRepository::class)
 */
class Conceptos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="conceptos")
     */
    private $empresa;

    /**
     * @ORM\Column(type="integer", options={"comment": "1 = Percepcion, 2 = Deduccion, 3 = Otros Pagos"})
     */
    private $tipo_concepto;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $tipo_concepto_sat;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $concepto;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getTipoConcepto(): ?int
    {
        return $this->tipo_concepto;
    }

    public function setTipoConcepto(int $tipo_concepto): self
    {
        $this->tipo_concepto = $tipo_concepto;

        return $this;
    }

    public function getTipoConceptoSat(): ?string
    {
        return $this->tipo_concepto_sat;
    }

    public function setTipoConceptoSat(string $tipo_concepto_sat): self
    {
        $this->tipo_concepto_sat = $tipo_concepto_sat;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    public function setConcepto(string $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
