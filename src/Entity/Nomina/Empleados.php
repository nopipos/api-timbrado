<?php

namespace App\Entity\Nomina;

use App\Entity\Empresas;
use App\Repository\Nomina\EmpleadosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpleadosRepository::class)
 */
class Empleados
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empleados")
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $num_empleado;

    /**
     * @ORM\Column(type="string", length=13)
     */
    private $rfc;

    /**
     * @ORM\Column(type="string", length=19, nullable=true)
     */
    private $curp;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $regimen;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $seguro_social;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $departamento;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $clabe;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $banco;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_incio_laboral;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $antiguedad;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $puesto;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $tipo_contrato;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $jornada;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $sbca;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $riesgo_puesto;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdi;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $dias_pagados;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $sindicalizado;

    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     */
    private $rfc_labora;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $porcentaje_tiempo;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $tipo_nomina;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $calle;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $municipio;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $localidad;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $estado;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $pais;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $no_exterior;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $no_interior;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $colonia;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $referencia;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoConcepto::class, mappedBy="empleado")
     */
    private $empleadoConceptos;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoPercepciones::class, mappedBy="empleado")
     */
    private $empleadoPercepciones;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoDeducciones::class, mappedBy="empleado")
     */
    private $empleadoDeducciones;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoIncapacidades::class, mappedBy="empleado")
     */
    private $empleadoIncapacidades;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoHorasExtra::class, mappedBy="empleado")
     */
    private $empleadoHorasExtras;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoJubilacion::class, mappedBy="empleado")
     */
    private $empleadoJubilacions;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoSeparacion::class, mappedBy="empleado")
     */
    private $empleadoSeparacions;

    public function __construct()
    {
        $this->empleadoConceptos = new ArrayCollection();
        $this->empleadoPercepciones = new ArrayCollection();
        $this->empleadoDeducciones = new ArrayCollection();
        $this->empleadoIncapacidades = new ArrayCollection();
        $this->empleadoHorasExtras = new ArrayCollection();
        $this->empleadoJubilacions = new ArrayCollection();
        $this->empleadoSeparacions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getNumEmpleado(): ?string
    {
        return $this->num_empleado;
    }

    public function setNumEmpleado(string $num_empleado): self
    {
        $this->num_empleado = $num_empleado;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getCurp(): ?string
    {
        return $this->curp;
    }

    public function setCurp(?string $curp): self
    {
        $this->curp = $curp;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRegimen(): ?string
    {
        return $this->regimen;
    }

    public function setRegimen(string $regimen): self
    {
        $this->regimen = $regimen;

        return $this;
    }

    public function getSeguroSocial(): ?string
    {
        return $this->seguro_social;
    }

    public function setSeguroSocial(string $seguro_social): self
    {
        $this->seguro_social = $seguro_social;

        return $this;
    }

    public function getDepartamento(): ?string
    {
        return $this->departamento;
    }

    public function setDepartamento(?string $departamento): self
    {
        $this->departamento = $departamento;

        return $this;
    }

    public function getClabe(): ?string
    {
        return $this->clabe;
    }

    public function setClabe(?string $clabe): self
    {
        $this->clabe = $clabe;

        return $this;
    }

    public function getBanco(): ?string
    {
        return $this->banco;
    }

    public function setBanco(?string $banco): self
    {
        $this->banco = $banco;

        return $this;
    }

    public function getFechaIncioLaboral(): ?\DateTimeInterface
    {
        return $this->fecha_incio_laboral;
    }

    public function setFechaIncioLaboral(?\DateTimeInterface $fecha_incio_laboral): self
    {
        $this->fecha_incio_laboral = $fecha_incio_laboral;

        return $this;
    }

    public function getAntiguedad(): ?string
    {
        return $this->antiguedad;
    }

    public function setAntiguedad(?string $antiguedad): self
    {
        $this->antiguedad = $antiguedad;

        return $this;
    }

    public function getPuesto(): ?string
    {
        return $this->puesto;
    }

    public function setPuesto(?string $puesto): self
    {
        $this->puesto = $puesto;

        return $this;
    }

    public function getTipoContrato(): ?string
    {
        return $this->tipo_contrato;
    }

    public function setTipoContrato(?string $tipo_contrato): self
    {
        $this->tipo_contrato = $tipo_contrato;

        return $this;
    }

    public function getJornada(): ?string
    {
        return $this->jornada;
    }

    public function setJornada(?string $jornada): self
    {
        $this->jornada = $jornada;

        return $this;
    }

    public function getSbca(): ?float
    {
        return $this->sbca;
    }

    public function setSbca(?float $sbca): self
    {
        $this->sbca = $sbca;

        return $this;
    }

    public function getRiesgoPuesto(): ?int
    {
        return $this->riesgo_puesto;
    }

    public function setRiesgoPuesto(?int $riesgo_puesto): self
    {
        $this->riesgo_puesto = $riesgo_puesto;

        return $this;
    }

    public function getSdi(): ?float
    {
        return $this->sdi;
    }

    public function setSdi(?float $sdi): self
    {
        $this->sdi = $sdi;

        return $this;
    }

    public function getDiasPagados(): ?float
    {
        return $this->dias_pagados;
    }

    public function setDiasPagados(?float $dias_pagados): self
    {
        $this->dias_pagados = $dias_pagados;

        return $this;
    }

    public function getSindicalizado(): ?string
    {
        return $this->sindicalizado;
    }

    public function setSindicalizado(?string $sindicalizado): self
    {
        $this->sindicalizado = $sindicalizado;

        return $this;
    }

    public function getRfcLabora(): ?string
    {
        return $this->rfc_labora;
    }

    public function setRfcLabora(?string $rfc_labora): self
    {
        $this->rfc_labora = $rfc_labora;

        return $this;
    }

    public function getPorcentajeTiempo(): ?float
    {
        return $this->porcentaje_tiempo;
    }

    public function setPorcentajeTiempo(?float $porcentaje_tiempo): self
    {
        $this->porcentaje_tiempo = $porcentaje_tiempo;

        return $this;
    }

    public function getTipoNomina(): ?string
    {
        return $this->tipo_nomina;
    }

    public function setTipoNomina(?string $tipo_nomina): self
    {
        $this->tipo_nomina = $tipo_nomina;

        return $this;
    }

    public function getCalle(): ?string
    {
        return $this->calle;
    }

    public function setCalle(?string $calle): self
    {
        $this->calle = $calle;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    public function setMunicipio(?string $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(?string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getPais(): ?string
    {
        return $this->pais;
    }

    public function setPais(?string $pais): self
    {
        $this->pais = $pais;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getNoExterior(): ?string
    {
        return $this->no_exterior;
    }

    public function setNoExterior(?string $no_exterior): self
    {
        $this->no_exterior = $no_exterior;

        return $this;
    }

    public function getNoInterior(): ?string
    {
        return $this->no_interior;
    }

    public function setNoInterior(?string $no_interior): self
    {
        $this->no_interior = $no_interior;

        return $this;
    }

    public function getColonia(): ?string
    {
        return $this->colonia;
    }

    public function setColonia(?string $colonia): self
    {
        $this->colonia = $colonia;

        return $this;
    }

    public function getReferencia(): ?string
    {
        return $this->referencia;
    }

    public function setReferencia(?string $referencia): self
    {
        $this->referencia = $referencia;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|EmpleadoConcepto[]
     */
    public function getEmpleadoConceptos(): Collection
    {
        return $this->empleadoConceptos;
    }

    public function addEmpleadoConcepto(EmpleadoConcepto $empleadoConcepto): self
    {
        if (!$this->empleadoConceptos->contains($empleadoConcepto)) {
            $this->empleadoConceptos[] = $empleadoConcepto;
            $empleadoConcepto->setEmpleado($this);
        }

        return $this;
    }

    public function removeEmpleadoConcepto(EmpleadoConcepto $empleadoConcepto): self
    {
        if ($this->empleadoConceptos->removeElement($empleadoConcepto)) {
            // set the owning side to null (unless already changed)
            if ($empleadoConcepto->getEmpleado() === $this) {
                $empleadoConcepto->setEmpleado(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoPercepciones[]
     */
    public function getEmpleadoPercepciones(): Collection
    {
        return $this->empleadoPercepciones;
    }

    public function addEmpleadoPercepcione(EmpleadoPercepciones $empleadoPercepcione): self
    {
        if (!$this->empleadoPercepciones->contains($empleadoPercepcione)) {
            $this->empleadoPercepciones[] = $empleadoPercepcione;
            $empleadoPercepcione->setEmpleado($this);
        }

        return $this;
    }

    public function removeEmpleadoPercepcione(EmpleadoPercepciones $empleadoPercepcione): self
    {
        if ($this->empleadoPercepciones->removeElement($empleadoPercepcione)) {
            // set the owning side to null (unless already changed)
            if ($empleadoPercepcione->getEmpleado() === $this) {
                $empleadoPercepcione->setEmpleado(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoDeducciones[]
     */
    public function getEmpleadoDeducciones(): Collection
    {
        return $this->empleadoDeducciones;
    }

    public function addEmpleadoDeduccione(EmpleadoDeducciones $empleadoDeduccione): self
    {
        if (!$this->empleadoDeducciones->contains($empleadoDeduccione)) {
            $this->empleadoDeducciones[] = $empleadoDeduccione;
            $empleadoDeduccione->setEmpleado($this);
        }

        return $this;
    }

    public function removeEmpleadoDeduccione(EmpleadoDeducciones $empleadoDeduccione): self
    {
        if ($this->empleadoDeducciones->removeElement($empleadoDeduccione)) {
            // set the owning side to null (unless already changed)
            if ($empleadoDeduccione->getEmpleado() === $this) {
                $empleadoDeduccione->setEmpleado(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoIncapacidades[]
     */
    public function getEmpleadoIncapacidades(): Collection
    {
        return $this->empleadoIncapacidades;
    }

    public function addEmpleadoIncapacidade(EmpleadoIncapacidades $empleadoIncapacidade): self
    {
        if (!$this->empleadoIncapacidades->contains($empleadoIncapacidade)) {
            $this->empleadoIncapacidades[] = $empleadoIncapacidade;
            $empleadoIncapacidade->setEmpleado($this);
        }

        return $this;
    }

    public function removeEmpleadoIncapacidade(EmpleadoIncapacidades $empleadoIncapacidade): self
    {
        if ($this->empleadoIncapacidades->removeElement($empleadoIncapacidade)) {
            // set the owning side to null (unless already changed)
            if ($empleadoIncapacidade->getEmpleado() === $this) {
                $empleadoIncapacidade->setEmpleado(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoHorasExtra[]
     */
    public function getEmpleadoHorasExtras(): Collection
    {
        return $this->empleadoHorasExtras;
    }

    public function addEmpleadoHorasExtra(EmpleadoHorasExtra $empleadoHorasExtra): self
    {
        if (!$this->empleadoHorasExtras->contains($empleadoHorasExtra)) {
            $this->empleadoHorasExtras[] = $empleadoHorasExtra;
            $empleadoHorasExtra->setEmpleado($this);
        }

        return $this;
    }

    public function removeEmpleadoHorasExtra(EmpleadoHorasExtra $empleadoHorasExtra): self
    {
        if ($this->empleadoHorasExtras->removeElement($empleadoHorasExtra)) {
            // set the owning side to null (unless already changed)
            if ($empleadoHorasExtra->getEmpleado() === $this) {
                $empleadoHorasExtra->setEmpleado(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoJubilacion[]
     */
    public function getEmpleadoJubilacions(): Collection
    {
        return $this->empleadoJubilacions;
    }

    public function addEmpleadoJubilacion(EmpleadoJubilacion $empleadoJubilacion): self
    {
        if (!$this->empleadoJubilacions->contains($empleadoJubilacion)) {
            $this->empleadoJubilacions[] = $empleadoJubilacion;
            $empleadoJubilacion->setEmpleado($this);
        }

        return $this;
    }

    public function removeEmpleadoJubilacion(EmpleadoJubilacion $empleadoJubilacion): self
    {
        if ($this->empleadoJubilacions->removeElement($empleadoJubilacion)) {
            // set the owning side to null (unless already changed)
            if ($empleadoJubilacion->getEmpleado() === $this) {
                $empleadoJubilacion->setEmpleado(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoSeparacion[]
     */
    public function getEmpleadoSeparacions(): Collection
    {
        return $this->empleadoSeparacions;
    }

    public function addEmpleadoSeparacion(EmpleadoSeparacion $empleadoSeparacion): self
    {
        if (!$this->empleadoSeparacions->contains($empleadoSeparacion)) {
            $this->empleadoSeparacions[] = $empleadoSeparacion;
            $empleadoSeparacion->setEmpleado($this);
        }

        return $this;
    }

    public function removeEmpleadoSeparacion(EmpleadoSeparacion $empleadoSeparacion): self
    {
        if ($this->empleadoSeparacions->removeElement($empleadoSeparacion)) {
            // set the owning side to null (unless already changed)
            if ($empleadoSeparacion->getEmpleado() === $this) {
                $empleadoSeparacion->setEmpleado(null);
            }
        }

        return $this;
    }
}
