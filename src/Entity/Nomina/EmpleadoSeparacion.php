<?php

namespace App\Entity\Nomina;

use App\Entity\Empresas;
use App\Repository\Nomina\EmpleadoSeparacionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpleadoSeparacionRepository::class)
 */
class EmpleadoSeparacion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empleadoSeparacions")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Nomina::class, inversedBy="empleadoSeparacions")
     */
    private $nomina;

    /**
     * @ORM\ManyToOne(targetEntity=Empleados::class, inversedBy="empleadoSeparacions")
     */
    private $empleado;

    /**
     * @ORM\Column(type="float")
     */
    private $total_pagado;

    /**
     * @ORM\Column(type="integer")
     */
    private $num_anios_serv;

    /**
     * @ORM\Column(type="float")
     */
    private $ultimo_sueldo_mensual;

    /**
     * @ORM\Column(type="float")
     */
    private $ingreso_acumulable;

    /**
     * @ORM\Column(type="float")
     */
    private $ingreso_no_acumulable;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getNomina(): ?Nomina
    {
        return $this->nomina;
    }

    public function setNomina(?Nomina $nomina): self
    {
        $this->nomina = $nomina;

        return $this;
    }

    public function getEmpleado(): ?Empleados
    {
        return $this->empleado;
    }

    public function setEmpleado(?Empleados $empleado): self
    {
        $this->empleado = $empleado;

        return $this;
    }

    public function getTotalPagado(): ?float
    {
        return $this->total_pagado;
    }

    public function setTotalPagado(float $total_pagado): self
    {
        $this->total_pagado = $total_pagado;

        return $this;
    }

    public function getNumAniosServ(): ?int
    {
        return $this->num_anios_serv;
    }

    public function setNumAniosServ(int $num_anios_serv): self
    {
        $this->num_anios_serv = $num_anios_serv;

        return $this;
    }

    public function getUltimoSueldoMensual(): ?float
    {
        return $this->ultimo_sueldo_mensual;
    }

    public function setUltimoSueldoMensual(float $ultimo_sueldo_mensual): self
    {
        $this->ultimo_sueldo_mensual = $ultimo_sueldo_mensual;

        return $this;
    }

    public function getIngresoAcumulable(): ?float
    {
        return $this->ingreso_acumulable;
    }

    public function setIngresoAcumulable(float $ingreso_acumulable): self
    {
        $this->ingreso_acumulable = $ingreso_acumulable;

        return $this;
    }

    public function getIngresoNoAcumulable(): ?float
    {
        return $this->ingreso_no_acumulable;
    }

    public function setIngresoNoAcumulable(float $ingreso_no_acumulable): self
    {
        $this->ingreso_no_acumulable = $ingreso_no_acumulable;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
