<?php

namespace App\Entity\Nomina;

use App\Entity\Empresas;
use App\Repository\Nomina\EmpleadoOtrosPagosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpleadoOtrosPagosRepository::class)
 */
class EmpleadoOtrosPagos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empleadoOtrosPagos")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Nomina::class, inversedBy="empleadoOtrosPagos")
     */
    private $nomina;

    /**
     * @ORM\OneToOne(targetEntity=Empleados::class, cascade={"persist", "remove"})
     */
    private $empleado;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $concepto;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $clave_interna;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $clave_sat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $importe;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $subsidio_causado;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $saldo_favor;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $anio;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $remanente_saldo_favor;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getNomina(): ?Nomina
    {
        return $this->nomina;
    }

    public function setNomina(?Nomina $nomina): self
    {
        $this->nomina = $nomina;

        return $this;
    }

    public function getEmpleado(): ?Empleados
    {
        return $this->empleado;
    }

    public function setEmpleado(?Empleados $empleado): self
    {
        $this->empleado = $empleado;

        return $this;
    }

    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    public function setConcepto(?string $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }

    public function getClaveInterna(): ?string
    {
        return $this->clave_interna;
    }

    public function setClaveInterna(string $clave_interna): self
    {
        $this->clave_interna = $clave_interna;

        return $this;
    }

    public function getClaveSat(): ?string
    {
        return $this->clave_sat;
    }

    public function setClaveSat(string $clave_sat): self
    {
        $this->clave_sat = $clave_sat;

        return $this;
    }

    public function getImporte(): ?float
    {
        return $this->importe;
    }

    public function setImporte(?float $importe): self
    {
        $this->importe = $importe;

        return $this;
    }

    public function getSubsidioCausado(): ?float
    {
        return $this->subsidio_causado;
    }

    public function setSubsidioCausado(?float $subsidio_causado): self
    {
        $this->subsidio_causado = $subsidio_causado;

        return $this;
    }

    public function getSaldoFavor(): ?float
    {
        return $this->saldo_favor;
    }

    public function setSaldoFavor(?float $saldo_favor): self
    {
        $this->saldo_favor = $saldo_favor;

        return $this;
    }

    public function getAnio(): ?string
    {
        return $this->anio;
    }

    public function setAnio(?string $anio): self
    {
        $this->anio = $anio;

        return $this;
    }

    public function getRemanenteSaldoFavor(): ?float
    {
        return $this->remanente_saldo_favor;
    }

    public function setRemanenteSaldoFavor(?float $remanente_saldo_favor): self
    {
        $this->remanente_saldo_favor = $remanente_saldo_favor;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
