<?php

namespace App\Entity\Nomina;

use App\Entity\Empresas;
use App\Repository\Nomina\EmpleadoPercepcionesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpleadoPercepcionesRepository::class)
 */
class EmpleadoPercepciones
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empleadoPercepciones")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Empleados::class, inversedBy="empleadoPercepciones")
     */
    private $empleado;

    /**
     * @ORM\ManyToOne(targetEntity=Nomina::class, inversedBy="empleadoPercepciones")
     */
    private $nomina;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $tipo_percepcion;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $concepto;

    /**
     * @ORM\Column(type="float")
     */
    private $importe_gravado;

    /**
     * @ORM\Column(type="float")
     */
    private $importe_excento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $valor_mercado;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precio_otorgar;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoHorasExtra::class, mappedBy="percepcion")
     */
    private $empleadoHorasExtras;

    public function __construct()
    {
        $this->empleadoHorasExtras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getEmpleado(): ?Empleados
    {
        return $this->empleado;
    }

    public function setEmpleado(?Empleados $empleado): self
    {
        $this->empleado = $empleado;

        return $this;
    }

    public function getNomina(): ?Nomina
    {
        return $this->nomina;
    }

    public function setNomina(?Nomina $nomina): self
    {
        $this->nomina = $nomina;

        return $this;
    }

    public function getTipoPercepcion(): ?string
    {
        return $this->tipo_percepcion;
    }

    public function setTipoPercepcion(string $tipo_percepcion): self
    {
        $this->tipo_percepcion = $tipo_percepcion;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    public function setConcepto(string $concepto): self
    {
        $this->concepto = $concepto;

        return $this;
    }

    public function getImporteGravado(): ?float
    {
        return $this->importe_gravado;
    }

    public function setImporteGravado(float $importe_gravado): self
    {
        $this->importe_gravado = $importe_gravado;

        return $this;
    }

    public function getImporteExcento(): ?float
    {
        return $this->importe_excento;
    }

    public function setImporteExcento(float $importe_excento): self
    {
        $this->importe_excento = $importe_excento;

        return $this;
    }

    public function getValorMercado(): ?float
    {
        return $this->valor_mercado;
    }

    public function setValorMercado(?float $valor_mercado): self
    {
        $this->valor_mercado = $valor_mercado;

        return $this;
    }

    public function getPrecioOtorgar(): ?float
    {
        return $this->precio_otorgar;
    }

    public function setPrecioOtorgar(?float $precio_otorgar): self
    {
        $this->precio_otorgar = $precio_otorgar;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|EmpleadoHorasExtra[]
     */
    public function getEmpleadoHorasExtras(): Collection
    {
        return $this->empleadoHorasExtras;
    }

    public function addEmpleadoHorasExtra(EmpleadoHorasExtra $empleadoHorasExtra): self
    {
        if (!$this->empleadoHorasExtras->contains($empleadoHorasExtra)) {
            $this->empleadoHorasExtras[] = $empleadoHorasExtra;
            $empleadoHorasExtra->setPercepcion($this);
        }

        return $this;
    }

    public function removeEmpleadoHorasExtra(EmpleadoHorasExtra $empleadoHorasExtra): self
    {
        if ($this->empleadoHorasExtras->removeElement($empleadoHorasExtra)) {
            // set the owning side to null (unless already changed)
            if ($empleadoHorasExtra->getPercepcion() === $this) {
                $empleadoHorasExtra->setPercepcion(null);
            }
        }

        return $this;
    }
}
