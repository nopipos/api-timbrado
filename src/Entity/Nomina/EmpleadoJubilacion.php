<?php

namespace App\Entity\Nomina;

use App\Entity\Empresas;
use App\Repository\Nomina\EmpleadoJubilacionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpleadoJubilacionRepository::class)
 */
class EmpleadoJubilacion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empleadoJubilacions")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Nomina::class, inversedBy="empleadoJubilacions")
     */
    private $nomina;

    /**
     * @ORM\ManyToOne(targetEntity=Empleados::class, inversedBy="empleadoJubilacions")
     */
    private $empleado;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_Exhibicion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_parcialidad;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $monto_diario;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $acumulable;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $no_acumulable;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getNomina(): ?Nomina
    {
        return $this->nomina;
    }

    public function setNomina(?Nomina $nomina): self
    {
        $this->nomina = $nomina;

        return $this;
    }

    public function getEmpleado(): ?Empleados
    {
        return $this->empleado;
    }

    public function setEmpleado(?Empleados $empleado): self
    {
        $this->empleado = $empleado;

        return $this;
    }

    public function getTotalExhibicion(): ?float
    {
        return $this->total_Exhibicion;
    }

    public function setTotalExhibicion(?float $total_Exhibicion): self
    {
        $this->total_Exhibicion = $total_Exhibicion;

        return $this;
    }

    public function getTotalParcialidad(): ?float
    {
        return $this->total_parcialidad;
    }

    public function setTotalParcialidad(?float $total_parcialidad): self
    {
        $this->total_parcialidad = $total_parcialidad;

        return $this;
    }

    public function getMontoDiario(): ?float
    {
        return $this->monto_diario;
    }

    public function setMontoDiario(?float $monto_diario): self
    {
        $this->monto_diario = $monto_diario;

        return $this;
    }

    public function getAcumulable(): ?float
    {
        return $this->acumulable;
    }

    public function setAcumulable(?float $acumulable): self
    {
        $this->acumulable = $acumulable;

        return $this;
    }

    public function getNoAcumulable(): ?float
    {
        return $this->no_acumulable;
    }

    public function setNoAcumulable(?float $no_acumulable): self
    {
        $this->no_acumulable = $no_acumulable;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
