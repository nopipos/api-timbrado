<?php

namespace App\Entity\Nomina;

use App\Repository\Nomina\NominaEmpleadosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NominaEmpleadosRepository::class)
 */
class NominaEmpleados
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Nomina::class, inversedBy="nominaEmpleados")
     */
    private $nomina;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $num_empleado;

    /**
     * @ORM\Column(type="string", length=13)
     */
    private $rfc;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $curp;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $regimen;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $seguro_social;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $departamento;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $clabe;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha_inicio_laboral;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $antiguedad;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $puesto;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $tipo_contrato;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $tipo_jornada;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sbca;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $riesgo_puesto;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $sdi;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $percepcion_total_gravado;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $percepcion_total_excento;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $deduccion_total_gravado;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $deduccion_total_excento;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $serie;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $folio;

    /**
     * @ORM\Column(type="integer")
     */
    private $timbrado;

    /**
     * @ORM\Column(type="integer")
     */
    private $num_dias_pagados;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_xml;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_percepciones;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_deducciones;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_otros_pagos;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $sindicalizado;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_sueldos;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_separacion_indemnizacion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_jubilacion_pension;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_otras_deducciones;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_impuestos_retenidos;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $tipo_nomina;

    /**
     * @ORM\Column(type="string", length=38, nullable=true)
     */
    private $uuid_relacionado;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $tipo_relacion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $folio_fiscal;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha_timbrado;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fecha_cancelacion;

    /**
     * @ORM\OneToOne(targetEntity=Empleados::class, cascade={"persist", "remove"})
     */
    private $empleado;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $tipo_regimen;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $banco;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomina(): ?Nomina
    {
        return $this->nomina;
    }

    public function setNomina(?Nomina $nomina): self
    {
        $this->nomina = $nomina;

        return $this;
    }

    public function getNumEmpleado(): ?string
    {
        return $this->num_empleado;
    }

    public function setNumEmpleado(?string $num_empleado): self
    {
        $this->num_empleado = $num_empleado;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getCurp(): ?string
    {
        return $this->curp;
    }

    public function setCurp(?string $curp): self
    {
        $this->curp = $curp;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRegimen(): ?string
    {
        return $this->regimen;
    }

    public function setRegimen(?string $regimen): self
    {
        $this->regimen = $regimen;

        return $this;
    }

    public function getSeguroSocial(): ?string
    {
        return $this->seguro_social;
    }

    public function setSeguroSocial(?string $seguro_social): self
    {
        $this->seguro_social = $seguro_social;

        return $this;
    }

    public function getDepartamento(): ?string
    {
        return $this->departamento;
    }

    public function setDepartamento(?string $departamento): self
    {
        $this->departamento = $departamento;

        return $this;
    }

    public function getClabe(): ?string
    {
        return $this->clabe;
    }

    public function setClabe(?string $clabe): self
    {
        $this->clabe = $clabe;

        return $this;
    }

    public function getFechaInicioLaboral(): ?\DateTimeInterface
    {
        return $this->fecha_inicio_laboral;
    }

    public function setFechaInicioLaboral(?\DateTimeInterface $fecha_inicio_laboral): self
    {
        $this->fecha_inicio_laboral = $fecha_inicio_laboral;

        return $this;
    }

    public function getAntiguedad(): ?string
    {
        return $this->antiguedad;
    }

    public function setAntiguedad(?string $antiguedad): self
    {
        $this->antiguedad = $antiguedad;

        return $this;
    }

    public function getPuesto(): ?string
    {
        return $this->puesto;
    }

    public function setPuesto(?string $puesto): self
    {
        $this->puesto = $puesto;

        return $this;
    }

    public function getTipoContrato(): ?string
    {
        return $this->tipo_contrato;
    }

    public function setTipoContrato(?string $tipo_contrato): self
    {
        $this->tipo_contrato = $tipo_contrato;

        return $this;
    }

    public function getTipoJornada(): ?string
    {
        return $this->tipo_jornada;
    }

    public function setTipoJornada(?string $tipo_jornada): self
    {
        $this->tipo_jornada = $tipo_jornada;

        return $this;
    }

    public function getSbca(): ?string
    {
        return $this->sbca;
    }

    public function setSbca(?string $sbca): self
    {
        $this->sbca = $sbca;

        return $this;
    }

    public function getRiesgoPuesto(): ?string
    {
        return $this->riesgo_puesto;
    }

    public function setRiesgoPuesto(?string $riesgo_puesto): self
    {
        $this->riesgo_puesto = $riesgo_puesto;

        return $this;
    }

    public function getSdi(): ?float
    {
        return $this->sdi;
    }

    public function setSdi(?float $sdi): self
    {
        $this->sdi = $sdi;

        return $this;
    }

    public function getPercepcionTotalGravado(): ?float
    {
        return $this->percepcion_total_gravado;
    }

    public function setPercepcionTotalGravado(?float $percepcion_total_gravado): self
    {
        $this->percepcion_total_gravado = $percepcion_total_gravado;

        return $this;
    }

    public function getPercepcionTotalExcento(): ?float
    {
        return $this->percepcion_total_excento;
    }

    public function setPercepcionTotalExcento(?float $percepcion_total_excento): self
    {
        $this->percepcion_total_excento = $percepcion_total_excento;

        return $this;
    }

    public function getDeduccionTotalGravado(): ?float
    {
        return $this->deduccion_total_gravado;
    }

    public function setDeduccionTotalGravado(?float $deduccion_total_gravado): self
    {
        $this->deduccion_total_gravado = $deduccion_total_gravado;

        return $this;
    }

    public function getDeduccionTotalExcento(): ?float
    {
        return $this->deduccion_total_excento;
    }

    public function setDeduccionTotalExcento(?float $deduccion_total_excento): self
    {
        $this->deduccion_total_excento = $deduccion_total_excento;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(?string $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getFolio(): ?string
    {
        return $this->folio;
    }

    public function setFolio(?string $folio): self
    {
        $this->folio = $folio;

        return $this;
    }

    public function getTimbrado(): ?int
    {
        return $this->timbrado;
    }

    public function setTimbrado(int $timbrado): self
    {
        $this->timbrado = $timbrado;

        return $this;
    }

    public function getNumDiasPagados(): ?int
    {
        return $this->num_dias_pagados;
    }

    public function setNumDiasPagados(int $num_dias_pagados): self
    {
        $this->num_dias_pagados = $num_dias_pagados;

        return $this;
    }

    public function getTotalXml(): ?float
    {
        return $this->total_xml;
    }

    public function setTotalXml(?float $total_xml): self
    {
        $this->total_xml = $total_xml;

        return $this;
    }

    public function getTotalPercepciones(): ?float
    {
        return $this->total_percepciones;
    }

    public function setTotalPercepciones(?float $total_percepciones): self
    {
        $this->total_percepciones = $total_percepciones;

        return $this;
    }

    public function getTotalDeducciones(): ?float
    {
        return $this->total_deducciones;
    }

    public function setTotalDeducciones(?float $total_deducciones): self
    {
        $this->total_deducciones = $total_deducciones;

        return $this;
    }

    public function getTotalOtrosPagos(): ?float
    {
        return $this->total_otros_pagos;
    }

    public function setTotalOtrosPagos(?float $total_otros_pagos): self
    {
        $this->total_otros_pagos = $total_otros_pagos;

        return $this;
    }

    public function getSindicalizado(): ?string
    {
        return $this->sindicalizado;
    }

    public function setSindicalizado(string $sindicalizado): self
    {
        $this->sindicalizado = $sindicalizado;

        return $this;
    }

    public function getTotalSueldos(): ?float
    {
        return $this->total_sueldos;
    }

    public function setTotalSueldos(?float $total_sueldos): self
    {
        $this->total_sueldos = $total_sueldos;

        return $this;
    }

    public function getTotalSeparacionIndemnizacion(): ?float
    {
        return $this->total_separacion_indemnizacion;
    }

    public function setTotalSeparacionIndemnizacion(?float $total_separacion_indemnizacion): self
    {
        $this->total_separacion_indemnizacion = $total_separacion_indemnizacion;

        return $this;
    }

    public function getTotalJubilacionPension(): ?float
    {
        return $this->total_jubilacion_pension;
    }

    public function setTotalJubilacionPension(?float $total_jubilacion_pension): self
    {
        $this->total_jubilacion_pension = $total_jubilacion_pension;

        return $this;
    }

    public function getTotalOtrasDeducciones(): ?float
    {
        return $this->total_otras_deducciones;
    }

    public function setTotalOtrasDeducciones(?float $total_otras_deducciones): self
    {
        $this->total_otras_deducciones = $total_otras_deducciones;

        return $this;
    }

    public function getTotalImpuestosRetenidos(): ?float
    {
        return $this->total_impuestos_retenidos;
    }

    public function setTotalImpuestosRetenidos(?float $total_impuestos_retenidos): self
    {
        $this->total_impuestos_retenidos = $total_impuestos_retenidos;

        return $this;
    }

    public function getTipoNomina(): ?string
    {
        return $this->tipo_nomina;
    }

    public function setTipoNomina(?string $tipo_nomina): self
    {
        $this->tipo_nomina = $tipo_nomina;

        return $this;
    }

    public function getUuidRelacionado(): ?string
    {
        return $this->uuid_relacionado;
    }

    public function setUuidRelacionado(?string $uuid_relacionado): self
    {
        $this->uuid_relacionado = $uuid_relacionado;

        return $this;
    }

    public function getTipoRelacion(): ?string
    {
        return $this->tipo_relacion;
    }

    public function setTipoRelacion(?string $tipo_relacion): self
    {
        $this->tipo_relacion = $tipo_relacion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getFolioFiscal(): ?string
    {
        return $this->folio_fiscal;
    }

    public function setFolioFiscal(?string $folio_fiscal): self
    {
        $this->folio_fiscal = $folio_fiscal;

        return $this;
    }

    public function getFechaTimbrado(): ?\DateTimeInterface
    {
        return $this->fecha_timbrado;
    }

    public function setFechaTimbrado(?\DateTimeInterface $fecha_timbrado): self
    {
        $this->fecha_timbrado = $fecha_timbrado;

        return $this;
    }

    public function getFechaCancelacion(): ?\DateTimeInterface
    {
        return $this->fecha_cancelacion;
    }

    public function setFechaCancelacion(?\DateTimeInterface $fecha_cancelacion): self
    {
        $this->fecha_cancelacion = $fecha_cancelacion;

        return $this;
    }

    public function getEmpleado(): ?Empleados
    {
        return $this->empleado;
    }

    public function setEmpleado(?Empleados $empleado): self
    {
        $this->empleado = $empleado;

        return $this;
    }

    public function getTipoRegimen(): ?string
    {
        return $this->tipo_regimen;
    }

    public function setTipoRegimen(?string $tipo_regimen): self
    {
        $this->tipo_regimen = $tipo_regimen;

        return $this;
    }

    public function getBanco(): ?string
    {
        return $this->banco;
    }

    public function setBanco(string $banco): self
    {
        $this->banco = $banco;

        return $this;
    }
}
