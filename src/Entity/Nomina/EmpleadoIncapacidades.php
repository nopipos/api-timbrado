<?php

namespace App\Entity\Nomina;

use App\Entity\Empresas;
use App\Repository\Nomina\EmpleadoIncapacidadesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpleadoIncapacidadesRepository::class)
 */
class EmpleadoIncapacidades
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empleadoIncapacidades")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Nomina::class, inversedBy="empleadoIncapacidades")
     */
    private $nomina;

    /**
     * @ORM\ManyToOne(targetEntity=Empleados::class, inversedBy="empleadoIncapacidades")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empleado;

    /**
     * @ORM\Column(type="float")
     */
    private $dias_incapacidad;

    /**
     * @ORM\Column(type="float")
     */
    private $descuento;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $tipo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getNomina(): ?Nomina
    {
        return $this->nomina;
    }

    public function setNomina(?Nomina $nomina): self
    {
        $this->nomina = $nomina;

        return $this;
    }

    public function getEmpleado(): ?Empleados
    {
        return $this->empleado;
    }

    public function setEmpleado(?Empleados $empleado): self
    {
        $this->empleado = $empleado;

        return $this;
    }

    public function getDiasIncapacidad(): ?float
    {
        return $this->dias_incapacidad;
    }

    public function setDiasIncapacidad(float $dias_incapacidad): self
    {
        $this->dias_incapacidad = $dias_incapacidad;

        return $this;
    }

    public function getDescuento(): ?float
    {
        return $this->descuento;
    }

    public function setDescuento(float $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }
}
