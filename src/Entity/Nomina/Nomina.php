<?php

namespace App\Entity\Nomina;

use App\Entity\Empresas;
use App\Entity\Sucursales;
use App\Repository\Nomina\NominaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NominaRepository::class)
 */
class Nomina
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="nominas")
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $registro_patronal;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha_pago;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha_inicial_pago;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha_final_pago;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $periocidad_pago;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="integer")
     */
    private $timbrado;

    /**
     * @ORM\Column(type="float")
     */
    private $subtotal;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $moneda;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $metodo_pago;

    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     */
    private $rfc_patron_origen;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $origen_recurso;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $monto_recursos_propios;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $tipo_nomina;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $num_dias_pagados;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $periodo;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=NominaEmpleados::class, mappedBy="nomina")
     */
    private $nominaEmpleados;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoPercepciones::class, mappedBy="nomina")
     */
    private $empleadoPercepciones;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoDeducciones::class, mappedBy="nomina")
     */
    private $empleadoDeducciones;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoIncapacidades::class, mappedBy="nomina")
     */
    private $empleadoIncapacidades;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoHorasExtra::class, mappedBy="nomina")
     */
    private $empleadoHorasExtras;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoJubilacion::class, mappedBy="nomina")
     */
    private $empleadoJubilacions;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoSeparacion::class, mappedBy="nomina")
     */
    private $empleadoSeparacions;

    /**
     * @ORM\ManyToOne(targetEntity=Sucursales::class, inversedBy="nominas")
     */
    private $sucursal;

    /**
     * @ORM\OneToMany(targetEntity=EmpleadoOtrosPagos::class, mappedBy="nomina")
     */
    private $empleadoOtrosPagos;

    public function __construct()
    {
        $this->nominaEmpleados = new ArrayCollection();
        $this->empleadoPercepciones = new ArrayCollection();
        $this->empleadoDeducciones = new ArrayCollection();
        $this->empleadoIncapacidades = new ArrayCollection();
        $this->empleadoHorasExtras = new ArrayCollection();
        $this->empleadoJubilacions = new ArrayCollection();
        $this->empleadoSeparacions = new ArrayCollection();
        $this->empleadoOtrosPagos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getRegistroPatronal(): ?string
    {
        return $this->registro_patronal;
    }

    public function setRegistroPatronal(string $registro_patronal): self
    {
        $this->registro_patronal = $registro_patronal;

        return $this;
    }

    public function getFechaPago(): ?\DateTimeInterface
    {
        return $this->fecha_pago;
    }

    public function setFechaPago(\DateTimeInterface $fecha_pago): self
    {
        $this->fecha_pago = $fecha_pago;

        return $this;
    }

    public function getFechaInicialPago(): ?\DateTimeInterface
    {
        return $this->fecha_inicial_pago;
    }

    public function setFechaInicialPago(\DateTimeInterface $fecha_inicial_pago): self
    {
        $this->fecha_inicial_pago = $fecha_inicial_pago;

        return $this;
    }

    public function getFechaFinalPago(): ?\DateTimeInterface
    {
        return $this->fecha_final_pago;
    }

    public function setFechaFinalPago(\DateTimeInterface $fecha_final_pago): self
    {
        $this->fecha_final_pago = $fecha_final_pago;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getPeriocidadPago(): ?string
    {
        return $this->periocidad_pago;
    }

    public function setPeriocidadPago(?string $periocidad_pago): self
    {
        $this->periocidad_pago = $periocidad_pago;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getTimbrado(): ?int
    {
        return $this->timbrado;
    }

    public function setTimbrado(int $timbrado): self
    {
        $this->timbrado = $timbrado;

        return $this;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(float $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getMetodoPago(): ?string
    {
        return $this->metodo_pago;
    }

    public function setMetodoPago(string $metodo_pago): self
    {
        $this->metodo_pago = $metodo_pago;

        return $this;
    }

    public function getRfcPatronOrigen(): ?string
    {
        return $this->rfc_patron_origen;
    }

    public function setRfcPatronOrigen(?string $rfc_patron_origen): self
    {
        $this->rfc_patron_origen = $rfc_patron_origen;

        return $this;
    }

    public function getOrigenRecurso(): ?string
    {
        return $this->origen_recurso;
    }

    public function setOrigenRecurso(?string $origen_recurso): self
    {
        $this->origen_recurso = $origen_recurso;

        return $this;
    }

    public function getMontoRecursosPropios(): ?float
    {
        return $this->monto_recursos_propios;
    }

    public function setMontoRecursosPropios(?float $monto_recursos_propios): self
    {
        $this->monto_recursos_propios = $monto_recursos_propios;

        return $this;
    }

    public function getTipoNomina(): ?string
    {
        return $this->tipo_nomina;
    }

    public function setTipoNomina(string $tipo_nomina): self
    {
        $this->tipo_nomina = $tipo_nomina;

        return $this;
    }

    public function getNumDiasPagados(): ?string
    {
        return $this->num_dias_pagados;
    }

    public function setNumDiasPagados(?string $num_dias_pagados): self
    {
        $this->num_dias_pagados = $num_dias_pagados;

        return $this;
    }

    public function getPeriodo(): ?string
    {
        return $this->periodo;
    }

    public function setPeriodo(?string $periodo): self
    {
        $this->periodo = $periodo;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|NominaEmpleados[]
     */
    public function getNominaEmpleados(): Collection
    {
        return $this->nominaEmpleados;
    }

    public function addNominaEmpleado(NominaEmpleados $nominaEmpleado): self
    {
        if (!$this->nominaEmpleados->contains($nominaEmpleado)) {
            $this->nominaEmpleados[] = $nominaEmpleado;
            $nominaEmpleado->setNomina($this);
        }

        return $this;
    }

    public function removeNominaEmpleado(NominaEmpleados $nominaEmpleado): self
    {
        if ($this->nominaEmpleados->removeElement($nominaEmpleado)) {
            // set the owning side to null (unless already changed)
            if ($nominaEmpleado->getNomina() === $this) {
                $nominaEmpleado->setNomina(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoPercepciones[]
     */
    public function getEmpleadoPercepciones(): Collection
    {
        return $this->empleadoPercepciones;
    }

    public function addEmpleadoPercepcione(EmpleadoPercepciones $empleadoPercepcione): self
    {
        if (!$this->empleadoPercepciones->contains($empleadoPercepcione)) {
            $this->empleadoPercepciones[] = $empleadoPercepcione;
            $empleadoPercepcione->setNomina($this);
        }

        return $this;
    }

    public function removeEmpleadoPercepcione(EmpleadoPercepciones $empleadoPercepcione): self
    {
        if ($this->empleadoPercepciones->removeElement($empleadoPercepcione)) {
            // set the owning side to null (unless already changed)
            if ($empleadoPercepcione->getNomina() === $this) {
                $empleadoPercepcione->setNomina(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoDeducciones[]
     */
    public function getEmpleadoDeducciones(): Collection
    {
        return $this->empleadoDeducciones;
    }

    public function addEmpleadoDeduccione(EmpleadoDeducciones $empleadoDeduccione): self
    {
        if (!$this->empleadoDeducciones->contains($empleadoDeduccione)) {
            $this->empleadoDeducciones[] = $empleadoDeduccione;
            $empleadoDeduccione->setNomina($this);
        }

        return $this;
    }

    public function removeEmpleadoDeduccione(EmpleadoDeducciones $empleadoDeduccione): self
    {
        if ($this->empleadoDeducciones->removeElement($empleadoDeduccione)) {
            // set the owning side to null (unless already changed)
            if ($empleadoDeduccione->getNomina() === $this) {
                $empleadoDeduccione->setNomina(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoIncapacidades[]
     */
    public function getEmpleadoIncapacidades(): Collection
    {
        return $this->empleadoIncapacidades;
    }

    public function addEmpleadoIncapacidade(EmpleadoIncapacidades $empleadoIncapacidade): self
    {
        if (!$this->empleadoIncapacidades->contains($empleadoIncapacidade)) {
            $this->empleadoIncapacidades[] = $empleadoIncapacidade;
            $empleadoIncapacidade->setNomina($this);
        }

        return $this;
    }

    public function removeEmpleadoIncapacidade(EmpleadoIncapacidades $empleadoIncapacidade): self
    {
        if ($this->empleadoIncapacidades->removeElement($empleadoIncapacidade)) {
            // set the owning side to null (unless already changed)
            if ($empleadoIncapacidade->getNomina() === $this) {
                $empleadoIncapacidade->setNomina(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoHorasExtra[]
     */
    public function getEmpleadoHorasExtras(): Collection
    {
        return $this->empleadoHorasExtras;
    }

    public function addEmpleadoHorasExtra(EmpleadoHorasExtra $empleadoHorasExtra): self
    {
        if (!$this->empleadoHorasExtras->contains($empleadoHorasExtra)) {
            $this->empleadoHorasExtras[] = $empleadoHorasExtra;
            $empleadoHorasExtra->setNomina($this);
        }

        return $this;
    }

    public function removeEmpleadoHorasExtra(EmpleadoHorasExtra $empleadoHorasExtra): self
    {
        if ($this->empleadoHorasExtras->removeElement($empleadoHorasExtra)) {
            // set the owning side to null (unless already changed)
            if ($empleadoHorasExtra->getNomina() === $this) {
                $empleadoHorasExtra->setNomina(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoJubilacion[]
     */
    public function getEmpleadoJubilacions(): Collection
    {
        return $this->empleadoJubilacions;
    }

    public function addEmpleadoJubilacion(EmpleadoJubilacion $empleadoJubilacion): self
    {
        if (!$this->empleadoJubilacions->contains($empleadoJubilacion)) {
            $this->empleadoJubilacions[] = $empleadoJubilacion;
            $empleadoJubilacion->setNomina($this);
        }

        return $this;
    }

    public function removeEmpleadoJubilacion(EmpleadoJubilacion $empleadoJubilacion): self
    {
        if ($this->empleadoJubilacions->removeElement($empleadoJubilacion)) {
            // set the owning side to null (unless already changed)
            if ($empleadoJubilacion->getNomina() === $this) {
                $empleadoJubilacion->setNomina(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmpleadoSeparacion[]
     */
    public function getEmpleadoSeparacions(): Collection
    {
        return $this->empleadoSeparacions;
    }

    public function addEmpleadoSeparacion(EmpleadoSeparacion $empleadoSeparacion): self
    {
        if (!$this->empleadoSeparacions->contains($empleadoSeparacion)) {
            $this->empleadoSeparacions[] = $empleadoSeparacion;
            $empleadoSeparacion->setNomina($this);
        }

        return $this;
    }

    public function removeEmpleadoSeparacion(EmpleadoSeparacion $empleadoSeparacion): self
    {
        if ($this->empleadoSeparacions->removeElement($empleadoSeparacion)) {
            // set the owning side to null (unless already changed)
            if ($empleadoSeparacion->getNomina() === $this) {
                $empleadoSeparacion->setNomina(null);
            }
        }

        return $this;
    }

    public function getSucursal(): ?Sucursales
    {
        return $this->sucursal;
    }

    public function setSucursal(?Sucursales $sucursal): self
    {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * @return Collection|EmpleadoOtrosPagos[]
     */
    public function getEmpleadoOtrosPagos(): Collection
    {
        return $this->empleadoOtrosPagos;
    }

    public function addEmpleadoOtrosPago(EmpleadoOtrosPagos $empleadoOtrosPago): self
    {
        if (!$this->empleadoOtrosPagos->contains($empleadoOtrosPago)) {
            $this->empleadoOtrosPagos[] = $empleadoOtrosPago;
            $empleadoOtrosPago->setNomina($this);
        }

        return $this;
    }

    public function removeEmpleadoOtrosPago(EmpleadoOtrosPagos $empleadoOtrosPago): self
    {
        if ($this->empleadoOtrosPagos->removeElement($empleadoOtrosPago)) {
            // set the owning side to null (unless already changed)
            if ($empleadoOtrosPago->getNomina() === $this) {
                $empleadoOtrosPago->setNomina(null);
            }
        }

        return $this;
    }
}
