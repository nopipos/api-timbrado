<?php


namespace App\Entity\response;


class MensajeTimbrado
{
    /**
     * @var string
     */
    public $codigo;

    /**
     * @var string
     */
    public $resultado;

    /**
     * @var string
     */
    public $xmlData;

    /**
     * @var string
     */
    public $mensaje;

    /**
     * @var string
     */
    public $pdf;

    /**
     * @var string
     */
    public $uuid;

    /**
     * @var string
     */
    public $fechaTimbrado;

    /**
     * @var string
     */
    public $rfcProvCertif;

    /**
     * @var string
     */
    public $selloCFD;

    /**
     * @var string
     */
    public $noCertificadoSAT;

    /**
     * @var string
     */
    public $selloSAT;

    /**
     * @var string
     */
    public $path;

    /**
     * @return string
     */
    public function getCodigo(): string
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     */
    public function setCodigo(string $codigo): void
    {
        $this->codigo = $codigo;
    }

    /**
     * @return string
     */
    public function getResultado(): string
    {
        return $this->resultado;
    }

    /**
     * @param string $resultado
     */
    public function setResultado(string $resultado): void
    {
        $this->resultado = $resultado;
    }

    /**
     * @return string
     */
    public function getXmlData(): string
    {
        return $this->xmlData;
    }

    /**
     * @param string $xmlData
     */
    public function setXmlData(string $xmlData): void
    {
        $this->xmlData = $xmlData;
    }

    /**
     * @return string
     */
    public function getMensaje(): string
    {
        return $this->mensaje;
    }

    /**
     * @param string $mensaje
     */
    public function setMensaje(string $mensaje): void
    {
        $this->mensaje = $mensaje;
    }

    /**
     * @return string
     */
    public function getPdf(): string
    {
        return $this->pdf;
    }

    /**
     * @param string $pdf
     */
    public function setPdf(string $pdf): void
    {
        $this->pdf = $pdf;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getFechaTimbrado(): string
    {
        return $this->fechaTimbrado;
    }

    /**
     * @param string $fechaTimbrado
     */
    public function setFechaTimbrado(string $fechaTimbrado): void
    {
        $this->fechaTimbrado = $fechaTimbrado;
    }

    /**
     * @return string
     */
    public function getRfcProvCertif(): string
    {
        return $this->rfcProvCertif;
    }

    /**
     * @param string $rfcProvCertif
     */
    public function setRfcProvCertif(string $rfcProvCertif): void
    {
        $this->rfcProvCertif = $rfcProvCertif;
    }

    /**
     * @return string
     */
    public function getSelloCFD(): string
    {
        return $this->selloCFD;
    }

    /**
     * @param string $selloCFD
     */
    public function setSelloCFD(string $selloCFD): void
    {
        $this->selloCFD = $selloCFD;
    }

    /**
     * @return string
     */
    public function getNoCertificadoSAT(): string
    {
        return $this->noCertificadoSAT;
    }

    /**
     * @param string $noCertificadoSAT
     */
    public function setNoCertificadoSAT(string $noCertificadoSAT): void
    {
        $this->noCertificadoSAT = $noCertificadoSAT;
    }

    /**
     * @return string
     */
    public function getSelloSAT(): string
    {
        return $this->selloSAT;
    }

    /**
     * @param string $selloSAT
     */
    public function setSelloSAT(string $selloSAT): void
    {
        $this->selloSAT = $selloSAT;
    }



    /**
     * Get the value of path
     *
     * @return  string
     */ 
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set the value of path
     *
     * @param  string  $path
     *
     * @return  self
     */ 
    public function setPath(string $path)
    {
        $this->path = $path;

        return $this;
    }
}