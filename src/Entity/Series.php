<?php

namespace App\Entity;

use App\Repository\SeriesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SeriesRepository::class)
 * @ORM\Table(name="series",indexes={@ORM\Index(name="name_serie", columns={"serie"})})
 */
class Series
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="series")
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $serie;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $folio;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $prefijo;

    /**
     * @ORM\Column(type="integer" , options={"unsigned"=true,"default" = 1})
     */
    private $estatus;

    /**
     * @ORM\ManyToOne(targetEntity=Sucursales::class, inversedBy="series")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sucursal;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $tipoDocumento;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(string $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getFolio(): ?string
    {
        return $this->folio;
    }

    public function setFolio(string $folio): self
    {
        $this->folio = $folio;

        return $this;
    }

    public function getPrefijo(): ?string
    {
        return $this->prefijo;
    }

    public function setPrefijo(?string $prefijo): self
    {
        $this->prefijo = $prefijo;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getSucursal(): ?Sucursales
    {
        return $this->sucursal;
    }

    public function setSucursal(?Sucursales $sucursal): self
    {
        $this->sucursal = $sucursal;

        return $this;
    }

    public function getTipoDocumento(): ?string
    {
        return $this->tipoDocumento;
    }

    public function setTipoDocumento(string $tipoDocumento): self
    {
        $this->tipoDocumento = $tipoDocumento;

        return $this;
    }
}
