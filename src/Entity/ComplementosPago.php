<?php

namespace App\Entity;

use App\Repository\ComplementosPagoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComplementosPagoRepository::class)
 * @ORM\Table (name="complementos_pago")
 */
class ComplementosPago
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="pagos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $factura;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="pagos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empresa;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha_pago;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $forma_pago;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $moneda;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tipo_cambio;

    /**
     * @ORM\Column(type="float")
     */
    private $monto;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $num_operacion;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $rfc_emisor_cta_ord;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $nom_banco_ord;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $cta_ordenante;

    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     */
    private $rfc_emisor_cta_ben;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $cta_beneficiario;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $tipo_cad_pago;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cert_pago;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cad_pago;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $sello_pago;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true,"default" = 1})
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=ComplementosPagoDocumento::class, mappedBy="pago")
     */
    private $documentos;

    public function __construct()
    {
        $this->documentos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFechaPago(): ?\DateTimeInterface
    {
        return $this->fecha_pago;
    }

    public function setFechaPago(\DateTimeInterface $fecha_pago): self
    {
        $this->fecha_pago = $fecha_pago;

        return $this;
    }

    public function getFormaPago(): ?string
    {
        return $this->forma_pago;
    }

    public function setFormaPago(string $forma_pago): self
    {
        $this->forma_pago = $forma_pago;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getTipoCambio(): ?float
    {
        return $this->tipo_cambio;
    }

    public function setTipoCambio(?float $tipo_cambio): self
    {
        $this->tipo_cambio = $tipo_cambio;

        return $this;
    }

    public function getMonto(): ?float
    {
        return $this->monto;
    }

    public function setMonto(float $monto): self
    {
        $this->monto = $monto;

        return $this;
    }

    public function getNumOperacion(): ?string
    {
        return $this->num_operacion;
    }

    public function setNumOperacion(string $num_operacion): self
    {
        $this->num_operacion = $num_operacion;

        return $this;
    }

    public function getRfcEmisorCtaOrd(): ?string
    {
        return $this->rfc_emisor_cta_ord;
    }

    public function setRfcEmisorCtaOrd(?string $rfc_emisor_cta_ord): self
    {
        $this->rfc_emisor_cta_ord = $rfc_emisor_cta_ord;

        return $this;
    }

    public function getNomBancoOrd(): ?string
    {
        return $this->nom_banco_ord;
    }

    public function setNomBancoOrd(?string $nom_banco_ord): self
    {
        $this->nom_banco_ord = $nom_banco_ord;

        return $this;
    }

    public function getCtaOrdenante(): ?string
    {
        return $this->cta_ordenante;
    }

    public function setCtaOrdenante(?string $cta_ordenante): self
    {
        $this->cta_ordenante = $cta_ordenante;

        return $this;
    }

    public function getRfcEmisorCtaBen(): ?string
    {
        return $this->rfc_emisor_cta_ben;
    }

    public function setRfcEmisorCtaBen(?string $rfc_emisor_cta_ben): self
    {
        $this->rfc_emisor_cta_ben = $rfc_emisor_cta_ben;

        return $this;
    }

    public function getCtaBeneficiario(): ?string
    {
        return $this->cta_beneficiario;
    }

    public function setCtaBeneficiario(?string $cta_beneficiario): self
    {
        $this->cta_beneficiario = $cta_beneficiario;

        return $this;
    }

    public function getTipoCadPago(): ?string
    {
        return $this->tipo_cad_pago;
    }

    public function setTipoCadPago(string $tipo_cad_pago): self
    {
        $this->tipo_cad_pago = $tipo_cad_pago;

        return $this;
    }

    public function getCertPago(): ?string
    {
        return $this->cert_pago;
    }

    public function setCertPago(?string $cert_pago): self
    {
        $this->cert_pago = $cert_pago;

        return $this;
    }

    public function getCadPago(): ?string
    {
        return $this->cad_pago;
    }

    public function setCadPago(?string $cad_pago): self
    {
        $this->cad_pago = $cad_pago;

        return $this;
    }

    public function getSelloPago(): ?string
    {
        return $this->sello_pago;
    }

    public function setSelloPago(?string $sello_pago): self
    {
        $this->sello_pago = $sello_pago;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|ComplementosPagoDocumento[]
     */
    public function getDocumentos(): Collection
    {
        return $this->documentos;
    }

    public function addDocumento(ComplementosPagoDocumento $documento): self
    {
        if (!$this->documentos->contains($documento)) {
            $this->documentos[] = $documento;
            $documento->setPago($this);
        }

        return $this;
    }

    public function removeDocumento(ComplementosPagoDocumento $documento): self
    {
        if ($this->documentos->removeElement($documento)) {
            // set the owning side to null (unless already changed)
            if ($documento->getPago() === $this) {
                $documento->setPago(null);
            }
        }

        return $this;
    }
}
