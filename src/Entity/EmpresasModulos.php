<?php

namespace App\Entity;

use App\Repository\EmpresasModulosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmpresasModulosRepository::class)
 * @ORM\Table (name="empresas_modulos")
 */
class EmpresasModulos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="modulos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Modulos::class, inversedBy="modulo")
     * @ORM\JoinColumn(nullable=false)
     */
    private $modulo;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getModulo(): ?Modulos
    {
        return $this->modulo;
    }

    public function setModulo(?Modulos $modulo): self
    {
        $this->modulo = $modulo;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
