<?php

namespace App\Entity;

use App\Repository\FacturasCfdiRelacionadosUuidRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturasCfdiRelacionadosUuidRepository::class)
 */
class FacturasCfdiRelacionadosUuid
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=FacturasCfdiRelacionados::class, inversedBy="facturasCfdiRelacionadosUuids")
     */
    private $tipo_relacion;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $uuid;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipoRelacion(): ?FacturasCfdiRelacionados
    {
        return $this->tipo_relacion;
    }

    public function setTipoRelacion(?FacturasCfdiRelacionados $tipo_relacion): self
    {
        $this->tipo_relacion = $tipo_relacion;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
