<?php

namespace App\Entity\SAT;

use App\Repository\SAT\CatFormaPagoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatFormaPagoRepository::class)
 */
class CatFormaPago
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $CFormaPago;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $Bancarizado;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $NumOperacion;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $RFCEmisorCuentaORD;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $CuentaOrdenante;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $PatronCuentaOrdenante;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $RFCEmisorCuentaBEN;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $CuentaBeneficiaria;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $PatronCuentaBeneficiaria;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $TipoCadenaPago;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $BancoEmisorExtranjero;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCFormaPago(): ?string
    {
        return $this->CFormaPago;
    }

    public function setCFormaPago(string $CFormaPago): self
    {
        $this->CFormaPago = $CFormaPago;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getBancarizado(): ?string
    {
        return $this->Bancarizado;
    }

    public function setBancarizado(?string $Bancarizado): self
    {
        $this->Bancarizado = $Bancarizado;

        return $this;
    }

    public function getNumOperacion(): ?string
    {
        return $this->NumOperacion;
    }

    public function setNumOperacion(?string $NumOperacion): self
    {
        $this->NumOperacion = $NumOperacion;

        return $this;
    }

    public function getRFCEmisorCuentaORD(): ?string
    {
        return $this->RFCEmisorCuentaORD;
    }

    public function setRFCEmisorCuentaORD(?string $RFCEmisorCuentaORD): self
    {
        $this->RFCEmisorCuentaORD = $RFCEmisorCuentaORD;

        return $this;
    }

    public function getCuentaOrdenante(): ?string
    {
        return $this->CuentaOrdenante;
    }

    public function setCuentaOrdenante(?string $CuentaOrdenante): self
    {
        $this->CuentaOrdenante = $CuentaOrdenante;

        return $this;
    }

    public function getPatronCuentaOrdenante(): ?string
    {
        return $this->PatronCuentaOrdenante;
    }

    public function setPatronCuentaOrdenante(?string $PatronCuentaOrdenante): self
    {
        $this->PatronCuentaOrdenante = $PatronCuentaOrdenante;

        return $this;
    }

    public function getRFCEmisorCuentaBEN(): ?string
    {
        return $this->RFCEmisorCuentaBEN;
    }

    public function setRFCEmisorCuentaBEN(?string $RFCEmisorCuentaBEN): self
    {
        $this->RFCEmisorCuentaBEN = $RFCEmisorCuentaBEN;

        return $this;
    }

    public function getCuentaBeneficiaria(): ?string
    {
        return $this->CuentaBeneficiaria;
    }

    public function setCuentaBeneficiaria(?string $CuentaBeneficiaria): self
    {
        $this->CuentaBeneficiaria = $CuentaBeneficiaria;

        return $this;
    }

    public function getPatronCuentaBeneficiaria(): ?string
    {
        return $this->PatronCuentaBeneficiaria;
    }

    public function setPatronCuentaBeneficiaria(?string $PatronCuentaBeneficiaria): self
    {
        $this->PatronCuentaBeneficiaria = $PatronCuentaBeneficiaria;

        return $this;
    }

    public function getTipoCadenaPago(): ?string
    {
        return $this->TipoCadenaPago;
    }

    public function setTipoCadenaPago(?string $TipoCadenaPago): self
    {
        $this->TipoCadenaPago = $TipoCadenaPago;

        return $this;
    }

    public function getBancoEmisorExtranjero(): ?string
    {
        return $this->BancoEmisorExtranjero;
    }

    public function setBancoEmisorExtranjero(?string $BancoEmisorExtranjero): self
    {
        $this->BancoEmisorExtranjero = $BancoEmisorExtranjero;

        return $this;
    }
    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'CFormaPago'=>$this->getCFormaPago(),
            'descripcion'=>$this->getDescripcion(),
            'bancarizado'=>$this->getBancarizado(),
            'bancoEmisorExtranjero'=>$this->getBancoEmisorExtranjero(),
            'CuentaBeneficiaria'=>$this->getCuentaBeneficiaria(),
            'descripcion_singular'=>$this->getCuentaOrdenante(),
            'porcentaje_variacion'=>$this->getNumOperacion(),
        ];

    }
}
