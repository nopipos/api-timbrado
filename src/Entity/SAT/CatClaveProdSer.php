<?php

namespace App\Entity\SAT;

use App\Repository\SAT\CatClaveProdSerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatClaveProdSerRepository::class)
 */
class CatClaveProdSer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $CClaveProdSer;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $iva;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $ieps;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $complemento;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCClaveProdSer(): ?string
    {
        return $this->CClaveProdSer;
    }

    public function setCClaveProdSer(string $CClaveProdSer): self
    {
        $this->CClaveProdSer = $CClaveProdSer;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getIva(): ?string
    {
        return $this->iva;
    }

    public function setIva(string $iva): self
    {
        $this->iva = $iva;

        return $this;
    }

    public function getIeps(): ?string
    {
        return $this->ieps;
    }

    public function setIeps(string $ieps): self
    {
        $this->ieps = $ieps;

        return $this;
    }

    public function getComplemento(): ?string
    {
        return $this->complemento;
    }

    public function setComplemento(?string $complemento): self
    {
        $this->complemento = $complemento;

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'clave'=>$this->getCClaveProdSer(),
            'descripcion'=>$this->getDescripcion(),
            'iva'=>$this->getIva(),
            'ieps'=>$this->getIeps(),
            'complemento'=>$this->getComplemento(),

        ];

    }
}
