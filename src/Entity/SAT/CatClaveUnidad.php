<?php

namespace App\Entity\SAT;

use App\Repository\SAT\CatClaveUnidadRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatClaveUnidadRepository::class)
 */
class CatClaveUnidad
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $CClaveUnidad;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $descripcion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCClaveUnidad(): ?string
    {
        return $this->CClaveUnidad;
    }

    public function setCClaveUnidad(string $CClaveUnidad): self
    {
        $this->CClaveUnidad = $CClaveUnidad;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'clave'=>$this->getCClaveUnidad(),
            'descripcion'=>$this->getDescripcion()

        ];

    }
}
