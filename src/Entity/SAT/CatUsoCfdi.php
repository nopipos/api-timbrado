<?php

namespace App\Entity\SAT;

use App\Repository\SAT\CatUsoCfdiRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatUsoCfdiRepository::class)
 */
class CatUsoCfdi
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $CUsoCfdi;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $descripcion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCUsoCfdi(): ?string
    {
        return $this->CUsoCfdi;
    }

    public function setCUsoCfdi(string $CUsoCfdi): self
    {
        $this->CUsoCfdi = $CUsoCfdi;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'clave'=>$this->getCUsoCfdi(),
            'descripcion'=>$this->getDescripcion()

        ];

    }
}
