<?php

namespace App\Entity\SAT;

use App\Repository\SAT\CatMetodoPagoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatMetodoPagoRepository::class)
 */
class CatMetodoPago
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $CMetodoPago;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $descripcion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCMetodoPago(): ?string
    {
        return $this->CMetodoPago;
    }

    public function setCMetodoPago(string $CMetodoPago): self
    {
        $this->CMetodoPago = $CMetodoPago;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'CMetodoPago'=>$this->getCMetodoPago(),
            'descripcion'=>$this->getDescripcion()
        ];

    }
}
