<?php

namespace App\Entity\SAT;

use App\Repository\SAT\CatMonedaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatMonedaRepository::class)
 */
class CatMoneda
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $CMoneda;

    /**
     * @ORM\Column(type="string", length=90)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $decimales;

    /**
     * @ORM\Column(type="integer")
     */
    private $porcVariacion;


    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $descripcionSingular;

    /**
     * @ORM\Column(type="string", length=105, nullable=true)
     */
    private $descripcionPlural;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCMoneda(): ?string
    {
        return $this->CMoneda;
    }

    public function setCMoneda(string $CMoneda): self
    {
        $this->CMoneda = $CMoneda;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getDecimales(): ?int
    {
        return $this->decimales;
    }

    public function setDecimales(int $decimales): self
    {
        $this->decimales = $decimales;

        return $this;
    }

    public function getDescripcionSingular(): ?string
    {
        return $this->descripcionSingular;
    }

    public function setDescripcionSingular(?string $descripcionSingular): self
    {
        $this->descripcionSingular = $descripcionSingular;

        return $this;
    }

    public function getDescripcionPlural(): ?string
    {
        return $this->descripcionPlural;
    }

    public function setDescripcionPlural(?string $descripcionPlural): self
    {
        $this->descripcionPlural = $descripcionPlural;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPorcVariacion()
    {
        return $this->porcVariacion;
    }

    /**
     * @param mixed $porcVariacion
     */
    public function setPorcVariacion(int $porcVariacion): self
    {
        $this->porcVariacion = $porcVariacion;
    }
    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'CMoneda'=>$this->getCMoneda(),
            'descripcion'=>$this->getDescripcion(),
            'decimales'=>$this->getDecimales(),
            'descripcion_plural'=>$this->getDescripcionPlural(),
            'descripcion_singular'=>$this->getDescripcionSingular(),
            'porcentaje_variacion'=>$this->getPorcVariacion(),
        ];

    }

}
