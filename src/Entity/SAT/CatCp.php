<?php

namespace App\Entity\SAT;

use App\Repository\SAT\CatCpRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatCpRepository::class)
 */
class CatCp
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $codigoPostal;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $estado;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $municipio;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $localidad;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $aplicaIva8;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigoPostal(): ?string
    {
        return $this->codigoPostal;
    }

    public function setCodigoPostal(string $codigoPostal): self
    {
        $this->codigoPostal = $codigoPostal;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    public function setMunicipio(string $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(?string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getAplicaIva8(): ?string
    {
        return $this->aplicaIva8;
    }

    public function setAplicaIva8(?string $aplicaIva8): self
    {
        $this->aplicaIva8 = $aplicaIva8;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'CP'=>$this->getCodigoPostal(),
            'estado' => $this->getEstado()
        ];

    }
}
