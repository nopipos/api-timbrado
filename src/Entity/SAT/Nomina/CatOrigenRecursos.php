<?php

namespace App\Entity\SAT\Nomina;

use App\Repository\SAT\Nomina\CatOrigenRecursosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatOrigenRecursosRepository::class)
 */
class CatOrigenRecursos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $c_OrigenRecurso;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCOrigenRecurso(): ?string
    {
        return $this->c_OrigenRecurso;
    }

    public function setCOrigenRecurso(string $c_OrigenRecurso): self
    {
        $this->c_OrigenRecurso = $c_OrigenRecurso;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
