<?php

namespace App\Entity\SAT\Nomina;

use App\Repository\SAT\Nomina\CatEstadoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatEstadoRepository::class)
 */
class CatEstado
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $C_estado;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $c_pais;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $nombreEstado;

    /**
     * @ORM\Column(type="integer")
     */
    private $esatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCEstado(): ?string
    {
        return $this->C_estado;
    }

    public function setCEstado(string $C_estado): self
    {
        $this->C_estado = $C_estado;

        return $this;
    }

    public function getCPais(): ?string
    {
        return $this->c_pais;
    }

    public function setCPais(string $c_pais): self
    {
        $this->c_pais = $c_pais;

        return $this;
    }

    public function getNombreEstado(): ?string
    {
        return $this->nombreEstado;
    }

    public function setNombreEstado(string $nombreEstado): self
    {
        $this->nombreEstado = $nombreEstado;

        return $this;
    }

    public function getEsatus(): ?int
    {
        return $this->esatus;
    }

    public function setEsatus(int $esatus): self
    {
        $this->esatus = $esatus;

        return $this;
    }

    public function getAttributes(){

        return [
            'id'=>$this->getId(),
            'cEstado'=>$this->getCEstado(),
            'descripcion'=>$this->getNombreEstado(),
            'cpais' => $this->getCPais()
        ];

    }
}
