<?php

namespace App\Entity\SAT\Nomina;

use App\Repository\SAT\Nomina\CatTipoHorasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatTipoHorasRepository::class)
 */
class CatTipoHoras
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $c_TipoHoras;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCTipoHoras(): ?string
    {
        return $this->c_TipoHoras;
    }

    public function setCTipoHoras(string $c_TipoHoras): self
    {
        $this->c_TipoHoras = $c_TipoHoras;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
