<?php

namespace App\Entity\SAT\Nomina;

use App\Repository\SAT\Nomina\CatBancoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatBancoRepository::class)
 */
class CatBanco
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $c_Banco;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCBanco(): ?string
    {
        return $this->c_Banco;
    }

    public function setCBanco(string $c_Banco): self
    {
        $this->c_Banco = $c_Banco;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(?string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getAttributes(){

        return [
            'c_Banco'=>$this->getCBanco(),
            'nombre'=>$this->getNombre(),
            'Descripcion'=>$this->getDescripcion(),

        ];

    }
}
