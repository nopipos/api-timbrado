<?php

namespace App\Entity\SAT\Nomina;

use App\Repository\SAT\Nomina\CatPeriocidadPagoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatPeriocidadPagoRepository::class)
 */
class CatPeriocidadPago
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $c_PeriodicidadPago;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCPeriodicidadPago(): ?string
    {
        return $this->c_PeriodicidadPago;
    }

    public function setCPeriodicidadPago(string $c_PeriodicidadPago): self
    {
        $this->c_PeriodicidadPago = $c_PeriodicidadPago;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
