<?php

namespace App\Entity\SAT\Nomina;

use App\Repository\SAT\Nomina\CatTipoOtroPagoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatTipoOtroPagoRepository::class)
 */
class CatTipoOtroPago
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $c_TipoOtroPago;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCTipoOtroPago(): ?string
    {
        return $this->c_TipoOtroPago;
    }

    public function setCTipoOtroPago(string $c_TipoOtroPago): self
    {
        $this->c_TipoOtroPago = $c_TipoOtroPago;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
