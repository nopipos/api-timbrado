<?php

namespace App\Entity\SAT\Nomina;

use App\Repository\SAT\Nomina\CatRiesgoPuestoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatRiesgoPuestoRepository::class)
 */
class CatRiesgoPuesto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $c_RiesgoPuesto;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCRiesgoPuesto(): ?string
    {
        return $this->c_RiesgoPuesto;
    }

    public function setCRiesgoPuesto(string $c_RiesgoPuesto): self
    {
        $this->c_RiesgoPuesto = $c_RiesgoPuesto;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getAttributes(){

        return [
            'c_riesgo_puesto'=>$this->getCRiesgoPuesto(),
            'descripcion'=>$this->getDescripcion(),
        ];
    }
}
