<?php

namespace App\Entity\SAT\Nomina;

use App\Repository\SAT\Nomina\CatTipoPercepcionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatTipoPercepcionRepository::class)
 */
class CatTipoPercepcion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $c_TipoPercepcion;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCTipoPercepcion(): ?string
    {
        return $this->c_TipoPercepcion;
    }

    public function setCTipoPercepcion(string $c_TipoPercepcion): self
    {
        $this->c_TipoPercepcion = $c_TipoPercepcion;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
