<?php

namespace App\Entity\SAT\Nomina;

use App\Repository\SAT\Nomina\CatTipoJornadaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatTipoJornadaRepository::class)
 */
class CatTipoJornada
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $c_TipoJornada;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCTipoJornada(): ?string
    {
        return $this->c_TipoJornada;
    }

    public function setCTipoJornada(string $c_TipoJornada): self
    {
        $this->c_TipoJornada = $c_TipoJornada;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getAttributes(){

        return [
            'c_tipo_jornada'=>$this->getCTipoJornada(),
            'descripcion'=>$this->getDescripcion(),

        ];
    }
}
