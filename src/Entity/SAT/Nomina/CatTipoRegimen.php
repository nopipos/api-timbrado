<?php

namespace App\Entity\SAT\Nomina;

use App\Repository\SAT\Nomina\CatTipoRegimenRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatTipoRegimenRepository::class)
 */
class CatTipoRegimen
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $c_TipoRegimen;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCTipoRegimen(): ?string
    {
        return $this->c_TipoRegimen;
    }

    public function setCTipoRegimen(string $c_TipoRegimen): self
    {
        $this->c_TipoRegimen = $c_TipoRegimen;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getAttributes(){

        return [
            'c_tipo_regimen'=>$this->getCTipoRegimen(),
            'descripcion'=>$this->getDescripcion(),

        ];
    }
}
