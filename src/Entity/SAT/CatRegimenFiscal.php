<?php

namespace App\Entity\SAT;

use App\Repository\SAT\CatRegimenFiscalRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatRegimenFiscalRepository::class)
 */
class CatRegimenFiscal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $CRegimenFiscal;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $Fisica;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $Moral;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCRegimenFiscal(): ?string
    {
        return $this->CRegimenFiscal;
    }

    public function setCRegimenFiscal(string $CRegimenFiscal): self
    {
        $this->CRegimenFiscal = $CRegimenFiscal;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getFisica(): ?string
    {
        return $this->Fisica;
    }

    public function setFisica(string $Fisica): self
    {
        $this->Fisica = $Fisica;

        return $this;
    }

    public function getMoral(): ?string
    {
        return $this->Moral;
    }

    public function setMoral(string $Moral): self
    {
        $this->Moral = $Moral;

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'RegimenFiscal'=>$this->getCRegimenFiscal(),
            'Descripcion'=>$this->getDescripcion(),
            'Fisica'=>$this->getFisica(),
            'Moral'=>$this->getMoral(),
        ];

    }
}
