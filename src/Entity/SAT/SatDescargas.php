<?php

namespace App\Entity\SAT;

use App\Entity\Empresas;
use App\Repository\SAT\SatDescargasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SatDescargasRepository::class)
 */
class SatDescargas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empresa")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empresa;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaInicio;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaFin;

    /**
     * @ORM\Column(type="integer")
     */
    private $tipo;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $solicitud;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $codigoSolicitud;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $estadoSolicitud;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $paquete;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numeroCfdi;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFechaInicio(): ?\DateTimeInterface
    {
        return $this->fechaInicio;
    }

    public function setFechaInicio(\DateTimeInterface $fechaInicio): self
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    public function getFechaFin(): ?\DateTimeInterface
    {
        return $this->fechaFin;
    }

    public function setFechaFin(\DateTimeInterface $fechaFin): self
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    public function getTipo(): ?int
    {
        return $this->tipo;
    }

    public function setTipo(int $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getSolicitud(): ?string
    {
        return $this->solicitud;
    }

    public function setSolicitud(?string $solicitud): self
    {
        $this->solicitud = $solicitud;

        return $this;
    }

    public function getCodigoSolicitud(): ?string
    {
        return $this->codigoSolicitud;
    }

    public function setCodigoSolicitud(?string $codigoSolicitud): self
    {
        $this->codigoSolicitud = $codigoSolicitud;

        return $this;
    }

    public function getEstadoSolicitud(): ?string
    {
        return $this->estadoSolicitud;
    }

    public function setEstadoSolicitud(?string $estadoSolicitud): self
    {
        $this->estadoSolicitud = $estadoSolicitud;

        return $this;
    }

    public function getPaquete(): ?string
    {
        return $this->paquete;
    }

    public function setPaquete(?string $paquete): self
    {
        $this->paquete = $paquete;

        return $this;
    }

    public function getNumeroCfdi(): ?int
    {
        return $this->numeroCfdi;
    }

    public function setNumeroCfdi(?int $numeroCfdi): self
    {
        $this->numeroCfdi = $numeroCfdi;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
