<?php

namespace App\Entity;

use App\Repository\FacturasSatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturasSatRepository::class)
 */
class FacturasSat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="facturasSats")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="facturasSats")
     */
    private $factura;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $estado_cancelacion;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $estadoSat;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getEstadoCancelacion(): ?string
    {
        return $this->estado_cancelacion;
    }

    public function setEstadoCancelacion(?string $estado_cancelacion): self
    {
        $this->estado_cancelacion = $estado_cancelacion;

        return $this;
    }

    public function getEstadoSat(): ?string
    {
        return $this->estadoSat;
    }

    public function setEstadoSat(?string $estadoSat): self
    {
        $this->estadoSat = $estadoSat;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
