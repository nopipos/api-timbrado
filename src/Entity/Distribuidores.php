<?php

namespace App\Entity;

use App\Repository\DistribuidoresRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DistribuidoresRepository::class)
 */
class Distribuidores
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=DistribuidorTipos::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $distribuidor_tipo;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $razon_social;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $rfc;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $cuenta;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $bank;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $clabe_stp;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function __construct()
    {
        $this->estatus = 1;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(?string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRazonSocial(): ?string
    {
        return $this->razon_social;
    }

    public function setRazonSocial(?string $razon_social): self
    {
        $this->razon_social = $razon_social;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(?string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(?string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getCuenta(): ?string
    {
        return $this->cuenta;
    }

    public function setCuenta(?string $cuenta): self
    {
        $this->cuenta = $cuenta;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getDistribuidorTipo(): ?DistribuidorTipos
    {
        return $this->distribuidor_tipo;
    }

    public function setDistribuidorTipo(?DistribuidorTipos $distribuidor_tipo): self
    {
        $this->distribuidor_tipo = $distribuidor_tipo;

        return $this;
    }

    public function getBank(): ?string
    {
        return $this->bank;
    }

    public function setBank(?string $bank): self
    {
        $this->bank = $bank;

        return $this;
    }

    public function getClabeStp(): ?string
    {
        return $this->clabe_stp;
    }

    public function setClabeStp(?string $clabe_stp): self
    {
        $this->clabe_stp = $clabe_stp;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }



}
