<?php

namespace App\Entity;

use App\Repository\FacturasInfoBlobalRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturasInfoBlobalRepository::class)
 */
class FacturasInfoBlobal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="facturasInfoBlobals")
     */
    private $factura;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="facturasInfoBlobals")
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $periodicidad;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $meses;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $anio;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getPeriodicidad(): ?string
    {
        return $this->periodicidad;
    }

    public function setPeriodicidad(string $periodicidad): self
    {
        $this->periodicidad = $periodicidad;

        return $this;
    }

    public function getMeses(): ?string
    {
        return $this->meses;
    }

    public function setMeses(string $meses): self
    {
        $this->meses = $meses;

        return $this;
    }

    public function getAnio(): ?string
    {
        return $this->anio;
    }

    public function setAnio(string $anio): self
    {
        $this->anio = $anio;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
