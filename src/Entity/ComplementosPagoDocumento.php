<?php

namespace App\Entity;

use App\Repository\ComplementosPagoDocumentoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComplementosPagoDocumentoRepository::class)
 * @ORM\Table (name="complementos_pago_documento")
 */
class ComplementosPagoDocumento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=ComplementosPago::class, inversedBy="documentos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pago;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="documentos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $factura;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="documentos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=38)
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $serie;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $folio;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $moneda;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tipo_cambio;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $metodo_pago;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned"=true})
     */
    private $num_parcialidad;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $imp_saldo_ant;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $imp_pagado;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $imp_saldo_ins;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $total_fac;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $serie_nc;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $folio_nc;

    /**
     * @ORM\Column(type="integer" , options={"unsigned"=true,"default" = 1})
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPago(): ?ComplementosPago
    {
        return $this->pago;
    }

    public function setPago(?ComplementosPago $pago): self
    {
        $this->pago = $pago;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(?string $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getFolio(): ?string
    {
        return $this->folio;
    }

    public function setFolio(?string $folio): self
    {
        $this->folio = $folio;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getTipoCambio(): ?float
    {
        return $this->tipo_cambio;
    }

    public function setTipoCambio(?float $tipo_cambio): self
    {
        $this->tipo_cambio = $tipo_cambio;

        return $this;
    }

    public function getMetodoPago(): ?string
    {
        return $this->metodo_pago;
    }

    public function setMetodoPago(string $metodo_pago): self
    {
        $this->metodo_pago = $metodo_pago;

        return $this;
    }

    public function getNumParcialidad(): ?int
    {
        return $this->num_parcialidad;
    }

    public function setNumParcialidad(?int $num_parcialidad): self
    {
        $this->num_parcialidad = $num_parcialidad;

        return $this;
    }

    public function getImpSaldoAnt(): ?float
    {
        return $this->imp_saldo_ant;
    }

    public function setImpSaldoAnt(?float $imp_saldo_ant): self
    {
        $this->imp_saldo_ant = $imp_saldo_ant;

        return $this;
    }

    public function getImpPagado(): ?float
    {
        return $this->imp_pagado;
    }

    public function setImpPagado(?float $imp_pagado): self
    {
        $this->imp_pagado = $imp_pagado;

        return $this;
    }

    public function getImpSaldoIns(): ?float
    {
        return $this->imp_saldo_ins;
    }

    public function setImpSaldoIns(?float $imp_saldo_ins): self
    {
        $this->imp_saldo_ins = $imp_saldo_ins;

        return $this;
    }

    public function getTotalFac(): ?float
    {
        return $this->total_fac;
    }

    public function setTotalFac(?float $total_fac): self
    {
        $this->total_fac = $total_fac;

        return $this;
    }

    public function getSerieNc(): ?string
    {
        return $this->serie_nc;
    }

    public function setSerieNc(?string $serie_nc): self
    {
        $this->serie_nc = $serie_nc;

        return $this;
    }

    public function getFolioNc(): ?string
    {
        return $this->folio_nc;
    }

    public function setFolioNc(?string $folio_nc): self
    {
        $this->folio_nc = $folio_nc;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
