<?php

namespace App\Entity;

use App\Repository\ClientesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientesRepository::class)
 * @ORM\Table(name="clientes",indexes={@ORM\Index(name="name_em1", columns={"rfc"})})

 */
class Clientes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="clientes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $no_proveedor;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=13)
     */
        private $rfc;

    /**
     * @ORM\Column(type="string", length=19, nullable=true)
     */
    private $curp;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $num_reg;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $uso_cfdi;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $metodo_pago;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $condiciones_pago;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $forma_pago;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $dias_credito;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true,"default" = 1})
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=ClienteDireccion::class, mappedBy="cliente")
     */
    private $direcciones;

    /**
     * @ORM\OneToMany(targetEntity=ClientesMail::class, mappedBy="cliente")
     */
    private $mails;

    /**
     * @ORM\OneToMany(targetEntity=Facturas::class, mappedBy="cliente")
     */
    private $clientes;

    /**
     * @ORM\OneToMany(targetEntity=FActurasCliente::class, mappedBy="cliente")
     */
    private $cliente;

    public function __construct()
    {
        $this->direcciones = new ArrayCollection();
        $this->mails = new ArrayCollection();
        $this->clientes = new ArrayCollection();
        $this->cliente = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(?string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getNoProveedor(): ?string
    {
        return $this->no_proveedor;
    }

    public function setNoProveedor(?string $no_proveedor): self
    {
        $this->no_proveedor = $no_proveedor;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getCurp(): ?string
    {
        return $this->curp;
    }

    public function setCurp(?string $curp): self
    {
        $this->curp = $curp;

        return $this;
    }

    public function getNumReg(): ?string
    {
        return $this->num_reg;
    }

    public function setNumReg(?string $num_reg): self
    {
        $this->num_reg = $num_reg;

        return $this;
    }

    public function getUsoCfdi(): ?string
    {
        return $this->uso_cfdi;
    }

    public function setUsoCfdi(?string $uso_cfdi): self
    {
        $this->uso_cfdi = $uso_cfdi;

        return $this;
    }

    public function getMetodoPago(): ?string
    {
        return $this->metodo_pago;
    }

    public function setMetodoPago(?string $metodo_pago): self
    {
        $this->metodo_pago = $metodo_pago;

        return $this;
    }

    public function getCondicionesPago(): ?string
    {
        return $this->condiciones_pago;
    }

    public function setCondicionesPago(?string $condiciones_pago): self
    {
        $this->condiciones_pago = $condiciones_pago;

        return $this;
    }

    public function getFormaPago(): ?string
    {
        return $this->forma_pago;
    }

    public function setFormaPago(?string $forma_pago): self
    {
        $this->forma_pago = $forma_pago;

        return $this;
    }

    public function getDiasCredito(): ?string
    {
        return $this->dias_credito;
    }

    public function setDiasCredito(?string $dias_credito): self
    {
        $this->dias_credito = $dias_credito;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection|ClienteDireccion[]
     */
    public function getDirecciones(): Collection
    {
        return $this->direcciones;
    }

    public function addDireccione(ClienteDireccion $direccione): self
    {
        if (!$this->direcciones->contains($direccione)) {
            $this->direcciones[] = $direccione;
            $direccione->setCliente($this);
        }

        return $this;
    }

    public function removeDireccione(ClienteDireccion $direccione): self
    {
        if ($this->direcciones->removeElement($direccione)) {
            // set the owning side to null (unless already changed)
            if ($direccione->getCliente() === $this) {
                $direccione->setCliente(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClientesMail[]
     */
    public function getMails(): Collection
    {
        return $this->mails;
    }

    public function addMail(ClientesMail $mail): self
    {
        if (!$this->mails->contains($mail)) {
            $this->mails[] = $mail;
            $mail->setCliente($this);
        }

        return $this;
    }

    public function removeMail(ClientesMail $mail): self
    {
        if ($this->mails->removeElement($mail)) {
            // set the owning side to null (unless already changed)
            if ($mail->getCliente() === $this) {
                $mail->setCliente(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Facturas[]
     */
    public function getClientes(): Collection
    {
        return $this->clientes;
    }

    public function addCliente(Facturas $cliente): self
    {
        if (!$this->clientes->contains($cliente)) {
            $this->clientes[] = $cliente;
            $cliente->setCliente($this);
        }

        return $this;
    }

    public function removeCliente(Facturas $cliente): self
    {
        if ($this->clientes->removeElement($cliente)) {
            // set the owning side to null (unless already changed)
            if ($cliente->getCliente() === $this) {
                $cliente->setCliente(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FActurasCliente[]
     */
    public function getCliente(): Collection
    {
        return $this->cliente;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'noProveedor'=>$this->getNoProveedor(),
            'rfc'=>$this->getRfc(),
            'nombre'=>$this->getNombre(),
            'usoCfdi'=>$this->getUsoCfdi(),
            'condicionesPago'=>$this->getCondicionesPago(),
            'curp'=>$this->getCurp(),
            'diasCredito'=>$this->getDiasCredito(),
            'formaPago'=>$this->getFormaPago(),
            'metodoPago'=>$this->getMetodoPago(),
            'numReg'=>$this->getNumReg(),
        ];

    }
}
