<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CP\cpRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpRepository::class)
 */
class cp
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cps")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="cps")
     */
    private $factura;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $TranspInternac;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $EntradaSalidaMerc;

    /**
     * @ORM\Column(type="string", length=6, nullable=true)
     */
    private $PaisOrigenDestino;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $ViaEntradaSalida;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $TotalDistRec;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=cpUbicacion::class, mappedBy="cp")
     */
    private $cpUbicacions;

    /**
     * @ORM\OneToMany(targetEntity=cpFigura::class, mappedBy="cp")
     */
    private $cpFiguras;

    /**
     * @ORM\OneToMany(targetEntity=cpFiguraParte::class, mappedBy="cp")
     */
    private $parte;

    /**
     * @ORM\OneToMany(targetEntity=cpMercancia::class, mappedBy="cp")
     */
    private $cpMercancias;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalle::class, mappedBy="cp")
     */
    private $cpMercanciaDetalles;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetallePedi::class, mappedBy="cp")
     */
    private $cpMercanciaDetallePedis;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalleGuia::class, mappedBy="cp")
     */
    private $cpMercanciaDetalleGuias;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalleCantidad::class, mappedBy="cp")
     */
    private $mercancia;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaAutoFed::class, mappedBy="cp")
     */
    private $cpMercanciaAutoFeds;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaAutoFedRemo::class, mappedBy="cp")
     */
    private $cpMercanciaAutoFedRemos;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalles::class, mappedBy="cp")
     */
    private $cpMercanciaDetallesdet;

    public function __construct()
    {
        $this->cpUbicacions = new ArrayCollection();
        $this->cpFiguras = new ArrayCollection();
        $this->parte = new ArrayCollection();
        $this->cpMercancias = new ArrayCollection();
        $this->cpMercanciaDetalles = new ArrayCollection();
        $this->cpMercanciaDetallePedis = new ArrayCollection();
        $this->cpMercanciaDetalleGuias = new ArrayCollection();
        $this->mercancia = new ArrayCollection();
        $this->cpMercanciaAutoFeds = new ArrayCollection();
        $this->cpMercanciaAutoFedRemos = new ArrayCollection();
        $this->cpMercanciaDetallesdet = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getTranspInternac(): ?string
    {
        return $this->TranspInternac;
    }

    public function setTranspInternac(string $TranspInternac): self
    {
        $this->TranspInternac = $TranspInternac;

        return $this;
    }

    public function getEntradaSalidaMerc(): ?string
    {
        return $this->EntradaSalidaMerc;
    }

    public function setEntradaSalidaMerc(?string $EntradaSalidaMerc): self
    {
        $this->EntradaSalidaMerc = $EntradaSalidaMerc;

        return $this;
    }

    public function getPaisOrigenDestino(): ?string
    {
        return $this->PaisOrigenDestino;
    }

    public function setPaisOrigenDestino(?string $PaisOrigenDestino): self
    {
        $this->PaisOrigenDestino = $PaisOrigenDestino;

        return $this;
    }

    public function getViaEntradaSalida(): ?string
    {
        return $this->ViaEntradaSalida;
    }

    public function setViaEntradaSalida(?string $ViaEntradaSalida): self
    {
        $this->ViaEntradaSalida = $ViaEntradaSalida;

        return $this;
    }

    public function getTotalDistRec(): ?float
    {
        return $this->TotalDistRec;
    }

    public function setTotalDistRec(?float $TotalDistRec): self
    {
        $this->TotalDistRec = $TotalDistRec;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection<int, cpUbicacion>
     */
    public function getCpUbicacions(): Collection
    {
        return $this->cpUbicacions;
    }

    public function addCpUbicacion(cpUbicacion $cpUbicacion): self
    {
        if (!$this->cpUbicacions->contains($cpUbicacion)) {
            $this->cpUbicacions[] = $cpUbicacion;
            $cpUbicacion->setCp($this);
        }

        return $this;
    }

    public function removeCpUbicacion(cpUbicacion $cpUbicacion): self
    {
        if ($this->cpUbicacions->removeElement($cpUbicacion)) {
            // set the owning side to null (unless already changed)
            if ($cpUbicacion->getCp() === $this) {
                $cpUbicacion->setCp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpFigura>
     */
    public function getCpFiguras(): Collection
    {
        return $this->cpFiguras;
    }

    public function addCpFigura(cpFigura $cpFigura): self
    {
        if (!$this->cpFiguras->contains($cpFigura)) {
            $this->cpFiguras[] = $cpFigura;
            $cpFigura->setCp($this);
        }

        return $this;
    }

    public function removeCpFigura(cpFigura $cpFigura): self
    {
        if ($this->cpFiguras->removeElement($cpFigura)) {
            // set the owning side to null (unless already changed)
            if ($cpFigura->getCp() === $this) {
                $cpFigura->setCp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpFiguraParte>
     */
    public function getParte(): Collection
    {
        return $this->parte;
    }

    public function addParte(cpFiguraParte $parte): self
    {
        if (!$this->parte->contains($parte)) {
            $this->parte[] = $parte;
            $parte->setCp($this);
        }

        return $this;
    }

    public function removeParte(cpFiguraParte $parte): self
    {
        if ($this->parte->removeElement($parte)) {
            // set the owning side to null (unless already changed)
            if ($parte->getCp() === $this) {
                $parte->setCp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercancia>
     */
    public function getCpMercancias(): Collection
    {
        return $this->cpMercancias;
    }

    public function addCpMercancia(cpMercancia $cpMercancia): self
    {
        if (!$this->cpMercancias->contains($cpMercancia)) {
            $this->cpMercancias[] = $cpMercancia;
            $cpMercancia->setCp($this);
        }

        return $this;
    }

    public function removeCpMercancia(cpMercancia $cpMercancia): self
    {
        if ($this->cpMercancias->removeElement($cpMercancia)) {
            // set the owning side to null (unless already changed)
            if ($cpMercancia->getCp() === $this) {
                $cpMercancia->setCp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalle>
     */
    public function getCpMercanciaDetalles(): Collection
    {
        return $this->cpMercanciaDetalles;
    }

    public function addCpMercanciaDetalle(cpMercanciaDetalle $cpMercanciaDetalle): self
    {
        if (!$this->cpMercanciaDetalles->contains($cpMercanciaDetalle)) {
            $this->cpMercanciaDetalles[] = $cpMercanciaDetalle;
            $cpMercanciaDetalle->setCp($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetalle(cpMercanciaDetalle $cpMercanciaDetalle): self
    {
        if ($this->cpMercanciaDetalles->removeElement($cpMercanciaDetalle)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetalle->getCp() === $this) {
                $cpMercanciaDetalle->setCp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetallePedi>
     */
    public function getCpMercanciaDetallePedis(): Collection
    {
        return $this->cpMercanciaDetallePedis;
    }

    public function addCpMercanciaDetallePedi(cpMercanciaDetallePedi $cpMercanciaDetallePedi): self
    {
        if (!$this->cpMercanciaDetallePedis->contains($cpMercanciaDetallePedi)) {
            $this->cpMercanciaDetallePedis[] = $cpMercanciaDetallePedi;
            $cpMercanciaDetallePedi->setCp($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetallePedi(cpMercanciaDetallePedi $cpMercanciaDetallePedi): self
    {
        if ($this->cpMercanciaDetallePedis->removeElement($cpMercanciaDetallePedi)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetallePedi->getCp() === $this) {
                $cpMercanciaDetallePedi->setCp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalleGuia>
     */
    public function getCpMercanciaDetalleGuias(): Collection
    {
        return $this->cpMercanciaDetalleGuias;
    }

    public function addCpMercanciaDetalleGuia(cpMercanciaDetalleGuia $cpMercanciaDetalleGuia): self
    {
        if (!$this->cpMercanciaDetalleGuias->contains($cpMercanciaDetalleGuia)) {
            $this->cpMercanciaDetalleGuias[] = $cpMercanciaDetalleGuia;
            $cpMercanciaDetalleGuia->setMercancia($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetalleGuia(cpMercanciaDetalleGuia $cpMercanciaDetalleGuia): self
    {
        if ($this->cpMercanciaDetalleGuias->removeElement($cpMercanciaDetalleGuia)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetalleGuia->getMercancia() === $this) {
                $cpMercanciaDetalleGuia->setMercancia(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalleCantidad>
     */
    public function getMercancia(): Collection
    {
        return $this->mercancia;
    }

    public function addMercancium(cpMercanciaDetalleCantidad $mercancium): self
    {
        if (!$this->mercancia->contains($mercancium)) {
            $this->mercancia[] = $mercancium;
            $mercancium->setCp($this);
        }

        return $this;
    }

    public function removeMercancium(cpMercanciaDetalleCantidad $mercancium): self
    {
        if ($this->mercancia->removeElement($mercancium)) {
            // set the owning side to null (unless already changed)
            if ($mercancium->getCp() === $this) {
                $mercancium->setCp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaAutoFed>
     */
    public function getCpMercanciaAutoFeds(): Collection
    {
        return $this->cpMercanciaAutoFeds;
    }

    public function addCpMercanciaAutoFed(cpMercanciaAutoFed $cpMercanciaAutoFed): self
    {
        if (!$this->cpMercanciaAutoFeds->contains($cpMercanciaAutoFed)) {
            $this->cpMercanciaAutoFeds[] = $cpMercanciaAutoFed;
            $cpMercanciaAutoFed->setCp($this);
        }

        return $this;
    }

    public function removeCpMercanciaAutoFed(cpMercanciaAutoFed $cpMercanciaAutoFed): self
    {
        if ($this->cpMercanciaAutoFeds->removeElement($cpMercanciaAutoFed)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaAutoFed->getCp() === $this) {
                $cpMercanciaAutoFed->setCp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaAutoFedRemo>
     */
    public function getCpMercanciaAutoFedRemos(): Collection
    {
        return $this->cpMercanciaAutoFedRemos;
    }

    public function addCpMercanciaAutoFedRemo(cpMercanciaAutoFedRemo $cpMercanciaAutoFedRemo): self
    {
        if (!$this->cpMercanciaAutoFedRemos->contains($cpMercanciaAutoFedRemo)) {
            $this->cpMercanciaAutoFedRemos[] = $cpMercanciaAutoFedRemo;
            $cpMercanciaAutoFedRemo->setCp($this);
        }

        return $this;
    }

    public function removeCpMercanciaAutoFedRemo(cpMercanciaAutoFedRemo $cpMercanciaAutoFedRemo): self
    {
        if ($this->cpMercanciaAutoFedRemos->removeElement($cpMercanciaAutoFedRemo)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaAutoFedRemo->getCp() === $this) {
                $cpMercanciaAutoFedRemo->setCp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalles>
     */
    public function getCpMercanciaDetallesdet(): Collection
    {
        return $this->cpMercanciaDetallesdet;
    }

    public function addCpMercanciaDetallesdet(cpMercanciaDetalles $cpMercanciaDetallesdet): self
    {
        if (!$this->cpMercanciaDetallesdet->contains($cpMercanciaDetallesdet)) {
            $this->cpMercanciaDetallesdet[] = $cpMercanciaDetallesdet;
            $cpMercanciaDetallesdet->setCp($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetallesdet(cpMercanciaDetalles $cpMercanciaDetallesdet): self
    {
        if ($this->cpMercanciaDetallesdet->removeElement($cpMercanciaDetallesdet)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetallesdet->getCp() === $this) {
                $cpMercanciaDetallesdet->setCp(null);
            }
        }

        return $this;
    }

    public function getAttributes(){
        return [
            'id'=>$this->getId(),
            'TranspInternac'=>$this->getTranspInternac(),
            'EntradaSalidaMerc'=>$this->getEntradaSalidaMerc(),
            'PaisOrigenDestino'=>$this->getPaisOrigenDestino(),
            'ViaEntradaSalida'=>$this->getViaEntradaSalida(),
            'TotalDistRec'=>$this->getTotalDistRec(),
            'estatus'=>$this->getEstatus(),
            
        ];
    }
}
