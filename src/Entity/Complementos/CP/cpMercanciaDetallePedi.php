<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Repository\Complementos\CP\cpMercanciaDetallePediRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpMercanciaDetallePediRepository::class)
 */
class cpMercanciaDetallePedi
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpMercanciaDetallePedis")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=cp::class, inversedBy="cpMercanciaDetallePedis")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=cpMercanciaDetalle::class, inversedBy="cpMercanciaDetallesdet")
     */
    private $mercancia;


    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $pedimento;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?cp
    {
        return $this->cp;
    }

    public function setCp(?cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getMercancia(): ?cpMercanciaDetalle
    {
        return $this->mercancia;
    }

    public function setMercancia(?cpMercanciaDetalle $mercancia): self
    {
        $this->mercancia = $mercancia;

        return $this;
    }

    public function getPedimento(): ?string
    {
        return $this->pedimento;
    }

    public function setPedimento(?string $pedimento): self
    {
        $this->pedimento = $pedimento;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
