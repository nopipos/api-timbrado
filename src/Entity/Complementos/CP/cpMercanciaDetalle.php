<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CP\cpMercanciaDetalleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpMercanciaDetalleRepository::class)
 */
class cpMercanciaDetalle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpMercanciaDetalles")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=cp::class, inversedBy="cpMercanciaDetalles")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="cpMercanciaDetalles")
     */
    private $factura;

    /**
     * @ORM\ManyToOne(targetEntity=cpMercancia::class, inversedBy="cpMercanciaDetalles")
     */
    private $mercancia;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $BienesTransp;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $ClaveSTCC;

    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     */
    private $Descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $Cantidad;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $ClaveUnidad;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $Unidad;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $Dimensiones;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $MaterialPeligroso;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $CveMaterialPeligroso;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $Embalaje;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $DescripEmbalaje;

    /**
     * @ORM\Column(type="float")
     */
    private $PesoEnKg;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ValorMercancia;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $Moneda;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $FraccionArancelaria;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $UUIDComercioExt;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetallePedi::class, mappedBy="mercancia")
     */
    private $cpMercanciaDetallePedis;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalleGuia::class, mappedBy="mercancia")
     */
    private $cpMercanciaDetalleGuias;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalleCantidad::class, mappedBy="mercancia")
     */
    private $cpMercanciaDetalleCantidads;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalles::class, mappedBy="mercancia")
     */
    private $cpMercanciaDetalles;

    public function __construct()
    {
        $this->cpMercanciaDetallePedis = new ArrayCollection();
        $this->cpMercanciaDetalleGuias = new ArrayCollection();
        $this->cpMercanciaDetalleCantidads = new ArrayCollection();
        $this->cpMercanciaDetalles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?cp
    {
        return $this->cp;
    }

    public function setCp(?cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getMercancia(): ?cpMercancia
    {
        return $this->mercancia;
    }

    public function setMercancia(?cpMercancia $mercancia): self
    {
        $this->mercancia = $mercancia;

        return $this;
    }

    public function getBienesTransp(): ?string
    {
        return $this->BienesTransp;
    }

    public function setBienesTransp(?string $BienesTransp): self
    {
        $this->BienesTransp = $BienesTransp;

        return $this;
    }

    public function getClaveSTCC(): ?string
    {
        return $this->ClaveSTCC;
    }

    public function setClaveSTCC(?string $ClaveSTCC): self
    {
        $this->ClaveSTCC = $ClaveSTCC;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(?string $Descripcion): self
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getCantidad(): ?float
    {
        return $this->Cantidad;
    }

    public function setCantidad(?float $Cantidad): self
    {
        $this->Cantidad = $Cantidad;

        return $this;
    }

    public function getClaveUnidad(): ?string
    {
        return $this->ClaveUnidad;
    }

    public function setClaveUnidad(?string $ClaveUnidad): self
    {
        $this->ClaveUnidad = $ClaveUnidad;

        return $this;
    }

    public function getUnidad(): ?string
    {
        return $this->Unidad;
    }

    public function setUnidad(?string $Unidad): self
    {
        $this->Unidad = $Unidad;

        return $this;
    }

    public function getDimensiones(): ?string
    {
        return $this->Dimensiones;
    }

    public function setDimensiones(?string $Dimensiones): self
    {
        $this->Dimensiones = $Dimensiones;

        return $this;
    }

    public function getMaterialPeligroso(): ?string
    {
        return $this->MaterialPeligroso;
    }

    public function setMaterialPeligroso(?string $MaterialPeligroso): self
    {
        $this->MaterialPeligroso = $MaterialPeligroso;

        return $this;
    }

    public function getCveMaterialPeligroso(): ?string
    {
        return $this->CveMaterialPeligroso;
    }

    public function setCveMaterialPeligroso(?string $CveMaterialPeligroso): self
    {
        $this->CveMaterialPeligroso = $CveMaterialPeligroso;

        return $this;
    }

    public function getEmbalaje(): ?string
    {
        return $this->Embalaje;
    }

    public function setEmbalaje(?string $Embalaje): self
    {
        $this->Embalaje = $Embalaje;

        return $this;
    }

    public function getDescripEmbalaje(): ?string
    {
        return $this->DescripEmbalaje;
    }

    public function setDescripEmbalaje(?string $DescripEmbalaje): self
    {
        $this->DescripEmbalaje = $DescripEmbalaje;

        return $this;
    }

    public function getPesoEnKg(): ?float
    {
        return $this->PesoEnKg;
    }

    public function setPesoEnKg(float $PesoEnKg): self
    {
        $this->PesoEnKg = $PesoEnKg;

        return $this;
    }

    public function getValorMercancia(): ?float
    {
        return $this->ValorMercancia;
    }

    public function setValorMercancia(?float $ValorMercancia): self
    {
        $this->ValorMercancia = $ValorMercancia;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->Moneda;
    }

    public function setMoneda(?string $Moneda): self
    {
        $this->Moneda = $Moneda;

        return $this;
    }

    public function getFraccionArancelaria(): ?string
    {
        return $this->FraccionArancelaria;
    }

    public function setFraccionArancelaria(?string $FraccionArancelaria): self
    {
        $this->FraccionArancelaria = $FraccionArancelaria;

        return $this;
    }

    public function getUUIDComercioExt(): ?string
    {
        return $this->UUIDComercioExt;
    }

    public function setUUIDComercioExt(?string $UUIDComercioExt): self
    {
        $this->UUIDComercioExt = $UUIDComercioExt;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetallePedi>
     */
    public function getCpMercanciaDetallePedis(): Collection
    {
        return $this->cpMercanciaDetallePedis;
    }

    public function addCpMercanciaDetallePedi(cpMercanciaDetallePedi $cpMercanciaDetallePedi): self
    {
        if (!$this->cpMercanciaDetallePedis->contains($cpMercanciaDetallePedi)) {
            $this->cpMercanciaDetallePedis[] = $cpMercanciaDetallePedi;
            $cpMercanciaDetallePedi->setMercancia($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetallePedi(cpMercanciaDetallePedi $cpMercanciaDetallePedi): self
    {
        if ($this->cpMercanciaDetallePedis->removeElement($cpMercanciaDetallePedi)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetallePedi->getMercancia() === $this) {
                $cpMercanciaDetallePedi->setMercancia(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalleGuia>
     */
    public function getCpMercanciaDetalleGuias(): Collection
    {
        return $this->cpMercanciaDetalleGuias;
    }

    public function addCpMercanciaDetalleGuia(cpMercanciaDetalleGuia $cpMercanciaDetalleGuia): self
    {
        if (!$this->cpMercanciaDetalleGuias->contains($cpMercanciaDetalleGuia)) {
            $this->cpMercanciaDetalleGuias[] = $cpMercanciaDetalleGuia;
            $cpMercanciaDetalleGuia->setCp($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetalleGuia(cpMercanciaDetalleGuia $cpMercanciaDetalleGuia): self
    {
        if ($this->cpMercanciaDetalleGuias->removeElement($cpMercanciaDetalleGuia)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetalleGuia->getCp() === $this) {
                $cpMercanciaDetalleGuia->setCp(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalleCantidad>
     */
    public function getCpMercanciaDetalleCantidads(): Collection
    {
        return $this->cpMercanciaDetalleCantidads;
    }

    public function addCpMercanciaDetalleCantidad(cpMercanciaDetalleCantidad $cpMercanciaDetalleCantidad): self
    {
        if (!$this->cpMercanciaDetalleCantidads->contains($cpMercanciaDetalleCantidad)) {
            $this->cpMercanciaDetalleCantidads[] = $cpMercanciaDetalleCantidad;
            $cpMercanciaDetalleCantidad->setMercancia($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetalleCantidad(cpMercanciaDetalleCantidad $cpMercanciaDetalleCantidad): self
    {
        if ($this->cpMercanciaDetalleCantidads->removeElement($cpMercanciaDetalleCantidad)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetalleCantidad->getMercancia() === $this) {
                $cpMercanciaDetalleCantidad->setMercancia(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalles>
     */
    public function getcpMercanciaDetalles(): Collection
    {
        return $this->cpMercanciaDetalles;
    }

    public function addcpMercanciaDetalles(cpMercanciaDetalles $cpMercanciaDetalles): self
    {
        if (!$this->cpMercanciaDetalleCantidads->contains($cpMercanciaDetalles)) {
            $this->cpMercanciaDetalleCantidads[] = $cpMercanciaDetalles;
            $cpMercanciaDetalles->setMercancia($this);
        }

        return $this;
    }

    public function addCpMercanciaDetalle(cpMercanciaDetalles $cpMercanciaDetalle): self
    {
        if (!$this->cpMercanciaDetalles->contains($cpMercanciaDetalle)) {
            $this->cpMercanciaDetalles[] = $cpMercanciaDetalle;
            $cpMercanciaDetalle->setMercancia($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetalle(cpMercanciaDetalles $cpMercanciaDetalle): self
    {
        if ($this->cpMercanciaDetalles->removeElement($cpMercanciaDetalle)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetalle->getMercancia() === $this) {
                $cpMercanciaDetalle->setMercancia(null);
            }
        }

        return $this;
    }

}
