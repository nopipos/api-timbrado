<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Repository\Complementos\CP\cpMercanciaDetalleCantidadRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpMercanciaDetalleCantidadRepository::class)
 */
class cpMercanciaDetalleCantidad
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpMercanciaDetalleCantidads")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=cp::class, inversedBy="cpMercanciaDetallesdet")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=cpMercanciaDetalle::class, inversedBy="cpMercanciaDetallesdet")
     */
    private $mercancia;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $Cantidad;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $IDOrigen;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $IDDestino;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $CvesTransporte;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?cp
    {
        return $this->cp;
    }

    public function setCp(?cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getMercancia(): ?cpMercanciaDetalle
    {
        return $this->mercancia;
    }

    public function setMercancia(?cpMercanciaDetalle $mercancia): self
    {
        $this->mercancia = $mercancia;

        return $this;
    }

    public function getCantidad(): ?float
    {
        return $this->Cantidad;
    }

    public function setCantidad(?float $Cantidad): self
    {
        $this->Cantidad = $Cantidad;

        return $this;
    }

    public function getIDOrigen(): ?string
    {
        return $this->IDOrigen;
    }

    public function setIDOrigen(?string $IDOrigen): self
    {
        $this->IDOrigen = $IDOrigen;

        return $this;
    }

    public function getIDDestino(): ?string
    {
        return $this->IDDestino;
    }

    public function setIDDestino(?string $IDDestino): self
    {
        $this->IDDestino = $IDDestino;

        return $this;
    }

    public function getCvesTransporte(): ?string
    {
        return $this->CvesTransporte;
    }

    public function setCvesTransporte(?string $CvesTransporte): self
    {
        $this->CvesTransporte = $CvesTransporte;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
