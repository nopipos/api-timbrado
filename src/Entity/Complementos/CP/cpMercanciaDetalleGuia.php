<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Repository\Complementos\CP\cpMercanciaDetalleGuiaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpMercanciaDetalleGuiaRepository::class)
 */
class cpMercanciaDetalleGuia
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpMercanciaDetalleGuias")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=cp::class, inversedBy="cpMercanciaDetalleGuias")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=cpMercanciaDetalle::class, inversedBy="cpMercanciaDetalleGuias")
     */
    private $mercancia;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $NumeroGuiaIdentificacion;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $DescripGuiaIdentificacion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PesoGuiaIdentificacion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?cp
    {
        return $this->cp;
    }

    public function setCp(?cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getMercancia(): ?cpMercanciaDetalle
    {
        return $this->mercancia;
    }

    public function setMercancia(?cpMercanciaDetalle $mercancia): self
    {
        $this->mercancia = $mercancia;

        return $this;
    }

    public function getNumeroGuiaIdentificacion(): ?string
    {
        return $this->NumeroGuiaIdentificacion;
    }

    public function setNumeroGuiaIdentificacion(?string $NumeroGuiaIdentificacion): self
    {
        $this->NumeroGuiaIdentificacion = $NumeroGuiaIdentificacion;

        return $this;
    }

    public function getDescripGuiaIdentificacion(): ?string
    {
        return $this->DescripGuiaIdentificacion;
    }

    public function setDescripGuiaIdentificacion(?string $DescripGuiaIdentificacion): self
    {
        $this->DescripGuiaIdentificacion = $DescripGuiaIdentificacion;

        return $this;
    }

    public function getPesoGuiaIdentificacion(): ?float
    {
        return $this->PesoGuiaIdentificacion;
    }

    public function setPesoGuiaIdentificacion(?float $PesoGuiaIdentificacion): self
    {
        $this->PesoGuiaIdentificacion = $PesoGuiaIdentificacion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
