<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Repository\Complementos\CP\cpMercanciaDetallesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpMercanciaDetallesRepository::class)
 */
class cpMercanciaDetalles
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpMercanciaDetallesdet")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=cp::class, inversedBy="cpMercanciaDetallesdet")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=cpMercanciaDetalle::class, inversedBy="cpMercanciaDetallesdet")
     */
    private $mercancia;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $UnidadPeso;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PesoBruto;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PesoNeto;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PesoTara;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NumPiezas;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?cp
    {
        return $this->cp;
    }

    public function setCp(?cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getMercancia(): ?cpMercanciaDetalle
    {
        return $this->mercancia;
    }

    public function setMercancia(?cpMercanciaDetalle $mercancia): self
    {
        $this->mercancia = $mercancia;

        return $this;
    }

    public function getUnidadPeso(): ?string
    {
        return $this->UnidadPeso;
    }

    public function setUnidadPeso(?string $UnidadPeso): self
    {
        $this->UnidadPeso = $UnidadPeso;

        return $this;
    }

    public function getPesoBruto(): ?float
    {
        return $this->PesoBruto;
    }

    public function setPesoBruto(?float $PesoBruto): self
    {
        $this->PesoBruto = $PesoBruto;

        return $this;
    }

    public function getPesoNeto(): ?float
    {
        return $this->PesoNeto;
    }

    public function setPesoNeto(?float $PesoNeto): self
    {
        $this->PesoNeto = $PesoNeto;

        return $this;
    }

    public function getPesoTara(): ?float
    {
        return $this->PesoTara;
    }

    public function setPesoTara(?float $PesoTara): self
    {
        $this->PesoTara = $PesoTara;

        return $this;
    }

    public function getNumPiezas(): ?int
    {
        return $this->NumPiezas;
    }

    public function setNumPiezas(?int $NumPiezas): self
    {
        $this->NumPiezas = $NumPiezas;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
