<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Repository\Complementos\CP\cpMercanciaAutoFedRemoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpMercanciaAutoFedRemoRepository::class)
 */
class cpMercanciaAutoFedRemo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpMercanciaAutoFedRemos")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=cp::class, inversedBy="cpMercanciaAutoFedRemos")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=cpMercanciaAutoFed::class, inversedBy="cpMercanciaAutoFedRemos")
     */
    private $auto;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $SubTipoRem;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $Placa;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?cp
    {
        return $this->cp;
    }

    public function setCp(?cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getAuto(): ?cpMercanciaAutoFed
    {
        return $this->auto;
    }

    public function setAuto(?cpMercanciaAutoFed $auto): self
    {
        $this->auto = $auto;

        return $this;
    }

    public function getSubTipoRem(): ?string
    {
        return $this->SubTipoRem;
    }

    public function setSubTipoRem(?string $SubTipoRem): self
    {
        $this->SubTipoRem = $SubTipoRem;

        return $this;
    }

    public function getPlaca(): ?string
    {
        return $this->Placa;
    }

    public function setPlaca(?string $Placa): self
    {
        $this->Placa = $Placa;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
