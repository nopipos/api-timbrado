<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CP\cpMercanciaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpMercanciaRepository::class)
 */
class cpMercancia
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpMercancias")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=cp::class, inversedBy="cpMercancias")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="cpMercancias")
     */
    private $factura;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PesoBrutoTotal;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PesoNetoTotal;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $UnidadPeso;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $NumTotalMercancias;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $CargoPorTasacion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaDetalle::class, mappedBy="mercancia")
     */
    private $cpMercanciaDetalles;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaAutoFed::class, mappedBy="mercancia")
     */
    private $cpMercanciaAutoFeds;

    public function __construct()
    {
        $this->cpMercanciaDetalles = new ArrayCollection();
        $this->cpMercanciaAutoFeds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?cp
    {
        return $this->cp;
    }

    public function setCp(?cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getPesoBrutoTotal(): ?float
    {
        return $this->PesoBrutoTotal;
    }

    public function setPesoBrutoTotal(?float $PesoBrutoTotal): self
    {
        $this->PesoBrutoTotal = $PesoBrutoTotal;

        return $this;
    }

    public function getPesoNetoTotal(): ?float
    {
        return $this->PesoNetoTotal;
    }

    public function setPesoNetoTotal(?float $PesoNetoTotal): self
    {
        $this->PesoNetoTotal = $PesoNetoTotal;

        return $this;
    }

    public function getUnidadPeso(): ?string
    {
        return $this->UnidadPeso;
    }

    public function setUnidadPeso(?string $UnidadPeso): self
    {
        $this->UnidadPeso = $UnidadPeso;

        return $this;
    }

    public function getNumTotalMercancias(): ?float
    {
        return $this->NumTotalMercancias;
    }

    public function setNumTotalMercancias(?float $NumTotalMercancias): self
    {
        $this->NumTotalMercancias = $NumTotalMercancias;

        return $this;
    }

    public function getCargoPorTasacion(): ?float
    {
        return $this->CargoPorTasacion;
    }

    public function setCargoPorTasacion(?float $CargoPorTasacion): self
    {
        $this->CargoPorTasacion = $CargoPorTasacion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaDetalle>
     */
    public function getCpMercanciaDetalles(): Collection
    {
        return $this->cpMercanciaDetalles;
    }

    public function addCpMercanciaDetalle(cpMercanciaDetalle $cpMercanciaDetalle): self
    {
        if (!$this->cpMercanciaDetalles->contains($cpMercanciaDetalle)) {
            $this->cpMercanciaDetalles[] = $cpMercanciaDetalle;
            $cpMercanciaDetalle->setMercancia($this);
        }

        return $this;
    }

    public function removeCpMercanciaDetalle(cpMercanciaDetalle $cpMercanciaDetalle): self
    {
        if ($this->cpMercanciaDetalles->removeElement($cpMercanciaDetalle)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaDetalle->getMercancia() === $this) {
                $cpMercanciaDetalle->setMercancia(null);
            }
        }

        return $this;
    }
    
    public function getAttributes(){
        
        return [
            'id'=>$this->getId(),
            'PesoBrutoTotal'=>$this->getPesoBrutoTotal(),
            'UnidadPeso'=>$this->getUnidadPeso(),
            'PesoNetoTotal'=>$this->getPesoNetoTotal(),
            'NumTotalMercancias'=>$this->getNumTotalMercancias(),
            'CargoPorTasacion'=>$this->getCargoPorTasacion(),
            'estatus'=>$this->getEstatus()
        ];
    }

    /**
     * @return Collection<int, cpMercanciaAutoFed>
     */
    public function getCpMercanciaAutoFeds(): Collection
    {
        return $this->cpMercanciaAutoFeds;
    }

    public function addCpMercanciaAutoFed(cpMercanciaAutoFed $cpMercanciaAutoFed): self
    {
        if (!$this->cpMercanciaAutoFeds->contains($cpMercanciaAutoFed)) {
            $this->cpMercanciaAutoFeds[] = $cpMercanciaAutoFed;
            $cpMercanciaAutoFed->setMercancia($this);
        }

        return $this;
    }

    public function removeCpMercanciaAutoFed(cpMercanciaAutoFed $cpMercanciaAutoFed): self
    {
        if ($this->cpMercanciaAutoFeds->removeElement($cpMercanciaAutoFed)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaAutoFed->getMercancia() === $this) {
                $cpMercanciaAutoFed->setMercancia(null);
            }
        }

        return $this;
    }

}
