<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Repository\Complementos\CP\cMercanciasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cMercanciasRepository::class)
 */
class cMercancias
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cMercancias")
     */
    private $empresas;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $bienes_trans;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $clave_stc;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $cantidad;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $clave_unidad;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $unidad;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $dimensiones;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $mat_peligroso;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $clave_mat_peli;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $embalaje;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $desc_embalaje;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $peso_kg;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $valor_mer;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $moneda;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $fraccion_arancelaria;

    /**
     * @ORM\Column(type="string", length=38, nullable=true)
     */
    private $uuid_comercio;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $unidad_peso;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $peso_bruto;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $peso_neto;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $peso_tara;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $num_pz;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $clave;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresas(): ?Empresas
    {
        return $this->empresas;
    }

    public function setEmpresas(?Empresas $empresas): self
    {
        $this->empresas = $empresas;

        return $this;
    }

    public function getBienesTrans(): ?string
    {
        return $this->bienes_trans;
    }

    public function setBienesTrans(string $bienes_trans): self
    {
        $this->bienes_trans = $bienes_trans;

        return $this;
    }

    public function getClaveStc(): ?string
    {
        return $this->clave_stc;
    }

    public function setClaveStc(?string $clave_stc): self
    {
        $this->clave_stc = $clave_stc;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getCantidad(): ?float
    {
        return $this->cantidad;
    }

    public function setCantidad(?float $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getClaveUnidad(): ?string
    {
        return $this->clave_unidad;
    }

    public function setClaveUnidad(?string $clave_unidad): self
    {
        $this->clave_unidad = $clave_unidad;

        return $this;
    }

    public function getUnidad(): ?string
    {
        return $this->unidad;
    }

    public function setUnidad(?string $unidad): self
    {
        $this->unidad = $unidad;

        return $this;
    }

    public function getDimensiones(): ?string
    {
        return $this->dimensiones;
    }

    public function setDimensiones(?string $dimensiones): self
    {
        $this->dimensiones = $dimensiones;

        return $this;
    }

    public function getMatPeligroso(): ?string
    {
        return $this->mat_peligroso;
    }

    public function setMatPeligroso(?string $mat_peligroso): self
    {
        $this->mat_peligroso = $mat_peligroso;

        return $this;
    }

    public function getClaveMatPeli(): ?string
    {
        return $this->clave_mat_peli;
    }

    public function setClaveMatPeli(?string $clave_mat_peli): self
    {
        $this->clave_mat_peli = $clave_mat_peli;

        return $this;
    }

    public function getEmbalaje(): ?string
    {
        return $this->embalaje;
    }

    public function setEmbalaje(?string $embalaje): self
    {
        $this->embalaje = $embalaje;

        return $this;
    }

    public function getDescEmbalaje(): ?string
    {
        return $this->desc_embalaje;
    }

    public function setDescEmbalaje(?string $desc_embalaje): self
    {
        $this->desc_embalaje = $desc_embalaje;

        return $this;
    }

    public function getPesoKg(): ?string
    {
        return $this->peso_kg;
    }

    public function setPesoKg(string $peso_kg): self
    {
        $this->peso_kg = $peso_kg;

        return $this;
    }

    public function getValorMer(): ?float
    {
        return $this->valor_mer;
    }

    public function setValorMer(?float $valor_mer): self
    {
        $this->valor_mer = $valor_mer;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(?string $moneda): self
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getFraccionArancelaria(): ?string
    {
        return $this->fraccion_arancelaria;
    }

    public function setFraccionArancelaria(?string $fraccion_arancelaria): self
    {
        $this->fraccion_arancelaria = $fraccion_arancelaria;

        return $this;
    }

    public function getUuidComercio(): ?string
    {
        return $this->uuid_comercio;
    }

    public function setUuidComercio(?string $uuid_comercio): self
    {
        $this->uuid_comercio = $uuid_comercio;

        return $this;
    }

    public function getUnidadPeso(): ?string
    {
        return $this->unidad_peso;
    }

    public function setUnidadPeso(?string $unidad_peso): self
    {
        $this->unidad_peso = $unidad_peso;

        return $this;
    }

    public function getPesoBruto(): ?string
    {
        return $this->peso_bruto;
    }

    public function setPesoBruto(?string $peso_bruto): self
    {
        $this->peso_bruto = $peso_bruto;

        return $this;
    }

    public function getPesoNeto(): ?float
    {
        return $this->peso_neto;
    }

    public function setPesoNeto(?float $peso_neto): self
    {
        $this->peso_neto = $peso_neto;

        return $this;
    }

    public function getPesoTara(): ?float
    {
        return $this->peso_tara;
    }

    public function setPesoTara(?float $peso_tara): self
    {
        $this->peso_tara = $peso_tara;

        return $this;
    }

    public function getNumPz(): ?int
    {
        return $this->num_pz;
    }

    public function setNumPz(?int $num_pz): self
    {
        $this->num_pz = $num_pz;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(?string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'bienes_trans'=>$this->getBienesTrans(),
            'clave_stc'=>$this->getClaveStc(),
            'descripcion'=>$this->getDescripcion(),
            'cantidad'=>$this->getCantidad(),
            'clave_unidad'=>$this->getClaveUnidad(),
            'unidad'=>$this->getUnidad(),
            'dimensiones'=>$this->getDimensiones(),
            'mat_peligroso'=>$this->getMatPeligroso(),
            'clave_mat_peli'=>$this->getClaveMatPeli(),
            'embalaje'=>$this->getEmbalaje(),
            'desc_embalaje'=>$this->getDescEmbalaje(),
            'peso_kg'=>$this->getPesoKg(),
            'valor_mer'=>$this->getValorMer(),
            'moneda'=>$this->getMoneda(),
            'fraccion_arancelaria'=>$this->getFraccionArancelaria(),
            'uuid_comercio'=>$this->getUuidComercio(),
            'unidad_peso'=>$this->getUnidadPeso(),
            'peso_bruto'=>$this->getPesoBruto(),
            'peso_neto'=>$this->getPesoNeto(),
            'peso_tara'=>$this->getPesoTara(),
            'num_pz'=>$this->getNumPz(),
            'clave'=>$this->getClave(),
        ];

    }
}
