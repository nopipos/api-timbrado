<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CP\cpMercanciaAutoFedRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpMercanciaAutoFedRepository::class)
 */
class cpMercanciaAutoFed
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpMercanciaAutoFeds")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=cp::class, inversedBy="cpMercanciaAutoFeds")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="cpMercanciaAutoFeds")
     */
    private $factura;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $PermSCT;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $NumPermisoSCT;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $ConfigVehicular;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $PlacaVM;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $AnioModeloVM;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $AseguraRespCivil;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $PolizaRespCivil;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $AseguraMedAmbiente;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $PolizaMedAmbiente;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $AseguraCarga;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $PolizaCarga;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PrimaSeguro;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=cpMercanciaAutoFedRemo::class, mappedBy="auto")
     */
    private $cpMercanciaAutoFedRemos;

    /**
     * @ORM\ManyToOne(targetEntity=cpMercancia::class, inversedBy="cpMercanciaAutoFeds")
     */
    private $mercancia;

    public function __construct()
    {
        $this->cpMercanciaAutoFedRemos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?cp
    {
        return $this->cp;
    }

    public function setCp(?cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getPermSCT(): ?string
    {
        return $this->PermSCT;
    }

    public function setPermSCT(?string $PermSCT): self
    {
        $this->PermSCT = $PermSCT;

        return $this;
    }

    public function getNumPermisoSCT(): ?string
    {
        return $this->NumPermisoSCT;
    }

    public function setNumPermisoSCT(?string $NumPermisoSCT): self
    {
        $this->NumPermisoSCT = $NumPermisoSCT;

        return $this;
    }

    public function getConfigVehicular(): ?string
    {
        return $this->ConfigVehicular;
    }

    public function setConfigVehicular(?string $ConfigVehicular): self
    {
        $this->ConfigVehicular = $ConfigVehicular;

        return $this;
    }

    public function getPlacaVM(): ?string
    {
        return $this->PlacaVM;
    }

    public function setPlacaVM(?string $PlacaVM): self
    {
        $this->PlacaVM = $PlacaVM;

        return $this;
    }

    public function getAnioModeloVM(): ?string
    {
        return $this->AnioModeloVM;
    }

    public function setAnioModeloVM(?string $AnioModeloVM): self
    {
        $this->AnioModeloVM = $AnioModeloVM;

        return $this;
    }

    public function getAseguraRespCivil(): ?string
    {
        return $this->AseguraRespCivil;
    }

    public function setAseguraRespCivil(?string $AseguraRespCivil): self
    {
        $this->AseguraRespCivil = $AseguraRespCivil;

        return $this;
    }

    public function getPolizaRespCivil(): ?string
    {
        return $this->PolizaRespCivil;
    }

    public function setPolizaRespCivil(?string $PolizaRespCivil): self
    {
        $this->PolizaRespCivil = $PolizaRespCivil;

        return $this;
    }

    public function getAseguraMedAmbiente(): ?string
    {
        return $this->AseguraMedAmbiente;
    }

    public function setAseguraMedAmbiente(?string $AseguraMedAmbiente): self
    {
        $this->AseguraMedAmbiente = $AseguraMedAmbiente;

        return $this;
    }

    public function getPolizaMedAmbiente(): ?string
    {
        return $this->PolizaMedAmbiente;
    }

    public function setPolizaMedAmbiente(?string $PolizaMedAmbiente): self
    {
        $this->PolizaMedAmbiente = $PolizaMedAmbiente;

        return $this;
    }

    public function getAseguraCarga(): ?string
    {
        return $this->AseguraCarga;
    }

    public function setAseguraCarga(?string $AseguraCarga): self
    {
        $this->AseguraCarga = $AseguraCarga;

        return $this;
    }

    public function getPolizaCarga(): ?string
    {
        return $this->PolizaCarga;
    }

    public function setPolizaCarga(?string $PolizaCarga): self
    {
        $this->PolizaCarga = $PolizaCarga;

        return $this;
    }

    public function getPrimaSeguro(): ?float
    {
        return $this->PrimaSeguro;
    }

    public function setPrimaSeguro(?float $PrimaSeguro): self
    {
        $this->PrimaSeguro = $PrimaSeguro;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection<int, cpMercanciaAutoFedRemo>
     */
    public function getCpMercanciaAutoFedRemos(): Collection
    {
        return $this->cpMercanciaAutoFedRemos;
    }

    public function addCpMercanciaAutoFedRemo(cpMercanciaAutoFedRemo $cpMercanciaAutoFedRemo): self
    {
        if (!$this->cpMercanciaAutoFedRemos->contains($cpMercanciaAutoFedRemo)) {
            $this->cpMercanciaAutoFedRemos[] = $cpMercanciaAutoFedRemo;
            $cpMercanciaAutoFedRemo->setAuto($this);
        }

        return $this;
    }

    public function removeCpMercanciaAutoFedRemo(cpMercanciaAutoFedRemo $cpMercanciaAutoFedRemo): self
    {
        if ($this->cpMercanciaAutoFedRemos->removeElement($cpMercanciaAutoFedRemo)) {
            // set the owning side to null (unless already changed)
            if ($cpMercanciaAutoFedRemo->getAuto() === $this) {
                $cpMercanciaAutoFedRemo->setAuto(null);
            }
        }

        return $this;
    }

    public function getMercancia(): ?cpMercancia
    {
        return $this->mercancia;
    }

    public function setMercancia(?cpMercancia $mercancia): self
    {
        $this->mercancia = $mercancia;

        return $this;
    }
}
