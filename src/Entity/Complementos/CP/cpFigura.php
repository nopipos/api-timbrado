<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CP\cpFiguraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpFiguraRepository::class)
 */
class cpFigura
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpFiguras")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=cp::class, inversedBy="cpFiguras")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="cpFiguras")
     */
    private $factura;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $TipoFigura;

    /**
     * @ORM\Column(type="string", length=13, nullable=true)
     */
    private $RFCFigura;

    /**
     * @ORM\Column(type="string", length=18, nullable=true)
     */
    private $NumLicencia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NombreFigura;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $NumRegIdTribFigura;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $ResidenciaFiscalFigura;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $Calle;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $NumeroExterior;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $NumeroInterior;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $Colonia;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $Localidad;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $Referencia;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $Municipio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $Estado;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $Pais;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $CodigoPostal;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=cpFiguraParte::class, mappedBy="figura")
     */
    private $cpFiguraPartes;

    public function __construct()
    {
        $this->cpFiguraPartes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?cp
    {
        return $this->cp;
    }

    public function setCp(?cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getTipoFigura(): ?string
    {
        return $this->TipoFigura;
    }

    public function setTipoFigura(?string $TipoFigura): self
    {
        $this->TipoFigura = $TipoFigura;

        return $this;
    }

    public function getRFCFigura(): ?string
    {
        return $this->RFCFigura;
    }

    public function setRFCFigura(?string $RFCFigura): self
    {
        $this->RFCFigura = $RFCFigura;

        return $this;
    }

    public function getNumLicencia(): ?string
    {
        return $this->NumLicencia;
    }

    public function setNumLicencia(?string $NumLicencia): self
    {
        $this->NumLicencia = $NumLicencia;

        return $this;
    }

    public function getNombreFigura(): ?string
    {
        return $this->NombreFigura;
    }

    public function setNombreFigura(?string $NombreFigura): self
    {
        $this->NombreFigura = $NombreFigura;

        return $this;
    }

    public function getNumRegIdTribFigura(): ?string
    {
        return $this->NumRegIdTribFigura;
    }

    public function setNumRegIdTribFigura(?string $NumRegIdTribFigura): self
    {
        $this->NumRegIdTribFigura = $NumRegIdTribFigura;

        return $this;
    }

    public function getResidenciaFiscalFigura(): ?string
    {
        return $this->ResidenciaFiscalFigura;
    }

    public function setResidenciaFiscalFigura(?string $ResidenciaFiscalFigura): self
    {
        $this->ResidenciaFiscalFigura = $ResidenciaFiscalFigura;

        return $this;
    }

    public function getCalle(): ?string
    {
        return $this->Calle;
    }

    public function setCalle(?string $Calle): self
    {
        $this->Calle = $Calle;

        return $this;
    }

    public function getNumeroExterior(): ?string
    {
        return $this->NumeroExterior;
    }

    public function setNumeroExterior(?string $NumeroExterior): self
    {
        $this->NumeroExterior = $NumeroExterior;

        return $this;
    }

    public function getNumeroInterior(): ?string
    {
        return $this->NumeroInterior;
    }

    public function setNumeroInterior(?string $NumeroInterior): self
    {
        $this->NumeroInterior = $NumeroInterior;

        return $this;
    }

    public function getColonia(): ?string
    {
        return $this->Colonia;
    }

    public function setColonia(?string $Colonia): self
    {
        $this->Colonia = $Colonia;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->Localidad;
    }

    public function setLocalidad(?string $Localidad): self
    {
        $this->Localidad = $Localidad;

        return $this;
    }

    public function getReferencia(): ?string
    {
        return $this->Referencia;
    }

    public function setReferencia(?string $Referencia): self
    {
        $this->Referencia = $Referencia;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->Municipio;
    }

    public function setMunicipio(?string $Municipio): self
    {
        $this->Municipio = $Municipio;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->Estado;
    }

    public function setEstado(?string $Estado): self
    {
        $this->Estado = $Estado;

        return $this;
    }

    public function getPais(): ?string
    {
        return $this->Pais;
    }

    public function setPais(?string $Pais): self
    {
        $this->Pais = $Pais;

        return $this;
    }

    public function getCodigoPostal(): ?string
    {
        return $this->CodigoPostal;
    }

    public function setCodigoPostal(?string $CodigoPostal): self
    {
        $this->CodigoPostal = $CodigoPostal;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection<int, cpFiguraParte>
     */
    public function getCpFiguraPartes(): Collection
    {
        return $this->cpFiguraPartes;
    }

    public function addCpFiguraParte(cpFiguraParte $cpFiguraParte): self
    {
        if (!$this->cpFiguraPartes->contains($cpFiguraParte)) {
            $this->cpFiguraPartes[] = $cpFiguraParte;
            $cpFiguraParte->setFigura($this);
        }

        return $this;
    }

    public function removeCpFiguraParte(cpFiguraParte $cpFiguraParte): self
    {
        if ($this->cpFiguraPartes->removeElement($cpFiguraParte)) {
            // set the owning side to null (unless already changed)
            if ($cpFiguraParte->getFigura() === $this) {
                $cpFiguraParte->setFigura(null);
            }
        }

        return $this;
    }
}
