<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Repository\Complementos\CP\cPersonasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cPersonasRepository::class)
 */
class cPersonas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cPersonas")
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $tipo;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $rfc;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $llicencia;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $residencia;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $num_reg;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $calle;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $num_int;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $num_ext;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $colonia;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $localidad;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $referencia;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $municipio;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $estado;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $pais;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $cp;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(?string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getLlicencia(): ?string
    {
        return $this->llicencia;
    }

    public function setLlicencia(?string $llicencia): self
    {
        $this->llicencia = $llicencia;

        return $this;
    }

    public function getResidencia(): ?string
    {
        return $this->residencia;
    }

    public function setResidencia(?string $residencia): self
    {
        $this->residencia = $residencia;

        return $this;
    }

    public function getNumReg(): ?string
    {
        return $this->num_reg;
    }

    public function setNumReg(?string $num_reg): self
    {
        $this->num_reg = $num_reg;

        return $this;
    }

    public function getCalle(): ?string
    {
        return $this->calle;
    }

    public function setCalle(?string $calle): self
    {
        $this->calle = $calle;

        return $this;
    }

    public function getNumInt(): ?string
    {
        return $this->num_int;
    }

    public function setNumInt(?string $num_int): self
    {
        $this->num_int = $num_int;

        return $this;
    }

    public function getNumExt(): ?string
    {
        return $this->num_ext;
    }

    public function setNumExt(?string $num_ext): self
    {
        $this->num_ext = $num_ext;

        return $this;
    }

    public function getColonia(): ?string
    {
        return $this->colonia;
    }

    public function setColonia(?string $colonia): self
    {
        $this->colonia = $colonia;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->localidad;
    }

    public function setLocalidad(?string $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getReferencia(): ?string
    {
        return $this->referencia;
    }

    public function setReferencia(?string $referencia): self
    {
        $this->referencia = $referencia;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    public function setMunicipio(?string $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getPais(): ?string
    {
        return $this->pais;
    }

    public function setPais(?string $pais): self
    {
        $this->pais = $pais;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getId(),
            'tipo'=>$this->getTipo(),
            'nombre'=>$this->getNombre(),
            'rfc'=>$this->getRfc(),
            'llicencia'=>$this->getLlicencia(),
            'residencia'=>$this->getResidencia(),
            'num_reg'=>$this->getNumReg(),
            'calle'=>$this->getCalle(),
            'num_int'=>$this->getNumInt(),
            'num_ext'=>$this->getNumExt(),
            'colonia'=>$this->getColonia(),
            'localidad'=>$this->getLocalidad(),
            'referencia'=>$this->getReferencia(),
            'municipio'=>$this->getMunicipio(),
            'estado'=>$this->getEstado(),
            'pais'=>$this->getPais(),
            'cp'=>$this->getCp(),
        ];

    }
}
