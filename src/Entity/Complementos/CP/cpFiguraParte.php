<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Repository\Complementos\CP\cpFiguraParteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpFiguraParteRepository::class)
 */
class cpFiguraParte
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpFiguraPartes")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=cp::class, inversedBy="parte")
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $parte;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\ManyToOne(targetEntity=cpFigura::class, inversedBy="cpFiguraPartes")
     */
    private $figura;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?cp
    {
        return $this->cp;
    }

    public function setCp(?cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getParte(): ?string
    {
        return $this->parte;
    }

    public function setParte(?string $parte): self
    {
        $this->parte = $parte;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getFigura(): ?cpFigura
    {
        return $this->figura;
    }

    public function setFigura(?cpFigura $figura): self
    {
        $this->figura = $figura;

        return $this;
    }
}
