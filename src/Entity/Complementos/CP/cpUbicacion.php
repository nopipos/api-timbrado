<?php

namespace App\Entity\Complementos\CP;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CP\cpUbicacionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cpUbicacionRepository::class)
 */
class cpUbicacion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cpUbicacions")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Cp::class, inversedBy="cpUbicacions")
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="cpUbicacions")
     */
    private $factura;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $TipoUbicacion;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $IDUbicacion;

    /**
     * @ORM\Column(type="string", length=14, nullable=true)
     */
    private $RFCRemitenteDestinatario;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $NombreRemitenteDestinatario;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $NumRegIdTrib;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $ResidenciaFiscal;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $NumEstacion;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $NombreEstacion;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $NavegacionTrafico;

    /**
     * @ORM\Column(type="datetime")
     */
    private $FechaHoraSalidaLlegada;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $TipoEstacion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $DistanciaRecorrida;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $Calle;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $NumeroExterior;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $NumeroInterior;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $Colonia;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $Localidad;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $Referencia;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $Municipio;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $Estado;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $Pais;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $CodigoPostal;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getCp(): ?Cp
    {
        return $this->cp;
    }

    public function setCp(?Cp $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getTipoUbicacion(): ?string
    {
        return $this->TipoUbicacion;
    }

    public function setTipoUbicacion(string $TipoUbicacion): self
    {
        $this->TipoUbicacion = $TipoUbicacion;

        return $this;
    }

    public function getIDUbicacion(): ?string
    {
        return $this->IDUbicacion;
    }

    public function setIDUbicacion(?string $IDUbicacion): self
    {
        $this->IDUbicacion = $IDUbicacion;

        return $this;
    }

    public function getRFCRemitenteDestinatario(): ?string
    {
        return $this->RFCRemitenteDestinatario;
    }

    public function setRFCRemitenteDestinatario(?string $RFCRemitenteDestinatario): self
    {
        $this->RFCRemitenteDestinatario = $RFCRemitenteDestinatario;

        return $this;
    }

    public function getNombreRemitenteDestinatario(): ?string
    {
        return $this->NombreRemitenteDestinatario;
    }

    public function setNombreRemitenteDestinatario(?string $NombreRemitenteDestinatario): self
    {
        $this->NombreRemitenteDestinatario = $NombreRemitenteDestinatario;

        return $this;
    }

    public function getNumRegIdTrib(): ?string
    {
        return $this->NumRegIdTrib;
    }

    public function setNumRegIdTrib(?string $NumRegIdTrib): self
    {
        $this->NumRegIdTrib = $NumRegIdTrib;

        return $this;
    }

    public function getResidenciaFiscal(): ?string
    {
        return $this->ResidenciaFiscal;
    }

    public function setResidenciaFiscal(?string $ResidenciaFiscal): self
    {
        $this->ResidenciaFiscal = $ResidenciaFiscal;

        return $this;
    }

    public function getNumEstacion(): ?string
    {
        return $this->NumEstacion;
    }

    public function setNumEstacion(?string $NumEstacion): self
    {
        $this->NumEstacion = $NumEstacion;

        return $this;
    }

    public function getNombreEstacion(): ?string
    {
        return $this->NombreEstacion;
    }

    public function setNombreEstacion(?string $NombreEstacion): self
    {
        $this->NombreEstacion = $NombreEstacion;

        return $this;
    }

    public function getNavegacionTrafico(): ?string
    {
        return $this->NavegacionTrafico;
    }

    public function setNavegacionTrafico(?string $NavegacionTrafico): self
    {
        $this->NavegacionTrafico = $NavegacionTrafico;

        return $this;
    }

    public function getFechaHoraSalidaLlegada(): ?\DateTimeInterface
    {
        return $this->FechaHoraSalidaLlegada;
    }

    public function setFechaHoraSalidaLlegada(\DateTimeInterface $FechaHoraSalidaLlegada): self
    {
        $this->FechaHoraSalidaLlegada = $FechaHoraSalidaLlegada;

        return $this;
    }

    public function getTipoEstacion(): ?string
    {
        return $this->TipoEstacion;
    }

    public function setTipoEstacion(?string $TipoEstacion): self
    {
        $this->TipoEstacion = $TipoEstacion;

        return $this;
    }

    public function getDistanciaRecorrida(): ?float
    {
        return $this->DistanciaRecorrida;
    }

    public function setDistanciaRecorrida(?float $DistanciaRecorrida): self
    {
        $this->DistanciaRecorrida = $DistanciaRecorrida;

        return $this;
    }

    public function getCalle(): ?string
    {
        return $this->Calle;
    }

    public function setCalle(?string $Calle): self
    {
        $this->Calle = $Calle;

        return $this;
    }

    public function getNumeroExterior(): ?string
    {
        return $this->NumeroExterior;
    }

    public function setNumeroExterior(?string $NumeroExterior): self
    {
        $this->NumeroExterior = $NumeroExterior;

        return $this;
    }

    public function getNumeroInterior(): ?string
    {
        return $this->NumeroInterior;
    }

    public function setNumeroInterior(?string $NumeroInterior): self
    {
        $this->NumeroInterior = $NumeroInterior;

        return $this;
    }

    public function getColonia(): ?string
    {
        return $this->Colonia;
    }

    public function setColonia(?string $Colonia): self
    {
        $this->Colonia = $Colonia;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->Localidad;
    }

    public function setLocalidad(?string $Localidad): self
    {
        $this->Localidad = $Localidad;

        return $this;
    }

    public function getReferencia(): ?string
    {
        return $this->Referencia;
    }

    public function setReferencia(?string $Referencia): self
    {
        $this->Referencia = $Referencia;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->Municipio;
    }

    public function setMunicipio(?string $Municipio): self
    {
        $this->Municipio = $Municipio;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->Estado;
    }

    public function setEstado(?string $Estado): self
    {
        $this->Estado = $Estado;

        return $this;
    }

    public function getPais(): ?string
    {
        return $this->Pais;
    }

    public function setPais(?string $Pais): self
    {
        $this->Pais = $Pais;

        return $this;
    }

    public function getCodigoPostal(): ?string
    {
        return $this->CodigoPostal;
    }

    public function setCodigoPostal(?string $CodigoPostal): self
    {
        $this->CodigoPostal = $CodigoPostal;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getAttributes(){

        return [
            'id'=>$this->getId(),
            'TipoUbicacion'=>$this->getTipoUbicacion(),
            'IDUbicacion'=>$this->getIDUbicacion(),
            'RFCRemitenteDestinatario'=>$this->getRFCRemitenteDestinatario(),
            'NombreRemitenteDestinatario'=>$this->getNombreRemitenteDestinatario(),
            'NumRegIdTrib'=>$this->getNumRegIdTrib(),
            'ResidenciaFiscal'=>$this->getResidenciaFiscal(),
            'NumEstacion'=>$this->getNumEstacion(),
            'NombreEstacion'=>$this->getNombreEstacion(),
            'NavegacionTrafico'=>$this->getNavegacionTrafico(),
            'FechaHoraSalidaLlegada'=>str_replace(" ","T",$this->getFechaHoraSalidaLlegada()->format('Y-m-d H:i:s')),
            'TipoEstacion'=>$this->getTipoEstacion(),
            'DistanciaRecorrida'=>$this->getDistanciaRecorrida(),
            "Domicilio"=>[
                'Calle'=>$this->getCalle(),
                'NumeroExterior'=>$this->getNumeroExterior(),
                'NumeroInterior'=>$this->getNumeroInterior(),
                'Colonia'=>$this->getColonia(),
                'Localidad'=>$this->getLocalidad(),
                'Referencia'=>$this->getReferencia(),
                'Municipio'=>$this->getMunicipio(),
                'Estado'=>$this->getEstado(),
                'Pais'=>$this->getPais(),
                'CodigoPostal'=>$this->getCodigoPostal(),
            ],
            'Calle'=>$this->getCalle(),
            'NumeroExterior'=>$this->getNumeroExterior(),
            'NumeroInterior'=>$this->getNumeroInterior(),
            'Colonia'=>$this->getColonia(),
            'Localidad'=>$this->getLocalidad(),
            'Referencia'=>$this->getReferencia(),
            'Municipio'=>$this->getMunicipio(),
            'Estado'=>$this->getEstado(),
            'Pais'=>$this->getPais(),
            'CodigoPostal'=>$this->getCodigoPostal(),
            'estatus'=>$this->getEstatus(),
            
        ];
    }
}
