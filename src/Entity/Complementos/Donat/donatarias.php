<?php

namespace App\Entity\Complementos\Donat;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\Donat\donatariasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=donatariasRepository::class)
 */
class donatarias
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="donatarias")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="donatarias")
     */
    private $factura;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $noAutorizacion;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $leyenda;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getNoAutorizacion(): ?string
    {
        return $this->noAutorizacion;
    }

    public function setNoAutorizacion(string $noAutorizacion): self
    {
        $this->noAutorizacion = $noAutorizacion;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getLeyenda(): ?string
    {
        return $this->leyenda;
    }

    public function setLeyenda(string $leyenda): self
    {
        $this->leyenda = $leyenda;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
