<?php

namespace App\Entity\Complementos\CCE;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CCE\cceMercanciasDetalleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cceMercanciasDetalleRepository::class)
 */
class cceMercanciasDetalle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cceMercanciasDetalles")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="cceMercanciasDetalles")
     */
    private $factura;

    /**
     * @ORM\ManyToOne(targetEntity=cce::class, inversedBy="cceMercanciasDetalles")
     */
    private $cce;

    /**
     * @ORM\ManyToOne(targetEntity=cceMercancias::class, inversedBy="cceMercanciasDetalles")
     */
    private $mercancia;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $Marca;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $Modelo;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $Submodelo;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $NumeroSerie;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getCce(): ?cce
    {
        return $this->cce;
    }

    public function setCce(?cce $cce): self
    {
        $this->cce = $cce;

        return $this;
    }

    public function getMercancia(): ?cceMercancias
    {
        return $this->mercancia;
    }

    public function setMercancia(?cceMercancias $mercancia): self
    {
        $this->mercancia = $mercancia;

        return $this;
    }

    public function getMarca(): ?string
    {
        return $this->Marca;
    }

    public function setMarca(?string $Marca): self
    {
        $this->Marca = $Marca;

        return $this;
    }

    public function getModelo(): ?string
    {
        return $this->Modelo;
    }

    public function setModelo(?string $Modelo): self
    {
        $this->Modelo = $Modelo;

        return $this;
    }

    public function getSubmodelo(): ?string
    {
        return $this->Submodelo;
    }

    public function setSubmodelo(?string $Submodelo): self
    {
        $this->Submodelo = $Submodelo;

        return $this;
    }

    public function getNumeroSerie(): ?string
    {
        return $this->NumeroSerie;
    }

    public function setNumeroSerie(?string $NumeroSerie): self
    {
        $this->NumeroSerie = $NumeroSerie;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
