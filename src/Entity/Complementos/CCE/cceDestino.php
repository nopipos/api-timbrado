<?php

namespace App\Entity\Complementos\CCE;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CCE\cceDestinoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cceDestinoRepository::class)
 */
class cceDestino
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cceDestinos")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="cceDestinos")
     */
    private $factura;

    /**
     * @ORM\ManyToOne(targetEntity=cce::class, inversedBy="cceDestinos")
     */
    private $cce;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $NumRegIdTrib;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Nombre;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $Calle;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $NumeroExterior;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $NumeroInterior;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $Colonia;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $Localidad;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $Referencia;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $Municipio;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $Estado;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $Pais;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $CodigoPostal;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getCce(): ?cce
    {
        return $this->cce;
    }

    public function setCce(?cce $cce): self
    {
        $this->cce = $cce;

        return $this;
    }

    public function getNumRegIdTrib(): ?string
    {
        return $this->NumRegIdTrib;
    }

    public function setNumRegIdTrib(?string $NumRegIdTrib): self
    {
        $this->NumRegIdTrib = $NumRegIdTrib;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->Nombre;
    }

    public function setNombre(?string $Nombre): self
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    public function getCalle(): ?string
    {
        return $this->Calle;
    }

    public function setCalle(?string $Calle): self
    {
        $this->Calle = $Calle;

        return $this;
    }

    public function getNumeroExterior(): ?string
    {
        return $this->NumeroExterior;
    }

    public function setNumeroExterior(?string $NumeroExterior): self
    {
        $this->NumeroExterior = $NumeroExterior;

        return $this;
    }

    public function getNumeroInterior(): ?string
    {
        return $this->NumeroInterior;
    }

    public function setNumeroInterior(?string $NumeroInterior): self
    {
        $this->NumeroInterior = $NumeroInterior;

        return $this;
    }

    public function getColonia(): ?string
    {
        return $this->Colonia;
    }

    public function setColonia(?string $Colonia): self
    {
        $this->Colonia = $Colonia;

        return $this;
    }

    public function getLocalidad(): ?string
    {
        return $this->Localidad;
    }

    public function setLocalidad(?string $Localidad): self
    {
        $this->Localidad = $Localidad;

        return $this;
    }

    public function getReferencia(): ?string
    {
        return $this->Referencia;
    }

    public function setReferencia(?string $Referencia): self
    {
        $this->Referencia = $Referencia;

        return $this;
    }

    public function getMunicipio(): ?string
    {
        return $this->Municipio;
    }

    public function setMunicipio(?string $Municipio): self
    {
        $this->Municipio = $Municipio;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->Estado;
    }

    public function setEstado(?string $Estado): self
    {
        $this->Estado = $Estado;

        return $this;
    }

    public function getPais(): ?string
    {
        return $this->Pais;
    }

    public function setPais(?string $Pais): self
    {
        $this->Pais = $Pais;

        return $this;
    }

    public function getCodigoPostal(): ?string
    {
        return $this->CodigoPostal;
    }

    public function setCodigoPostal(?string $CodigoPostal): self
    {
        $this->CodigoPostal = $CodigoPostal;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getAttributes(){
        return [
            'id'=>$this->getId(),
            'NumRegIdTrib'=>$this->getNumRegIdTrib(),
            'Nombre'=>$this->getNombre(),
            'Domicilio'=>[
                'Calle'=>$this->getCalle(),
                'NumeroExterior'=>$this->getNumeroExterior(),
                'NumeroInterior'=>$this->getNumeroInterior(),
                'Colonia'=>$this->getColonia(),
                'Localidad'=>$this->getLocalidad(),
                'Referencia'=>$this->getReferencia(),
                'Municipio'=>$this->getMunicipio(),
                'Estado'=>$this->getEstado(),
                'Pais'=>$this->getPais(),
                'CodigoPostal'=>$this->getCodigoPostal(),
            ],
            'estatus'=>$this->getEstatus(),
        ];
    }
}
