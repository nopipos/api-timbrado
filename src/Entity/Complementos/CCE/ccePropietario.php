<?php

namespace App\Entity\Complementos\CCE;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CCE\ccePropietarioRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ccePropietarioRepository::class)
 */
class ccePropietario
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="ccePropietarios")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="ccePropietarios")
     */
    private $factura;

    /**
     * @ORM\ManyToOne(targetEntity=cce::class, inversedBy="ccePropietarios")
     */
    private $cce;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $NumRegIdTrib;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $ResidenciaFiscal;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getCce(): ?cce
    {
        return $this->cce;
    }

    public function setCce(?cce $cce): self
    {
        $this->cce = $cce;

        return $this;
    }

    public function getNumRegIdTrib(): ?string
    {
        return $this->NumRegIdTrib;
    }

    public function setNumRegIdTrib(?string $NumRegIdTrib): self
    {
        $this->NumRegIdTrib = $NumRegIdTrib;

        return $this;
    }

    public function getResidenciaFiscal(): ?string
    {
        return $this->ResidenciaFiscal;
    }

    public function setResidenciaFiscal(?string $ResidenciaFiscal): self
    {
        $this->ResidenciaFiscal = $ResidenciaFiscal;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
    public function getAttributes(){

        return[

            'id'=>$this->getId(),
            'NumRegIdTrib'=>$this->getNumRegIdTrib(),
            'ResidenciaFiscal'=>$this->getResidenciaFiscal(),
            'estatus'=>$this->getEstatus(),
        ];
    }
}
