<?php

namespace App\Entity\Complementos\CCE;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CCE\cceMercanciasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cceMercanciasRepository::class)
 */
class cceMercancias
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cceMercancias")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="cceMercancias")
     */
    private $factura;

    /**
     * @ORM\ManyToOne(targetEntity=cce::class, inversedBy="cceMercancias")
     */
    private $cce;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $NoIdentificacion;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $FraccionArancelaria;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $CantidadAduana;

    /**
     * @ORM\Column(type="string", length=26, nullable=true)
     */
    private $UnidadAduana;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ValorUnitarioAduana;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ValorDolares;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=cceMercanciasDetalle::class, mappedBy="mercancia")
     */
    private $cceMercanciasDetalles;

    public function __construct()
    {
        $this->cceMercanciasDetalles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getCce(): ?cce
    {
        return $this->cce;
    }

    public function setCce(?cce $cce): self
    {
        $this->cce = $cce;

        return $this;
    }

    public function getNoIdentificacion(): ?string
    {
        return $this->NoIdentificacion;
    }

    public function setNoIdentificacion(?string $NoIdentificacion): self
    {
        $this->NoIdentificacion = $NoIdentificacion;

        return $this;
    }

    public function getFraccionArancelaria(): ?string
    {
        return $this->FraccionArancelaria;
    }

    public function setFraccionArancelaria(?string $FraccionArancelaria): self
    {
        $this->FraccionArancelaria = $FraccionArancelaria;

        return $this;
    }

    public function getCantidadAduana(): ?float
    {
        return $this->CantidadAduana;
    }

    public function setCantidadAduana(?float $CantidadAduana): self
    {
        $this->CantidadAduana = $CantidadAduana;

        return $this;
    }

    public function getUnidadAduana(): ?string
    {
        return $this->UnidadAduana;
    }

    public function setUnidadAduana(?string $UnidadAduana): self
    {
        $this->UnidadAduana = $UnidadAduana;

        return $this;
    }

    public function getValorUnitarioAduana(): ?float
    {
        return $this->ValorUnitarioAduana;
    }

    public function setValorUnitarioAduana(?float $ValorUnitarioAduana): self
    {
        $this->ValorUnitarioAduana = $ValorUnitarioAduana;

        return $this;
    }

    public function getValorDolares(): ?float
    {
        return $this->ValorDolares;
    }

    public function setValorDolares(?float $ValorDolares): self
    {
        $this->ValorDolares = $ValorDolares;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection<int, cceMercanciasDetalle>
     */
    public function getCceMercanciasDetalles(): Collection
    {
        return $this->cceMercanciasDetalles;
    }

    public function addCceMercanciasDetalle(cceMercanciasDetalle $cceMercanciasDetalle): self
    {
        if (!$this->cceMercanciasDetalles->contains($cceMercanciasDetalle)) {
            $this->cceMercanciasDetalles[] = $cceMercanciasDetalle;
            $cceMercanciasDetalle->setMercancia($this);
        }

        return $this;
    }

    public function removeCceMercanciasDetalle(cceMercanciasDetalle $cceMercanciasDetalle): self
    {
        if ($this->cceMercanciasDetalles->removeElement($cceMercanciasDetalle)) {
            // set the owning side to null (unless already changed)
            if ($cceMercanciasDetalle->getMercancia() === $this) {
                $cceMercanciasDetalle->setMercancia(null);
            }
        }

        return $this;
    }
}
