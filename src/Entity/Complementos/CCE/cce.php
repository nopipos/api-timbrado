<?php

namespace App\Entity\Complementos\CCE;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Complementos\CCE\cceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=cceRepository::class)
 */
class cce
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="cces")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="cces")
     */
    private $factura;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $Version;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $MotivoTraslado;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $TipoOperacion;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $ClaveDePedimento;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $CertificadoOrigen;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $NumCertificadoOrigen;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $NumeroExportadorConfiable;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $Incoterm;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $Subdivision;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $Observaciones;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $TipoCambioUSD;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $TotalUSD;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=cceDestino::class, mappedBy="cce")
     */
    private $cceDestinos;

    /**
     * @ORM\OneToMany(targetEntity=ccePropietario::class, mappedBy="cce")
     */
    private $ccePropietarios;

    /**
     * @ORM\OneToMany(targetEntity=cceMercancias::class, mappedBy="cce")
     */
    private $cceMercancias;

    /**
     * @ORM\OneToMany(targetEntity=cceMercanciasDetalle::class, mappedBy="cce")
     */
    private $cceMercanciasDetalles;

    public function __construct()
    {
        $this->cceDestinos = new ArrayCollection();
        $this->ccePropietarios = new ArrayCollection();
        $this->cceMercancias = new ArrayCollection();
        $this->cceMercanciasDetalles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->Version;
    }

    public function setVersion(string $Version): self
    {
        $this->Version = $Version;

        return $this;
    }

    public function getMotivoTraslado(): ?string
    {
        return $this->MotivoTraslado;
    }

    public function setMotivoTraslado(?string $MotivoTraslado): self
    {
        $this->MotivoTraslado = $MotivoTraslado;

        return $this;
    }

    public function getTipoOperacion(): ?string
    {
        return $this->TipoOperacion;
    }

    public function setTipoOperacion(?string $TipoOperacion): self
    {
        $this->TipoOperacion = $TipoOperacion;

        return $this;
    }

    public function getClaveDePedimento(): ?string
    {
        return $this->ClaveDePedimento;
    }

    public function setClaveDePedimento(?string $ClaveDePedimento): self
    {
        $this->ClaveDePedimento = $ClaveDePedimento;

        return $this;
    }

    public function getCertificadoOrigen(): ?string
    {
        return $this->CertificadoOrigen;
    }

    public function setCertificadoOrigen(?string $CertificadoOrigen): self
    {
        $this->CertificadoOrigen = $CertificadoOrigen;

        return $this;
    }

    public function getNumCertificadoOrigen(): ?string
    {
        return $this->NumCertificadoOrigen;
    }

    public function setNumCertificadoOrigen(?string $NumCertificadoOrigen): self
    {
        $this->NumCertificadoOrigen = $NumCertificadoOrigen;

        return $this;
    }

    public function getNumeroExportadorConfiable(): ?string
    {
        return $this->NumeroExportadorConfiable;
    }

    public function setNumeroExportadorConfiable(?string $NumeroExportadorConfiable): self
    {
        $this->NumeroExportadorConfiable = $NumeroExportadorConfiable;

        return $this;
    }

    public function getIncoterm(): ?string
    {
        return $this->Incoterm;
    }

    public function setIncoterm(?string $Incoterm): self
    {
        $this->Incoterm = $Incoterm;

        return $this;
    }

    public function getSubdivision(): ?string
    {
        return $this->Subdivision;
    }

    public function setSubdivision(?string $Subdivision): self
    {
        $this->Subdivision = $Subdivision;

        return $this;
    }

    public function getObservaciones(): ?string
    {
        return $this->Observaciones;
    }

    public function setObservaciones(?string $Observaciones): self
    {
        $this->Observaciones = $Observaciones;

        return $this;
    }

    public function getTipoCambioUSD(): ?float
    {
        return $this->TipoCambioUSD;
    }

    public function setTipoCambioUSD(?float $TipoCambioUSD): self
    {
        $this->TipoCambioUSD = $TipoCambioUSD;

        return $this;
    }

    public function getTotalUSD(): ?float
    {
        return $this->TotalUSD;
    }

    public function setTotalUSD(?float $TotalUSD): self
    {
        $this->TotalUSD = $TotalUSD;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection<int, cceDestino>
     */
    public function getCceDestinos(): Collection
    {
        return $this->cceDestinos;
    }

    public function addCceDestino(cceDestino $cceDestino): self
    {
        if (!$this->cceDestinos->contains($cceDestino)) {
            $this->cceDestinos[] = $cceDestino;
            $cceDestino->setCce($this);
        }

        return $this;
    }

    public function removeCceDestino(cceDestino $cceDestino): self
    {
        if ($this->cceDestinos->removeElement($cceDestino)) {
            // set the owning side to null (unless already changed)
            if ($cceDestino->getCce() === $this) {
                $cceDestino->setCce(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ccePropietario>
     */
    public function getCcePropietarios(): Collection
    {
        return $this->ccePropietarios;
    }

    public function addCcePropietario(ccePropietario $ccePropietario): self
    {
        if (!$this->ccePropietarios->contains($ccePropietario)) {
            $this->ccePropietarios[] = $ccePropietario;
            $ccePropietario->setCce($this);
        }

        return $this;
    }

    public function removeCcePropietario(ccePropietario $ccePropietario): self
    {
        if ($this->ccePropietarios->removeElement($ccePropietario)) {
            // set the owning side to null (unless already changed)
            if ($ccePropietario->getCce() === $this) {
                $ccePropietario->setCce(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cceMercancias>
     */
    public function getCceMercancias(): Collection
    {
        return $this->cceMercancias;
    }

    public function addCceMercancia(cceMercancias $cceMercancia): self
    {
        if (!$this->cceMercancias->contains($cceMercancia)) {
            $this->cceMercancias[] = $cceMercancia;
            $cceMercancia->setCce($this);
        }

        return $this;
    }

    public function removeCceMercancia(cceMercancias $cceMercancia): self
    {
        if ($this->cceMercancias->removeElement($cceMercancia)) {
            // set the owning side to null (unless already changed)
            if ($cceMercancia->getCce() === $this) {
                $cceMercancia->setCce(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, cceMercanciasDetalle>
     */
    public function getCceMercanciasDetalles(): Collection
    {
        return $this->cceMercanciasDetalles;
    }

    public function addCceMercanciasDetalle(cceMercanciasDetalle $cceMercanciasDetalle): self
    {
        if (!$this->cceMercanciasDetalles->contains($cceMercanciasDetalle)) {
            $this->cceMercanciasDetalles[] = $cceMercanciasDetalle;
            $cceMercanciasDetalle->setCce($this);
        }

        return $this;
    }

    public function removeCceMercanciasDetalle(cceMercanciasDetalle $cceMercanciasDetalle): self
    {
        if ($this->cceMercanciasDetalles->removeElement($cceMercanciasDetalle)) {
            // set the owning side to null (unless already changed)
            if ($cceMercanciasDetalle->getCce() === $this) {
                $cceMercanciasDetalle->setCce(null);
            }
        }

        return $this;
    }
}
