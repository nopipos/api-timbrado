<?php

namespace App\Entity;

use App\Repository\FacturaDetalleImpuestoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturaDetalleImpuestoRepository::class)
 * @ORM\Table (name="factura_detalle_impuesto")
 */
class FacturaDetalleImpuesto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=FacturasDetalle::class, inversedBy="impuestos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $detalle;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="facturas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $factura;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empresas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $tipo_impuesto;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $tasa_cuota;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $base;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $porcentaje;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $importe;

    /**
     * @ORM\Column(type="integer")
     */
    private $traslado;


    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDetalle(): ?FacturasDetalle
    {
        return $this->detalle;
    }

    public function setDetalle(?FacturasDetalle $detalle): self
    {
        $this->detalle = $detalle;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTipoImpuesto()
    {
        return $this->tipo_impuesto;
    }

    /**
     * @param mixed $tipo_impuesto
     */
    public function setTipoImpuesto($tipo_impuesto): void
    {
        $this->tipo_impuesto = $tipo_impuesto;
    }

    /**
     * @return mixed
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param mixed $base
     */
    public function setBase($base): void
    {
        $this->base = $base;
    }

    /**
     * @return mixed
     */
    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    /**
     * @param mixed $porcentaje
     */
    public function setPorcentaje($porcentaje): void
    {
        $this->porcentaje = $porcentaje;
    }

    /**
     * @return mixed
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * @param mixed $importe
     */
    public function setImporte($importe): void
    {
        $this->importe = $importe;
    }

    /**
     * @return mixed
     */
    public function getTasaCuota()
    {
        return $this->tasa_cuota;
    }

    /**
     * @param mixed $tasa_cuota
     */
    public function setTasaCuota($tasa_cuota): void
    {
        $this->tasa_cuota = $tasa_cuota;
    }

    /**
     * @return mixed
     */
    public function getTraslado()
    {
        return $this->traslado;
    }

    /**
     * @param mixed $traslado
     */
    public function setTraslado($traslado): void
    {
        $this->traslado = $traslado;
    }



    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

}