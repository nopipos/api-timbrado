<?php

namespace App\Entity;

use App\Repository\ProductosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductosRepository::class)
 */
class Productos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="productos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $empresa;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $clave_sat;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $unidad;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precio_unitario;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $unidad_sat;

    /**
     * @ORM\Column(type="integer")
     */
    private $iva;

    /**
     * @ORM\Column(type="integer")
     */
    private $ieps;

    /**
     * @ORM\Column(type="integer")
     */
    private $ret_iva;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $fraccion_arancelaria;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true,"default" = 1})
     */
    private $estatus;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PorcentajeIva;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PorcentajeIeps;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $PorcentajeRetIva;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(?string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getClaveSat(): ?string
    {
        return $this->clave_sat;
    }

    public function setClaveSat(?string $clave_sat): self
    {
        $this->clave_sat = $clave_sat;

        return $this;
    }

    public function getUnidad(): ?string
    {
        return $this->unidad;
    }

    public function setUnidad(?string $unidad): self
    {
        $this->unidad = $unidad;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPrecioUnitario(): ?float
    {
        return $this->precio_unitario;
    }

    public function setPrecioUnitario(?float $precio_unitario): self
    {
        $this->precio_unitario = $precio_unitario;

        return $this;
    }

    public function getUnidadSat(): ?string
    {
        return $this->unidad_sat;
    }

    public function setUnidadSat(string $unidad_sat): self
    {
        $this->unidad_sat = $unidad_sat;

        return $this;
    }

    public function getIva(): ?int
    {
        return $this->iva;
    }

    public function setIva(int $iva): self
    {
        $this->iva = $iva;

        return $this;
    }

    public function getIeps(): ?int
    {
        return $this->ieps;
    }

    public function setIeps(int $ieps): self
    {
        $this->ieps = $ieps;

        return $this;
    }

    public function getRetIva(): ?int
    {
        return $this->ret_iva;
    }

    public function setRetIva(int $ret_iva): self
    {
        $this->ret_iva = $ret_iva;

        return $this;
    }

    public function getFraccionArancelaria(): ?string
    {
        return $this->fraccion_arancelaria;
    }

    public function setFraccionArancelaria(?string $fraccion_arancelaria): self
    {
        $this->fraccion_arancelaria = $fraccion_arancelaria;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    public function getPorcentajeIva(): ?float
    {
        return $this->PorcentajeIva;
    }

    public function setPorcentajeIva(?float $PorcentajeIva): self
    {
        $this->PorcentajeIva = $PorcentajeIva;

        return $this;
    }

    public function getPorcentajeIeps(): ?float
    {
        return $this->PorcentajeIeps;
    }

    public function setPorcentajeIeps(?float $PorcentajeIeps): self
    {
        $this->PorcentajeIeps = $PorcentajeIeps;

        return $this;
    }

    public function getPorcentajeRetIva(): ?float
    {
        return $this->PorcentajeRetIva;
    }

    public function setPorcentajeRetIva(?float $PorcentajeRetIva): self
    {
        $this->PorcentajeRetIva = $PorcentajeRetIva;

        return $this;
    }

    public function getAttributes(){
        return [
            'Id'=>$this->getId(),
            'clave'=>$this->getClave(),
            'clave_sat'=>$this->getClaveSat(),
            'unidad'=>$this->getUnidad(),
            'descripcion'=>$this->getDescripcion(),
            'precio_unitario'=>$this->getPrecioUnitario(),
            'unidad_sat'=>$this->getUnidadSat(),
            'iva'=>$this->getIva(),
            'ieps'=>$this->getIeps(),
            'ret_iva'=>$this->getRetIva(),
            'fraccion_arancelaria'=>$this->getFraccionArancelaria(),
            'PorcentajeIeps'=>$this->getPorcentajeIeps(),
            'estatus'=>$this->getEstatus(),
            'PorcentajeIva'=>$this->getPorcentajeIva(),
            'PorcentajeRetIva'=>$this->getPorcentajeRetIva()
        ];
    }
}
