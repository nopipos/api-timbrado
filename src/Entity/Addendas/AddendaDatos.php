<?php

namespace App\Entity\Addendas;

use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Repository\Addendas\AddendaDatosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AddendaDatosRepository::class)
 */
class AddendaDatos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="addendaDatos")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="addendaDatos")
     */
    private $factura;

    /**
     * @ORM\ManyToOne(targetEntity=AddendaClientes::class, inversedBy="addendaDatos")
     */
    private $addenda;

    /**
     * @ORM\ManyToOne(targetEntity=AddendaEstructura::class, inversedBy="addendaDatos")
     */
    private $estructura;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $valor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getFactura(): ?Facturas
    {
        return $this->factura;
    }

    public function setFactura(?Facturas $factura): self
    {
        $this->factura = $factura;

        return $this;
    }

    public function getAddenda(): ?AddendaClientes
    {
        return $this->addenda;
    }

    public function setAddenda(?AddendaClientes $addenda): self
    {
        $this->addenda = $addenda;

        return $this;
    }

    public function getEstructura(): ?AddendaEstructura
    {
        return $this->estructura;
    }

    public function setEstructura(?AddendaEstructura $estructura): self
    {
        $this->estructura = $estructura;

        return $this;
    }

    public function getValor(): ?string
    {
        return $this->valor;
    }

    public function setValor(?string $valor): self
    {
        $this->valor = $valor;

        return $this;
    }
}
