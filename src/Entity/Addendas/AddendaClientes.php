<?php

namespace App\Entity\Addendas;

use App\Repository\Addendas\AddendaClientesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AddendaClientesRepository::class)
 */
class AddendaClientes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=14)
     */
    private $rfc;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clase;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=AddendaEmpresas::class, mappedBy="addenda")
     */
    private $addendaEmpresas;

    /**
     * @ORM\OneToMany(targetEntity=AddendaEstructura::class, mappedBy="addenda")
     */
    private $addendaEstructuras;

    /**
     * @ORM\OneToMany(targetEntity=AddendaDatos::class, mappedBy="addenda")
     */
    private $addendaDatos;

    public function __construct()
    {
        $this->addendaEmpresas = new ArrayCollection();
        $this->addendaEstructuras = new ArrayCollection();
        $this->addendaDatos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getClase(): ?string
    {
        return $this->clase;
    }

    public function setClase(string $clase): self
    {
        $this->clase = $clase;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection<int, AddendaEmpresas>
     */
    public function getAddendaEmpresas(): Collection
    {
        return $this->addendaEmpresas;
    }

    public function addAddendaEmpresa(AddendaEmpresas $addendaEmpresa): self
    {
        if (!$this->addendaEmpresas->contains($addendaEmpresa)) {
            $this->addendaEmpresas[] = $addendaEmpresa;
            $addendaEmpresa->setAddenda($this);
        }

        return $this;
    }

    public function removeAddendaEmpresa(AddendaEmpresas $addendaEmpresa): self
    {
        if ($this->addendaEmpresas->removeElement($addendaEmpresa)) {
            // set the owning side to null (unless already changed)
            if ($addendaEmpresa->getAddenda() === $this) {
                $addendaEmpresa->setAddenda(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AddendaEstructura>
     */
    public function getAddendaEstructuras(): Collection
    {
        return $this->addendaEstructuras;
    }

    public function addAddendaEstructura(AddendaEstructura $addendaEstructura): self
    {
        if (!$this->addendaEstructuras->contains($addendaEstructura)) {
            $this->addendaEstructuras[] = $addendaEstructura;
            $addendaEstructura->setAddenda($this);
        }

        return $this;
    }

    public function removeAddendaEstructura(AddendaEstructura $addendaEstructura): self
    {
        if ($this->addendaEstructuras->removeElement($addendaEstructura)) {
            // set the owning side to null (unless already changed)
            if ($addendaEstructura->getAddenda() === $this) {
                $addendaEstructura->setAddenda(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, AddendaDatos>
     */
    public function getAddendaDatos(): Collection
    {
        return $this->addendaDatos;
    }

    public function addAddendaDato(AddendaDatos $addendaDato): self
    {
        if (!$this->addendaDatos->contains($addendaDato)) {
            $this->addendaDatos[] = $addendaDato;
            $addendaDato->setAddenda($this);
        }

        return $this;
    }

    public function removeAddendaDato(AddendaDatos $addendaDato): self
    {
        if ($this->addendaDatos->removeElement($addendaDato)) {
            // set the owning side to null (unless already changed)
            if ($addendaDato->getAddenda() === $this) {
                $addendaDato->setAddenda(null);
            }
        }

        return $this;
    }
}
