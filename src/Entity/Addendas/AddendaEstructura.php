<?php

namespace App\Entity\Addendas;

use App\Repository\Addendas\AddendaEstructuraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AddendaEstructuraRepository::class)
 */
class AddendaEstructura
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=AddendaClientes::class, inversedBy="addendaEstructuras")
     */
    private $addenda;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $tipo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $requerido;

    /**
     * @ORM\Column(type="boolean")
     */
    private $recursivo;

    /**
     * @ORM\Column(type="integer")
     */
    private $numeroRenglon;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $tooltip;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $origen;

    /**
     * @ORM\Column(type="integer")
     */
    private $posicion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=AddendaDatos::class, mappedBy="estructura")
     */
    private $addendaDatos;

    public function __construct()
    {
        $this->addendaDatos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddenda(): ?AddendaClientes
    {
        return $this->addenda;
    }

    public function setAddenda(?AddendaClientes $addenda): self
    {
        $this->addenda = $addenda;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getRequerido(): ?bool
    {
        return $this->requerido;
    }

    public function setRequerido(bool $requerido): self
    {
        $this->requerido = $requerido;

        return $this;
    }

    public function getRecursivo(): ?bool
    {
        return $this->recursivo;
    }

    public function setRecursivo(bool $recursivo): self
    {
        $this->recursivo = $recursivo;

        return $this;
    }

    public function getNumeroRenglon(): ?int
    {
        return $this->numeroRenglon;
    }

    public function setNumeroRenglon(int $numeroRenglon): self
    {
        $this->numeroRenglon = $numeroRenglon;

        return $this;
    }

    public function getTooltip(): ?string
    {
        return $this->tooltip;
    }

    public function setTooltip(?string $tooltip): self
    {
        $this->tooltip = $tooltip;

        return $this;
    }

    public function getOrigen(): ?string
    {
        return $this->origen;
    }

    public function setOrigen(?string $origen): self
    {
        $this->origen = $origen;

        return $this;
    }

    public function getPosicion(): ?int
    {
        return $this->posicion;
    }

    public function setPosicion(int $posicion): self
    {
        $this->posicion = $posicion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection<int, AddendaDatos>
     */
    public function getAddendaDatos(): Collection
    {
        return $this->addendaDatos;
    }

    public function addAddendaDato(AddendaDatos $addendaDato): self
    {
        if (!$this->addendaDatos->contains($addendaDato)) {
            $this->addendaDatos[] = $addendaDato;
            $addendaDato->setEstructura($this);
        }

        return $this;
    }

    public function removeAddendaDato(AddendaDatos $addendaDato): self
    {
        if ($this->addendaDatos->removeElement($addendaDato)) {
            // set the owning side to null (unless already changed)
            if ($addendaDato->getEstructura() === $this) {
                $addendaDato->setEstructura(null);
            }
        }

        return $this;
    }
}
