<?php

namespace App\Entity\Addendas;

use App\Entity\Empresas;
use App\Repository\Addendas\AddendaEmpresasRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AddendaEmpresasRepository::class)
 */
class AddendaEmpresas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="addendaEmpresas")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=AddendaClientes::class, inversedBy="addendaEmpresas")
     */
    private $addenda;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private $estatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getAddenda(): ?AddendaClientes
    {
        return $this->addenda;
    }

    public function setAddenda(?AddendaClientes $addenda): self
    {
        $this->addenda = $addenda;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }
}
