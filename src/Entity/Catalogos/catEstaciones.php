<?php

namespace App\Entity\Catalogos;

use App\Repository\Catalogos\catEstacionesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=catEstacionesRepository::class)
 */
class catEstaciones
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $claveTransporte;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    private $nacionalidad;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $iata;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ferrea;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getClaveTransporte(): ?string
    {
        return $this->claveTransporte;
    }

    public function setClaveTransporte(string $claveTransporte): self
    {
        $this->claveTransporte = $claveTransporte;

        return $this;
    }

    public function getNacionalidad(): ?string
    {
        return $this->nacionalidad;
    }

    public function setNacionalidad(?string $nacionalidad): self
    {
        $this->nacionalidad = $nacionalidad;

        return $this;
    }

    public function getIata(): ?string
    {
        return $this->iata;
    }

    public function setIata(?string $iata): self
    {
        $this->iata = $iata;

        return $this;
    }

    public function getFerrea(): ?string
    {
        return $this->ferrea;
    }

    public function setFerrea(?string $ferrea): self
    {
        $this->ferrea = $ferrea;

        return $this;
    }

    public function getAttributes(){

        return [
            'Id'=>$this->getClave(),
            'descripcion'=>$this->getDescripcion(),
        ];

    }
}
