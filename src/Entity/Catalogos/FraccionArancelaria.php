<?php

namespace App\Entity\Catalogos;

use App\Repository\Catalogos\FraccionArancelariaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FraccionArancelariaRepository::class)
 */
class FraccionArancelaria
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private $clave;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }
    public function getAttributes(){
        return [
            'id'=>$this->getId(),
            'clave'=>$this->getClave(),
            'descripcion'=>$this->getDescripcion(),

        ];
    }
}
