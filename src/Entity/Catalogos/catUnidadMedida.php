<?php

namespace App\Entity\Catalogos;

use App\Repository\Catalogos\catUnidadMedidaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=catUnidadMedidaRepository::class)
 */
class catUnidadMedida
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $descripcion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }
    public function getAttributes(){
        return [
            'id'=>$this->getId(),
            'clave'=>$this->getClave(),
            'descripcion'=>$this->getDescripcion(),

        ];
    }
    
}
