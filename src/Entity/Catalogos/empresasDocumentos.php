<?php

namespace App\Entity\Catalogos;

use App\Entity\Empresas;
use App\Repository\Catalogos\empresasDocumentosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=empresasDocumentosRepository::class)
 */
class empresasDocumentos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Empresas::class, inversedBy="empresasDocumentos")
     */
    private $empresa;

    /**
     * @ORM\ManyToOne(targetEntity=CatTipoDoc::class, inversedBy="empresasDocumentos")
     */
    private $documento;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmpresa(): ?Empresas
    {
        return $this->empresa;
    }

    public function setEmpresa(?Empresas $empresa): self
    {
        $this->empresa = $empresa;

        return $this;
    }

    public function getDocumento(): ?CatTipoDoc
    {
        return $this->documento;
    }

    public function setDocumento(?CatTipoDoc $documento): self
    {
        $this->documento = $documento;

        return $this;
    }

    public function getAttributes(){
        return [
            'Id'=>$this->getId(),
        ];
    }

    

}
