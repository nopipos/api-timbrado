<?php

namespace App\Entity\Catalogos;

use App\Repository\Catalogos\catClaveUnidadPesoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=catClaveUnidadPesoRepository::class)
 */
class catClaveUnidadPeso
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=700)
     */
    private $descripcion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getAttributes(){

        return [
            'c_unidad'=>$this->getClave(),
            'descripcion'=>$this->getDescripcion(),
        ];
    }
}
