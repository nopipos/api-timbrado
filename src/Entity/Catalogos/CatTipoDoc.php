<?php

namespace App\Entity\Catalogos;

use App\Repository\Catalogos\catTipoDocRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=catTipoDocRepository::class)
 */
class CatTipoDoc
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $clave;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $estatus;

    /**
     * @ORM\OneToMany(targetEntity=empresasDocumentos::class, mappedBy="documento")
     */
    private $empresasDocumentos;

    public function __construct()
    {
        $this->empresasDocumentos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClave(): ?string
    {
        return $this->clave;
    }

    public function setClave(string $clave): self
    {
        $this->clave = $clave;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstatus(): ?int
    {
        return $this->estatus;
    }

    public function setEstatus(int $estatus): self
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * @return Collection<int, empresasDocumentos>
     */
    public function getEmpresasDocumentos(): Collection
    {
        return $this->empresasDocumentos;
    }

    public function addEmpresasDocumento(empresasDocumentos $empresasDocumento): self
    {
        if (!$this->empresasDocumentos->contains($empresasDocumento)) {
            $this->empresasDocumentos[] = $empresasDocumento;
            $empresasDocumento->setDocumento($this);
        }

        return $this;
    }

    public function removeEmpresasDocumento(empresasDocumentos $empresasDocumento): self
    {
        if ($this->empresasDocumentos->removeElement($empresasDocumento)) {
            // set the owning side to null (unless already changed)
            if ($empresasDocumento->getDocumento() === $this) {
                $empresasDocumento->setDocumento(null);
            }
        }

        return $this;
    }
}
