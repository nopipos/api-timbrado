<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="facturas_canceladas")
 */
class FacturasCanceladas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Facturas::class, inversedBy="facturas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $factura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="motivo", type="string", length=150, nullable=true)
     */
    private $motivo;

    /**
     * @var string
     * 'pendiente', 'rechazada', 'vencida', 'cancelada', 'error'
     * @ORM\Column(name="estatus", type="string", length=10, nullable=false)
     */
    private $estatus;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="string", length=450, nullable=true)
     */
    private $info;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_peticion", type="datetime", nullable=false)
     */
    private $fechaPeticion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_actualizacion", type="datetime", nullable=true)
     */
    private $fechaActualizacion;

    public function __construct()
    {
        $this->fechaPeticion = new \DateTime();
        $this->estatus = 'pendiente';
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * @param mixed $factura
     */
    public function setFactura($factura): void
    {
        $this->factura = $factura;
    }

    /**
     * @return string|null
     */
    public function getMotivo(): ?string
    {
        return $this->motivo;
    }

    /**
     * @param string|null $motivo
     */
    public function setMotivo(?string $motivo): void
    {
        $this->motivo = $motivo;
    }

    /**
     * @return string
     */
    public function getEstatus(): string
    {
        return $this->estatus;
    }

    /**
     * @param string $estatus
     */
    public function setEstatus(string $estatus): void
    {
        $this->estatus = $estatus;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo(string $info): void
    {
        $this->info = $info;
    }

    /**
     * @return \DateTime
     */
    public function getFechaPeticion(): \DateTime
    {
        return $this->fechaPeticion;
    }

    /**
     * @param \DateTime $fechaPeticion
     */
    public function setFechaPeticion(\DateTime $fechaPeticion): void
    {
        $this->fechaPeticion = $fechaPeticion;
    }

    /**
     * @return \DateTime
     */
    public function getFechaActualizacion(): \DateTime
    {
        return $this->fechaActualizacion;
    }

    /**
     * @param \DateTime $fechaActualizacion
     */
    public function setFechaActualizacion(\DateTime $fechaActualizacion): void
    {
        $this->fechaActualizacion = $fechaActualizacion;
    }
}
