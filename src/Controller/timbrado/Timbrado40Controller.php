<?php


namespace App\Controller\timbrado;

use app\components\helpers\mail\enviarMail;
use App\Entity\response\MensajeTimbrado;
use App\Service\Cfdi40\Impuestos;
use App\Service\Cfdi40\Pdf;
use App\Service\Cfdi40\timbrado\Timbrar;
use App\Service\Cfdi40\Validaciones;
use App\Service\Cfdi40\Xml40;
use App\Service\Login;
use App\Service\Utils\amazon\S3;
use App\Service\Utils\Mail\SendMail;
use App\Service\Utils\Peticiones;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Timbrado40Controller extends AbstractController
{

    /**
     * @Route("/app/cfdi40/timbrar", name="facturas_timbrar40", methods={"POST","GET"})
     */
    public function index(Request $request): Response
    {

      if($request->isMethod("GET")){
        return $this->json([
            'mensaje' => 'Metodo no disponible',
            
        ],403);
      }

        //$entityManager = $this->getDoctrine()->getManager('portal');
        $doctrine = $this->getDoctrine();

        $datos = json_decode($request->getContent(),true);

        $resultLogin = Login::logueo($doctrine,$datos);

        switch ($resultLogin['estatus']){
            case 0:
                return $this->json([
                    'mensaje' => 'datos de acceo incorrectos',
                    'codigo' => '402',
                ],200);
                break;
            case 2:
                return $this->json([
                    'mensaje' => 'La informacion del  RFC emisor no se encontro',
                    'codigo' => '402',
                ],200);
                break;

            case 3:
                return $this->json([
                    'mensaje' => 'El usuario no esta relacionado a la empresa emisora, favor de contactar a tu administrador',
                    'codigo' => '402',
                ],200);
                break;
            case 4:
                return $this->json([
                    'mensaje' => 'El usuario no tiene configurado sus CSD favor de contactar a soporte',
                    'codigo' => '402',
                ],200);
                break;
            case 5:
                return $this->json([
                    'mensaje' => 'El usuario no tiene configurado un PAC favor de contactar a soporte',
                    'codigo' => '402',
                ],200);
                break;
            case 6:
                    return $this->json([
                        'mensaje' => 'El pac que tiene asignado el usuario: '.$resultLogin['empresa']->getPac1().', no esta dado de alta, favor de contactar a soporte',
                        'codigo' => '402',
                    ],200);
                    break;

        }
        #verifico si la peticion viene con XML

        $xml = "";
        $xmlObj = new Xml40();

        if(isset($datos['xml'])){

            $xml = $datos['xml'];
            #armo el XML
        }else{

            $xml = $xmlObj->generarXml($datos,$doctrine);
        }


        #verifico los impuestos
        $impuestos = new Impuestos($xml,$doctrine);

        
        if($impuestos->verifyImpuestos() == 0)
            $xml = $impuestos->addImpuestos($xml);


        #aplico validaciones como de serie y folio para no repetir el timbrado

        
        $validaciones = new Validaciones($xml,$resultLogin,(isset($datos['esPortal']) &&  $datos['esPortal']==true));
        $validaciones->init();

        $messageVal = $validaciones->validacionSeriaAndFolio($datos,$doctrine);

        if( is_a($messageVal,'App\Entity\response\MensajeTimbrado') ){

            return $this->json($messageVal);
            #se regresa el mxl y pdf timbrados
        }

        #guardo la peticion
        $resultadoPeticion = Peticiones::guardarPeticion($xml,$doctrine,$datos,$resultLogin);

        #le genero el sello
        $xml = $xmlObj->sellarXml40($resultadoPeticion['peticion'],$resultadoPeticion['resultLogin'],$xml);

        #mando a timbrar
        $resultaddoTimbre = Timbrar::timbrar($xml,$resultadoPeticion['pacs'],$resultadoPeticion['peticion'],$resultLogin,$doctrine);

        $response = new MensajeTimbrado();

        if($resultaddoTimbre == null){

            $response->setMensaje("Error econ el PAC");
            $response->setCodigo(5000);
            $response->setUuid("");
            $response->setXmlData($xml);
            $response->setSelloSAT("");
            $response->setSelloCFD("");
            $response->setResultado(false);
            $response->setPdf("");
            $response->setNoCertificadoSAT("");
            $response->setFechaTimbrado(null);
            $response->setRfcProvCertif("");
            $response->setPath("");

            return $this->json($response,200);

        }

        $rutaPdf = "";
         if($resultaddoTimbre['codigo'] != "200") {
            $response->setMensaje($resultaddoTimbre['mensaje']);
            $response->setCodigo($resultaddoTimbre['codigo']);
            $response->setUuid($resultaddoTimbre['uuid'] == null ? "":$resultaddoTimbre['uuid']);
            $response->setXmlData($resultaddoTimbre['xmlData'] == null ? "":$resultaddoTimbre['xmlData']);
            $response->setSelloSAT($resultaddoTimbre['selloSAT'] == null ? "":$resultaddoTimbre['selloSAT']);
            $response->setSelloCFD($resultaddoTimbre['selloCFD'] == null ? "":$resultaddoTimbre['selloCFD']);
            $response->setResultado($resultaddoTimbre['resultado']);
            $response->setPath("");
             $pdfBase64 = "";
            if($resultaddoTimbre['codigo'] == "202"){
                #intento recuperar el pdf de amazon
                $fecha = $datos['Comprobante']['Fecha'];

                $fechaDoc = str_replace(" ","T",$fecha);
                $fechaExplo = explode("T", $fechaDoc);
                $fechaExplo = explode("-", $fechaExplo[0]);

                $año = $fechaExplo[0];
                $mes = $fechaExplo[1];
                $dia= $fechaExplo[2];
                $rfcEmisor = $datos['Comprobante']['Emisor']['Rfc'];
                $tipoComprobante = $datos['Comprobante']['TipoDeComprobante'];
                $pathFactura = "clickfactura/".$rfcEmisor."/emision/";
                if($tipoComprobante == "I")
                    $pathFactura = $pathFactura."factura/";
                elseif($tipoComprobante == "E")
                    $pathFactura = $pathFactura."notaCredito/";
                else
                    $pathFactura = $pathFactura."factura/";
        

                $rutaPdf = $pathFactura.$año."/".$mes."/".$dia."/".$resultaddoTimbre['uuid'].".pdf";
                
                $pdfBase64 = S3::obtener($rutaPdf);
                if($pdfBase64 == null)
                    $pdfBase64 = "";

            }
            $response->setPdf($pdfBase64);
            $response->setNoCertificadoSAT($resultaddoTimbre['noCertificadoSAT'] == null ? "":$resultaddoTimbre['noCertificadoSAT']);
            $response->setFechaTimbrado($resultaddoTimbre['FechaTimbrado'] == null ? "":$resultaddoTimbre['FechaTimbrado']);
            $response->setRfcProvCertif($resultaddoTimbre['rfcProvCertif'] == null ? "":$resultaddoTimbre['rfcProvCertif']);
            $response->setPath($rutaPdf);

            return $this->json($response,200);
        }

        #crear factura en BD

        #crear Addenda

        

        $pdfBase64 = "";
        if($resultaddoTimbre['codigo'] == "200") {

            #guardar pdf
            $pdfBase64 = Pdf::crear($resultaddoTimbre,$doctrine,$resultLogin,$datos['Comprobante']['Receptor']['Rfc'],$datos['Comprobante']['LugarExpedicion'],$datos['Comprobante']['Fecha']);

            #guardo la peticion timbrada
            $resultadoPeticionTimbre = Peticiones::guardarPeticionTimbre($resultaddoTimbre,$resultadoPeticion['peticion'],$segundos,$doctrine,$pdfBase64);

            $response->setMensaje($resultaddoTimbre['mensaje']);
            $response->setCodigo($resultaddoTimbre['codigo']);
            $response->setUuid($resultaddoTimbre['uuid'] == null ? "":$resultaddoTimbre['uuid']);
            $response->setXmlData($resultaddoTimbre['xmlData']);
            $response->setSelloSAT($resultaddoTimbre['selloSAT']);
            $response->setSelloCFD($resultaddoTimbre['selloCFD']);
            $response->setResultado($resultaddoTimbre['resultado']);
            $response->setPdf($pdfBase64);
            $response->setNoCertificadoSAT($resultaddoTimbre['noCertificadoSAT']);
            $response->setFechaTimbrado($resultaddoTimbre['FechaTimbrado']);
            $response->setRfcProvCertif($resultaddoTimbre['rfcProvCertif']);
            $response->setPath($resultadoPeticionTimbre['path']);


            $mails = [];

            file_put_contents("info.txt",print_r($datos,true));
            if(isset($datos['datosAdicionales'])){
                foreach ($datos['datosAdicionales'] as $key ) {

                    file_put_contents("infoFor.txt",print_r($key,true));

                    if(isset($key['mail'])){
                        file_put_contents("infomailKey.txt",print_r($key,true));
                        $mails = explode(",", $key['mail']);
                    }
                        
                }
            }
            $files = [
                [
                    'name'=>base64_encode('xml.xml'),
                    'body'=>$resultaddoTimbre['xmlData'],
                ],[
                    'name'=>base64_encode('pdf.pdf'),
                    'body'=>$pdfBase64,
                ]
            ];

            file_put_contents("infomail.txt",print_r($mails,true));

            if(count($mails)> 0)
                SendMail::enviarMail($mails,base64_encode("html body"),null,base64_encode("factura generada"),$files,base64_encode("Webservice"));
            return $this->json($response,200);
        }

        #mandar Mail
        
    }

}