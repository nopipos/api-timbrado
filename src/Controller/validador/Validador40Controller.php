<?php


namespace App\Controller\validador;

use App\Entity\Clientes;
use App\Entity\Facturas;
use App\Entity\FacturasSat;
use App\Entity\FacturasTimbre;
use App\Entity\Sucursales;
use App\Entity\Timbre\ValPeticiones;
use App\Entity\Timbre\ValPeticionesSat;
use App\Service\Cfdi40\Validador\ConsultaSAT;
use App\Service\Cfdi40\Validador\ResponseValidador;
use App\Service\Cfdi40\Validador\Sello;
use App\Service\Cfdi40\Validador\ValidarXSD;
use App\Service\Login;
use App\Service\Utils\amazon\S3;
use App\Service\Utils\XML2Array;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Validador40Controller extends AbstractController
{

    /**
     * @Route("/app/cfdi40/validar", name="facturas_validar40", methods={"POST"})
     */
    public function index(Request $request): Response
    {
        error_reporting(E_ALL ^ E_WARNING);
        $time = date("Y-m-d H:i:s");
        //$entityManager = $this->getDoctrine()->getManager('portal');
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();
        $datos = json_decode($request->getContent(),true);

        $resultLogin = Login::logueo($doctrine,$datos);

        switch ($resultLogin['estatus']){
            case 0:
                return $this->json([
                    'mensaje' => 'datos de acceo incorrectos',
                    'codigo' => '402',
                ],200);
                break;
            case 2:
                return $this->json([
                    'mensaje' => 'La informacion del  RFC emisor no se encontro',
                    'codigo' => '402',
                ],200);
                break;

            case 3:
                return $this->json([
                    'mensaje' => 'El usuario no esta relacionado a la empresa emisora, favor de contactar a tu administrador',
                    'codigo' => '402',
                ],200);
                break;

        }
        $respuesta = new ResponseValidador();

        #extraigo la informacion
        $xmlCadena = base64_decode($datos['xml']);

        #si el xml trae addenda le quito esa informacion
        $datos['xml'] = preg_replace('{<Addenda.*/Addenda>}is', '', base64_decode($datos['xml']));
        $datos['xml'] = preg_replace('{<cfdi:Addenda.*/cfdi:Addenda>}is', '', $datos['xml']);
        $datos['xml'] = preg_replace('{<cfdi:Addenda />}is', '', $datos['xml']);
        $datos['xml'] = preg_replace('{<cfdi:Addenda/>}is', '', $datos['xml']);
        $datos['xml'] = base64_encode($datos['xml']);

        $xml = new \DOMDocument();
        
        if(!$xml->loadXML($xmlCadena)){

            $response = ['error'=>'El xml no se puede leer'];
            return $this->json($response,200);
        }

        $root = $xml->getElementsByTagName('Comprobante')->item(0);

        $version = $root->getAttribute("Version");
        $serie = $root->getAttribute('Serie');
        $folio = $root->getAttribute('Folio');
        $fecha = $root->getAttribute('Fecha');
        $tipo = $root->getAttribute('TipoDeComprobante');
        $formaPago = $root->getAttribute('FormaPago');
        $condicionesPago = $root->getAttribute('CondicionesDePago');
        $moneda = $root->getAttribute('Moneda');
        $condicionesPago = $root->getAttribute('CondicionesDePago');
        $total = $root->getAttribute('Total');
        $subtotal = $root->getAttribute('SubTotal');
        $metodoPago = $root->getAttribute('MetodoPago');
        $exportacion = $root->getAttribute('Exportacion');

        $fechaExplo = explode("T", $fecha);
        $fechaExplo = explode("-", $fechaExplo[0]);

        $año = $fechaExplo[0];
        $mes = $fechaExplo[1];
        $dia= $fechaExplo[2];

        $Emisor = $root->getElementsByTagName('Emisor')->item(0);
        $rfcEmisor = $Emisor->getAttribute('Rfc');

        $Receptor = $root->getElementsByTagName('Receptor')->item(0);
        $rfcReceptor = $Receptor->getAttribute('Rfc');
        $TFD = $root->getElementsByTagName('TimbreFiscalDigital')->item(0);

        $uuid = "";
        $certificadoSAT = "";
        $fechaTimbrado = "";
        if($TFD != null){
            $uuid = $TFD->getAttribute("UUID");
            $certificadoSAT = $TFD->getAttribute("NoCertificadoSAT");
            $fechaTimbrado = $TFD->getAttribute("FechaTimbrado");

        }
        if($uuid == ""){
            $uuid = rand(99,999);
        }
        
        #obtengo la sucursal
        $sucursalesRepo = $doctrine->getRepository(Sucursales::class);
        
        $sucursal =  $sucursalesRepo->findOneBy(['empresa'=>$resultLogin['empresa']->getId(),'estatus'=>'1']);

        $clientesRepo = $doctrine->getRepository(Clientes::class);
        $cliente =  $clientesRepo->findOneBy(['empresa'=>$resultLogin['empresa']->getId(),'estatus'=>'1']);

        #guardo la info de la factura
        $factura = new Facturas();
        
        $factura->setEmpresa($resultLogin['empresa']);
        $factura->setUsuario($resultLogin['usuario']);
        $factura->setSucursal($sucursal);
        $factura->setCliente($cliente);
        $factura->setSerie($serie);
        $factura->setFolio($folio);
        $factura->setFecha(\DateTime::createFromFormat('Y-m-d H:i:s',str_replace("T"," ",$fecha)));
        $factura->setFormaPago($formaPago);
        $factura->setMetodoPago($metodoPago);
        $factura->setTimbrado(1);
        $factura->setCancelado(0);
        $factura->setTotal($total);
        $factura->setSubtotal($subtotal);
        $factura->setMoneda($moneda);
        $factura->setFechaTimbrado(\DateTime::createFromFormat('Y-m-d H:i:s',str_replace("T"," ",$fechaTimbrado)));
        $factura->setCondicionesPago($condicionesPago);
        $factura->setEstatus(1);
        $factura->setTipo(2);
        $factura->setVersion($version);
        $factura->setTipoComprobante($tipo);
        $factura->setExportacion($exportacion);
        $em->persist($factura);
        $em->flush();


        #guardo la peticion de la validacion
        $peticion = new FacturasTimbre();
        $peticion->setEmpresa($resultLogin['empresa']);
        $peticion->setFactura($factura);
        $peticion->setFechaPeticion(\DateTime::createFromFormat('Y-m-d H:i:s',date("Y-m-d H:i:s")));
        $peticion->setFechaTimbrado(\DateTime::createFromFormat('Y-m-d H:i:s',str_replace("T"," ",$fechaTimbrado)));
        $peticion->setFolioFiscal($uuid);
        
        $em->persist($peticion);
        $em->flush();


        #guardo el xml en amazon
        $rutaMain = "recepcion/xml/".$rfcEmisor."/".$año."/".$año."-".$mes."/".$dia."/".$uuid.".xml";
        S3::crear($rutaMain,$datos['xml']);
        $fecha = str_replace("T"," ",$fecha);


        #valida sello
        $sello = new Sello();
        if(!$sello->validarSello($xmlCadena,$peticion->getId())){

            $respuesta->Codigo = "302";
            $respuesta->Estatus = "";
            $respuesta->EsCancelable = "";
            $respuesta->EstatusCancelacion = "";
            $respuesta->Mensaje = "Sello del XML mal formado.";
            $respuesta->Errores = ["Sello del XML mal formado."];
            return $this->json($respuesta,200);
            
        }

        #verifica en el SAT su estatus
        $resultado = ConsultaSAT::consultarSAT($datos['usuario'],$xmlCadena);

        
        
        if($resultado->Estado == "SIN CP"){
            #mando error
           
            $errores[] = "El archivo es un CP y no tiene documentos relacionados";
            $respuesta->Codigo = "301";
            $respuesta->Estatus = "";
            $respuesta->EsCancelable = "";
            $respuesta->EstatusCancelacion = $resultado->EstatusCancelacion;
            $respuesta->Mensaje = "El archivo es un CP y no tiene documentos relacionados";
            $respuesta->Errores = $errores;

            return $this->json($respuesta,200);

        }

        $estado_cancelacion = "";
        if($resultado->EstatusCancelacion == "Cancelable con aceptación"){
            $estado_cancelacion = "1";
        }
        if($resultado->EstatusCancelacion == ""){
            $estado_cancelacion = "";
        }
        if($resultado->EstatusCancelacion == "No Cancelable"){
            $estado_cancelacion = "3";
        }
        else
            $estado_cancelacion = "2";

        $estado_sat = "";
        if($resultado->Estado == "No Encontrado"){
            $estado_sat = "2";
        }

        if($resultado->Estado == "Vigente"){
            $estado_sat = "1";
        }

        $esCancelado = strpos($resultado->Estado,"Cancelado");

        if($esCancelado){
            $estado_sat = "3";
        }

        $respuesta->EstatusCancelacion = $resultado->EstatusCancelacion;
        $respuesta->Estatus = $resultado->Estado;
        $respuesta->EsCancelable = $resultado->EsCancelable;

        $peticionSat = new FacturasSat();

        $peticionSat->setEmpresa($resultLogin['empresa']);
        $peticionSat->setFactura($factura);
        $peticionSat->setFecha(\DateTime::createFromFormat('Y-m-d H:i:s',date("Y-m-d H:i:s")));
        $peticionSat->setEstadoCancelacion($resultado->EsCancelable);
        $peticionSat->setEstadoSat($resultado->Estado);
        $peticionSat->setEstatus(1);

        $em->persist($peticionSat);
        $em->flush();

        $fechaFinal = date("Y-m-d H:i:s");
        $segundos = strtotime($fechaFinal) - strtotime($time);
        $peticion->setCodigo("301");

        $em->persist($peticion);
        $em->flush();

        if($esCancelado) {

            #mando error
            $respuesta->Mensaje = $resultado->Estado. " XML Cancelado";
            $respuesta->Codigo = "301";
            $respuesta->EstatusCancelacion = $resultado->EstatusCancelacion;
            $respuesta->Estatus =$resultado->Estado;
            $respuesta->EsCancelable = "";
            //$respuesta->Errores = "Xml mal formado.";

            //$errores = array_merge(["El XML no es válido."], $errores);
            $respuesta->Errores = $errores;
            return $this->json($respuesta,200);
        }

        #valida xsd
        $datos = new ValidarXSD();
        $errores = $datos->validar($xmlCadena);
        
        if(count($errores) > 0){
            
            $fechaFinal = date("Y-m-d H:i:s");
            $segundos = strtotime($fechaFinal) - strtotime($time);
    
            $peticion->setCodigo("301");
            $em->persist($peticion);
            $em->flush();   
        }

        $peticion->setCodigo("200");
        $fechaFinal = date("Y-m-d H:i:s");
        $segundos = strtotime($fechaFinal) - strtotime($time);

        if($peticion->existeUUID($doctrine,$uuid)){
            $peticion->setCodigo("202");
        }

        $em->persist($peticion);
        $em->flush();

        $respuesta->Mensaje = "Validado correctamente.";
        $respuesta->Codigo = "200";
        $respuesta->EstatusCancelacion = $resultado->EstatusCancelacion;
        $respuesta->Estatus = $resultado->Estado;
        $respuesta->EsCancelable = $resultado->EsCancelable;
        $respuesta->Errores = [];
        $xmlArray = "";//XML2Array::createArray($xmlCadena);

       
        $respuesta->Xml = $xmlArray;
        return $this->json($respuesta,200);        
    }
}