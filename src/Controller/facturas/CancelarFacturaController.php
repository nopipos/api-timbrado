<?php


namespace App\Controller\facturas;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\Utils\Peticiones;
use App\Service\Cfdi33\timbrado\CancelarService;
use App\Entity\Timbre\FacturasCanceladas;
use Symfony\Component\Routing\Annotation\Route;

class CancelarFacturaController extends AbstractController {
    /**
     * @Route("/app/facturas/cfdi33/cancelar", name="facturas_cancelar", methods={"POST"})
     */
    public function index(Request $request): Response{
        #region token auth
        $getToken = $request->headers->get('authorization');
        if(!$getToken){
            return $this->json(['mensaje' => 'Token inválido'],401);
        }

        $infoToken = explode("Bearer ",$getToken);
        $serverToken = 'eyJhbGciOiJIUzI1NiJ9.eyJhcGlFbWlzb3IiOiJwb3J0YWxDbGlja1YzIiwiVXNlcm5hbWUiOiJwb3J0YWxWMyIsIlBlcm1pc3Npb25zIjoiY2FsbCBtZXRob2RzIiwiaWF0IjoxNjM4OTQ3NzIyfQ.xChXMnCavkmiXjKxeXoh99-Oe43_LfBlXenXhb9hl3Y';
        if($infoToken[1] != $serverToken){
            return $this->json(['mensaje' => 'Token inválido.'],401);
        }
        #endregion

        #region variables y validaciones
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager('timbre');

        $info = json_decode($request->getContent(),true);

        if(empty($info['uuid']) || empty($info['password']) || empty($info['rfc']) || empty($info['b64Cer']) || empty($info['b64Key']) || empty($info['motivo'])){
            return $this->json(['mensaje' => 'Parametros inválidos'],406);
        }
        #endregion

        #region envio a servicio para saber los pacs disponibles y valida que sea ok
        $pacs = Peticiones::getPacsEmpresa(NULL, $info['rfc'], $doctrine);
        if(!$pacs['success']){
            return $this->json(['mensaje' => $pacs['msg']],409);
        }
        #endregion

        #region inserta log
        $addLog = new FacturasCanceladas();
        $addLog->setUuid($info['uuid']);
        $addLog->setEmisor($info['rfc']);
        $em->persist($addLog);
        $em->flush();
        #endregion

        #region envia a serviicio para conectar con el pack, valida y hace response
        $response = CancelarService::cancelar($info, $pacs['msg'], $doctrine);

        if($response['success']){
            $addLog->setPac($response['pac']);
            $addLog->setAcuse($response['msg']['acuse']);
            $addLog->setEstatus('success');
            $addLog->setInfo($response['msg']['uuid']);
            $addLog->setFechaResponse(new \Datetime());
            $em->persist($addLog);
            $em->flush();
            return $this->json($response['msg'],200);
        }else{
            $addLog->setPac($response['pac']);
            $addLog->setEstatus('error');
            $addLog->setInfo(substr($response['msg'], 0, 450));
            $addLog->setFechaResponse(new \Datetime());
            $em->persist($addLog);
            $em->flush();
            return $this->json(['mensaje' => $response['msg']],409);
        }
        #endregion
    }

}
