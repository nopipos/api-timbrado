<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    public function index(): Response
    {

        return $this->json(["Aplicativos de timbrado"]);
    }


    /**
    * @Route("/test/api", name="test_logger", methods={"GET","POST","PUT","PATCH","DELETE"})
    */
    public function logger(Request $request): Response{
        $methodType = $request->getMethod();
        switch ($methodType){
            case "GET":
                return $this->json(['message' => 'metodo get OK'],200);
            case "POST":
                return $this->json(['message' => 'metodo post OK'],200);
            case "PUT":
                return $this->json(['message' => 'metodo put OK'],200);
            case "PATCH":
                return $this->json(['message' => 'metodo patch OK'],200);
            case "DELETE":
                return $this->json(['message' => 'metodo delete OK'],200);
            default:
                return $this->json(['message' => 'Metodo no permitido'],200);
        }
    }

}
