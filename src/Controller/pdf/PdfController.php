<?php


namespace App\Controller\pdf;


use App\Service\Utils\pdf\GenerarPdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PdfController extends AbstractController
{
    /**
     * @Route("/app/pdf/crearModel", name="pdf_from_model", methods={"POST"})
     */
    public function index(Request $request): Response{

        $pdfBase64 = "";
        $token = $request->headers->get('token');

        if($token != "05217fc30d38eeda8212aef7305b3ed260eed2b1"){
            return $this->json([
                'message' => "token invalido",
            ],401);
        }

        $datos = json_decode($request->getContent(),true);

        if(!is_array($datos)){
            return $this->json([
                'message' => "se necesita una estructura valida",
            ],401);
        }

        $params = [];

        $pdf = GenerarPdf::generaPdf33("CFDI33",$idUsuario,$tipoComprobante,$params);

        return $this->json(['pdf'=>$pdfBase64],200);
    }

    /**
     * @Route("/app/pdf/crearXml", name="pdf_from_xml", methods={"POST"})
     */
    public function pdfFromXml(Request $request): Response{

        $pdfBase64 = "";
        $token = $request->headers->get('token');

        if($token != "05217fc30d38eeda8212aef7305b3ed260eed2b1"){
            return $this->json([
                'message' => "token invalido",
            ],401);
        }

        $datos = json_decode($request->getContent(),true);
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $pdfBase64 = GenerarPdf::generarPdf(base64_decode($datos['xml']),$datos['adicionales'],$datos['empresa'],$conn);

        if(!is_array($datos)){
            return $this->json([
                'message' => "se necesita una estructura valida",
            ],401);
        }

        return $this->json(['pdf'=>$pdfBase64],200);
    }
}