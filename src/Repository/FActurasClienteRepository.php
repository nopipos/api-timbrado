<?php

namespace App\Repository;

use App\Entity\FActurasCliente;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FActurasCliente|null find($id, $lockMode = null, $lockVersion = null)
 * @method FActurasCliente|null findOneBy(array $criteria, array $orderBy = null)
 * @method FActurasCliente[]    findAll()
 * @method FActurasCliente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FActurasClienteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FActurasCliente::class);
    }

    // /**
    //  * @return FActurasCliente[] Returns an array of FActurasCliente objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FActurasCliente
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
