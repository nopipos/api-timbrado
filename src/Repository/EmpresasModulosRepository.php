<?php

namespace App\Repository;

use App\Entity\EmpresasModulos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmpresasModulos|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmpresasModulos|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmpresasModulos[]    findAll()
 * @method EmpresasModulos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmpresasModulosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmpresasModulos::class);
    }

    // /**
    //  * @return EmpresasModulos[] Returns an array of EmpresasModulos objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmpresasModulos
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
