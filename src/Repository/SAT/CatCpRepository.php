<?php

namespace App\Repository\SAT;

use App\Entity\SAT\CatCp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CatCp|null find($id, $lockMode = null, $lockVersion = null)
 * @method CatCp|null findOneBy(array $criteria, array $orderBy = null)
 * @method CatCp[]    findAll()
 * @method CatCp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CatCpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CatCp::class);
    }

    // /**
    //  * @return CatCp[] Returns an array of CatCp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CatCp
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
