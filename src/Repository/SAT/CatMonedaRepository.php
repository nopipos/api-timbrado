<?php

namespace App\Repository\SAT;

use App\Entity\SAT\CatMoneda;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CatMoneda|null find($id, $lockMode = null, $lockVersion = null)
 * @method CatMoneda|null findOneBy(array $criteria, array $orderBy = null)
 * @method CatMoneda[]    findAll()
 * @method CatMoneda[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CatMonedaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CatMoneda::class);
    }

    // /**
    //  * @return CatMoneda[] Returns an array of CatMoneda objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CatMoneda
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
