<?php

namespace App\Repository;

use App\Entity\Clientes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr;

/**
 * @method Clientes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Clientes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Clientes[]    findAll()
 * @method Clientes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Clientes::class);
    }

    private $pageSize = 10;

    /**
     * Get Facturas from selected Empresas
     *
     * @param $pageNumber
     * @param $idEmpresas
     * @return array
     */
    public function getClientes($pageNumber, $idEmpresas): array
    {

        $qb = $this->createQueryBuilder('c');

        $query = $qb
            ->select(
                [
                    'empresas.nombre',
                    'c.id',
                    'c.clave',
                    'c.no_proveedor',
                    'c.nombre',
                    'c.rfc',
                    'c.curp',
                    'c.num_reg',
                    'c.uso_cfdi',
                    'c.metodo_pago',
                    'c.condiciones_pago',
                    'c.forma_pago',
                    'c.dias_credito',
                    'c.estatus'
                ]
            )
            ->innerJoin('c.empresa', 'empresas',Expr\Join::ON)
            ->where('c.estatus =:estatus')
            ->andwhere('c.empresa IN(:empresa_id)')
            ->setParameters(array(
                ':estatus' => 1,
                ':empresa_id' => $idEmpresas,
            ))->getQuery();

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        $totalItems = count($paginator);

        $pageCount = ceil($totalItems / $this->pageSize);

        $paginator
            ->getQuery()
            ->setFirstResult($this->pageSize * ($pageNumber-1))
            ->setMaxResults($this->pageSize);

        return ['clientes'=>$paginator->getQuery()->getResult(), 'total'=>$totalItems, 'pagina'=>$pageCount];

    }

    // /**
    //  * @return Clientes[] Returns an array of Clientes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Clientes
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
