<?php

namespace App\Repository;

use App\Entity\Unidades;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr;

/**
 * @method Unidades|null find($id, $lockMode = null, $lockVersion = null)
 * @method Unidades|null findOneBy(array $criteria, array $orderBy = null)
 * @method Unidades[]    findAll()
 * @method Unidades[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnidadesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Unidades::class);
    }
    private $pageSize = 10;

    /**
     * Get Productos from selected Empresas
     *
     * @param $pageNumber
     * @param $idEmpresas
     * @return array
     */
    public function getUnidades($pageNumber, $idEmpresas): array
    {

        $qb = $this->createQueryBuilder('u');

        $query = $qb
            ->select(
                [
                    'empresas.nombre',
                    'u.id',
                    'u.descripcion',
                    'u.estatus'
                ]
            )
            ->innerJoin('u.empresa', 'empresas',Expr\Join::ON)
            ->where('u.estatus =:estatus')
            ->andwhere('u.empresa IN(:empresa_id)')
            ->setParameters(array(
                ':estatus' => 1,
                ':empresa_id' => $idEmpresas,
            ))->getQuery();

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        $totalItems = count($paginator);

        $pageCount = ceil($totalItems / $this->pageSize);

        $paginator
            ->getQuery()
            ->setFirstResult($this->pageSize * ($pageNumber-1))
            ->setMaxResults($this->pageSize);

        return ['unidades'=>$paginator->getQuery()->getResult(), 'total'=>$totalItems, 'pagina'=>$pageCount];

    }


    /**
     * Get Productos from selected Empresas
     *
     * @param $pageNumber
     * @param $idEmpresas
     * @param $search
     * @return array
     */
    public function search($pageNumber, $idEmpresas,$search): array
    {

        $qb = $this->createQueryBuilder('u');

        $query = $qb
            ->select(
                [
                    'u.id',
                    'u.descripcion',
                ]
            )
            ->innerJoin('p.empresa', 'empresas',Expr\Join::ON)
            ->where('p.estatus =:estatus')
            ->andwhere('p.empresa IN(:empresa_id)')
            ->andwhere($qb->expr()->like('p.descripcion', ':descripcion'))
            //->andwhere('p.descripcion like %:descripcion%')
            ->setParameters(array(
                ':estatus' => 1,
                ':empresa_id' => $idEmpresas,
                ':descripcion'=>"%".$search."%",
            ))->getQuery();

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        $totalItems = count($paginator);

        $pageCount = ceil($totalItems / $this->pageSize);

        $paginator
            ->getQuery()
            ->setFirstResult($this->pageSize * ($pageNumber-1))
            ->setMaxResults($this->pageSize);

        return ['unidades'=>$paginator->getQuery()->getResult(), 'total'=>$totalItems, 'pagina'=>$pageCount];

    }
}