<?php

namespace App\Repository;

use App\Entity\Facturas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Facturas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Facturas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Facturas[]    findAll()
 * @method Facturas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacturasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Facturas::class);
    }


    private $pageSize = 10;

    /**
     * Get Facturas from selected Empresas
     *
     * @param $pageNumber
     * @param $idEmpresas
     * @return array
     */
    public function getFacturas($pageNumber, $idEmpresas): array
    {

        $qb = $this->createQueryBuilder('f');

        $query = $qb
            ->select(
                [
                    'f.id',
                    'f.serie',
                    'f.folio',
                    'f.total',
                    "f.moneda",
                    "f.tipoComprobante",
                    "f.metodo_pago",
                    "f.forma_pago",
                    'f.subtotal',
                    "clientes.nombre",
                    "clientes.rfc",
                    "empresas.nombre as emisor",
                    "empresas.id as idEmpresa",
                    "sucursal.nombre as sucursal_nombre",
                    "timbres.folio_fiscal",
                    "f.fecha",
                    "f.iva",
                    "f.cancelado",
                    "f.timbrado",
                    "f.estatus",
                    "DATE_DIFF(CURRENT_DATE(),f.fecha) AS dias_factura"
                ]
            )
            ->innerJoin('f.cliente', 'clientes',Expr\Join::ON)
            ->innerJoin('f.empresa', 'empresas',Expr\Join::ON)
            ->innerJoin('f.sucursal', 'sucursal',Expr\Join::ON)
            ->leftJoin('f.timbre','timbres',Expr\Join::ON)
            
            ->where('f.estatus =:estatus')
            ->andwhere('f.empresa IN(:empresa_id)')
            ->setParameters(array(
                ':estatus' => 1,
                ':empresa_id' => $idEmpresas,
            ))
            ->orderBy('f.id', 'DESC')
            ->getQuery();




        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        $totalItems = count($paginator);

        $pageCount = ceil($totalItems / $this->pageSize);

        $paginator
            ->getQuery()
            ->setFirstResult($this->pageSize * ($pageNumber-1))
            ->setMaxResults($this->pageSize);

        return ['facturas'=>$paginator->getQuery()->getResult(), 'total'=>$totalItems, 'pagina'=>$pageCount];
    }


    /**
     * search Facturas from selected Empresas
     *
     * @param $pageNumber
     * @param $idEmpresas
     * @param $serie
     * @param $folio
     * @param $tipoDocumento
     * @param $total
     * @param $uuid
     * @param $sucursal
     * @param $pago
     * @return array
     */
    public function buscarFacturas($pageNumber, $idEmpresas,$serie,$folio,$tipoDocumento,$total,$uuid,$sucursal,$pago): array
    {
        $qb = $this->createQueryBuilder('f');
        $query = $qb
            ->select(
                [
                    'f.id',
                    'f.serie',
                    'f.folio',
                    'f.total',
                    "f.moneda",
                    "f.tipoComprobante",
                    "f.metodo_pago",
                    "f.forma_pago",
                    'f.subtotal',
                    "clientes.nombre",
                    "clientes.rfc",
                    "empresas.nombre as emisor",
                    "sucursal.nombre as sucursal_nombre",
                    "f.folio_fiscal",
                    "f.fecha",
                    "f.iva",
                    "f.cancelado",
                    "f.timbrado",
                    "f.estatus",
                    "f.tipo_cambio",
                    "DATE_DIFF(CURRENT_DATE(),f.fecha) AS dias_factura"
                ]
            )
            ->innerJoin('f.cliente', 'clientes',Expr\Join::ON)
            ->innerJoin('f.empresa', 'empresas',Expr\Join::ON)
            ->innerJoin('f.sucursal', 'sucursal',Expr\Join::ON)
            ->where('f.estatus =:estatus')
            ->setParameter(
                ':estatus' , 1,
            )
            ->andwhere('f.empresa IN(:empresa_id)')
            ->setParameter(
                ':empresa_id',$idEmpresas,
                
            )
            ->andwhere('f.tipoComprobante =:tipo_comprobante')
            ->setParameter(
                ':tipo_comprobante','FA',
                
            )
            ->andwhere('f.timbrado =:timbrado')
            ->setParameter(
                ':timbrado',1,
                
            );

            if(isset($serie) && !empty($serie)){
                $qb->andwhere('f.serie =:serie')
                ->setParameter(
                    ':serie' , $serie,
                );
            }    
            if(isset($folio) && !empty($folio)){
                $qb->andwhere('f.folio =:folio')
                ->setParameter(
                    ':folio' , $folio,
                );
            } 
            if(isset($tipoComprobante) && !empty($tipoComprobante)){
                $qb->andwhere('f.tipoComprobante =:tipoComprobante')
                ->setParameter(
                    ':tipoComprobante' , $tipoComprobante,
                );
            } 
            if(isset($total) && !empty($total)){
                $qb->andwhere('f.total =:total')
                ->setParameter(
                    ':total' , $total,
                );
            } 
            if(isset($uuid) && !empty($uuid)){
                $qb->andwhere('f.folio_fiscal =:folio_fiscal')
                ->setParameter(
                    ':folio_fiscal', $uuid,
                );
            } 
            if(isset($sucursal) && !empty($sucursal)){
                $qb->andwhere('f.sucursal =:sucursal')
                ->setParameter(
                    ':sucursal' , $sucursal,
                );
            }
            if(isset($pago) && !empty($pago)){
                $qb->andwhere('f.forma_pago =:forma_pago')
                ->setParameter(
                    ':forma_pago' , $pago,
                );
            }
            $qb->orderBy('f.fecha', 'DESC');
            $query = $qb->getQuery();

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        $totalItems = count($paginator);

        $pageCount = ceil($totalItems / $this->pageSize);

        $paginator
            ->getQuery()
            ->setFirstResult($this->pageSize * ($pageNumber-1))
            ->setMaxResults($this->pageSize);

        return ['facturas'=>$paginator->getQuery()->getResult(), 'total'=>$totalItems, 'pagina'=>$pageCount];
    }


    /**
     * search Facturas from selected Empresas
     *
     * @param $serie
     * @param $folio
     * @param $uuid
     * @param $idEmpresas
     * @param $pageNumber
     * @return array
     */
    public function buscarFacturaPago($serie,$folio,$uuid,$idEmpresas,$pageNumber): array
    {
        $qb = $this->createQueryBuilder('f');
        $query = $qb
            ->select(
                [
                    'f.id',
                    'f.serie',
                    'f.folio',
                    'f.total',
                    "f.moneda",
                    "f.tipoComprobante",
                    "f.metodo_pago",
                    "f.forma_pago",
                    'f.subtotal',
                    "clientes.nombre",
                    "clientes.rfc",
                    "empresas.nombre as emisor",
                    "sucursal.nombre as sucursal_nombre",
                    "f.folio_fiscal",
                    "f.fecha",
                    "f.iva",
                    "f.cancelado",
                    "f.timbrado",
                    "f.estatus",
                    "DATE_DIFF(CURRENT_DATE(),f.fecha) AS dias_factura"
                ]
            )
            ->innerJoin('f.cliente', 'clientes',Expr\Join::ON)
            ->innerJoin('f.empresa', 'empresas',Expr\Join::ON)
            ->innerJoin('f.sucursal', 'sucursal',Expr\Join::ON)
            ->where('f.estatus =:estatus')
            ->setParameter(
                ':estatus' , 1,
            )
            ->andwhere('f.empresa IN(:empresa_id)')
            ->setParameter(
                ':empresa_id',$idEmpresas,
                
            )
            ->andwhere('f.timbrado =:timbrado')
            ->setParameter(
                ':timbrado',1,
                
            );

            if(isset($serie) && !empty($serie)){
                $qb->andwhere('f.serie =:serie')
                ->setParameter(
                    ':serie' , $serie,
                );
            }    
            if(isset($folio) && !empty($folio)){
                $qb->andwhere('f.folio =:folio')
                ->setParameter(
                    ':folio' , $folio,
                );
            } 
            if(isset($uuid) && !empty($uuid)){
                $qb->andwhere('f.folio_fiscal =:folio_fiscal')
                ->setParameter(
                    ':folio_fiscal', $uuid,
                );
            } 
           
            
            $query = $qb->getQuery();

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        $totalItems = count($paginator);

        $pageCount = ceil($totalItems / $this->pageSize);

        $paginator
            ->getQuery()
            ->setFirstResult($this->pageSize * ($pageNumber-1))
            ->setMaxResults($this->pageSize);

        return ['facturas'=>$paginator->getQuery()->getResult(), 'total'=>$totalItems, 'pagina'=>$pageCount];
    }


    // /**
    //  * @return Facturas[] Returns an array of Facturas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Facturas
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
