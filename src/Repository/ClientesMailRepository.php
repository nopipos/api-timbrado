<?php

namespace App\Repository;

use App\Entity\ClientesMail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ClientesMail|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientesMail|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientesMail[]    findAll()
 * @method ClientesMail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientesMailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClientesMail::class);
    }

    // /**
    //  * @return ClientesMail[] Returns an array of ClientesMail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClientesMail
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
