<?php

namespace App\Repository;

use App\Entity\ClienteDireccion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr;

/**
 * @method ClienteDireccion|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClienteDireccion|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClienteDireccion[]    findAll()
 * @method ClienteDireccion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClienteDireccionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClienteDireccion::class);
    }
    private $pageSize = 10;
    /**
     * Get Facturas from selected Empresas
     *
     * @param $pageNumber
     * @param $idEmpresas
     * @param $idCliente
     * @return array
     */
    public function getDirecciones($pageNumber, $idEmpresas,$idCliente): array
    {

        $qb = $this->createQueryBuilder('d');

        $query = $qb
            ->select(
                [
                    'clientes.nombre as cliente',
                    'clientes.id as idCliente',
                    'empresas.id as idempresa',
                    'd.id',
                    'd.nombre',
                    'd.calle',
                    'd.no_ext',
                    'd.no_int',
                    'd.colonia',
                    'd.cp',
                    'd.municipio',
                    'd.estado',
                    'd.pais',
                    'd.telefono',
                    'd.referencia',
                    'd.estatus'
                ]
            )
            ->innerJoin('d.cliente', 'clientes',Expr\Join::ON)
            ->innerJoin('clientes.empresa', 'empresas',Expr\Join::ON)
            ->where('d.estatus =:estatus')
            ->andwhere('d.cliente IN(:cliente_id)')
            ->andwhere('clientes.empresa IN(:empresa_id)')
            ->setParameters(array(
                ':estatus' => 1,
                ':cliente_id' => $idCliente,
                ':empresa_id' => $idEmpresas,
            ))->getQuery();

        $paginator = new Paginator($query);
        $paginator->setUseOutputWalkers(false);

        $totalItems = count($paginator);

        $pageCount = ceil($totalItems / $this->pageSize);

        $paginator
            ->getQuery()
            ->setFirstResult($this->pageSize * ($pageNumber-1))
            ->setMaxResults($this->pageSize);

        return ['clientes'=>$paginator->getQuery()->getResult(), 'total'=>$totalItems, 'pagina'=>$pageCount];

    }
    // /**
    //  * @return ClienteDireccion[] Returns an array of ClienteDireccion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClienteDireccion
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
