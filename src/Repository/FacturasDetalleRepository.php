<?php

namespace App\Repository;

use App\Entity\FacturasDetalle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FacturasDetalle|null find($id, $lockMode = null, $lockVersion = null)
 * @method FacturasDetalle|null findOneBy(array $criteria, array $orderBy = null)
 * @method FacturasDetalle[]    findAll()
 * @method FacturasDetalle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacturasDetalleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FacturasDetalle::class);
    }

    // /**
    //  * @return FacturasDetalle[] Returns an array of FacturasDetalle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FacturasDetalle
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
