<?php

namespace App\Repository;

use App\Entity\UsuariosEmpresas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UsuariosEmpresas|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsuariosEmpresas|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsuariosEmpresas[]    findAll()
 * @method UsuariosEmpresas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsuariosEmpresasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsuariosEmpresas::class);
    }

    // /**
    //  * @return UsuariosEmpresas[] Returns an array of UsuariosEmpresas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsuariosEmpresas
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
