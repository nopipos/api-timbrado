<?php

namespace App\Service\Cfdi40\Validador;


class ConsultaEstatusSAT{

    static function consultarSAT($rfcEmisor,$rfcReceptor,$total,$uuid){

        $url = "https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc";

        $consultaCfdi = SatQueryRequest::soapRequest($url,$rfcEmisor,$rfcReceptor,$total,$uuid);

        if(is_array($consultaCfdi->EstatusCancelacion)){
            $consultaCfdi->EstatusCancelacion = "";
        }
        if(is_array($consultaCfdi->EsCancelable)){
            $consultaCfdi->EsCancelable = "";
        }

        return $consultaCfdi;
        
    }


}

?>