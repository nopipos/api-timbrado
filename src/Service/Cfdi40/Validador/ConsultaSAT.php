<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 31/01/2019
 * Time: 03:23 PM
 */

namespace App\Service\Cfdi40\Validador;


class ConsultaSAT
{

    static function consultarSAT($mail,$xmlCadena){


        if( substr($xmlCadena, 0,3) == pack("CCC",0xef,0xbb,0xbf) ) {
            $xmlCadena = substr($xmlCadena, 3);
        }

        libxml_use_internal_errors(true);
        $xml = new \DOMDocument();
        $ok = $xml->loadXML($xmlCadena);

        $root = $xml->getElementsByTagName('Comprobante')->item(0);
        $version = $root->getAttribute("version");
        if ($version==null)
            $version = $root->getAttribute("Version");

        $tipoCp = $root->getAttribute("TipoDeComprobante");

        $total = $root->getAttribute('Total');

        $Emisor = $root->getElementsByTagName('Emisor')->item(0);
        $rfcEmisor = $Emisor->getAttribute('Rfc');

        $Receptor = $root->getElementsByTagName('Receptor')->item(0);
        $rfcReceptor = $Receptor->getAttribute('Rfc');


        $TFD = $root->getElementsByTagName('TimbreFiscalDigital')->item(0);

        $uuid = "";
        if($TFD != null)
            $uuid = $TFD->getAttribute("UUID");

        $resultadoCP = "";

        if($tipoCp == "P")
            $resultadoCP = self::consultaSATDoctodCP($mail,$xmlCadena);

        if($resultadoCP != "")
            return $resultadoCP;
        
        return self::webservice($rfcEmisor,$rfcReceptor,$total,$uuid,$mail);

    }

    static function consultaSATDoctodCP($mail,$xmlCadena){

        if( substr($xmlCadena, 0,3) == pack("CCC",0xef,0xbb,0xbf) ) {
            $xmlCadena = substr($xmlCadena, 3);
        }

        libxml_use_internal_errors(true);
        $xml = new \DOMDocument();
        $ok = $xml->loadXML($xmlCadena);
        $xpath = new \DOMXPath($xml);
        $xpath->registerNamespace('ns1', 'http://www.sat.gob.mx/cfd/4');
        $xpath->registerNamespace('ns2', 'http://www.sat.gob.mx/TimbreFiscalDigital');
        $xpath->registerNamespace('ns3', 'http://www.sat.gob.mx/Pagos20');
        $Pagos = $xpath->query("//ns1:Comprobante/ns1:Complemento/ns3:Pagos/ns3:Pago/ns3:DoctoRelacionado");

        $root = $xml->getElementsByTagName('Comprobante')->item(0);
        $version = $root->getAttribute("version");
        if ($version==null)
            $version = $root->getAttribute("Version");

        $Emisor = $root->getElementsByTagName('Emisor')->item(0);
        $rfcEmisor = $Emisor->getAttribute('Rfc');

        $Receptor = $root->getElementsByTagName('Receptor')->item(0);
        $rfcReceptor = $Receptor->getAttribute('Rfc');

        if($Pagos->length > 0){

            if($Pagos->length < 50)
            foreach($Pagos as $pago){
                $uuid = $pago->getAttribute('IdDocumento');
                $total = $pago->getAttribute('ImpPagado');

                $resultado =  self::webservice($rfcEmisor,$rfcReceptor,$total,$uuid,$mail);

              
                if($resultado->Estado == "Cancelado"){
                    $resultado->Estado = "El documento es un CP y este tiene un UUID relacionado que esta Cancelado: ".$uuid;
                    return $resultado;
                }
            }
        }
        else{
            $resultado =  (object)[
                'EstatusCancelacion'=>'',
                'EsCancelable'=>'',
                'Estado'=>'SIN CP',
            ];
            return $resultado;
        }

        #logica para rescatar los uuid y los totales de los documentos relacionados

    }
    static function webservice($rfcEmisor,$rfcReceptor,$total,$uuid,$mail){

        $res = ConsultaEstatusSAT::consultarSAT($rfcEmisor,$rfcReceptor,$total,$uuid);
        
        return $res;
    }
}