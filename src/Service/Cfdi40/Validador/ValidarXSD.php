<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 31/01/2019
 * Time: 05:00 PM
 */

namespace App\Service\Cfdi40\Validador;


class ValidarXSD
{

    function validar($xmlCadena){

        libxml_use_internal_errors(true);
        $xml = new \DOMDocument();
        $ok = $xml->loadXML($xmlCadena);

        $ok = $xml->schemaValidate("./../../SAT/v4/cfdv40.xsd");


        if($ok){
            return [];
        }else{
            return $this->display_xml_errors($xmlCadena);
        }

    }

    function display_xml_errors($texto) {

        $lineas = explode("\n", $texto);
        $errors = libxml_get_errors();

        $erroresListado = [];
        $erroresListado[] = "El xml no es valido.";
        foreach ($errors as $error) {

            $erroresListado[] =  $this->display_xml_error($error, $lineas);
        }





        libxml_clear_errors();

        return $erroresListado;


    }
/// }}}}
// {{{ display_xml_error
    function display_xml_error($error, $lineas) {

        //$return  = htmlspecialchars($lineas[$error->line - 1]) . "\n";
        $return  = "";
        //$return .= str_repeat('-', $error->column) . "^\n";

        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "Warning $error->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "Error $error->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "Fatal Error $error->code: ";
                break;
        }

        $return .= trim($error->message) .
            "  Linea: $error->line" .
            "  Columna: $error->column";

       return $return;
    }
}