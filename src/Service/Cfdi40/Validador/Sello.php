<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 12/02/2019
 * Time: 12:32 PM
 */

namespace App\Service\Cfdi40\Validador;


class Sello
{
    function validarSello($xml,$idPeticion){

        $doc = new \DomDocument("1.0");
        $doc->loadXML($xml);
        $root = $doc->firstChild;
        $selloEmisor = $root->getAttribute("Sello");

        $selloArchivo = "";
        while (strlen($selloEmisor) > 0) {
            $secuenciaSello = substr($selloEmisor, 0, 64);
            $selloArchivo .= $secuenciaSello . "\n";
            $selloEmisor = str_replace($secuenciaSello, "", $selloEmisor);
        }

        $cadenaOriginalEmisor = "./".$idPeticion . "cadenaOriginalEmisor.txt";
        $archivo_selloEmisor = "./".$idPeticion . "selloEmisor.txt";

        //file_put_contents($archivo_selloEmisor,$selloArchivo);

        $noCertificado = $root->getAttribute("noCertificado");
        $certificado = $root->getAttribute("Certificado");
        $certificado = str_replace("\n", "", $certificado);
        $certificado = str_replace("\r", "", $certificado);

        $certificado = trim($certificado);

        $archivo_nocer = "./".$idPeticion .$noCertificado . ".cer";

        $cadenaRes = "-----BEGIN CERTIFICATE-----\n" . chunk_split($certificado, 64) . "-----END CERTIFICATE-----\n";
        //file_put_contents($archivo_nocer . ".pem",$cadenaRes);

        //shell_exec("xsltproc xsd/xslt/cadenaoriginal_3_3.xslt $pathXml > $cadenaOriginalEmisor");

        // Ruta al archivo XSLT
        $xslFile = "./../../SAT/v4/cadenaoriginal.xslt";
        //$xslFile = "./../../SAT/v3/cadenaoriginal_3_3.xslt";


        // Crear un objeto DOMDocument para cargar el archivo de transformación XSLT
        $xsl = new \DOMDocument();
        $xsl->load($xslFile);

        // Crear el procesador XSLT que nos generará la cadena original con base en las reglas descritas en el XSLT
        $proc = new \XSLTProcessor;
        // Cargar las reglas de transformación desde el archivo XSLT.
        $proc->importStyleSheet($xsl);
        // Generar la cadena original y asignarla a una variable
        $cadenaOriginal = $proc->transformToXML($doc);

        // nueva forma de validar el sello
        $cadena = $cadenaOriginal;
        //$sello = trim(file_get_contents($archivo_selloEmisor));
        $sello = $selloArchivo;
        //$cer = file_get_contents($archivo_nocer . ".pem");
        $cer = $cadenaRes;

        $pubkeyid = openssl_get_publickey(openssl_x509_read($cadenaRes));

        $ok = openssl_verify($cadena, base64_decode($sello), $pubkeyid, OPENSSL_ALGO_SHA256);

        if(file_exists($cadenaOriginalEmisor)){
            unlink($cadenaOriginalEmisor);
        }
        if(file_exists($archivo_selloEmisor)){
            unlink($archivo_selloEmisor);
        }
        if(file_exists($archivo_nocer.".pem")){
            unlink($archivo_nocer.".pem");
        }
        
        if ($ok == 1) {

            return true;
        } else {
            return false;
        }

    }
}