<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 31/01/2019
 * Time: 02:39 PM
 */

namespace App\Service\Cfdi40\Validador;


class ResponseValidador
{

    /**
     * @var string
     * @soap
     */
    public $Codigo;
    /**
     * @var string
     * @soap
     */
    public $Estatus;
    /**
     * @var string
     * @soap
     */
    public $EsCancelable;
    /**
     * @var string
     * @soap
     */
    public $EstatusCancelacion;
    /**
     * @var string
     * @soap
     */
    public $Mensaje;
    /**
     * @var string[]
     * @soap
     */
    public $Errores  = [];

    /**
     * @var string
     * @soap
     */
    public $Xml;



}