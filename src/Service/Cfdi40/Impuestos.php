<?php


namespace App\Service\Cfdi40;


use App\Entity\SAT\CatMoneda;
use Doctrine\Persistence\ManagerRegistry;
use App\Service\Utils\XML2Array;
use \DOMDocument;
use \DOMXPath;

class Impuestos
{
    private $xpath;
    private $xmlArray;
    private $moneda;

    const TRASLADOS = 1;
    const RETENIDOS = 2;
    /**
     * Validaciones constructor.
     * @param $xpath
     */
    public function __construct($xmlString,ManagerRegistry $doctrine) {


        $this->xmlArray = XML2Array::createArray($xmlString);

        $dom = new DOMDocument;
        $dom->loadXml($xmlString);
        $this->xpath = new DOMXPath($dom);
        $this->xpath->registerNamespace('ns1', 'http://www.sat.gob.mx/cfd/4');
        $this->xpath->registerNamespace('ns2', 'http://www.sat.gob.mx/TimbreFiscalDigital');

        $monedaVal = $this->xpath->query("//ns1:Comprobante/@Moneda")->item(0)->value;

        $CatMonedaRepo = $doctrine->getRepository(CatMoneda::class);
        $CatMoneda = $CatMonedaRepo->findOneBy(['CMoneda'=>$monedaVal]);

        $this->moneda = $CatMoneda;

    }

    /**
     * Revisamos si hay impuestos en conceptos y si no hay impuestos de factura return = 0, agregamos los impuestos de fatura
     * <br>
     * Si hay los 2 return  = 1 = ok
     * <br>
     * Si no hay imp en concepto y si imp de factura return = 2, no modificamos nada
     *
     * @return mixed
     */
    public function verifyImpuestos() {
        $vConImpTras = $this->xpath->evaluate('boolean(//ns1:Comprobante/ns1:Conceptos/ns1:Concepto/ns1:Impuestos/ns1:Traslados/ns1:Traslado/@Impuesto)');
        $vConImpRet = $this->xpath->evaluate('boolean(//ns1:Comprobante/ns1:Conceptos/ns1:Concepto/ns1:Impuestos/ns1:Retenciones/ns1:Retencion/@Impuesto)');

        $v1 = $vConImpTras || $vConImpRet;

        $vFacImpTras = $this->xpath->evaluate('boolean(//ns1:Comprobante/ns1:Impuestos/ns1:Traslados/ns1:Traslado/@Impuesto)');
        $vFacImpRet = $this->xpath->evaluate('boolean(//ns1:Comprobante/ns1:Impuestos/ns1:Retenciones/ns1:Retencion/@Impuesto)');

        $v2 = $vFacImpRet || $vFacImpTras;

        try {
            if ($v1 && $v2) {
                return 1;
            } elseif (!$v1 && $v2) {
                return 2;
            } elseif ($v1 && !$v2) {
                return 0;
            }
        } finally {

        }
    }

    /**
     *
     * Funcion para obtener el total de impuestos apartir de los conceptos
     *
     * @param $tipo 1 = traslados, 2 = retenciones
     */
    private function getTotalImpuestos($tipo) {
        switch ($tipo) {
            case self::TRASLADOS:
                $xpathImp = "ns1:Impuestos/ns1:Traslados/ns1:Traslado";
                break;
            case self::RETENIDOS:
                $xpathImp = "ns1:Impuestos/ns1:Retenciones/ns1:Retencion";
                break;
        }
        $conceptos = $this->xpath->query("//ns1:Comprobante/ns1:Conceptos/ns1:Concepto");
        $total = 0;

        $scale = $this->moneda->getDecimales();

        foreach ($conceptos as $con) {
            $item = $this->xpath->query($xpathImp, $con);
            foreach ($item as $ret) {


                //$total = \bcadd($total, $ret->getAttribute('Importe'));
                
                $imp = $ret->getAttribute('Importe') == "" ? 0: floatval($ret->getAttribute('Importe'));
                $total = $total + $imp;
            }
        }

//        if ($total == 0) {
//            $total = '0';
//            if ($scale > 0) {
//                $total = $total . '.';
//            }
//            for ($i = 0; $i < $scale; $i++) {
//                $total = $total . '0';
//            }
//        }
//        $total = number_format($total, $scale, '.', '');
        $total = self::asignarDecimales($total, $scale);

        return $total;
    }

    /**
     * Funcion para obtener toda lista de impuestos de los conceptos agrupada por TasaOCuota, impuesto y tipofactor, con los importe sumados si es que aplica
     *
     * @return array
     */
    public function getImpuestosTraslados() {

        $impuestos = [];

        $conceptos = $this->xpath->query("//ns1:Comprobante/ns1:Conceptos/ns1:Concepto");

        $cont = 0;
        
        foreach ($conceptos as $con) {
            $item = $this->xpath->query("ns1:Impuestos/ns1:Traslados/ns1:Traslado", $con);
            
            
            foreach ($item as $imp) {
                $impuestos[$cont]['base'] = $imp->getAttribute('Base');
                $impuestos[$cont]['tipoFactor'] = $imp->getAttribute('TipoFactor');
                $impuestos[$cont]['tasaOCuota'] = $imp->getAttribute('TasaOCuota');
                $impuestos[$cont]['impuesto'] = $imp->getAttribute('Impuesto');
                $impuestos[$cont]['importe'] = $imp->getAttribute('Importe');
                $cont++;
            }
        }

        $res = [];

        $j = 0;

        //print_r($impuestos);
        $scale = $this->moneda->getDecimales();

        foreach ($impuestos as $item) {

            if (empty($res)) {
                $res[] = $item;
                $j++;
                continue;
            }

            $existe = true;

            for ($i = 0; $i < count($res); $i++) {


                if (strcmp($res[$i]['tipoFactor'], $item['tipoFactor']) == 0 &&
                    strcmp($res[$i]['tasaOCuota'], $item['tasaOCuota']) == 0 &&
                    strcmp($res[$i]['impuesto'], $item['impuesto']) == 0
                ) {


                    //$res[$i]['importe'] = \bcadd($res[$i]['importe'], $item['importe'], $scale);
                    $res[$i]['importe'] = $res[$i]['importe'] + $item['importe'];
                    $res[$i]['base'] = $res[$i]['base'] + $item['base'];

                    $existe = true;
                    break;
                } else {
                    $existe = false;
                }
            }


            if (!$existe) {
                $res[$j] = $item;
                $j++;
            }
        }

        //echo number_format(182.76,2);
        
        for ($i = 0; $i < count($res); $i++) {
            //$res[$i]['importe'] = number_format($res[$i]['importe'], $scale, '.', '');

            if(strlen(trim($res[$i]['importe']))>0)
                $res[$i]['importe'] = self::asignarDecimales($res[$i]['importe'], $scale);
            
        }


        return $res;
    }

    /**
     * Funcion para obtener toda lista de impuestos de los conceptos agrupada por TasaOCuota, impuesto y tipofactor, con los importe sumados si es que aplica
     *
     * @return array
     */
    public function getImpuestosRetenciones() {
        $impuestos = [];
        $conceptos = $this->xpath->query("//ns1:Comprobante/ns1:Conceptos/ns1:Concepto");

        $cont = 0;
        foreach ($conceptos as $con) {
            $item = $this->xpath->query("ns1:Impuestos/ns1:Retenciones/ns1:Retencion", $con);
            foreach ($item as $imp) {
                $impuestos[$cont]['base'] = $imp->getAttribute('Base');
                $impuestos[$cont]['tipoFactor'] = $imp->getAttribute('TipoFactor');
                $impuestos[$cont]['tasaOCuota'] = $imp->getAttribute('TasaOCuota');
                $impuestos[$cont]['impuesto'] = $imp->getAttribute('Impuesto');
                $impuestos[$cont]['importe'] = $imp->getAttribute('Importe');
                $cont++;
            }
        }

        $res = [];

        $scale = $this->moneda->Decimales;

        $j = 0;
        foreach ($impuestos as $item) {

            if (empty($res)) {
                $res[] = $item;
                $j++;
                continue;
            }

            $existe = true;

            for ($i = 0; $i < count($res); $i++) {


                if (strcmp($res[$i]['impuesto'], $item['impuesto']) == 0
                ) {


                    //$res[$i]['importe'] = \bcadd($res[$i]['importe'], $item['importe'], $scale);
                    $res[$i]['importe'] = $res[$i]['importe'] + $item['importe'];
                    $res[$i]['importe'] = self::asignarDecimales($res[$i]['importe'], $scale);
                    $existe = true;
                    break;
                } else {
                    $existe = false;
                }
            }

            if (!$existe) {
                $res[$j] = $item;
                $j++;
            }
        }


        return $res;
    }

    /**
     * Funcion para agregar los impuesto de factura en base a los impuestos de los conceptos
     *
     * @param $xmlString
     * @return string xml sin codificar
     */
    public function addImpuestos($xmlString) {


        
        $_trasladados = $this->getImpuestosTraslados();

      
        $_retenciones = $this->getImpuestosRetenciones();

        if (empty($_trasladados) && empty($_retenciones)) {
            return $xmlString;
        }


        $doc = new DOMDocument;
        $doc->loadXml($xmlString);
        $xpath = new DOMXPath($doc);
        $xpath->registerNamespace('ns1', 'http://www.sat.gob.mx/cfd/4');
        $xpath->registerNamespace('ns2', 'http://www.sat.gob.mx/TimbreFiscalDigital');

        $Comprobante = $doc->getElementsByTagName("Comprobante")->item(0);

       
        $impuestos = $doc->createElement('cfdi:Impuestos');

        //---------------------

        $nodeComprobante = $xpath->query("//ns1:Comprobante");
        $nodeComplemento = $xpath->query("//ns1:Comprobante/ns1:Complemento");

        
        if ($nodeComplemento->length > 0) {
            $nodeComprobante->item(0)->insertBefore($impuestos, $nodeComplemento->item(0));
        } else {
            $impuestos = $Comprobante->appendChild($impuestos);
        }

        if ($this->getTotalImpuestos(self::TRASLADOS) > 0 || !empty($_trasladados)) {
            
            if($_trasladados[0]['tipoFactor'] != "Exento")
                $impuestos->setAttribute('TotalImpuestosTrasladados', $this->getTotalImpuestos(self::TRASLADOS));
        }

        if ($this->getTotalImpuestos(self::RETENIDOS) > 0 || !empty($_retenciones)) {
            $impuestos->setAttribute('TotalImpuestosRetenidos', $this->getTotalImpuestos(self::RETENIDOS));
        }
        
        if (!empty($_retenciones)) {
            $retenciones = $doc->createElement('cfdi:Retenciones');
            $retenciones = $impuestos->appendChild($retenciones);
        }

        foreach ($_retenciones as $item) {
            $retencion = $doc->createElement('cfdi:Retencion');
            $retencion = $retenciones->appendChild($retencion);
            $retencion->setAttribute('Impuesto', $item['impuesto']);
            $retencion->setAttribute('Importe', $item['importe']);
        }

        if (!empty($_trasladados)) {
            $traslados = $doc->createElement('cfdi:Traslados');
            $traslados = $impuestos->appendChild($traslados);
        }

        foreach ($_trasladados as $item) {
            $traslado = $doc->createElement('cfdi:Traslado');
            $traslado = $traslados->appendChild($traslado);
            $traslado->setAttribute('Impuesto', $item['impuesto']);
            $traslado->setAttribute('Base', $item['base']);
            $traslado->setAttribute('TipoFactor', $item['tipoFactor']);
            if (strcmp($item['tipoFactor'], 'Exento') != 0) {
            $traslado->setAttribute('TasaOCuota', $item['tasaOCuota']);
            $traslado->setAttribute('Importe', $item['importe']);
            }
        }



        
        return $doc->saveXML();
    }

    static function asignarDecimales($number, $dec_length) {
        //echo $number;
//        $d = pow(10, $dec_length);
//        $res = ((int) ($number * $d)) / $d;
//        return number_format($res, $dec_length, ".", "");
//        //return self::toFixed(($number * $d) / $d, $dec_length);

        return number_format($number,$dec_length,'.','');

        $decimales = explode(".", $number);

        if (count($decimales) > 0) {
            if (isset($decimales[1])) {
                #cuando son la misma cantidad de deciamels
                if (strlen($decimales[1]) == $dec_length)
                    return $number;
                #cuando los decimales son mas y se tiene que truncar
                if (strlen($decimales[1]) > $dec_length) {
                    $decimalesDespuesPunto = substr($decimales[1], 0, $dec_length);
                    return $decimales[0] . "." . $decimalesDespuesPunto;
                }
            } else {
                return $number . ".00";
            }
        }

        $raiz = 10;
        $multiplicador = pow($raiz, $dec_length);
        $resultado = ($number * $multiplicador) / $multiplicador;
        $total = number_format($resultado, $dec_length);
        $total = str_replace(",", "", $total);

        return $total = str_replace(",", "", $total);
    }
}