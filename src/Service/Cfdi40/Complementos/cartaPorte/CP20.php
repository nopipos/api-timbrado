<?php

namespace App\Service\Cfdi40\Complementos\cartaPorte;

use App\Service\Cfdi40\Complementos\Complemento;

class CP20 extends Complemento
{
    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request) {


        if (!isset($request['Comprobante']['Complemento']['CartaPorte'])) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para agregar el complemento de nomina12
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request)
    {

        if (!self::existeComplemento($request)) {
            return $doc;
        }

        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        

        $cp = $request['Comprobante']['Complemento']['CartaPorte'];

        $cartaPorte = $doc->createElement('cartaporte20:CartaPorte');
        $cartaPorte = $ComplementoNode->appendChild($cartaPorte);
        //$Nomina->setAttribute('xmlns:nomina', "http://www.sat.gob.mx/nomina");
        $cartaPorte->setAttribute('Version', $cp['Version']);
        if(isset($cp['TranspInternac']) && strlen($cp['TranspInternac']) > 0)
            $cartaPorte->setAttribute('TranspInternac', $cp['TranspInternac']);
        if(isset($cp['EntradaSalidaMerc']) && strlen($cp['EntradaSalidaMerc']) > 0)
            $cartaPorte->setAttribute('EntradaSalidaMerc', $cp['EntradaSalidaMerc']);
        if(isset($cp['PaisOrigenDestino']) && strlen($cp['PaisOrigenDestino']) > 0)
            $cartaPorte->setAttribute('PaisOrigenDestino', $cp['PaisOrigenDestino']);
        if(isset($cp['ViaEntradaSalida']) && strlen($cp['ViaEntradaSalida']) > 0)
            $cartaPorte->setAttribute('ViaEntradaSalida', $cp['ViaEntradaSalida']);
        if(isset($cp['TotalDistRec']) && strlen($cp['TotalDistRec']) > 0)
            $cartaPorte->setAttribute('TotalDistRec', $cp['TotalDistRec']);

        $ubicaciones = $doc->createElement('cartaporte20:Ubicaciones');
        $ubicaciones = $cartaPorte->appendChild($ubicaciones);

        $ubicacionesArray= [];
        if(isset($cp['Ubicaciones']['Ubicacion']))
            $ubicacionesArray = $cp['Ubicaciones']['Ubicacion'];
        else
            $ubicacionesArray = $cp['Ubicaciones'];
        foreach($ubicacionesArray as $ubicacionesDatos){

            $ubicacion = $doc->createElement('cartaporte20:Ubicacion');
            $ubicacion = $ubicaciones->appendChild($ubicacion);

            if(isset($ubicacionesDatos['FechaHoraSalidaLlegada']) && strlen($ubicacionesDatos['FechaHoraSalidaLlegada']) > 0){

                if(isset($ubicacionesDatos['TipoUbicacion']) && strlen($ubicacionesDatos['TipoUbicacion']) > 0){
                    $ubicacion->setAttribute('TipoUbicacion', $ubicacionesDatos['TipoUbicacion']);
                }
                if(isset($ubicacionesDatos['IDUbicacion']) && strlen($ubicacionesDatos['IDUbicacion']) > 0){
                    $ubicacion->setAttribute('IDUbicacion', $ubicacionesDatos['IDUbicacion']);
                }

                if(isset($ubicacionesDatos['RFCRemitenteDestinatario']) && strlen($ubicacionesDatos['RFCRemitenteDestinatario']) > 0)
                    $ubicacion->setAttribute('RFCRemitenteDestinatario', $ubicacionesDatos['RFCRemitenteDestinatario']);
                if(isset($ubicacionesDatos['NombreRemitenteDestinatario']) && strlen($ubicacionesDatos['NombreRemitenteDestinatario']) > 0)
                    $ubicacion->setAttribute('NombreRemitenteDestinatario', $ubicacionesDatos['NombreRemitenteDestinatario']);
                if(isset($ubicacionesDatos['NumRegIdTrib']) && strlen($ubicacionesDatos['NumRegIdTrib']) > 0)
                    $ubicacion->setAttribute('NumRegIdTrib', $ubicacionesDatos['NumRegIdTrib']);
                if(isset($ubicacionesDatos['ResidenciaFiscal']) && strlen($ubicacionesDatos['ResidenciaFiscal']) > 0)
                    $ubicacion->setAttribute('ResidenciaFiscal', $ubicacionesDatos['ResidenciaFiscal']);
                if(isset($ubicacionesDatos['NumEstacion']) && strlen($ubicacionesDatos['NumEstacion']) > 0)
                    $ubicacion->setAttribute('NumEstacion', $ubicacionesDatos['NumEstacion']);
                if(isset($ubicacionesDatos['NombreEstacion']) && strlen($ubicacionesDatos['NombreEstacion']) > 0)
                    $ubicacion->setAttribute('NombreEstacion', $ubicacionesDatos['NombreEstacion']);
                if(isset($ubicacionesDatos['NavegacionTrafico']) && strlen($ubicacionesDatos['NavegacionTrafico']) > 0)
                    $ubicacion->setAttribute('NavegacionTrafico', $ubicacionesDatos['NavegacionTrafico']);
                if(isset($ubicacionesDatos['FechaHoraSalidaLlegada']) && strlen($ubicacionesDatos['FechaHoraSalidaLlegada']) > 0)
                    $ubicacion->setAttribute('FechaHoraSalidaLlegada', $ubicacionesDatos['FechaHoraSalidaLlegada']);
                if(isset($ubicacionesDatos['TipoEstacion']) && strlen($ubicacionesDatos['TipoEstacion']) > 0)
                    $ubicacion->setAttribute('TipoEstacion', $ubicacionesDatos['TipoEstacion']);
                if(isset($ubicacionesDatos['DistanciaRecorrida']) && strlen($ubicacionesDatos['DistanciaRecorrida']) > 0)
                    $ubicacion->setAttribute('DistanciaRecorrida', $ubicacionesDatos['DistanciaRecorrida']);

            }

            if(isset($ubicacionesDatos['Domicilio']['Pais']) && strlen($ubicacionesDatos['Domicilio']['Pais']) > 0){
                $domicilio = $doc->createElement('cartaporte20:Domicilio');
                $domicilio = $ubicacion->appendChild($domicilio);
                
            if(isset($ubicacionesDatos['Domicilio']['Calle']) && strlen($ubicacionesDatos['Domicilio']['Calle']) > 0)
                $domicilio->setAttribute('Calle', $ubicacionesDatos['Domicilio']['Calle']);
            if(isset($ubicacionesDatos['Domicilio']['NumeroExterior']) && strlen($ubicacionesDatos['Domicilio']['NumeroExterior']) > 0)
                $domicilio->setAttribute('NumeroExterior', $ubicacionesDatos['Domicilio']['NumeroExterior']);
            if(isset($ubicacionesDatos['Domicilio']['NumeroInterior']) && strlen($ubicacionesDatos['Domicilio']['NumeroInterior']) > 0)
                $domicilio->setAttribute('NumeroInterior', $ubicacionesDatos['Domicilio']['NumeroInterior']);
            if(isset($ubicacionesDatos['Domicilio']['Colonia']) && strlen($ubicacionesDatos['Domicilio']['Colonia']) > 0)
                $domicilio->setAttribute('Colonia', $ubicacionesDatos['Domicilio']['Colonia']);
            if(isset($ubicacionesDatos['Domicilio']['Localidad']) && strlen($ubicacionesDatos['Domicilio']['Localidad']) > 0)
                $domicilio->setAttribute('Localidad', $ubicacionesDatos['Domicilio']['Localidad']);
            if(isset($ubicacionesDatos['Domicilio']['Referencia']) && strlen($ubicacionesDatos['Domicilio']['Referencia']) > 0)
                $domicilio->setAttribute('Referencia', $ubicacionesDatos['Domicilio']['Referencia']);
            if(isset($ubicacionesDatos['Domicilio']['Municipio']) && strlen($ubicacionesDatos['Domicilio']['Municipio']) > 0)
                $domicilio->setAttribute('Municipio', $ubicacionesDatos['Domicilio']['Municipio']);
            if(isset($ubicacionesDatos['Domicilio']['Estado']) && strlen($ubicacionesDatos['Domicilio']['Estado']) > 0)
                $domicilio->setAttribute('Estado', $ubicacionesDatos['Domicilio']['Estado']);
            if(isset($ubicacionesDatos['Domicilio']['Pais']) && strlen($ubicacionesDatos['Domicilio']['Pais']) > 0)
                $domicilio->setAttribute('Pais', $ubicacionesDatos['Domicilio']['Pais']);
            if(isset($ubicacionesDatos['Domicilio']['CodigoPostal']) && strlen($ubicacionesDatos['Domicilio']['CodigoPostal']) > 0)
                $domicilio->setAttribute('CodigoPostal', $ubicacionesDatos['Domicilio']['CodigoPostal']);

            if(isset($ubicacionesDatos['TipoEstacion']) && strlen($ubicacionesDatos['TipoEstacion']) > 0)
                $ubicacion->setAttribute('TipoEstacion', $ubicacionesDatos['TipoEstacion']);
            if(isset($ubicacionesDatos['DistanciaRecorrida']) && strlen($ubicacionesDatos['DistanciaRecorrida']) > 0)
                $ubicacion->setAttribute('DistanciaRecorrida', $ubicacionesDatos['DistanciaRecorrida']);
            }


            

        }

        $mercancias = $doc->createElement('cartaporte20:Mercancias');
        $mercancias = $cartaPorte->appendChild($mercancias);

        if(isset($cp['Mercancias']['PesoBrutoTotal']) && strlen($cp['Mercancias']['PesoBrutoTotal']) > 0)
            $mercancias->setAttribute('PesoBrutoTotal', $cp['Mercancias']['PesoBrutoTotal']);

        if(isset($cp['Mercancias']['UnidadPeso']) && strlen($cp['Mercancias']['UnidadPeso']) > 0)
            $mercancias->setAttribute('UnidadPeso', $cp['Mercancias']['UnidadPeso']);

        if(isset($cp['Mercancias']['PesoNetoTotal']) && strlen($cp['Mercancias']['PesoNetoTotal']) > 0)
            $mercancias->setAttribute('PesoNetoTotal', $cp['Mercancias']['PesoNetoTotal']);

        if(isset($cp['Mercancias']['NumTotalMercancias']) && strlen($cp['Mercancias']['NumTotalMercancias']) > 0)
            $mercancias->setAttribute('NumTotalMercancias', $cp['Mercancias']['NumTotalMercancias']);

        if(isset($cp['Mercancias']['CargoPorTasacion']) && strlen($cp['Mercancias']['CargoPorTasacion']) > 0)
            $mercancias->setAttribute('CargoPorTasacion', $cp['Mercancias']['CargoPorTasacion']);

            if(isset($cp['Mercancias']['Mercancia'])){
                foreach($cp['Mercancias']['Mercancia'] as $mercancia){

                    $mercanciaDom = $doc->createElement('cartaporte20:Mercancia');
                    $mercanciaDom = $mercancias->appendChild($mercanciaDom);
        
                    if(isset($mercancia['Pedimentos']))
                        foreach($mercancia['Pedimentos'] as $pedimentosData){
            
                            $Pedimentos = $doc->createElement('cartaporte20:Pedimentos');
                            $Pedimentos = $mercanciaDom->appendChild($Pedimentos);
            
                            if(isset($pedimentosData['Pedimento']) && strlen($pedimentosData['Pedimento']) > 0)
                                $Pedimentos->setAttribute('Pedimento', $pedimentosData['Pedimento']);
                        }
                    if(isset($mercancia['GuiasIdentificacion']))
                        foreach($mercancia['GuiasIdentificacion'] as $guiasIdentificacionData){
            
                            $GuiasIdentificacion = $doc->createElement('cartaporte20:GuiasIdentificacion');
                            $GuiasIdentificacion = $mercanciaDom->appendChild($GuiasIdentificacion);
            
                            if(isset($guiasIdentificacionData['NumeroGuiaIdentificacion']) && strlen($guiasIdentificacionData['NumeroGuiaIdentificacion']) > 0)
                                $GuiasIdentificacion->setAttribute('NumeroGuiaIdentificacion', $guiasIdentificacionData['NumeroGuiaIdentificacion']);
            
                            if(isset($guiasIdentificacionData['DescripGuiaIdentificacion']) && strlen($guiasIdentificacionData['DescripGuiaIdentificacion']) > 0)
                                $GuiasIdentificacion->setAttribute('DescripGuiaIdentificacion', $guiasIdentificacionData['DescripGuiaIdentificacion']);
            
                            if(isset($guiasIdentificacionData['PesoGuiaIdentificacion']) && strlen($guiasIdentificacionData['PesoGuiaIdentificacion']) > 0)
                                $GuiasIdentificacion->setAttribute('PesoGuiaIdentificacion', $guiasIdentificacionData['PesoGuiaIdentificacion']);
                        }
                        if(isset($mercancia['CantidadTransporta']))
                            foreach($mercancia['CantidadTransporta'] as $cantidadTransportaDatos){  
                
                                $cantidadtransporta = $doc->createElement('cartaporte20:CantidadTransporta');
                                $cantidadtransporta = $mercanciaDom->appendChild($cantidadtransporta);
                
                                if(isset($cantidadTransportaDatos['Cantidad']) && strlen($cantidadTransportaDatos['Cantidad']) > 0)
                                    $cantidadtransporta->setAttribute('Cantidad', $cantidadTransportaDatos['Cantidad']);
                
                                if(isset($cantidadTransportaDatos['IDOrigen']) && strlen($cantidadTransportaDatos['IDOrigen']) > 0)
                                    $cantidadtransporta->setAttribute('IDOrigen', $cantidadTransportaDatos['IDOrigen']);
                
                                if(isset($cantidadTransportaDatos['IDDestino']) && strlen($cantidadTransportaDatos['IDDestino']) > 0)
                                    $cantidadtransporta->setAttribute('IDDestino', $cantidadTransportaDatos['IDDestino']);
                
                                if(isset($cantidadTransportaDatos['CvesTransporte']) && strlen($cantidadTransportaDatos['CvesTransporte']) > 0)
                                    $cantidadtransporta->setAttribute('CvesTransporte', $cantidadTransportaDatos['CvesTransporte']);
                            }
                
                    if(isset($mercancia['DetalleMercancia']['UnidadPeso']) && strlen($mercancia['DetalleMercancia']['UnidadPeso']) > 0){
        
                        $detalleMercancias = $doc->createElement('cartaporte20:DetalleMercancia');
                        $detalleMercancias = $mercanciaDom->appendChild($detalleMercancias);
        
                        if(isset($mercancia['DetalleMercancia']['UnidadPeso']) && strlen($mercancia['DetalleMercancia']['UnidadPeso']) > 0)
                        $detalleMercancias->setAttribute('UnidadPesoMerc', $mercancia['DetalleMercancia']['UnidadPeso']);
                        if(isset($mercancia['DetalleMercancia']['PesoBruto']) && strlen($mercancia['DetalleMercancia']['PesoBruto']) > 0)
                            $detalleMercancias->setAttribute('PesoBruto', $mercancia['DetalleMercancia']['PesoBruto']);
                        if(isset($mercancia['DetalleMercancia']['PesoNeto']) && strlen($mercancia['DetalleMercancia']['PesoNeto']) > 0)
                            $detalleMercancias->setAttribute('PesoNeto', $mercancia['DetalleMercancia']['PesoNeto']);
                        if(isset($mercancia['DetalleMercancia']['PesoTara']) && strlen($mercancia['DetalleMercancia']['PesoTara']) > 0)
                            $detalleMercancias->setAttribute('PesoTara', $mercancia['DetalleMercancia']['PesoTara']);
                        if(isset($mercancia['DetalleMercancia']['NumPiezas']) && strlen($mercancia['DetalleMercancia']['NumPiezas']) > 0)
                            $detalleMercancias->setAttribute('NumPiezas', $mercancia['DetalleMercancia']['NumPiezas']);
            
                    }
        
        
        
                    if(isset($mercancia['BienesTransp']) && strlen($mercancia['BienesTransp']) > 0)
                        $mercanciaDom->setAttribute('BienesTransp', $mercancia['BienesTransp']);
        
                    if(isset($mercancia['ClaveSTCC']) && strlen($mercancia['ClaveSTCC']) > 0)
                        $mercanciaDom->setAttribute('ClaveSTCC', $mercancia['ClaveSTCC']);
        
                    if(isset($mercancia['Descripcion']) && strlen($mercancia['Descripcion']) > 0){
                        if($request['Comprobante']['Emisor']['Rfc'] == "YPU821117G63")
                            $mercanciaDom->setAttribute('Descripcion', utf8_decode($mercancia['Descripcion']));
                        else
                            $mercanciaDom->setAttribute('Descripcion', ($mercancia['Descripcion']));
                    }
                        
        
                    if(isset($mercancia['Cantidad']) && strlen($mercancia['Cantidad']) > 0)
                        $mercanciaDom->setAttribute('Cantidad', $mercancia['Cantidad']);
        
                    if(isset($mercancia['ClaveUnidad']) && strlen($mercancia['ClaveUnidad']) > 0)
                        $mercanciaDom->setAttribute('ClaveUnidad', $mercancia['ClaveUnidad']);
        
                    if(isset($mercancia['Unidad']) && strlen($mercancia['Unidad']) > 0)
                        $mercanciaDom->setAttribute('Unidad', $mercancia['Unidad']);
        
                    if(isset($mercancia['Dimensiones']) && strlen($mercancia['Dimensiones']) > 0)
                        $mercanciaDom->setAttribute('Dimensiones', $mercancia['Dimensiones']);
        
                    if(isset($mercancia['MaterialPeligroso']) && strlen($mercancia['MaterialPeligroso']) > 0)
                        $mercanciaDom->setAttribute('MaterialPeligroso', $mercancia['MaterialPeligroso']);
        
                    if(isset($mercancia['CveMaterialPeligroso']) && strlen($mercancia['CveMaterialPeligroso']) > 0)
                        $mercanciaDom->setAttribute('CveMaterialPeligroso', $mercancia['CveMaterialPeligroso']);
        
                    if(isset($mercancia['Embalaje']) && strlen($mercancia['Embalaje']) > 0)
                        $mercanciaDom->setAttribute('Embalaje', $mercancia['Embalaje']);
        
                    if(isset($mercancia['DescripEmbalaje']) && strlen($mercancia['DescripEmbalaje']) > 0)
                        $mercanciaDom->setAttribute('DescripEmbalaje', $mercancia['DescripEmbalaje']);
        
                    if(isset($mercancia['PesoEnKg']) && strlen($mercancia['PesoEnKg']) > 0)
                        $mercanciaDom->setAttribute('PesoEnKg', $mercancia['PesoEnKg']);
        
                    if(isset($mercancia['ValorMercancia']) && strlen($mercancia['ValorMercancia']) > 0)
                        $mercanciaDom->setAttribute('ValorMercancia', $mercancia['ValorMercancia']);
        
                    if(isset($mercancia['Moneda']) && strlen($mercancia['Moneda']) > 0)
                        $mercanciaDom->setAttribute('Moneda', $mercancia['Moneda']);
        
                    if(isset($mercancia['FraccionArancelaria']) && strlen($mercancia['FraccionArancelaria']) > 0)
                        $mercanciaDom->setAttribute('FraccionArancelaria', $mercancia['FraccionArancelaria']);
        
                    if(isset($mercancia['UUIDComercioExt']) && strlen($mercancia['UUIDComercioExt']) > 0)
                        $mercanciaDom->setAttribute('UUIDComercioExt', $mercancia['UUIDComercioExt']);
        
                }
        
            }

        $autoTransporteFederal = $doc->createElement('cartaporte20:Autotransporte');
        $autoTransporteFederal = $mercancias->appendChild($autoTransporteFederal);

        $identificacionVehicular = $doc->createElement('cartaporte20:IdentificacionVehicular');
        $identificacionVehicular = $autoTransporteFederal->appendChild($identificacionVehicular);
        $mercanciasDatos = $cp['Mercancias'];
        if(isset($mercanciasDatos['Autotransporte']['IdentificacionVehicular']['ConfigVehicular']) && strlen($mercanciasDatos['Autotransporte']['IdentificacionVehicular']['ConfigVehicular']) > 0)
            $identificacionVehicular->setAttribute('ConfigVehicular', $mercanciasDatos['Autotransporte']['IdentificacionVehicular']['ConfigVehicular']);

        if(isset($mercanciasDatos['Autotransporte']['IdentificacionVehicular']['PlacaVM']) && strlen($mercanciasDatos['Autotransporte']['IdentificacionVehicular']['PlacaVM']) > 0)
            $identificacionVehicular->setAttribute('PlacaVM', $mercanciasDatos['Autotransporte']['IdentificacionVehicular']['PlacaVM']);

        if(isset($mercanciasDatos['Autotransporte']['IdentificacionVehicular']['AnioModeloVM']) && strlen($mercanciasDatos['Autotransporte']['IdentificacionVehicular']['AnioModeloVM']) > 0)
            $identificacionVehicular->setAttribute('AnioModeloVM', $mercanciasDatos['Autotransporte']['IdentificacionVehicular']['AnioModeloVM']);

            #======
        $Seguros = $doc->createElement('cartaporte20:Seguros');
        $Seguros = $autoTransporteFederal->appendChild($Seguros);

        if(isset($mercanciasDatos['Autotransporte']['Seguros']['AseguraRespCivil']) && strlen($mercanciasDatos['Autotransporte']['Seguros']['AseguraRespCivil']) > 0)
            $Seguros->setAttribute('AseguraRespCivil', $mercanciasDatos['Autotransporte']['Seguros']['AseguraRespCivil']);

        if(isset($mercanciasDatos['Autotransporte']['Seguros']['PolizaRespCivil']) && strlen($mercanciasDatos['Autotransporte']['Seguros']['PolizaRespCivil']) > 0)
            $Seguros->setAttribute('PolizaRespCivil', $mercanciasDatos['Autotransporte']['Seguros']['PolizaRespCivil']);

        if(isset($mercanciasDatos['Autotransporte']['Seguros']['AseguraMedAmbiente']) && strlen($mercanciasDatos['Autotransporte']['Seguros']['AseguraMedAmbiente']) > 0)
            $Seguros->setAttribute('AseguraMedAmbiente', $mercanciasDatos['Autotransporte']['Seguros']['AseguraMedAmbiente']);

        if(isset($mercanciasDatos['Autotransporte']['Seguros']['PolizaMedAmbiente']) && strlen($mercanciasDatos['Autotransporte']['Seguros']['PolizaMedAmbiente']) > 0)
            $Seguros->setAttribute('PolizaMedAmbiente', $mercanciasDatos['Autotransporte']['Seguros']['PolizaMedAmbiente']);

        if(isset($mercanciasDatos['Autotransporte']['Seguros']['AseguraCarga']) && strlen($mercanciasDatos['Autotransporte']['Seguros']['AseguraCarga']) > 0)
            $Seguros->setAttribute('AseguraCarga', $mercanciasDatos['Autotransporte']['Seguros']['AseguraCarga']);

        if(isset($mercanciasDatos['Autotransporte']['Seguros']['PolizaCarga']) && strlen($mercanciasDatos['Autotransporte']['Seguros']['PolizaCarga']) > 0)
            $Seguros->setAttribute('PolizaCarga', $mercanciasDatos['Autotransporte']['Seguros']['PolizaCarga']);

        if(isset($mercanciasDatos['Autotransporte']['Seguros']['PrimaSeguro']) && strlen($mercanciasDatos['Autotransporte']['Seguros']['PrimaSeguro']) > 0)
            $Seguros->setAttribute('PrimaSeguro', $mercanciasDatos['Autotransporte']['Seguros']['PrimaSeguro']);




        if(isset($mercanciasDatos['Autotransporte']['Remolques']['Remolque'][0])){
            $remolques = $doc->createElement('cartaporte20:Remolques');
            $remolques = $autoTransporteFederal->appendChild($remolques);
    
            foreach($mercanciasDatos['Autotransporte']['Remolques']['Remolque'] as $remolqueDatos){

                if(isset($remolqueDatos['SubTipoRem']) && strlen($remolqueDatos['SubTipoRem']) > 0){
    
                    $remolque = $doc->createElement('cartaporte20:Remolque');
                    $remolque = $remolques->appendChild($remolque);
                    $remolque->setAttribute('SubTipoRem', $remolqueDatos['SubTipoRem']);
    
                    if(isset($remolqueDatos['Placa']) && strlen($remolqueDatos['Placa']) > 0)
                    $remolque->setAttribute('Placa', $remolqueDatos['Placa']);
                }
    
            }
        }
        

        if(isset($mercanciasDatos['Autotransporte']['PermSCT']) && strlen($mercanciasDatos['Autotransporte']['PermSCT']) > 0)
            $autoTransporteFederal->setAttribute('PermSCT', $mercanciasDatos['Autotransporte']['PermSCT']);

        if(isset($mercanciasDatos['Autotransporte']['NumPermisoSCT']) && strlen($mercanciasDatos['Autotransporte']['NumPermisoSCT']) > 0)
            $autoTransporteFederal->setAttribute('NumPermisoSCT', $mercanciasDatos['Autotransporte']['NumPermisoSCT']);


        if(isset($mercanciasDatos['TransporteMaritimo'])){
            $transporteMaritimo = $doc->createElement('cartaporte20:TransporteMaritimo');
            $transporteMaritimo = $mercancias->appendChild($transporteMaritimo);

            foreach($mercanciasDatos['TransporteMaritimo']['Contenedor'] as $contenedorDatos){
                $contenedor = $doc->createElement('cartaporte20:Contenedor');
                $contenedor = $transporteMaritimo->appendChild($contenedor);

                if(isset($contenedorDatos['MatriculaContenedor']) && strlen($contenedorDatos['MatriculaContenedor']) > 0)
                    $contenedor->setAttribute('TransporteMaritimo', $contenedorDatos['MatriculaContenedor']);

                if(isset($contenedorDatos['TipoContenedor']) && strlen($contenedorDatos['TipoContenedor']) > 0)
                    $contenedor->setAttribute('TipoContenedor', $contenedorDatos['TipoContenedor']);

                if(isset($contenedorDatos['NumPrecinto']) && strlen($contenedorDatos['NumPrecinto']) > 0)
                    $contenedor->setAttribute('NumPrecinto', $contenedorDatos['NumPrecinto']);

            }

            if(isset($mercanciasDatos['TransporteMaritimo']['PermSCT']) && strlen($mercanciasDatos['TransporteMaritimo']['PermSCT']) > 0)
                $transporteMaritimo->setAttribute('PermSCT', $mercanciasDatos['TransporteMaritimo']['PermSCT']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumPermisoSCT']) && strlen($mercanciasDatos['TransporteMaritimo']['NumPermisoSCT']) > 0)
                $transporteMaritimo->setAttribute('NumPermisoSCT', $mercanciasDatos['TransporteMaritimo']['NumPermisoSCT']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NombreAseg']) && strlen($mercanciasDatos['TransporteMaritimo']['NombreAseg']) > 0)
                $transporteMaritimo->setAttribute('NombreAseg', $mercanciasDatos['TransporteMaritimo']['NombreAseg']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumPolizaSeguro']) && strlen($mercanciasDatos['TransporteMaritimo']['NumPolizaSeguro']) > 0)
                $transporteMaritimo->setAttribute('NumPolizaSeguro', $mercanciasDatos['TransporteMaritimo']['NumPolizaSeguro']);

            if(isset($mercanciasDatos['TransporteMaritimo']['TipoEmbarcacion']) && strlen($mercanciasDatos['TransporteMaritimo']['TipoEmbarcacion']) > 0)
                $transporteMaritimo->setAttribute('TipoEmbarcacion', $mercanciasDatos['TransporteMaritimo']['TipoEmbarcacion']);

            if(isset($mercanciasDatos['TransporteMaritimo']['Matricula']) && strlen($mercanciasDatos['TransporteMaritimo']['Matricula']) > 0)
                $transporteMaritimo->setAttribute('Matricula', $mercanciasDatos['TransporteMaritimo']['Matricula']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumeroOMI']) && strlen($mercanciasDatos['TransporteMaritimo']['NumeroOMI']) > 0)
                $transporteMaritimo->setAttribute('NumeroOMI', $mercanciasDatos['TransporteMaritimo']['NumeroOMI']);

            if(isset($mercanciasDatos['TransporteMaritimo']['AnioEmbarcacion']) && strlen($mercanciasDatos['TransporteMaritimo']['AnioEmbarcacion']) > 0)
                $transporteMaritimo->setAttribute('AnioEmbarcacion', $mercanciasDatos['TransporteMaritimo']['AnioEmbarcacion']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NombreEmbarc']) && strlen($mercanciasDatos['TransporteMaritimo']['NombreEmbarc']) > 0)
                $transporteMaritimo->setAttribute('NombreEmbarc', $mercanciasDatos['TransporteMaritimo']['NombreEmbarc']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NacionalidadEmbarc']) && strlen($mercanciasDatos['TransporteMaritimo']['NacionalidadEmbarc']) > 0)
                $transporteMaritimo->setAttribute('NacionalidadEmbarc', $mercanciasDatos['TransporteMaritimo']['NacionalidadEmbarc']);

            if(isset($mercanciasDatos['TransporteMaritimo']['UnidadesDeArqBruto']) && strlen($mercanciasDatos['TransporteMaritimo']['UnidadesDeArqBruto']) > 0)
                $transporteMaritimo->setAttribute('UnidadesDeArqBruto', $mercanciasDatos['TransporteMaritimo']['UnidadesDeArqBruto']);

            if(isset($mercanciasDatos['TransporteMaritimo']['TipoCarga']) && strlen($mercanciasDatos['TransporteMaritimo']['TipoCarga']) > 0)
                $transporteMaritimo->setAttribute('TipoCarga', $mercanciasDatos['TransporteMaritimo']['TipoCarga']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumCertITC']) && strlen($mercanciasDatos['TransporteMaritimo']['NumCertITC']) > 0)
                $transporteMaritimo->setAttribute('NumCertITC', $mercanciasDatos['TransporteMaritimo']['NumCertITC']);

            if(isset($mercanciasDatos['TransporteMaritimo']['Eslora']) && strlen($mercanciasDatos['TransporteMaritimo']['Eslora']) > 0)
                $transporteMaritimo->setAttribute('Eslora', $mercanciasDatos['TransporteMaritimo']['Eslora']);

            if(isset($mercanciasDatos['TransporteMaritimo']['Manga']) && strlen($mercanciasDatos['TransporteMaritimo']['Manga']) > 0)
                $transporteMaritimo->setAttribute('Manga', $mercanciasDatos['TransporteMaritimo']['Manga']);

            if(isset($mercanciasDatos['TransporteMaritimo']['Calado']) && strlen($mercanciasDatos['TransporteMaritimo']['Calado']) > 0)
                $transporteMaritimo->setAttribute('Calado', $mercanciasDatos['TransporteMaritimo']['Calado']);

            if(isset($mercanciasDatos['TransporteMaritimo']['LineaNaviera']) && strlen($mercanciasDatos['TransporteMaritimo']['LineaNaviera']) > 0)
                $transporteMaritimo->setAttribute('LineaNaviera', $mercanciasDatos['TransporteMaritimo']['LineaNaviera']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NombreAgenteNaviero']) && strlen($mercanciasDatos['TransporteMaritimo']['NombreAgenteNaviero']) > 0)
                $transporteMaritimo->setAttribute('NombreAgenteNaviero', $mercanciasDatos['TransporteMaritimo']['NombreAgenteNaviero']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumAutorizacionNaviero']) && strlen($mercanciasDatos['TransporteMaritimo']['NumAutorizacionNaviero']) > 0)
                $transporteMaritimo->setAttribute('NumAutorizacionNaviero', $mercanciasDatos['TransporteMaritimo']['NumAutorizacionNaviero']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumViaje']) && strlen($mercanciasDatos['TransporteMaritimo']['NumViaje']) > 0)
                $transporteMaritimo->setAttribute('NumViaje', $mercanciasDatos['TransporteMaritimo']['NumViaje']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumConocEmbarc']) && strlen($mercanciasDatos['TransporteMaritimo']['NumConocEmbarc']) > 0)
                $transporteMaritimo->setAttribute('NumConocEmbarc', $mercanciasDatos['TransporteMaritimo']['NumConocEmbarc']);

        }

        if(isset($mercanciasDatos['TransporteAereo'])){
            $transporteAereo = $doc->createElement('cartaporte20:TransporteAereo');
            $transporteAereo = $mercancias->appendChild($transporteAereo);


            if(isset($mercanciasDatos['TransporteAereo']['PermSCT']) && strlen($mercanciasDatos['TransporteAereo']['PermSCT']) > 0)
                $transporteAereo->setAttribute('PermSCT', $mercanciasDatos['TransporteAereo']['PermSCT']);

            if(isset($mercanciasDatos['TransporteAereo']['NumPermisoSCT']) && strlen($mercanciasDatos['TransporteAereo']['NumPermisoSCT']) > 0)
                $transporteAereo->setAttribute('NumPermisoSCT', $mercanciasDatos['TransporteAereo']['NumPermisoSCT']);

            if(isset($mercanciasDatos['TransporteAereo']['MatriculaAeronave']) && strlen($mercanciasDatos['TransporteAereo']['MatriculaAeronave']) > 0)
                $transporteAereo->setAttribute('MatriculaAeronave', $mercanciasDatos['TransporteAereo']['MatriculaAeronave']);

            if(isset($mercanciasDatos['TransporteAereo']['NombreAseg']) && strlen($mercanciasDatos['TransporteAereo']['NombreAseg']) > 0)
                $transporteAereo->setAttribute('NombreAseg', $mercanciasDatos['TransporteAereo']['NombreAseg']);

            if(isset($mercanciasDatos['TransporteAereo']['NumPolizaSeguro']) && strlen($mercanciasDatos['TransporteAereo']['NumPolizaSeguro']) > 0)
                $transporteAereo->setAttribute('NumPolizaSeguro', $mercanciasDatos['TransporteAereo']['NumPolizaSeguro']);

            if(isset($mercanciasDatos['TransporteAereo']['NumeroGuia']) && strlen($mercanciasDatos['TransporteAereo']['NumeroGuia']) > 0)
                $transporteAereo->setAttribute('NumeroGuia', $mercanciasDatos['TransporteAereo']['NumeroGuia']);

            if(isset($mercanciasDatos['TransporteAereo']['LugarContrato']) && strlen($mercanciasDatos['TransporteAereo']['LugarContrato']) > 0)
                $transporteAereo->setAttribute('LugarContrato', $mercanciasDatos['TransporteAereo']['LugarContrato']);

            if(isset($mercanciasDatos['TransporteAereo']['RFCTransportista']) && strlen($mercanciasDatos['TransporteAereo']['RFCTransportista']) > 0)
                $transporteAereo->setAttribute('RFCTransportista', $mercanciasDatos['TransporteAereo']['RFCTransportista']);

            if(isset($mercanciasDatos['TransporteAereo']['CodigoTransportista']) && strlen($mercanciasDatos['TransporteAereo']['CodigoTransportista']) > 0)
                $transporteAereo->setAttribute('CodigoTransportista', $mercanciasDatos['TransporteAereo']['CodigoTransportista']);

            if(isset($mercanciasDatos['TransporteAereo']['NumRegIdTribTranspor']) && strlen($mercanciasDatos['TransporteAereo']['NumRegIdTribTranspor']) > 0)
                $transporteAereo->setAttribute('NumRegIdTribTranspor', $mercanciasDatos['TransporteAereo']['NumRegIdTribTranspor']);

            if(isset($mercanciasDatos['TransporteAereo']['ResidenciaFiscalTranspor']) && strlen($mercanciasDatos['TransporteAereo']['ResidenciaFiscalTranspor']) > 0)
                $transporteAereo->setAttribute('ResidenciaFiscalTranspor', $mercanciasDatos['TransporteAereo']['ResidenciaFiscalTranspor']);

            if(isset($mercanciasDatos['TransporteAereo']['NombreTransportista']) && strlen($mercanciasDatos['TransporteAereo']['NombreTransportista']) > 0)
                $transporteAereo->setAttribute('NombreTransportista', $mercanciasDatos['TransporteAereo']['NombreTransportista']);

            if(isset($mercanciasDatos['TransporteAereo']['RFCEmbarcador']) && strlen($mercanciasDatos['TransporteAereo']['RFCEmbarcador']) > 0)
                $transporteAereo->setAttribute('RFCEmbarcador', $mercanciasDatos['TransporteAereo']['RFCEmbarcador']);

            if(isset($mercanciasDatos['TransporteAereo']['NumRegIdTribEmbarc']) && strlen($mercanciasDatos['TransporteAereo']['NumRegIdTribEmbarc']) > 0)
                $transporteAereo->setAttribute('NumRegIdTribEmbarc', $mercanciasDatos['TransporteAereo']['NumRegIdTribEmbarc']);

            if(isset($mercanciasDatos['TransporteAereo']['ResidenciaFiscalEmbarc']) && strlen($mercanciasDatos['TransporteAereo']['ResidenciaFiscalEmbarc']) > 0)
                $transporteAereo->setAttribute('ResidenciaFiscalEmbarc', $mercanciasDatos['TransporteAereo']['ResidenciaFiscalEmbarc']);

            if(isset($mercanciasDatos['TransporteAereo']['NombreEmbarcador']) && strlen($mercanciasDatos['TransporteAereo']['NombreEmbarcador']) > 0)
                $transporteAereo->setAttribute('NombreEmbarcador', $mercanciasDatos['TransporteAereo']['NombreEmbarcador']);

        }


        if(isset($mercanciasDatos['TransporteFerroviario'])){
            $transporteFerroviario = $doc->createElement('cartaporte20:TransporteFerroviario');
            $transporteFerroviario = $mercancias->appendChild($transporteFerroviario);

            foreach($mercanciasDatos['TransporteFerroviario']['DerechosDePaso'] as $derechosDePasoDatos){

                $derechosDePaso = $doc->createElement('cartaporte20:DerechosDePaso');
                $derechosDePaso = $transporteFerroviario->appendChild($derechosDePaso);

                if(isset($derechosDePasoDatos['TipoDerechoDePaso']) && strlen($derechosDePasoDatos['TipoDerechoDePaso']) > 0)
                    $derechosDePaso->setAttribute('TipoDerechoDePaso', $derechosDePasoDatos['TipoDerechoDePaso']);

                if(isset($derechosDePasoDatos['KilometrajePagado']) && strlen($derechosDePasoDatos['KilometrajePagado']) > 0)
                    $derechosDePaso->setAttribute('KilometrajePagado', $derechosDePasoDatos['KilometrajePagado']);
            }

            foreach($mercanciasDatos['TransporteFerroviario']['Carro'] as $CarroDatos){

                $carro = $doc->createElement('cartaporte20:Carro');
                $carro = $transporteFerroviario->appendChild($carro);


                foreach($CarroDatos['Contenedor'] as $contenedorDatos){

                    $contenedor = $doc->createElement('cartaporte20:Contenedor');
                    $contenedor = $carro->appendChild($contenedor);


                    if(isset($contenedorDatos['TipoContenedor']) && strlen($contenedorDatos['TipoContenedor']) > 0)
                        $contenedor->setAttribute('TipoContenedor', $contenedorDatos['TipoContenedor']);

                    if(isset($contenedorDatos['PesoContenedorVacio']) && strlen($contenedorDatos['PesoContenedorVacio']) > 0)
                        $contenedor->setAttribute('PesoContenedorVacio', $contenedorDatos['PesoContenedorVacio']);

                    if(isset($contenedorDatos['PesoNetoMercancia']) && strlen($contenedorDatos['PesoNetoMercancia']) > 0)
                        $contenedor->setAttribute('PesoNetoMercancia', $contenedorDatos['PesoNetoMercancia']);


                }
                if(isset($CarroDatos['TipoCarro']) && strlen($CarroDatos['TipoCarro']) > 0)
                    $carro->setAttribute('TipoCarro', $CarroDatos['TipoCarro']);

                if(isset($CarroDatos['MatriculaCarro']) && strlen($CarroDatos['MatriculaCarro']) > 0)
                    $carro->setAttribute('MatriculaCarro', $CarroDatos['MatriculaCarro']);

                if(isset($CarroDatos['GuiaCarro']) && strlen($CarroDatos['GuiaCarro']) > 0)
                    $carro->setAttribute('GuiaCarro', $CarroDatos['GuiaCarro']);

                if(isset($CarroDatos['ToneladasNetasCarro']) && strlen($CarroDatos['ToneladasNetasCarro']) > 0)
                    $carro->setAttribute('ToneladasNetasCarro', $CarroDatos['ToneladasNetasCarro']);

            }

            if(isset($mercanciasDatos['TransporteFerroviario']['TipoDeServicio']) && strlen($mercanciasDatos['TransporteFerroviario']['TipoDeServicio']) > 0)
                $transporteFerroviario->setAttribute('ToneladasNetasCarro', $mercanciasDatos['TransporteFerroviario']['TipoDeServicio']);

            if(isset($mercanciasDatos['TransporteFerroviario']['NombreAseg']) && strlen($mercanciasDatos['TransporteFerroviario']['NombreAseg']) > 0)
                $transporteFerroviario->setAttribute('NombreAseg', $mercanciasDatos['TransporteFerroviario']['NombreAseg']);

            if(isset($mercanciasDatos['TransporteFerroviario']['NumPolizaSeguro']) && strlen($mercanciasDatos['TransporteFerroviario']['NumPolizaSeguro']) > 0)
                $transporteFerroviario->setAttribute('NumPolizaSeguro', $mercanciasDatos['TransporteFerroviario']['NumPolizaSeguro']);

            if(isset($mercanciasDatos['TransporteFerroviario']['Concesionario']) && strlen($mercanciasDatos['TransporteFerroviario']['Concesionario']) > 0)
                $transporteFerroviario->setAttribute('Concesionario', $mercanciasDatos['TransporteFerroviario']['Concesionario']);

        }

        if(isset($cp['FiguraTransporte']['TiposFigura'][0])){

            $figuraTransporte = $doc->createElement('cartaporte20:FiguraTransporte');
            $figuraTransporte = $cartaPorte->appendChild($figuraTransporte);




            foreach ($cp['FiguraTransporte']['TiposFigura'] as $tiposFigura)
            {

                $TiposFigura = $doc->createElement('cartaporte20:TiposFigura');
                $TiposFigura = $figuraTransporte->appendChild($TiposFigura);

                if(isset($tiposFigura['PartesTransporte']))
                    foreach ($tiposFigura['PartesTransporte'] as $parteTransporte) {

                        if(isset($parteTransporte['ParteTransporte']) && strlen($parteTransporte['ParteTransporte']) > 0){
                            $PartesTransporte = $doc->createElement('cartaporte20:PartesTransporte');
                            $PartesTransporte = $TiposFigura->appendChild($PartesTransporte);
        
                            $PartesTransporte->setAttribute('ParteTransporte', $parteTransporte['ParteTransporte']);
                        }
                    }

    
                if(isset($tiposFigura['Domicilio']['Estado']) && strlen($tiposFigura['Domicilio']['Estado']) > 0){

                    $Domicilio = $doc->createElement('cartaporte20:Domicilio');
                    $Domicilio = $TiposFigura->appendChild($Domicilio);
                        
                    if(isset($tiposFigura['Domicilio']['Calle']) && strlen($tiposFigura['Domicilio']['Calle']) > 0)
                        $Domicilio->setAttribute('Calle', $tiposFigura['Domicilio']['Calle']);

                    if(isset($tiposFigura['Domicilio']['NumeroExterior']) && strlen($tiposFigura['Domicilio']['NumeroExterior']) > 0)
                        $Domicilio->setAttribute('NumeroExterior', $tiposFigura['Domicilio']['NumeroExterior']);

                    if(isset($tiposFigura['Domicilio']['NumeroInterior']) && strlen($tiposFigura['Domicilio']['NumeroInterior']) > 0)
                        $Domicilio->setAttribute('NumeroInterior', $tiposFigura['Domicilio']['NumeroInterior']);

                    if(isset($tiposFigura['Domicilio']['Colonia']) && strlen($tiposFigura['Domicilio']['Colonia']) > 0)
                        $Domicilio->setAttribute('Colonia', $tiposFigura['Domicilio']['Colonia']);

                    if(isset($tiposFigura['Domicilio']['Localidad']) && strlen($tiposFigura['Domicilio']['Localidad']) > 0)
                        $Domicilio->setAttribute('Localidad', $tiposFigura['Domicilio']['Localidad']);

                    if(isset($tiposFigura['Domicilio']['Referencia']) && strlen($tiposFigura['Domicilio']['Referencia']) > 0)
                        $Domicilio->setAttribute('Referencia', $tiposFigura['Domicilio']['Referencia']);

                    if(isset($tiposFigura['Domicilio']['Municipio']) && strlen($tiposFigura['Domicilio']['Municipio']) > 0)
                        $Domicilio->setAttribute('Municipio', $tiposFigura['Domicilio']['Municipio']);

                    if(isset($tiposFigura['Domicilio']['Estado']) && strlen($tiposFigura['Domicilio']['Estado']) > 0)
                        $Domicilio->setAttribute('Estado', $tiposFigura['Domicilio']['Estado']);

                    if(isset($tiposFigura['Domicilio']['Pais']) && strlen($tiposFigura['Domicilio']['Pais']) > 0)
                        $Domicilio->setAttribute('Pais', $tiposFigura['Domicilio']['Pais']);

                    if(isset($tiposFigura['Domicilio']['CodigoPostal']) && strlen($tiposFigura['Domicilio']['CodigoPostal']) > 0)
                        $Domicilio->setAttribute('CodigoPostal', $tiposFigura['Domicilio']['CodigoPostal']);

                }

                    
                if(isset($tiposFigura['TipoFigura']) && strlen($tiposFigura['TipoFigura']) > 0)
                    $TiposFigura->setAttribute('TipoFigura', $tiposFigura['TipoFigura']);

                if(isset($tiposFigura['RFCFigura']) && strlen($tiposFigura['RFCFigura']) > 0)
                    $TiposFigura->setAttribute('RFCFigura', $tiposFigura['RFCFigura']);

                if(isset($tiposFigura['NumLicencia']) && strlen($tiposFigura['NumLicencia']) > 0)
                    $TiposFigura->setAttribute('NumLicencia', $tiposFigura['NumLicencia']);

                if(isset($tiposFigura['NombreFigura']) && strlen($tiposFigura['NombreFigura']) > 0)
                    $TiposFigura->setAttribute('NombreFigura', $tiposFigura['NombreFigura']);

                if(isset($tiposFigura['NumRegIdTribFigura']) && strlen($tiposFigura['NumRegIdTribFigura']) > 0)
                    $TiposFigura->setAttribute('NumRegIdTribFigura', $tiposFigura['NumRegIdTribFigura']);

                if(isset($tiposFigura['ResidenciaFiscalFigura']) && strlen($tiposFigura['ResidenciaFiscalFigura']) > 0)
                    $TiposFigura->setAttribute('ResidenciaFiscalFigura', $tiposFigura['ResidenciaFiscalFigura']);

            }
        }

    }
}