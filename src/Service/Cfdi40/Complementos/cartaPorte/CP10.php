<?php

namespace App\Service\Cfdi40\Complementos\cartaPorte;

use App\Service\Cfdi33\Complementos\Complemento;

class CP10 extends Complemento
{
    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request) {


        if (!isset($request['Comprobante']['Complemento']['CartaPorte'])) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para agregar el complemento de nomina12
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request)
    {

        if (!self::existeComplemento($request)) {
            return $doc;
        }

        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        $cp = $request['Comprobante']['Complemento']['CartaPorte'];

        $cartaPorte = $doc->createElement('cartaporte:CartaPorte');
        $cartaPorte = $ComplementoNode->appendChild($cartaPorte);
        //$Nomina->setAttribute('xmlns:nomina', "http://www.sat.gob.mx/nomina");
        $cartaPorte->setAttribute('Version', $cp['Version']);
        if(isset($cp['TranspInternac']) && strlen($cp['TranspInternac']) > 0)
            $cartaPorte->setAttribute('TranspInternac', $cp['TranspInternac']);
        if(isset($cp['EntradaSalidaMerc']) && strlen($cp['EntradaSalidaMerc']) > 0)
            $cartaPorte->setAttribute('EntradaSalidaMerc', $cp['EntradaSalidaMerc']);
        if(isset($cp['ViaEntradaSalida']) && strlen($cp['ViaEntradaSalida']) > 0)
            $cartaPorte->setAttribute('ViaEntradaSalida', $cp['ViaEntradaSalida']);
        if(isset($cp['TotalDistRec']) && strlen($cp['TotalDistRec']) > 0)
            $cartaPorte->setAttribute('TotalDistRec', $cp['TotalDistRec']);

        $ubicaciones = $doc->createElement('cartaporte:Ubicaciones');
        $ubicaciones = $cartaPorte->appendChild($ubicaciones);

        foreach($cp['Ubicaciones']['Ubicacion'] as $ubicacionesDatos){

            $ubicacion = $doc->createElement('cartaporte:Ubicacion');
            $ubicacion = $ubicaciones->appendChild($ubicacion);


            if(isset($ubicacionesDatos['Origen']['FechaHoraSalida']) && strlen($ubicacionesDatos['Origen']['FechaHoraSalida']) > 0){
                $origen = $doc->createElement('cartaporte:Origen');
                $origen = $ubicacion->appendChild($origen);
            }

            if(isset($ubicacionesDatos['Origen']['IDOrigen']) && strlen($ubicacionesDatos['Origen']['IDOrigen']) > 0){
                $origen->setAttribute('IDOrigen', $ubicacionesDatos['Origen']['IDOrigen']);
            }

            if(isset($ubicacionesDatos['Origen']['RFCRemitente']) && strlen($ubicacionesDatos['Origen']['RFCRemitente']) > 0)
                $origen->setAttribute('RFCRemitente', $ubicacionesDatos['Origen']['RFCRemitente']);
            if(isset($ubicacionesDatos['Origen']['NombreRemitente']) && strlen($ubicacionesDatos['Origen']['NombreRemitente']) > 0)
                $origen->setAttribute('NombreRemitente', $ubicacionesDatos['Origen']['NombreRemitente']);
            if(isset($ubicacionesDatos['Origen']['NumRegIdTrib']) && strlen($ubicacionesDatos['Origen']['NumRegIdTrib']) > 0)
                $origen->setAttribute('NumRegIdTrib', $ubicacionesDatos['Origen']['NumRegIdTrib']);
            if(isset($ubicacionesDatos['Origen']['ResidenciaFiscal']) && strlen($ubicacionesDatos['Origen']['ResidenciaFiscal']) > 0)
                $origen->setAttribute('ResidenciaFiscal', $ubicacionesDatos['Origen']['ResidenciaFiscal']);
            if(isset($ubicacionesDatos['Origen']['NumEstacion']) && strlen($ubicacionesDatos['Origen']['NumEstacion']) > 0)
                $origen->setAttribute('NumEstacion', $ubicacionesDatos['Origen']['NumEstacion']);
            if(isset($ubicacionesDatos['Origen']['NombreEstacion']) && strlen($ubicacionesDatos['Origen']['NombreEstacion']) > 0)
                $origen->setAttribute('NombreEstacion', $ubicacionesDatos['Origen']['NombreEstacion']);
            if(isset($ubicacionesDatos['Origen']['NavegacionTrafico']) && strlen($ubicacionesDatos['Origen']['NavegacionTrafico']) > 0)
                $origen->setAttribute('NavegacionTrafico', $ubicacionesDatos['Origen']['NavegacionTrafico']);
            if(isset($ubicacionesDatos['Origen']['FechaHoraSalida']) && strlen($ubicacionesDatos['Origen']['FechaHoraSalida']) > 0)
                $origen->setAttribute('FechaHoraSalida', $ubicacionesDatos['Origen']['FechaHoraSalida']);


            if(isset($ubicacionesDatos['Destino']['FechaHoraProgLlegada']) && strlen($ubicacionesDatos['Destino']['FechaHoraProgLlegada']) > 0){
                $destino = $doc->createElement('cartaporte:Destino');
                $destino = $ubicacion->appendChild($destino);

            }

            if(isset($ubicacionesDatos['Destino']['IDDestino']) && strlen($ubicacionesDatos['Destino']['IDDestino']) > 0){
                $destino->setAttribute('IDDestino', $ubicacionesDatos['Destino']['IDDestino']);

            }



            if(isset($ubicacionesDatos['Destino']['RFCDestinatario']) && strlen($ubicacionesDatos['Destino']['RFCDestinatario']) > 0)
                $destino->setAttribute('RFCDestinatario', $ubicacionesDatos['Destino']['RFCDestinatario']);
            if(isset($ubicacionesDatos['Destino']['NombreDestinatario']) && strlen($ubicacionesDatos['Destino']['NombreDestinatario']) > 0)
                $destino->setAttribute('NombreDestinatario', $ubicacionesDatos['Destino']['NombreDestinatario']);
            if(isset($ubicacionesDatos['Destino']['NumRegIdTrib']) && strlen($ubicacionesDatos['Destino']['NumRegIdTrib']) > 0)
                $destino->setAttribute('NumRegIdTrib', $ubicacionesDatos['Destino']['NumRegIdTrib']);
            if(isset($ubicacionesDatos['Destino']['ResidenciaFiscal']) && strlen($ubicacionesDatos['Destino']['ResidenciaFiscal']) > 0)
                $destino->setAttribute('ResidenciaFiscal', $ubicacionesDatos['Destino']['ResidenciaFiscal']);
            if(isset($ubicacionesDatos['Destino']['NumEstacion']) && strlen($ubicacionesDatos['Destino']['NumEstacion']) > 0)
                $destino->setAttribute('NumEstacion', $ubicacionesDatos['Destino']['NumEstacion']);
            if(isset($ubicacionesDatos['Destino']['NombreEstacion']) && strlen($ubicacionesDatos['Destino']['NombreEstacion']) > 0)
                $destino->setAttribute('NombreEstacion', $ubicacionesDatos['Destino']['NombreEstacion']);
            if(isset($ubicacionesDatos['Destino']['NavegacionTrafico']) && strlen($ubicacionesDatos['Destino']['NavegacionTrafico']) > 0)
                $destino->setAttribute('NavegacionTrafico', $ubicacionesDatos['Destino']['NavegacionTrafico']);

            if(isset($ubicacionesDatos['Destino']['FechaHoraProgLlegada']) && strlen($ubicacionesDatos['Destino']['FechaHoraProgLlegada']) > 0)
                $destino->setAttribute('FechaHoraProgLlegada', $ubicacionesDatos['Destino']['FechaHoraProgLlegada']);

            $domicilio = $doc->createElement('cartaporte:Domicilio');
            $domicilio = $ubicacion->appendChild($domicilio);

            if(isset($ubicacionesDatos['Domicilio']['Calle']) && strlen($ubicacionesDatos['Domicilio']['Calle']) > 0)
                $domicilio->setAttribute('Calle', $ubicacionesDatos['Domicilio']['Calle']);
            if(isset($ubicacionesDatos['Domicilio']['NumeroExterior']) && strlen($ubicacionesDatos['Domicilio']['NumeroExterior']) > 0)
                $domicilio->setAttribute('NumeroExterior', $ubicacionesDatos['Domicilio']['NumeroExterior']);
            if(isset($ubicacionesDatos['Domicilio']['NumeroInterior']) && strlen($ubicacionesDatos['Domicilio']['NumeroInterior']) > 0)
                $domicilio->setAttribute('NumeroInterior', $ubicacionesDatos['Domicilio']['NumeroInterior']);
            if(isset($ubicacionesDatos['Domicilio']['Colonia']) && strlen($ubicacionesDatos['Domicilio']['Colonia']) > 0)
                $domicilio->setAttribute('Colonia', $ubicacionesDatos['Domicilio']['Colonia']);
            if(isset($ubicacionesDatos['Domicilio']['Localidad']) && strlen($ubicacionesDatos['Domicilio']['Localidad']) > 0)
                $domicilio->setAttribute('Localidad', $ubicacionesDatos['Domicilio']['Localidad']);
            if(isset($ubicacionesDatos['Domicilio']['Referencia']) && strlen($ubicacionesDatos['Domicilio']['Referencia']) > 0)
                $domicilio->setAttribute('Referencia', $ubicacionesDatos['Domicilio']['Referencia']);
            if(isset($ubicacionesDatos['Domicilio']['Municipio']) && strlen($ubicacionesDatos['Domicilio']['Municipio']) > 0)
                $domicilio->setAttribute('Municipio', $ubicacionesDatos['Domicilio']['Municipio']);
            if(isset($ubicacionesDatos['Domicilio']['Estado']) && strlen($ubicacionesDatos['Domicilio']['Estado']) > 0)
                $domicilio->setAttribute('Estado', $ubicacionesDatos['Domicilio']['Estado']);
            if(isset($ubicacionesDatos['Domicilio']['Pais']) && strlen($ubicacionesDatos['Domicilio']['Pais']) > 0)
                $domicilio->setAttribute('Pais', $ubicacionesDatos['Domicilio']['Pais']);
            if(isset($ubicacionesDatos['Domicilio']['CodigoPostal']) && strlen($ubicacionesDatos['Domicilio']['CodigoPostal']) > 0)
                $domicilio->setAttribute('CodigoPostal', $ubicacionesDatos['Domicilio']['CodigoPostal']);


            if(isset($ubicacionesDatos['TipoEstacion']) && strlen($ubicacionesDatos['TipoEstacion']) > 0)
                $ubicacion->setAttribute('TipoEstacion', $ubicacionesDatos['TipoEstacion']);
            if(isset($ubicacionesDatos['DistanciaRecorrida']) && strlen($ubicacionesDatos['DistanciaRecorrida']) > 0)
                $ubicacion->setAttribute('DistanciaRecorrida', $ubicacionesDatos['DistanciaRecorrida']);

        }

        $mercancias = $doc->createElement('cartaporte:Mercancias');
        $mercancias = $cartaPorte->appendChild($mercancias);

        if(isset($cp['Mercancias']['PesoBrutoTotal']) && strlen($cp['Mercancias']['PesoBrutoTotal']) > 0)
            $mercancias->setAttribute('PesoBrutoTotal', $cp['Mercancias']['PesoBrutoTotal']);

        if(isset($cp['Mercancias']['UnidadPeso']) && strlen($cp['Mercancias']['UnidadPeso']) > 0)
            $mercancias->setAttribute('UnidadPeso', $cp['Mercancias']['UnidadPeso']);

        if(isset($cp['Mercancias']['PesoNetoTotal']) && strlen($cp['Mercancias']['PesoNetoTotal']) > 0)
            $mercancias->setAttribute('PesoNetoTotal', $cp['Mercancias']['PesoNetoTotal']);

        if(isset($cp['Mercancias']['NumTotalMercancias']) && strlen($cp['Mercancias']['NumTotalMercancias']) > 0)
            $mercancias->setAttribute('NumTotalMercancias', $cp['Mercancias']['NumTotalMercancias']);

        if(isset($cp['Mercancias']['CargoPorTasacion']) && strlen($cp['Mercancias']['CargoPorTasacion']) > 0)
            $mercancias->setAttribute('CargoPorTasacion', $cp['Mercancias']['CargoPorTasacion']);

        foreach($cp['Mercancias']['Mercancia'] as $mercancia){

            $mercanciaDom = $doc->createElement('cartaporte:Mercancia');
            $mercanciaDom = $mercancias->appendChild($mercanciaDom);

            foreach($mercancia['CantidadTransporta'] as $cantidadTransportaDatos){

                $cantidadtransporta = $doc->createElement('cartaporte:CantidadTransporta');
                $cantidadtransporta = $mercanciaDom->appendChild($cantidadtransporta);

                if(isset($cantidadTransportaDatos['Cantidad']) && strlen($cantidadTransportaDatos['Cantidad']) > 0)
                    $cantidadtransporta->setAttribute('Cantidad', $cantidadTransportaDatos['Cantidad']);

                if(isset($cantidadTransportaDatos['IDOrigen']) && strlen($cantidadTransportaDatos['IDOrigen']) > 0)
                    $cantidadtransporta->setAttribute('IDOrigen', $cantidadTransportaDatos['IDOrigen']);

                if(isset($cantidadTransportaDatos['IDDestino']) && strlen($cantidadTransportaDatos['IDDestino']) > 0)
                    $cantidadtransporta->setAttribute('IDDestino', $cantidadTransportaDatos['IDDestino']);

                if(isset($cantidadTransportaDatos['CvesTransporte']) && strlen($cantidadTransportaDatos['CvesTransporte']) > 0)
                    $cantidadtransporta->setAttribute('CvesTransporte', $cantidadTransportaDatos['CvesTransporte']);
            }


            if(isset($mercancia['DetalleMercancia']['UnidadPeso']) && strlen($mercancia['DetalleMercancia']['UnidadPeso']) > 0){

                $detalleMercancias = $doc->createElement('cartaporte:DetalleMercancia');
                $detalleMercancias = $mercanciaDom->appendChild($detalleMercancias);

                $detalleMercancias->setAttribute('UnidadPeso', $mercancia['DetalleMercancia']['UnidadPeso']);
            }

            if(isset($mercancia['DetalleMercancia']['PesoBruto']) && strlen($mercancia['DetalleMercancia']['PesoBruto']) > 0)
                $detalleMercancias->setAttribute('PesoBruto', $mercancia['DetalleMercancia']['PesoBruto']);
            if(isset($mercancia['DetalleMercancia']['PesoNeto']) && strlen($mercancia['DetalleMercancia']['PesoNeto']) > 0)
                $detalleMercancias->setAttribute('PesoNeto', $mercancia['DetalleMercancia']['PesoNeto']);
            if(isset($mercancia['DetalleMercancia']['PesoTara']) && strlen($mercancia['DetalleMercancia']['PesoTara']) > 0)
                $detalleMercancias->setAttribute('PesoTara', $mercancia['DetalleMercancia']['PesoTara']);
            if(isset($mercancia['DetalleMercancia']['NumPiezas']) && strlen($mercancia['DetalleMercancia']['NumPiezas']) > 0)
                $detalleMercancias->setAttribute('NumPiezas', $mercancia['DetalleMercancia']['NumPiezas']);


            if(isset($mercancia['BienesTransp']) && strlen($mercancia['BienesTransp']) > 0)
                $mercanciaDom->setAttribute('BienesTransp', $mercancia['BienesTransp']);

            if(isset($mercancia['ClaveSTCC']) && strlen($mercancia['ClaveSTCC']) > 0)
                $mercanciaDom->setAttribute('ClaveSTCC', $mercancia['ClaveSTCC']);

            if(isset($mercancia['BienesTransp']) && strlen($mercancia['BienesTransp']) > 0)
                $mercanciaDom->setAttribute('BienesTransp', $mercancia['BienesTransp']);

            if(isset($mercancia['Descripcion']) && strlen($mercancia['Descripcion']) > 0)
                $mercanciaDom->setAttribute('Descripcion', $mercancia['Descripcion']);

            if(isset($mercancia['Cantidad']) && strlen($mercancia['Cantidad']) > 0)
                $mercanciaDom->setAttribute('Cantidad', $mercancia['Cantidad']);

            if(isset($mercancia['ClaveUnidad']) && strlen($mercancia['ClaveUnidad']) > 0)
                $mercanciaDom->setAttribute('ClaveUnidad', $mercancia['ClaveUnidad']);

            if(isset($mercancia['Unidad']) && strlen($mercancia['Unidad']) > 0)
                $mercanciaDom->setAttribute('Unidad', $mercancia['Unidad']);

            if(isset($mercancia['Dimensiones']) && strlen($mercancia['Dimensiones']) > 0)
                $mercanciaDom->setAttribute('Dimensiones', $mercancia['Dimensiones']);

            if(isset($mercancia['MaterialPeligroso']) && strlen($mercancia['MaterialPeligroso']) > 0)
                $mercanciaDom->setAttribute('MaterialPeligroso', $mercancia['MaterialPeligroso']);

            if(isset($mercancia['CveMaterialPeligroso']) && strlen($mercancia['CveMaterialPeligroso']) > 0)
                $mercanciaDom->setAttribute('CveMaterialPeligroso', $mercancia['CveMaterialPeligroso']);

            if(isset($mercancia['Embalaje']) && strlen($mercancia['Embalaje']) > 0)
                $mercanciaDom->setAttribute('Embalaje', $mercancia['Embalaje']);

            if(isset($mercancia['DescripEmbalaje']) && strlen($mercancia['DescripEmbalaje']) > 0)
                $mercanciaDom->setAttribute('DescripEmbalaje', $mercancia['DescripEmbalaje']);

            if(isset($mercancia['PesoEnKg']) && strlen($mercancia['PesoEnKg']) > 0)
                $mercanciaDom->setAttribute('PesoEnKg', $mercancia['PesoEnKg']);

            if(isset($mercancia['ValorMercancia']) && strlen($mercancia['ValorMercancia']) > 0)
                $mercanciaDom->setAttribute('ValorMercancia', $mercancia['ValorMercancia']);

            if(isset($mercancia['Moneda']) && strlen($mercancia['Moneda']) > 0)
                $mercanciaDom->setAttribute('Moneda', $mercancia['Moneda']);

            if(isset($mercancia['FraccionArancelaria']) && strlen($mercancia['FraccionArancelaria']) > 0)
                $mercanciaDom->setAttribute('FraccionArancelaria', $mercancia['FraccionArancelaria']);

            if(isset($mercancia['UUIDComercioExt']) && strlen($mercancia['UUIDComercioExt']) > 0)
                $mercanciaDom->setAttribute('UUIDComercioExt', $mercancia['UUIDComercioExt']);

        }

        $autoTransporteFederal = $doc->createElement('cartaporte:AutotransporteFederal');
        $autoTransporteFederal = $mercancias->appendChild($autoTransporteFederal);

        $identificacionVehicular = $doc->createElement('cartaporte:IdentificacionVehicular');
        $identificacionVehicular = $autoTransporteFederal->appendChild($identificacionVehicular);
        $mercanciasDatos = $cp['Mercancias'];
        if(isset($mercanciasDatos['AutotransporteFederal']['IdentificacionVehicular']['ConfigVehicular']) && strlen($mercanciasDatos['AutotransporteFederal']['IdentificacionVehicular']['ConfigVehicular']) > 0)
            $identificacionVehicular->setAttribute('ConfigVehicular', $mercanciasDatos['AutotransporteFederal']['IdentificacionVehicular']['ConfigVehicular']);

        if(isset($mercanciasDatos['AutotransporteFederal']['IdentificacionVehicular']['PlacaVM']) && strlen($mercanciasDatos['AutotransporteFederal']['IdentificacionVehicular']['PlacaVM']) > 0)
            $identificacionVehicular->setAttribute('PlacaVM', $mercanciasDatos['AutotransporteFederal']['IdentificacionVehicular']['PlacaVM']);

        if(isset($mercanciasDatos['AutotransporteFederal']['IdentificacionVehicular']['AnioModeloVM']) && strlen($mercanciasDatos['AutotransporteFederal']['IdentificacionVehicular']['AnioModeloVM']) > 0)
            $identificacionVehicular->setAttribute('AnioModeloVM', $mercanciasDatos['AutotransporteFederal']['IdentificacionVehicular']['AnioModeloVM']);

        $remolques = $doc->createElement('cartaporte:Remolques');
        $remolques = $autoTransporteFederal->appendChild($remolques);

        foreach($mercanciasDatos['AutotransporteFederal']['Remolques']['Remolque'] as $remolqueDatos){

            if(isset($remolqueDatos['SubTipoRem']) && strlen($remolqueDatos['SubTipoRem']) > 0){

                $remolque = $doc->createElement('cartaporte:Remolque');
                $remolque = $remolques->appendChild($remolque);
                $remolque->setAttribute('SubTipoRem', $remolqueDatos['SubTipoRem']);

            }

            if(isset($remolqueDatos['Placa']) && strlen($remolqueDatos['Placa']) > 0)
                $remolque->setAttribute('Placa', $remolqueDatos['Placa']);

        }

        if(isset($mercanciasDatos['AutotransporteFederal']['PermSCT']) && strlen($mercanciasDatos['AutotransporteFederal']['PermSCT']) > 0)
            $autoTransporteFederal->setAttribute('PermSCT', $mercanciasDatos['AutotransporteFederal']['PermSCT']);

        if(isset($mercanciasDatos['AutotransporteFederal']['NumPermisoSCT']) && strlen($mercanciasDatos['AutotransporteFederal']['NumPermisoSCT']) > 0)
            $autoTransporteFederal->setAttribute('NumPermisoSCT', $mercanciasDatos['AutotransporteFederal']['NumPermisoSCT']);

        if(isset($mercanciasDatos['AutotransporteFederal']['NombreAseg']) && strlen($mercanciasDatos['AutotransporteFederal']['NombreAseg']) > 0)
            $autoTransporteFederal->setAttribute('NombreAseg', $mercanciasDatos['AutotransporteFederal']['NombreAseg']);

        if(isset($mercanciasDatos['AutotransporteFederal']['NumPolizaSeguro']) && strlen($mercanciasDatos['AutotransporteFederal']['NumPolizaSeguro']) > 0)
            $autoTransporteFederal->setAttribute('NumPolizaSeguro', $mercanciasDatos['AutotransporteFederal']['NumPolizaSeguro']);


        if(isset($mercanciasDatos['TransporteMaritimo'])){
            $transporteMaritimo = $doc->createElement('cartaporte:TransporteMaritimo');
            $transporteMaritimo = $mercancias->appendChild($transporteMaritimo);

            foreach($mercanciasDatos['TransporteMaritimo']['Contenedor'] as $contenedorDatos){
                $contenedor = $doc->createElement('cartaporte:Contenedor');
                $contenedor = $transporteMaritimo->appendChild($contenedor);

                if(isset($contenedorDatos['MatriculaContenedor']) && strlen($contenedorDatos['MatriculaContenedor']) > 0)
                    $contenedor->setAttribute('TransporteMaritimo', $contenedorDatos['MatriculaContenedor']);

                if(isset($contenedorDatos['TipoContenedor']) && strlen($contenedorDatos['TipoContenedor']) > 0)
                    $contenedor->setAttribute('TipoContenedor', $contenedorDatos['TipoContenedor']);

                if(isset($contenedorDatos['NumPrecinto']) && strlen($contenedorDatos['NumPrecinto']) > 0)
                    $contenedor->setAttribute('NumPrecinto', $contenedorDatos['NumPrecinto']);

            }

            if(isset($mercanciasDatos['TransporteMaritimo']['PermSCT']) && strlen($mercanciasDatos['TransporteMaritimo']['PermSCT']) > 0)
                $transporteMaritimo->setAttribute('PermSCT', $mercanciasDatos['TransporteMaritimo']['PermSCT']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumPermisoSCT']) && strlen($mercanciasDatos['TransporteMaritimo']['NumPermisoSCT']) > 0)
                $transporteMaritimo->setAttribute('NumPermisoSCT', $mercanciasDatos['TransporteMaritimo']['NumPermisoSCT']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NombreAseg']) && strlen($mercanciasDatos['TransporteMaritimo']['NombreAseg']) > 0)
                $transporteMaritimo->setAttribute('NombreAseg', $mercanciasDatos['TransporteMaritimo']['NombreAseg']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumPolizaSeguro']) && strlen($mercanciasDatos['TransporteMaritimo']['NumPolizaSeguro']) > 0)
                $transporteMaritimo->setAttribute('NumPolizaSeguro', $mercanciasDatos['TransporteMaritimo']['NumPolizaSeguro']);

            if(isset($mercanciasDatos['TransporteMaritimo']['TipoEmbarcacion']) && strlen($mercanciasDatos['TransporteMaritimo']['TipoEmbarcacion']) > 0)
                $transporteMaritimo->setAttribute('TipoEmbarcacion', $mercanciasDatos['TransporteMaritimo']['TipoEmbarcacion']);

            if(isset($mercanciasDatos['TransporteMaritimo']['Matricula']) && strlen($mercanciasDatos['TransporteMaritimo']['Matricula']) > 0)
                $transporteMaritimo->setAttribute('Matricula', $mercanciasDatos['TransporteMaritimo']['Matricula']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumeroOMI']) && strlen($mercanciasDatos['TransporteMaritimo']['NumeroOMI']) > 0)
                $transporteMaritimo->setAttribute('NumeroOMI', $mercanciasDatos['TransporteMaritimo']['NumeroOMI']);

            if(isset($mercanciasDatos['TransporteMaritimo']['AnioEmbarcacion']) && strlen($mercanciasDatos['TransporteMaritimo']['AnioEmbarcacion']) > 0)
                $transporteMaritimo->setAttribute('AnioEmbarcacion', $mercanciasDatos['TransporteMaritimo']['AnioEmbarcacion']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NombreEmbarc']) && strlen($mercanciasDatos['TransporteMaritimo']['NombreEmbarc']) > 0)
                $transporteMaritimo->setAttribute('NombreEmbarc', $mercanciasDatos['TransporteMaritimo']['NombreEmbarc']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NacionalidadEmbarc']) && strlen($mercanciasDatos['TransporteMaritimo']['NacionalidadEmbarc']) > 0)
                $transporteMaritimo->setAttribute('NacionalidadEmbarc', $mercanciasDatos['TransporteMaritimo']['NacionalidadEmbarc']);

            if(isset($mercanciasDatos['TransporteMaritimo']['UnidadesDeArqBruto']) && strlen($mercanciasDatos['TransporteMaritimo']['UnidadesDeArqBruto']) > 0)
                $transporteMaritimo->setAttribute('UnidadesDeArqBruto', $mercanciasDatos['TransporteMaritimo']['UnidadesDeArqBruto']);

            if(isset($mercanciasDatos['TransporteMaritimo']['TipoCarga']) && strlen($mercanciasDatos['TransporteMaritimo']['TipoCarga']) > 0)
                $transporteMaritimo->setAttribute('TipoCarga', $mercanciasDatos['TransporteMaritimo']['TipoCarga']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumCertITC']) && strlen($mercanciasDatos['TransporteMaritimo']['NumCertITC']) > 0)
                $transporteMaritimo->setAttribute('NumCertITC', $mercanciasDatos['TransporteMaritimo']['NumCertITC']);

            if(isset($mercanciasDatos['TransporteMaritimo']['Eslora']) && strlen($mercanciasDatos['TransporteMaritimo']['Eslora']) > 0)
                $transporteMaritimo->setAttribute('Eslora', $mercanciasDatos['TransporteMaritimo']['Eslora']);

            if(isset($mercanciasDatos['TransporteMaritimo']['Manga']) && strlen($mercanciasDatos['TransporteMaritimo']['Manga']) > 0)
                $transporteMaritimo->setAttribute('Manga', $mercanciasDatos['TransporteMaritimo']['Manga']);

            if(isset($mercanciasDatos['TransporteMaritimo']['Calado']) && strlen($mercanciasDatos['TransporteMaritimo']['Calado']) > 0)
                $transporteMaritimo->setAttribute('Calado', $mercanciasDatos['TransporteMaritimo']['Calado']);

            if(isset($mercanciasDatos['TransporteMaritimo']['LineaNaviera']) && strlen($mercanciasDatos['TransporteMaritimo']['LineaNaviera']) > 0)
                $transporteMaritimo->setAttribute('LineaNaviera', $mercanciasDatos['TransporteMaritimo']['LineaNaviera']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NombreAgenteNaviero']) && strlen($mercanciasDatos['TransporteMaritimo']['NombreAgenteNaviero']) > 0)
                $transporteMaritimo->setAttribute('NombreAgenteNaviero', $mercanciasDatos['TransporteMaritimo']['NombreAgenteNaviero']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumAutorizacionNaviero']) && strlen($mercanciasDatos['TransporteMaritimo']['NumAutorizacionNaviero']) > 0)
                $transporteMaritimo->setAttribute('NumAutorizacionNaviero', $mercanciasDatos['TransporteMaritimo']['NumAutorizacionNaviero']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumViaje']) && strlen($mercanciasDatos['TransporteMaritimo']['NumViaje']) > 0)
                $transporteMaritimo->setAttribute('NumViaje', $mercanciasDatos['TransporteMaritimo']['NumViaje']);

            if(isset($mercanciasDatos['TransporteMaritimo']['NumConocEmbarc']) && strlen($mercanciasDatos['TransporteMaritimo']['NumConocEmbarc']) > 0)
                $transporteMaritimo->setAttribute('NumConocEmbarc', $mercanciasDatos['TransporteMaritimo']['NumConocEmbarc']);

        }

        if(isset($mercanciasDatos['TransporteAereo'])){
            $transporteAereo = $doc->createElement('cartaporte:TransporteAereo');
            $transporteAereo = $mercancias->appendChild($transporteAereo);


            if(isset($mercanciasDatos['TransporteAereo']['PermSCT']) && strlen($mercanciasDatos['TransporteAereo']['PermSCT']) > 0)
                $transporteAereo->setAttribute('PermSCT', $mercanciasDatos['TransporteAereo']['PermSCT']);

            if(isset($mercanciasDatos['TransporteAereo']['NumPermisoSCT']) && strlen($mercanciasDatos['TransporteAereo']['NumPermisoSCT']) > 0)
                $transporteAereo->setAttribute('NumPermisoSCT', $mercanciasDatos['TransporteAereo']['NumPermisoSCT']);

            if(isset($mercanciasDatos['TransporteAereo']['MatriculaAeronave']) && strlen($mercanciasDatos['TransporteAereo']['MatriculaAeronave']) > 0)
                $transporteAereo->setAttribute('MatriculaAeronave', $mercanciasDatos['TransporteAereo']['MatriculaAeronave']);

            if(isset($mercanciasDatos['TransporteAereo']['NombreAseg']) && strlen($mercanciasDatos['TransporteAereo']['NombreAseg']) > 0)
                $transporteAereo->setAttribute('NombreAseg', $mercanciasDatos['TransporteAereo']['NombreAseg']);

            if(isset($mercanciasDatos['TransporteAereo']['NumPolizaSeguro']) && strlen($mercanciasDatos['TransporteAereo']['NumPolizaSeguro']) > 0)
                $transporteAereo->setAttribute('NumPolizaSeguro', $mercanciasDatos['TransporteAereo']['NumPolizaSeguro']);

            if(isset($mercanciasDatos['TransporteAereo']['NumeroGuia']) && strlen($mercanciasDatos['TransporteAereo']['NumeroGuia']) > 0)
                $transporteAereo->setAttribute('NumeroGuia', $mercanciasDatos['TransporteAereo']['NumeroGuia']);

            if(isset($mercanciasDatos['TransporteAereo']['LugarContrato']) && strlen($mercanciasDatos['TransporteAereo']['LugarContrato']) > 0)
                $transporteAereo->setAttribute('LugarContrato', $mercanciasDatos['TransporteAereo']['LugarContrato']);

            if(isset($mercanciasDatos['TransporteAereo']['RFCTransportista']) && strlen($mercanciasDatos['TransporteAereo']['RFCTransportista']) > 0)
                $transporteAereo->setAttribute('RFCTransportista', $mercanciasDatos['TransporteAereo']['RFCTransportista']);

            if(isset($mercanciasDatos['TransporteAereo']['CodigoTransportista']) && strlen($mercanciasDatos['TransporteAereo']['CodigoTransportista']) > 0)
                $transporteAereo->setAttribute('CodigoTransportista', $mercanciasDatos['TransporteAereo']['CodigoTransportista']);

            if(isset($mercanciasDatos['TransporteAereo']['NumRegIdTribTranspor']) && strlen($mercanciasDatos['TransporteAereo']['NumRegIdTribTranspor']) > 0)
                $transporteAereo->setAttribute('NumRegIdTribTranspor', $mercanciasDatos['TransporteAereo']['NumRegIdTribTranspor']);

            if(isset($mercanciasDatos['TransporteAereo']['ResidenciaFiscalTranspor']) && strlen($mercanciasDatos['TransporteAereo']['ResidenciaFiscalTranspor']) > 0)
                $transporteAereo->setAttribute('ResidenciaFiscalTranspor', $mercanciasDatos['TransporteAereo']['ResidenciaFiscalTranspor']);

            if(isset($mercanciasDatos['TransporteAereo']['NombreTransportista']) && strlen($mercanciasDatos['TransporteAereo']['NombreTransportista']) > 0)
                $transporteAereo->setAttribute('NombreTransportista', $mercanciasDatos['TransporteAereo']['NombreTransportista']);

            if(isset($mercanciasDatos['TransporteAereo']['RFCEmbarcador']) && strlen($mercanciasDatos['TransporteAereo']['RFCEmbarcador']) > 0)
                $transporteAereo->setAttribute('RFCEmbarcador', $mercanciasDatos['TransporteAereo']['RFCEmbarcador']);

            if(isset($mercanciasDatos['TransporteAereo']['NumRegIdTribEmbarc']) && strlen($mercanciasDatos['TransporteAereo']['NumRegIdTribEmbarc']) > 0)
                $transporteAereo->setAttribute('NumRegIdTribEmbarc', $mercanciasDatos['TransporteAereo']['NumRegIdTribEmbarc']);

            if(isset($mercanciasDatos['TransporteAereo']['ResidenciaFiscalEmbarc']) && strlen($mercanciasDatos['TransporteAereo']['ResidenciaFiscalEmbarc']) > 0)
                $transporteAereo->setAttribute('ResidenciaFiscalEmbarc', $mercanciasDatos['TransporteAereo']['ResidenciaFiscalEmbarc']);

            if(isset($mercanciasDatos['TransporteAereo']['NombreEmbarcador']) && strlen($mercanciasDatos['TransporteAereo']['NombreEmbarcador']) > 0)
                $transporteAereo->setAttribute('NombreEmbarcador', $mercanciasDatos['TransporteAereo']['NombreEmbarcador']);

        }


        if(isset($mercanciasDatos['TransporteFerroviario'])){
            $transporteFerroviario = $doc->createElement('cartaporte:TransporteFerroviario');
            $transporteFerroviario = $mercancias->appendChild($transporteFerroviario);

            foreach($mercanciasDatos['TransporteFerroviario']['DerechosDePaso'] as $derechosDePasoDatos){

                $derechosDePaso = $doc->createElement('cartaporte:DerechosDePaso');
                $derechosDePaso = $transporteFerroviario->appendChild($derechosDePaso);

                if(isset($derechosDePasoDatos['TipoDerechoDePaso']) && strlen($derechosDePasoDatos['TipoDerechoDePaso']) > 0)
                    $derechosDePaso->setAttribute('TipoDerechoDePaso', $derechosDePasoDatos['TipoDerechoDePaso']);

                if(isset($derechosDePasoDatos['KilometrajePagado']) && strlen($derechosDePasoDatos['KilometrajePagado']) > 0)
                    $derechosDePaso->setAttribute('KilometrajePagado', $derechosDePasoDatos['KilometrajePagado']);
            }

            foreach($mercanciasDatos['TransporteFerroviario']['Carro'] as $CarroDatos){

                $carro = $doc->createElement('cartaporte:Carro');
                $carro = $transporteFerroviario->appendChild($carro);


                foreach($CarroDatos['Contenedor'] as $contenedorDatos){

                    $contenedor = $doc->createElement('cartaporte:Contenedor');
                    $contenedor = $carro->appendChild($contenedor);


                    if(isset($contenedorDatos['TipoContenedor']) && strlen($contenedorDatos['TipoContenedor']) > 0)
                        $contenedor->setAttribute('TipoContenedor', $contenedorDatos['TipoContenedor']);

                    if(isset($contenedorDatos['PesoContenedorVacio']) && strlen($contenedorDatos['PesoContenedorVacio']) > 0)
                        $contenedor->setAttribute('PesoContenedorVacio', $contenedorDatos['PesoContenedorVacio']);

                    if(isset($contenedorDatos['PesoNetoMercancia']) && strlen($contenedorDatos['PesoNetoMercancia']) > 0)
                        $contenedor->setAttribute('PesoNetoMercancia', $contenedorDatos['PesoNetoMercancia']);


                }
                if(isset($CarroDatos['TipoCarro']) && strlen($CarroDatos['TipoCarro']) > 0)
                    $carro->setAttribute('TipoCarro', $CarroDatos['TipoCarro']);

                if(isset($CarroDatos['MatriculaCarro']) && strlen($CarroDatos['MatriculaCarro']) > 0)
                    $carro->setAttribute('MatriculaCarro', $CarroDatos['MatriculaCarro']);

                if(isset($CarroDatos['GuiaCarro']) && strlen($CarroDatos['GuiaCarro']) > 0)
                    $carro->setAttribute('GuiaCarro', $CarroDatos['GuiaCarro']);

                if(isset($CarroDatos['ToneladasNetasCarro']) && strlen($CarroDatos['ToneladasNetasCarro']) > 0)
                    $carro->setAttribute('ToneladasNetasCarro', $CarroDatos['ToneladasNetasCarro']);

            }

            if(isset($mercanciasDatos['TransporteFerroviario']['TipoDeServicio']) && strlen($mercanciasDatos['TransporteFerroviario']['TipoDeServicio']) > 0)
                $transporteFerroviario->setAttribute('ToneladasNetasCarro', $mercanciasDatos['TransporteFerroviario']['TipoDeServicio']);

            if(isset($mercanciasDatos['TransporteFerroviario']['NombreAseg']) && strlen($mercanciasDatos['TransporteFerroviario']['NombreAseg']) > 0)
                $transporteFerroviario->setAttribute('NombreAseg', $mercanciasDatos['TransporteFerroviario']['NombreAseg']);

            if(isset($mercanciasDatos['TransporteFerroviario']['NumPolizaSeguro']) && strlen($mercanciasDatos['TransporteFerroviario']['NumPolizaSeguro']) > 0)
                $transporteFerroviario->setAttribute('NumPolizaSeguro', $mercanciasDatos['TransporteFerroviario']['NumPolizaSeguro']);

            if(isset($mercanciasDatos['TransporteFerroviario']['Concesionario']) && strlen($mercanciasDatos['TransporteFerroviario']['Concesionario']) > 0)
                $transporteFerroviario->setAttribute('Concesionario', $mercanciasDatos['TransporteFerroviario']['Concesionario']);



        }

        if(isset($cp['FiguraTransporte'])){

            $figuraTransporte = $doc->createElement('cartaporte:FiguraTransporte');
            $figuraTransporte = $cartaPorte->appendChild($figuraTransporte);

            if(isset($cp['FiguraTransporte']['CveTransporte']) && strlen($cp['FiguraTransporte']['CveTransporte']) > 0)
                $figuraTransporte->setAttribute('CveTransporte', $cp['FiguraTransporte']['CveTransporte']);

            ####
            if(isset($cp['FiguraTransporte']['Operadores'][0]['Operador'])){
                $operadores = $doc->createElement('cartaporte:Operadores');
                $operadores = $figuraTransporte->appendChild($operadores);

                foreach($cp['FiguraTransporte']['Operadores'][0]['Operador'] as $operadoresDatos){

                    $operador = $doc->createElement('cartaporte:Operador');
                    $operador = $operadores->appendChild($operador);

                    $domicilio = $doc->createElement('cartaporte:Domicilio');
                    $domicilio = $operador->appendChild($domicilio);

                    if(isset($operadoresDatos['Domicilio']['Calle']) && strlen($operadoresDatos['Domicilio']['Calle']) > 0)
                        $domicilio->setAttribute('Calle', $operadoresDatos['Domicilio']['Calle']);

                    if(isset($operadoresDatos['Domicilio']['NumeroExterior']) && strlen($operadoresDatos['Domicilio']['NumeroExterior']) > 0)
                        $domicilio->setAttribute('NumeroExterior', $operadoresDatos['Domicilio']['NumeroExterior']);

                    if(isset($operadoresDatos['Domicilio']['NumeroInterior']) && strlen($operadoresDatos['Domicilio']['NumeroInterior']) > 0)
                        $domicilio->setAttribute('NumeroInterior', $operadoresDatos['Domicilio']['NumeroInterior']);

                    if(isset($operadoresDatos['Domicilio']['Colonia']) && strlen($operadoresDatos['Domicilio']['Colonia']) > 0)
                        $domicilio->setAttribute('Colonia', $operadoresDatos['Domicilio']['Colonia']);

                    if(isset($operadoresDatos['Domicilio']['Localidad']) && strlen($operadoresDatos['Domicilio']['Localidad']) > 0)
                        $domicilio->setAttribute('Localidad', $operadoresDatos['Domicilio']['Localidad']);

                    if(isset($operadoresDatos['Domicilio']['Referencia']) && strlen($operadoresDatos['Domicilio']['Referencia']) > 0)
                        $domicilio->setAttribute('Referencia', $operadoresDatos['Domicilio']['Referencia']);

                    if(isset($operadoresDatos['Domicilio']['Municipio']) && strlen($operadoresDatos['Domicilio']['Municipio']) > 0)
                        $domicilio->setAttribute('Municipio', $operadoresDatos['Domicilio']['Municipio']);

                    if(isset($operadoresDatos['Domicilio']['Estado']) && strlen($operadoresDatos['Domicilio']['Estado']) > 0)
                        $domicilio->setAttribute('Estado', $operadoresDatos['Domicilio']['Estado']);

                    if(isset($operadoresDatos['Domicilio']['Pais']) && strlen($operadoresDatos['Domicilio']['Pais']) > 0)
                        $domicilio->setAttribute('Pais', $operadoresDatos['Domicilio']['Pais']);

                    if(isset($operadoresDatos['Domicilio']['CodigoPostal']) && strlen($operadoresDatos['Domicilio']['CodigoPostal']) > 0)
                        $domicilio->setAttribute('CodigoPostal', $operadoresDatos['Domicilio']['CodigoPostal']);


                    if(isset($operadoresDatos['RFCOperador']) && strlen($operadoresDatos['RFCOperador']) > 0)
                        $operador->setAttribute('RFCOperador', $operadoresDatos['RFCOperador']);

                    if(isset($operadoresDatos['NumLicencia']) && strlen($operadoresDatos['NumLicencia']) > 0)
                        $operador->setAttribute('NumLicencia', $operadoresDatos['NumLicencia']);

                    if(isset($operadoresDatos['NombreOperador']) && strlen($operadoresDatos['NombreOperador']) > 0)
                        $operador->setAttribute('NombreOperador', $operadoresDatos['NombreOperador']);

                    if(isset($operadoresDatos['NumRegIdTribOperador']) && strlen($operadoresDatos['NumRegIdTribOperador']) > 0)
                        $operador->setAttribute('NumRegIdTribOperador', $operadoresDatos['NumRegIdTribOperador']);

                    if(isset($operadoresDatos['ResidenciaFiscalOperador']) && strlen($operadoresDatos['ResidenciaFiscalOperador']) > 0)
                        $operador->setAttribute('ResidenciaFiscalOperador', $operadoresDatos['ResidenciaFiscalOperador']);

                }

            }

            if(isset($cp['FiguraTransporte']['Propietario'])){
                $propietario = $doc->createElement('cartaporte:Propietario');
                $propietario = $figuraTransporte->appendChild($propietario);

                foreach($cp['FiguraTransporte']['Propietario'] as $PropietarioDatos){

                    $domicilio = $doc->createElement('cartaporte:Domicilio');
                    $domicilio = $propietario->appendChild($domicilio);

                    if(isset($PropietarioDatos['Domicilio']['Calle']) && strlen($PropietarioDatos['Domicilio']['Calle']) > 0)
                        $domicilio->setAttribute('Calle', $PropietarioDatos['Domicilio']['Calle']);

                    if(isset($PropietarioDatos['Domicilio']['NumeroExterior']) && strlen($PropietarioDatos['Domicilio']['NumeroExterior']) > 0)
                        $domicilio->setAttribute('NumeroExterior', $PropietarioDatos['Domicilio']['NumeroExterior']);

                    if(isset($PropietarioDatos['Domicilio']['NumeroInterior']) && strlen($PropietarioDatos['Domicilio']['NumeroInterior']) > 0)
                        $domicilio->setAttribute('NumeroInterior', $PropietarioDatos['Domicilio']['NumeroInterior']);

                    if(isset($PropietarioDatos['Domicilio']['Colonia']) && strlen($PropietarioDatos['Domicilio']['Colonia']) > 0)
                        $domicilio->setAttribute('Colonia', $PropietarioDatos['Domicilio']['Colonia']);

                    if(isset($PropietarioDatos['Domicilio']['Localidad']) && strlen($PropietarioDatos['Domicilio']['Localidad']) > 0)
                        $domicilio->setAttribute('Localidad', $PropietarioDatos['Domicilio']['Localidad']);

                    if(isset($PropietarioDatos['Domicilio']['Referencia']) && strlen($PropietarioDatos['Domicilio']['Referencia']) > 0)
                        $domicilio->setAttribute('Referencia', $PropietarioDatos['Domicilio']['Referencia']);

                    if(isset($PropietarioDatos['Domicilio']['Municipio']) && strlen($PropietarioDatos['Domicilio']['Municipio']) > 0)
                        $domicilio->setAttribute('Municipio', $PropietarioDatos['Domicilio']['Municipio']);

                    if(isset($PropietarioDatos['Domicilio']['Estado']) && strlen($PropietarioDatos['Domicilio']['Estado']) > 0)
                        $domicilio->setAttribute('Estado', $PropietarioDatos['Domicilio']['Estado']);

                    if(isset($PropietarioDatos['Domicilio']['Pais']) && strlen($PropietarioDatos['Domicilio']['Pais']) > 0)
                        $domicilio->setAttribute('Pais', $PropietarioDatos['Domicilio']['Pais']);

                    if(isset($PropietarioDatos['Domicilio']['CodigoPostal']) && strlen($PropietarioDatos['Domicilio']['CodigoPostal']) > 0)
                        $domicilio->setAttribute('CodigoPostal', $PropietarioDatos['Domicilio']['CodigoPostal']);


                    if(isset($PropietarioDatos['RFCPropietario']) && strlen($PropietarioDatos['RFCPropietario']) > 0)
                        $propietario->setAttribute('RFCPropietario', $PropietarioDatos['RFCPropietario']);

                    if(isset($PropietarioDatos['NombrePropietario']) && strlen($PropietarioDatos['NombrePropietario']) > 0)
                        $propietario->setAttribute('NombrePropietario', $PropietarioDatos['NombrePropietario']);

                    if(isset($PropietarioDatos['NumRegIdTribPropietario']) && strlen($PropietarioDatos['NumRegIdTribPropietario']) > 0)
                        $propietario->setAttribute('NumRegIdTribPropietario', $PropietarioDatos['NumRegIdTribPropietario']);

                    if(isset($PropietarioDatos['ResidenciaFiscalPropietario']) && strlen($PropietarioDatos['ResidenciaFiscalPropietario']) > 0)
                        $propietario->setAttribute('ResidenciaFiscalPropietario', $PropietarioDatos['ResidenciaFiscalPropietario']);

                }

            }

            if(isset($cp['FiguraTransporte']['Arrendatario'])){
                $arrendatario = $doc->createElement('cartaporte:Arrendatario');
                $arrendatario = $figuraTransporte->appendChild($arrendatario);

                foreach($cp['FiguraTransporte']['Arrendatario'] as $arrendatarioDatos){

                    $domicilio = $doc->createElement('cartaporte:Domicilio');
                    $domicilio = $arrendatario->appendChild($domicilio);

                    if(isset($arrendatarioDatos['Domicilio']['Calle']) && strlen($arrendatarioDatos['Domicilio']['Calle']) > 0)
                        $domicilio->setAttribute('Calle', $arrendatarioDatos['Domicilio']['Calle']);

                    if(isset($arrendatarioDatos['Domicilio']['NumeroExterior']) && strlen($arrendatarioDatos['Domicilio']['NumeroExterior']) > 0)
                        $domicilio->setAttribute('NumeroExterior', $arrendatarioDatos['Domicilio']['NumeroExterior']);

                    if(isset($arrendatarioDatos['Domicilio']['NumeroInterior']) && strlen($arrendatarioDatos['Domicilio']['NumeroInterior']) > 0)
                        $domicilio->setAttribute('NumeroInterior', $arrendatarioDatos['Domicilio']['NumeroInterior']);

                    if(isset($arrendatarioDatos['Domicilio']['Colonia']) && strlen($arrendatarioDatos['Domicilio']['Colonia']) > 0)
                        $domicilio->setAttribute('Colonia', $arrendatarioDatos['Domicilio']['Colonia']);

                    if(isset($arrendatarioDatos['Domicilio']['Localidad']) && strlen($arrendatarioDatos['Domicilio']['Localidad']) > 0)
                        $domicilio->setAttribute('Localidad', $arrendatarioDatos['Domicilio']['Localidad']);

                    if(isset($arrendatarioDatos['Domicilio']['Referencia']) && strlen($arrendatarioDatos['Domicilio']['Referencia']) > 0)
                        $domicilio->setAttribute('Referencia', $arrendatarioDatos['Domicilio']['Referencia']);

                    if(isset($arrendatarioDatos['Domicilio']['Municipio']) && strlen($arrendatarioDatos['Domicilio']['Municipio']) > 0)
                        $domicilio->setAttribute('Municipio', $arrendatarioDatos['Domicilio']['Municipio']);

                    if(isset($arrendatarioDatos['Domicilio']['Estado']) && strlen($arrendatarioDatos['Domicilio']['Estado']) > 0)
                        $domicilio->setAttribute('Estado', $arrendatarioDatos['Domicilio']['Estado']);

                    if(isset($arrendatarioDatos['Domicilio']['Pais']) && strlen($arrendatarioDatos['Domicilio']['Pais']) > 0)
                        $domicilio->setAttribute('Pais', $arrendatarioDatos['Domicilio']['Pais']);

                    if(isset($arrendatarioDatos['Domicilio']['CodigoPostal']) && strlen($arrendatarioDatos['Domicilio']['CodigoPostal']) > 0)
                        $domicilio->setAttribute('CodigoPostal', $arrendatarioDatos['Domicilio']['CodigoPostal']);


                    if(isset($arrendatarioDatos['RFCArrendatario']) && strlen($arrendatarioDatos['RFCArrendatario']) > 0)
                        $arrendatario->setAttribute('RFCArrendatario', $arrendatarioDatos['RFCArrendatario']);

                    if(isset($arrendatarioDatos['NombreArrendatario']) && strlen($arrendatarioDatos['NombreArrendatario']) > 0)
                        $arrendatario->setAttribute('NombreArrendatario', $arrendatarioDatos['NombreArrendatario']);

                    if(isset($arrendatarioDatos['NumRegIdTribArrendatario']) && strlen($arrendatarioDatos['NumRegIdTribArrendatario']) > 0)
                        $arrendatario->setAttribute('NumRegIdTribArrendatario', $arrendatarioDatos['NumRegIdTribArrendatario']);

                    if(isset($arrendatarioDatos['ResidenciaFiscalArrendatario']) && strlen($arrendatarioDatos['ResidenciaFiscalArrendatario']) > 0)
                        $arrendatario->setAttribute('ResidenciaFiscalArrendatario', $arrendatarioDatos['ResidenciaFiscalArrendatario']);

                }

            }

            if(isset($cp['FiguraTransporte']['Notificado'])){
                $notificado = $doc->createElement('cartaporte:Notificado');
                $notificado = $figuraTransporte->appendChild($notificado);

                foreach($cp['FiguraTransporte']['Notificado'] as $notificadoDatos){

                    $domicilio = $doc->createElement('cartaporte:Domicilio');
                    $domicilio = $notificado->appendChild($domicilio);

                    if(isset($notificadoDatos['Domicilio']['Calle']) && strlen($notificadoDatos['Domicilio']['Calle']) > 0)
                        $domicilio->setAttribute('Calle', $notificadoDatos['Domicilio']['Calle']);

                    if(isset($notificadoDatos['Domicilio']['NumeroExterior']) && strlen($notificadoDatos['Domicilio']['NumeroExterior']) > 0)
                        $domicilio->setAttribute('NumeroExterior', $notificadoDatos['Domicilio']['NumeroExterior']);

                    if(isset($notificadoDatos['Domicilio']['NumeroInterior']) && strlen($notificadoDatos['Domicilio']['NumeroInterior']) > 0)
                        $domicilio->setAttribute('NumeroInterior', $notificadoDatos['Domicilio']['NumeroInterior']);

                    if(isset($notificadoDatos['Domicilio']['Colonia']) && strlen($notificadoDatos['Domicilio']['Colonia']) > 0)
                        $domicilio->setAttribute('Colonia', $notificadoDatos['Domicilio']['Colonia']);

                    if(isset($notificadoDatos['Domicilio']['Localidad']) && strlen($notificadoDatos['Domicilio']['Localidad']) > 0)
                        $domicilio->setAttribute('Localidad', $notificadoDatos['Domicilio']['Localidad']);

                    if(isset($notificadoDatos['Domicilio']['Referencia']) && strlen($notificadoDatos['Domicilio']['Referencia']) > 0)
                        $domicilio->setAttribute('Referencia', $notificadoDatos['Domicilio']['Referencia']);

                    if(isset($notificadoDatos['Domicilio']['Municipio']) && strlen($notificadoDatos['Domicilio']['Municipio']) > 0)
                        $domicilio->setAttribute('Municipio', $notificadoDatos['Domicilio']['Municipio']);

                    if(isset($notificadoDatos['Domicilio']['Estado']) && strlen($notificadoDatos['Domicilio']['Estado']) > 0)
                        $domicilio->setAttribute('Estado', $notificadoDatos['Domicilio']['Estado']);

                    if(isset($notificadoDatos['Domicilio']['Pais']) && strlen($notificadoDatos['Domicilio']['Pais']) > 0)
                        $domicilio->setAttribute('Pais', $notificadoDatos['Domicilio']['Pais']);

                    if(isset($notificadoDatos['Domicilio']['CodigoPostal']) && strlen($notificadoDatos['Domicilio']['CodigoPostal']) > 0)
                        $domicilio->setAttribute('CodigoPostal', $notificadoDatos['Domicilio']['CodigoPostal']);


                    if(isset($notificadoDatos['RFCNotificado']) && strlen($notificadoDatos['RFCNotificado']) > 0)
                        $arrendatario->setAttribute('RFCNotificado', $notificadoDatos['RFCNotificado']);

                    if(isset($notificadoDatos['NombreNotificado']) && strlen($notificadoDatos['NombreNotificado']) > 0)
                        $arrendatario->setAttribute('NombreNotificado', $notificadoDatos['NombreNotificado']);

                    if(isset($notificadoDatos['NumRegIdTribNotificado']) && strlen($notificadoDatos['NumRegIdTribNotificado']) > 0)
                        $arrendatario->setAttribute('NumRegIdTribNotificado', $notificadoDatos['NumRegIdTribNotificado']);

                    if(isset($notificadoDatos['ResidenciaFiscalNotificado']) && strlen($notificadoDatos['ResidenciaFiscalNotificado']) > 0)
                        $arrendatario->setAttribute('ResidenciaFiscalNotificado', $notificadoDatos['ResidenciaFiscalNotificado']);

                }

            }


        }

    }
}