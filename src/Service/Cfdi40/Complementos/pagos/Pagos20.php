<?php
/**
 * Created by PhpStorm.
 * User: Juan Sotero
 * Date: 17/04/2017
 * Time: 11:05 AM
 */

namespace App\Service\Cfdi40\Complementos\pagos;

use \DomDocument;
use App\Service\Cfdi40\Complementos\Complemento;

/**
 * Description of GenerarXML
 *
 * @author Miguel Solis <miguel.solis@clickfactura.mx>
 */
class Pagos20 extends Complemento
{

    public function Pagos20()
    {

    }


    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request)
    {
        if (!isset($request['Comprobante']['Complemento']['Pagos']['Version'])) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para agregar el complemento de pagos 10
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request)
    {


        if (!self::existeComplemento($request)) {
            return $doc;
        }


        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        #------------ pago
        if (count($request['Comprobante']['Complemento']['Pagos']['Pago']) > 0) {

            $Pagos = $doc->createElement('pago20:Pagos');
            $Pagos = $ComplementoNode->appendChild($Pagos);


            $comp = $request['Comprobante']['Complemento']['Pagos'];

            $Pagos->setAttribute("Version", $comp['Version']);

            $Totales = $doc->createElement('pago20:Totales');
            $Totales = $Pagos->appendChild($Totales);

            if(isset($comp['Totales']['TotalRetencionesIVA']) && strlen($comp['Totales']['TotalRetencionesIVA']) > 0)
                $Totales->setAttribute("TotalRetencionesIVA", $comp['Totales']['TotalRetencionesIVA']);
            if(isset($comp['Totales']['TotalRetencionesISR']) && strlen($comp['Totales']['TotalRetencionesISR']) > 0)
                $Totales->setAttribute("TotalRetencionesISR", $comp['Totales']['TotalRetencionesISR']);
            if(isset($comp['Totales']['TotalRetencionesIEPS']) && strlen($comp['Totales']['TotalRetencionesIEPS']) > 0)
                $Totales->setAttribute("TotalRetencionesIEPS", $comp['Totales']['TotalRetencionesIEPS']);
            if(isset($comp['Totales']['TotalTrasladosBaseIVA16']) && strlen($comp['Totales']['TotalTrasladosBaseIVA16']) > 0)
                $Totales->setAttribute("TotalTrasladosBaseIVA16", $comp['Totales']['TotalTrasladosBaseIVA16']);
            if(isset($comp['Totales']['TotalTrasladosImpuestoIVA16']) && strlen($comp['Totales']['TotalTrasladosImpuestoIVA16']) > 0)
                $Totales->setAttribute("TotalTrasladosImpuestoIVA16", $comp['Totales']['TotalTrasladosImpuestoIVA16']);
            if(isset($comp['Totales']['TotalTrasladosBaseIVA8']) && strlen($comp['Totales']['TotalTrasladosBaseIVA8']) > 0)
                $Totales->setAttribute("TotalTrasladosBaseIVA8", $comp['Totales']['TotalTrasladosBaseIVA8']);
            if(isset($comp['Totales']['TotalTrasladosImpuestoIVA8']) && strlen($comp['Totales']['TotalTrasladosImpuestoIVA8']) > 0)
                $Totales->setAttribute("TotalTrasladosImpuestoIVA8", $comp['Totales']['TotalTrasladosImpuestoIVA8']);
            if(isset($comp['Totales']['TotalTrasladosBaseIVA0']) && strlen($comp['Totales']['TotalTrasladosBaseIVA0']) > 0)
                $Totales->setAttribute("TotalTrasladosBaseIVA0", $comp['Totales']['TotalTrasladosBaseIVA0']);
            if(isset($comp['Totales']['TotalTrasladosImpuestoIVA0']) && strlen($comp['Totales']['TotalTrasladosImpuestoIVA0']) > 0)
                $Totales->setAttribute("TotalTrasladosImpuestoIVA0", $comp['Totales']['TotalTrasladosImpuestoIVA0']);
            if(isset($comp['Totales']['TotalTrasladosBaseIVAExento']) && strlen($comp['Totales']['TotalTrasladosBaseIVAExento']) > 0)
                $Totales->setAttribute("TotalTrasladosBaseIVAExento", $comp['Totales']['TotalTrasladosBaseIVAExento ']);
            
            $Totales->setAttribute("MontoTotalPagos", $comp['Totales']['MontoTotalPagos']);

            for ($i = 0; $i < count($comp['Pago']); $i++) {


                $pago = null;

                if (strlen($comp['Pago'][$i]['FechaPago']) < 1) {
                    continue;
                } else {
                    $pago = $doc->createElement('pago20:Pago');
                    $pago = $Pagos->appendChild($pago);
                }

                $pago->setAttribute("FechaPago", $comp['Pago'][$i]['FechaPago']);
                $pago->setAttribute("FormaDePagoP", $comp['Pago'][$i]['FormaDePagoP']);
                $pago->setAttribute("MonedaP", $comp['Pago'][$i]['MonedaP']);

                if (strlen($comp['Pago'][$i]['TipoCambioP']) > 0) {
                    $pago->setAttribute("TipoCambioP", $comp['Pago'][$i]['TipoCambioP']);
                }

                $pago->setAttribute("Monto", $comp['Pago'][$i]['Monto']);

                if (strlen($comp['Pago'][$i]['NumOperacion']) > 0) {
                    $pago->setAttribute("NumOperacion", $comp['Pago'][$i]['NumOperacion']);
                }

                if (strlen($comp['Pago'][$i]['RfcEmisorCtaOrd']) > 0) {
                    $pago->setAttribute("RfcEmisorCtaOrd", $comp['Pago'][$i]['RfcEmisorCtaOrd']);
                }

                if (strlen($comp['Pago'][$i]['NomBancoOrdExt']) > 0) {
                    $pago->setAttribute("NomBancoOrdExt", $comp['Pago'][$i]['NomBancoOrdExt']);
                }

                if (strlen($comp['Pago'][$i]['CtaOrdenante']) > 0) {
                    $pago->setAttribute("CtaOrdenante", $comp['Pago'][$i]['CtaOrdenante']);
                }

                if (strlen($comp['Pago'][$i]['RfcEmisorCtaBen']) > 0) {
                    $pago->setAttribute("RfcEmisorCtaBen", $comp['Pago'][$i]['RfcEmisorCtaBen']);
                }

                if (strlen($comp['Pago'][$i]['CtaBeneficiario']) > 0) {
                    $pago->setAttribute("CtaBeneficiario", $comp['Pago'][$i]['CtaBeneficiario']);
                }

                if (strlen($comp['Pago'][$i]['TipoCadPago']) > 0) {
                    $pago->setAttribute("TipoCadPago", $comp['Pago'][$i]['TipoCadPago']);
                }

                if (strlen($comp['Pago'][$i]['CertPago']) > 0) {
                    $pago->setAttribute("CertPago", $comp['Pago'][$i]['CertPago']);
                }

                if (strlen($comp['Pago'][$i]['CadPago']) > 0) {
                    $pago->setAttribute("CadPago", $comp['Pago'][$i]['CadPago']);
                }

                if (strlen($comp['Pago'][$i]['SelloPago']) > 0) {
                    $pago->setAttribute("SelloPago", $comp['Pago'][$i]['SelloPago']);
                }


                #--------------- DoctoRelacionado
                if (count($comp['Pago'][$i]['DoctoRelacionado']) > 0) {

                    for ($j = 0; $j < count($comp['Pago'][$i]['DoctoRelacionado']); $j++) {

                        $doctoRel = null;

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['IdDocumento']) < 1) {
                            continue;
                        } else {
                            $doctoRel = $doc->createElement('pago20:DoctoRelacionado');
                            $doctoRel = $pago->appendChild($doctoRel);
                        }

                        $doctoRel->setAttribute("IdDocumento", $comp['Pago'][$i]['DoctoRelacionado'][$j]['IdDocumento']);

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['Serie']) > 0) {
                            $doctoRel->setAttribute("Serie", $comp['Pago'][$i]['DoctoRelacionado'][$j]['Serie']);
                        }

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['Folio']) > 0) {
                            $doctoRel->setAttribute("Folio", $comp['Pago'][$i]['DoctoRelacionado'][$j]['Folio']);
                        }

                        $doctoRel->setAttribute("MonedaDR", $comp['Pago'][$i]['DoctoRelacionado'][$j]['MonedaDR']);
                        $doctoRel->setAttribute("EquivalenciaDR", $comp['Pago'][$i]['DoctoRelacionado'][$j]['EquivalenciaDR']);

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['TipoCambioDR']) > 0) {
                            $doctoRel->setAttribute("TipoCambioDR", $comp['Pago'][$i]['DoctoRelacionado'][$j]['TipoCambioDR']);
                        }

                        $doctoRel->setAttribute("ObjetoImpDR", $comp['Pago'][$i]['DoctoRelacionado'][$j]['ObjetoImpDR']);

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['NumParcialidad']) > 0) {
                            $doctoRel->setAttribute("NumParcialidad", $comp['Pago'][$i]['DoctoRelacionado'][$j]['NumParcialidad']);
                        }

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpSaldoAnt']) > 0) {
                            $doctoRel->setAttribute("ImpSaldoAnt", $comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpSaldoAnt']);
                        }

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpPagado']) > 0) {
                            $doctoRel->setAttribute("ImpPagado", $comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpPagado']);
                        }

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpSaldoInsoluto']) > 0) {
                            $doctoRel->setAttribute("ImpSaldoInsoluto", $comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpSaldoInsoluto']);
                        }


                        #se agregan los impuestos
                        if (isset($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpuestosDR']) ) {

                            #retenciones
                            $ImpuestosDR = $doc->createElement('pago20:ImpuestosDR');
                            $ImpuestosDR = $doctoRel->appendChild($ImpuestosDR);

                            if(isset($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpuestosDR']['RetencionesDR'])){

                                $RetencionesDR = $doc->createElement('pago20:RetencionesDR');
                                $RetencionesDR = $ImpuestosDR->appendChild($RetencionesDR);

                                $retencionesDrCiclar = [];
                                if(isset($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpuestosDR']['RetencionesDR']['RetencionDR'][0])){
                                     $retencionesDrCiclar = $comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpuestosDR']['RetencionesDR']['RetencionDR'];
                                }
                                else
                                    $retencionesDrCiclar[0] = $comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpuestosDR']['RetencionesDR']['RetencionDR'];

                                foreach($retencionesDrCiclar as $retenciondrCiclar){

                                    $RetencionDR = $doc->createElement('pago20:RetencionDR');
                                    $RetencionDR = $RetencionesDR->appendChild($RetencionDR);

                                    $RetencionDR->setAttribute("BaseDR", $retenciondrCiclar['BaseDR']);
                                    $RetencionDR->setAttribute("ImpuestoDR", $retenciondrCiclar['ImpuestoDR']);
                                    $RetencionDR->setAttribute("TipoFactorDR", $retenciondrCiclar['TipoFactorDR']);
                                    $RetencionDR->setAttribute("TasaOCuotaDR", $retenciondrCiclar['TasaOCuotaDR']);
                                    $RetencionDR->setAttribute("ImporteDR", $retenciondrCiclar['ImporteDR']);

                                }
                            }

                            #traslados
                            if(isset($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpuestosDR']['TrasladosDR'])){

                                $TrasladosDR = $doc->createElement('pago20:TrasladosDR');
                                $TrasladosDR = $ImpuestosDR->appendChild($TrasladosDR);

                                $trasladosDrCiclar = [];
                                if(isset($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpuestosDR']['TrasladosDR']['TrasladoDR'][0])){
                                     $trasladosDrCiclar = $comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpuestosDR']['TrasladosDR']['TrasladoDR'];
                                }
                                else
                                    $trasladosDrCiclar[0] = $comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpuestosDR']['TrasladosDR']['TrasladoDR'];

                                foreach($trasladosDrCiclar as $trasladodrCiclar){

                                    $RetencionDR = $doc->createElement('pago20:TrasladoDR');
                                    $RetencionDR = $TrasladosDR->appendChild($RetencionDR);

                                    $RetencionDR->setAttribute("BaseDR", $trasladodrCiclar['BaseDR']);
                                    $RetencionDR->setAttribute("ImpuestoDR", $trasladodrCiclar['ImpuestoDR']);
                                    $RetencionDR->setAttribute("TipoFactorDR", $trasladodrCiclar['TipoFactorDR']);
                                    $RetencionDR->setAttribute("TasaOCuotaDR", $trasladodrCiclar['TasaOCuotaDR']);
                                    $RetencionDR->setAttribute("ImporteDR", $trasladodrCiclar['ImporteDR']);

                                }
                            }

                        }
                    }

                }#--------------- DoctoRelacionado

                #--------------- pago20:Impuestos
               
                if (isset($comp['Pago'][$i]['ImpuestosP']) && count($comp['Pago'][$i]['ImpuestosP']) > 0) {

                    $j = 0;

                        $impues = null;
                        $impues = $doc->createElement('pago20:ImpuestosP');
                        $impues = $pago->appendChild($impues);
                       /* if (strlen($comp['Pago'][$i]['ImpuestosP'][$j]['TotalImpuestosRetenidos']) < 1 && strlen($comp['Pago'][$i]['ImpuestosP'][$j]['TotalImpuestosTrasladados']) < 1) {
                            continue;
                        } else {
                            
                        }

                        if (strlen($comp['Pago'][$i]['ImpuestosP'][$j]['TotalImpuestosRetenidos']) > 0) {
                            $impues->setAttribute("TotalImpuestosRetenidos", $comp['Pago'][$i]['Impuestos'][$j]['TotalImpuestosRetenidos']);
                        }

                        if (strlen($comp['Pago'][$i]['ImpuestosP'][$j]['TotalImpuestosTrasladados']) > 0) {
                            $impues->setAttribute("TotalImpuestosTrasladados", $comp['Pago'][$i]['Impuestos'][$j]['TotalImpuestosTrasladados']);
                        }*/


                        #--------------------


                       
                        
                        if (isset( $comp['Pago'][$i]['ImpuestosP']['RetencionesP'][0]['RetencionP']['ImpuestoP']) ) {

                            $Retenciones = $doc->createElement('pago20:RetencionesP');
                            $Retenciones = $impues->appendChild($Retenciones);

                            
                            for ($k = 0; $k < count($comp['Pago'][$i]['ImpuestosP']['RetencionesP']); $k++) {

                                $Retencion = $doc->createElement('pago20:RetencionP');
                                $Retencion = $Retenciones->appendChild($Retencion);

                                $Retencion->setAttribute('ImpuestoP', trim($comp['Pago'][$i]['ImpuestosP']['RetencionesP'][$k]['RetencionP']['ImpuestoP']));//CATALOGO
                                $Retencion->setAttribute('ImporteP', trim($comp['Pago'][$i]['ImpuestosP']['RetencionesP'][$k]['RetencionP']['ImporteP']));

                            }
                        }


                        if (isset( $comp['Pago'][$i]['ImpuestosP']['TrasladosP'][0]['TrasladoP']['ImporteP'])) {

                            $Traslados = $doc->createElement('pago20:TrasladosP');
                            $Traslados = $impues->appendChild($Traslados);

                            for ($k = 0; $k < count($comp['Pago'][$i]['ImpuestosP']['TrasladosP']); $k++) {

                     
                                $Traslado = $doc->createElement('pago20:TrasladoP');
                                $Traslado = $Traslados->appendChild($Traslado);

                                $Traslado->setAttribute('BaseP', trim($comp['Pago'][$i]['ImpuestosP']['TrasladosP'][$k]['TrasladoP']['BaseP']));//CATALOGO
                                $Traslado->setAttribute('ImpuestoP', trim($comp['Pago'][$i]['ImpuestosP']['TrasladosP'][$k]['TrasladoP']['ImpuestoP']));//CATALOGO
                                $Traslado->setAttribute('TipoFactorP', trim($comp['Pago'][$i]['ImpuestosP']['TrasladosP'][$k]['TrasladoP']['TipoFactorP']));//CATALOGO
                                $Traslado->setAttribute('TasaOCuotaP', trim($comp['Pago'][$i]['ImpuestosP']['TrasladosP'][$k]['TrasladoP']['TasaOCuotaP']));
                                $Traslado->setAttribute('ImporteP', trim($comp['Pago'][$i]['ImpuestosP']['TrasladosP'][$k]['TrasladoP']['ImporteP']));
                            
                            }
                        }


                        #--------------------



                }#--------------- pago20:Impuestos

            }#for Pago


        }
        #------------ pago
        return $doc;

    }
}