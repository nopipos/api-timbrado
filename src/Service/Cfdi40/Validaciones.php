<?php


namespace App\Service\Cfdi40;


use App\Entity\SAT\CatCp;
use App\Entity\response\MensajeTimbrado;
use App\Entity\Timbre\Pac;
use App\Entity\Timbre\Peticiones;
use App\Service\Utils\amazon\S3;
use App\Service\Utils\XML2Array;
use Doctrine\Persistence\ManagerRegistry;
use DOMDocument;
use DOMXPath;
class Validaciones
{
    private $xpath;
    private $xmlArray;
    private $usuario;
    private $esPortal;
    private $xmlData;

    /**
     * Validaciones constructor.
     * @param $xpath
     */
    public function __construct($xmlString, $usuario,$esPortal = null)
    {


        $this->esPortal = $esPortal;
        $this->usuario = $usuario;
        $this->xmlArray = XML2Array::createArray($xmlString);
        $this->xmlData = $xmlString;
        $dom = new DOMDocument;
        $dom->loadXml($xmlString);
        $this->xpath = new DOMXPath($dom);
        $this->xpath->registerNamespace('ns1', 'http://www.sat.gob.mx/cfd/4');
        $this->xpath->registerNamespace('ns2', 'http://www.sat.gob.mx/TimbreFiscalDigital');
    }

    /**
     * @param $xmlString
     * @param $acceso
     * @return array|null
     * @throws \Exception si hay algun error en la validacion
     *
     */
    public function init()
    {


        $codes = array();
        $msg = array();
        $error = false;

        try {
            self::validaRfcEmisor();
        } catch (\Exception $e) {
            $msg[] = $e->getMessage();
            $codes[] = 'CFDI' . $e->getCode();
            $error = true;
        }

        if ($error) {
            throw new \Exception(implode(", ", $codes) . '|' . implode(" - ", $msg));
        }
    }


    /**
     * LLamada directa en el controller
     *
     * Para validar que si el acceso tiene rechazaFacturas == 1 se valide que no se ha timbrado la serie y folio
     *
     * @param $xmlString
     * @param array $params parametros en la peticion
     *
     * @return array|null
     */
    public function validacionSeriaAndFolio($params, ManagerRegistry $doctrine)
    {

        
        if($this->usuario['empresa']->getRevisionFolios() == 1){

            $serie = $this->xpath->query("//ns1:Comprobante/@Serie")->item(0)->value;
            $folio = $this->xpath->query("//ns1:Comprobante/@Folio")->item(0)->value;
            $rfcEmisor = $this->xpath->query("//ns1:Comprobante/ns1:Emisor/@Rfc")->item(0)->value;
            $rfcReceptor = $this->xpath->query("//ns1:Comprobante/ns1:Receptor/@Rfc")->item(0)->value;
            $nombreReceptor = $this->xpath->query("//ns1:Comprobante/ns1:Receptor/@Nombre")->item(0)->value;
            $fecha = $this->xpath->query("//ns1:Comprobante/@Fecha")->item(0)->value;


            if (strlen($folio) == 0) {
                return null;
            }

            $pacRepo = $doctrine->getRepository(Pac::class);
            $pacTimbrado = $pacRepo->findOneBy(['id'=>$this->usuario['empresa']->getPac1()]);

            if($pacTimbrado != null){
                $listaPacs = $pacRepo->findBy(['pacPrueba'=>$pacTimbrado->getPacPrueba()]);
            }

            $buscarPacs = "";

            foreach($listaPacs as $pacs){

                $buscarPacs.= $pacs->getId().",";
            }
            $buscarPacs = substr($buscarPacs,0,strlen($buscarPacs)-1);


            $em = $doctrine->getManager('timbre');

            $whereSerie = "";
            if(strlen($serie)> 0){
                $whereSerie = "AND serie = '".$serie."'";
            }

            $whereFolio = "";
            if(strlen($folio)> 0){
                $whereFolio = "AND folio = '".$folio."'";
            }

            $query = "SELECT * FROM peticiones INNER JOIN peticionestimbre ON peticiones.id = peticionestimbre.peticion_id
WHERE peticiones.uuid is not null AND codigo in ('200','202') AND pac_id in($buscarPacs) AND emisor = '$rfcEmisor'  ".$whereSerie.$whereFolio;

            $statement = $em->getConnection()->prepare($query);
            $statement->execute();

            $result = $statement->fetchAssociative();

            if(isset($result['id'])){

                #obtengo el XML de amazon
                $xml = S3::obtenerFile($result['ruta_file']);

                #obtengo el pdf del amazon
                $rutaPdf = $result['ruta_file'];
                $rutaPdf = str_replace("xml","pdf",$rutaPdf);
                $pdf = S3::obtenerFile($rutaPdf);

                $pdfData = $pdf['body'];
                $xmlData = $xml['body'];
                $xmlArray = XML2Array::createArray(base64_decode($xmlData));

                $respuesta = new MensajeTimbrado();
                $respuesta->setUuid($result['uuid']);
                $respuesta->setResultado("true");
                $respuesta->setFechaTimbrado($result['fecha_timbrado']);
                $respuesta->setCodigo("202");
                $respuesta->setMensaje("Timbrado previamente");
                $respuesta->setNoCertificadoSAT($result['certificado_sat']);
                $respuesta->setPdf($pdfData);
                $respuesta->setRfcProvCertif($xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['RfcProvCertif']);
                $respuesta->setSelloCFD($xmlArray['cfdi:Comprobante']['@attributes']['Sello']);
                $respuesta->setSelloSAT($xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['SelloSAT']);
                $respuesta->setXmlData($xmlData);

                $modelPeticion = new Peticiones();
                $modelPeticion->setMensaje("timbrado previamente");
                $modelPeticion->setCodigo(202);
                $modelPeticion->setUuid($result['uuid']);
                $modelPeticion->setSerie($serie);
                $modelPeticion->setFolio($folio);
                $modelPeticion->setEstatus(1);
                $modelPeticion->setUsuario($result['usuario']);
                $modelPeticion->setEmisor($rfcEmisor);
                $modelPeticion->setFechapeticion(\DateTime::createFromFormat('Y-m-d H:i:s',$result['FechaPeticion']));
                $modelPeticion->setFechaxml(\DateTime::createFromFormat('Y-m-d H:i:s',$result['FechaXml']));
                $modelPeticion->setPac($pacTimbrado);
                $modelPeticion->setReceptor($rfcReceptor);
                $modelPeticion->setRutaFile($result['ruta_file']);
                $modelPeticion->setTipoComprobante($result['tipo_comprobante']);

                $em->persist($modelPeticion);
                $em->flush();

                return $respuesta;

            }
        }
    }
        /**
     * Funcion para validar que el rfc emisor sea igual al del rfc emisor
     * @throws \Exception si hay algun error en la validacion
     *
     */
    public function validaRfcEmisor()
    {
        $rfcEmisor = $this->xpath->query("//ns1:Comprobante/ns1:Emisor/@Rfc")->item(0)->value;

        $re = '/^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9][A-Z,0-9][0-9,A-Z]$/';

        preg_match_all($re, $this->usuario['empresa']->getRfc(), $matches, PREG_SET_ORDER, 0);

        //si es un rfc se valida con el emisor
        if (empty($matches)) {
            return; //no hay match con un rfc
        }

        if (strcmp($rfcEmisor, $this->usuario['empresa']->getRfc()) !== 0) {

            if(trim($rfcEmisor) == "")
                throw new \Exception("No es posible rescatar el RFC del emisor, valor encontrado: ".$rfcEmisor);
            else{
                if(!$this->esPortal)
                    throw new \Exception("Error: El RFC emisor no corresponde con el usuario: " . $this->usuario['empresa']->getRfc()."  ".$rfcEmisor);
            }
        }
    }

    /**
     * Funcion para validar la fecha del xml, tomando encuenta minutos y segudndos?
     *
     * @param $xmlString
     * @return void
     * @throws \Exception si la validacion no se cumple
     *
     */
    private function validaFechasXml()
    {

        $zoneMexico = new DateTimeZone('America/Mexico_City');


        $fecha = $this->xpath->query("//ns1:Comprobante/@Fecha");


        $fechaDate = date("Y-m-d H:i:s", strtotime($fecha->item(0)->value));

        $today = new DateTime(null, $zoneMexico);
        $xmlFecha = new DateTime($fechaDate, $zoneMexico);


        $diff = $today->diff($xmlFecha);
        $seconds = $diff->s + (($diff->i + ($diff->h + ($diff->days * 24)) * 60) * 60);


        if ($seconds > 259200) {
            $msgResponse = 'Error: La factura no puede ser timbrada, la factura se genero hace mas de 72 horas.';
            throw new \Exception($msgResponse);
        }
    }

    /**
     * Funcion para validar que si algun concepto tiene impuestos, deba existir el nodo general de los impuestos
     *
     * @param $xmlArray
     * @return void
     * @throws \Exception si la validacion no se cumple
     */
    private function validaNodesImpuestos()
    {


        $msgResponse = '';

        $arrayConceptos = array();

        if (isset($this->xmlArray['cfdi:Comprobante']['cfdi:Conceptos']['cfdi:Concepto'][0])) {
            for ($i = 0; $i < count($this->xmlArray['cfdi:Comprobante']['cfdi:Conceptos']['cfdi:Concepto']); $i++) {
                array_push($arrayConceptos, $this->xmlArray['cfdi:Comprobante']['cfdi:Conceptos']['cfdi:Concepto'][$i]);
            }
        } else {
            array_push($arrayConceptos, $this->xmlArray['cfdi:Comprobante']['cfdi:Conceptos']['cfdi:Concepto']);
        }


        $conceptoHasRetenciones = false;
        $conceptoHasTraslados = false;

        foreach ($arrayConceptos as $concepto) {


            $arrayImpConceptos = array();


            //agregamos traslados
            if (isset($concepto['cfdi:Impuestos']['cfdi:Traslados']['cfdi:Traslado'][0])) {
                for ($f = 0; $f < count($concepto['cfdi:Impuestos']['cfdi:Traslados']['cfdi:Traslado']); $f++) {
                    array_push($arrayImpConceptos, $concepto['cfdi:Impuestos']['cfdi:Traslados']['cfdi:Traslado'][$f]);
                }
            } else {
                array_push($arrayImpConceptos, $concepto['cfdi:Impuestos']['cfdi:Traslados']['cfdi:Traslado']);
            }

            foreach ($arrayImpConceptos as $impuesto) {
                if (strlen($impuesto['@attributes']['Impuesto']) > 0) {
                    $conceptoHasTraslados = true;
                    break;
                }
            }


            $arrayImpConceptos = array();
            //agregamos retenciones
            if (isset($concepto['cfdi:Impuestos']['cfdi:Retenciones']['cfdi:Retencion'][0])) {
                for ($f = 0; $f < count($concepto['cfdi:Impuestos']['cfdi:Retenciones']['cfdi:Retencion']); $f++) {
                    array_push($arrayImpConceptos, $concepto['cfdi:Impuestos']['cfdi:Retenciones']['cfdi:Retencion'][$f]);
                }
            } else {
                array_push($arrayImpConceptos, $concepto['cfdi:Impuestos']['cfdi:Retenciones']['cfdi:Retencion']);
            }


            foreach ($arrayImpConceptos as $impuesto) {
                if (strlen($impuesto['@attributes']['Impuesto']) > 0) {
                    $conceptoHasRetenciones = true;
                    break;
                }
            }
        }//for conceptos


        $impHasRetenciones = false;
        $impHasTraslados = false;

        $impuestos = $this->xmlArray['cfdi:Comprobante']['cfdi:Impuestos'];
        $arrayImp = array();

        //agregamos traslados
        if (isset($impuestos ['cfdi:Traslados']['cfdi:Traslado'][0])) {
            for ($f = 0; $f < count($impuestos ['cfdi:Traslados']['cfdi:Traslado']); $f++) {
                array_push($arrayImp, $impuestos ['cfdi:Traslados']['cfdi:Traslado'][$f]);
            }
        } else {
            array_push($arrayImp, $impuestos ['cfdi:Traslados']['cfdi:Traslado']);
        }


        foreach ($arrayImp as $impuesto) {
            if (strlen($impuesto['@attributes']['Impuesto']) > 0) {
                $impHasTraslados = true;
                break;
            }
        }


        $arrayImp = array();
        //agregamos retenciones
        if (isset($impuestos ['cfdi:Retenciones']['cfdi:Retencion'][0])) {
            for ($f = 0; $f < count($impuestos ['cfdi:Retenciones']['cfdi:Retencion']); $f++) {
                array_push($arrayImp, $impuestos ['cfdi:Retenciones']['cfdi:Retencion'][$f]);
            }
        } else {
            array_push($arrayImp, $impuestos ['cfdi:Retenciones']['cfdi:Retencion']);
        }


        foreach ($arrayImp as $impuesto) {
            if (strlen($impuesto['@attributes']['Impuesto']) > 0) {
                $impHasRetenciones = true;
                break;
            }
        }


        if (($conceptoHasRetenciones && !$impHasRetenciones) || ($conceptoHasTraslados && !$impHasTraslados)) {
            $msgResponse = 'CFDI33152 - En caso de utilizar el nodo Impuestos en un concepto, se deben incluir impuestos  de traslado y/o retenciones.';
        }


        if (strlen($msgResponse) > 0) {
            throw new \Exception($msgResponse, 33152);
        }
    }


}