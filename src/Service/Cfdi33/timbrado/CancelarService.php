<?php


namespace App\Service\Cfdi33\timbrado;


use App\Entity\Timbre\Pac;
use Doctrine\Persistence\ManagerRegistry;

class CancelarService
{

    /**
     * @param $info string xml en cadena sin codificicar
     * @param $pacs array array de los pacs que el usuario tiene configurado
     */
    public static function cancelar($info, $pacs, ManagerRegistry $doctrine){
        #region variables
        $PacRepo  = $doctrine->getRepository(Pac::class);
        $respuestaPac = null;
        $contadorPac = 0;
        #endregion

        #region ciclo para enviar peticiones a los packs
        while($respuestaPac == null){
            #region get info del pack
            $infoPac = $PacRepo->findOneBy(['id' => $pacs[$contadorPac], 'estatus' => 1]);
            if($infoPac == null){
                return ['success' => false, 'msg' => 'El pac configurado no se encontro favor de reportarlo a soporte: '.$pacs[0]];
            }

            $clasePath = $infoPac->getClase();
            $contadorPac++;
            #endregion

            #region valida si existe la funcion y responde
            if(class_exists($clasePath)){
                $resultado = $clasePath::cancelarFactura($info);
                if($resultado['success']){
                    $respuestaPac =1;
                    return ['success' => true, 'msg' => $resultado['msg'], 'pac' => $infoPac];
                }else{
                    return ['success' => false, 'msg' => $resultado['msg'], 'pac' => $infoPac];
                }
            } else{
                return ['success' => false, 'msg' => 'El pac configurado no se encontro favor de reportarlo a soporte: '.$pacs[0]];
            }
            #endregion
        }
        #endregion
    }
}
