<?php


namespace App\Service\Cfdi33\timbrado\pacs\luna;

use App\Service\Cfdi33\timbrado\Pacs;
use SWServices\Stamp\StampService;
use SWServices\Cancelation\CancelationService as CancelationService;
use SWServices\Authentication\AuthenticationService as Authentication;

class LunaTest implements Pacs
{

    public static function timbrar($xml, $peticion)
    {

        $params['url'] = "https://services.test.sw.com.mx/";
        $params['user'] = "liliana.bello@clickfactura.mx";
        $params['password'] = "BELLO+SW";

        $stamp = StampService::Set($params);
        $result = $stamp::StampV4($xml);

      
        if($result->status == "error" && $result->message == "307. El comprobante contiene un timbre previo.") {

            return array(
                'resultado' => 'true',
                'xmlData' => base64_encode($result->messageDetail),
                'codigo' => "202",
                'mensaje' => "Comprobante previamente timbrado",
                'error' => "",
                'uuid' => $result->data->uuid,
            );
        }

        if($result->status == "error"){

            $msajeCodigo = explode("-",$result->message);
            return array(
                'resultado' => "false",
                'xmlData' => base64_encode($xml),
                'codigo' => trim($msajeCodigo[0]),
                'mensaje' => utf8_encode($result->message." ".$result->messageDetail),
                'error' => utf8_encode($result->message." ".$result->messageDetail),
                'uuid' => "",
            );
        } else if($result->status == "success"){

            return array(
                'resultado' => 'true',
                'xmlData' => base64_encode($result->data->cfdi),
                'codigo' => "200",
                'mensaje' => "Comprobante timbrado",
                'error' => "",
                'uuid' => $result->data->uuid,
            );
        }

        return array(
            'resultado' => 'true',
            'xmlData' => base64_encode($result->data->cfdi),
            'codigo' => "200",
            'mensaje' => "Comprobante timbrado",
            'error' => "",
            'uuid' => $result->data->uuid,
        );
    }



    public static function cancelarFactura($params)
    {
        #region agrega info al paramas para enviar al apc
        $params['token'] = self::auth();
        $params['url'] = "https://services.test.sw.com.mx/";
        #endregion

        header('Content-type: application/json');
        $cancelationService = CancelationService::Set($params);
        $result = $cancelationService::CancelationByCSD();

        if($result->status == "error"){
            return ['success' => false, 'msg' => $result->message.". ".$result->messageDetail];
        }

        return ['success' => true, 'msg' => ['acuse' => base64_encode($result->data->acuse), 'uuid' => $result->data->uuid,]];
    }


    private function auth(){
        $params = [
            "url"=>"http://services.test.sw.com.mx",
            "user"=>"liliana.bello@clickfactura.mx",
            "password"=> "BELLO+SW"
        ];

        try
        {
            header('Content-type: application/json');
            $auth = Authentication::auth($params);
            $token = $auth::Token();
            return $token->data->token;
        }
        catch(Exception $e)
        {
            header('Content-type: text/plain');
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}
