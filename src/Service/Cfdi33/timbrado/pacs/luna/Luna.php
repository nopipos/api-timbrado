<?php


namespace App\Service\Cfdi33\timbrado\pacs\luna;


use App\Service\Cfdi33\timbrado\Pacs;
use SWServices\Authentication\AuthenticationService as Authentication;
use SWServices\Cancelation\CancelationService as CancelationService;

class Luna implements Pacs
{

    public static function timbrar($xml, $peticion)
    {
        return array(
            'resultado' => 'true',
            'xmlData' => base64_encode($result->data->cfdi),
            'codigo' => "200",
            'mensaje' => "Comprobante timbrado",
            'error' => "",
            'uuid' => $result->data->uuid,
        );
    }

    public static function cancelarFactura($params)
    {
        #region agrega info al paramas para enviar al apc
        $params['token'] = self::auth();
        $params['url'] = "https://services.sw.com.mx/";
        #endregion

        header('Content-type: application/json');
        $cancelationService = CancelationService::Set($params);
        $result = $cancelationService::CancelationByCSD();

        if($result->status == "error"){
            return ['success' => false, 'msg' => $result->message.". ".$result->messageDetail];
        }

        return ['success' => true, 'msg' => ['acuse' => base64_encode($result->data->acuse), 'uuid' => $result->data->uuid,]];
    }


    private function auth(){
        $params = [
            "url"=>"http://services.sw.com.mx",
            "user"=>"liliana.bello@clickfactura.mx",
            "password"=> "BELLO+SW"
        ];

        try
        {
            header('Content-type: application/json');
            $auth = Authentication::auth($params);
            $token = $auth::Token();
            return $token->data->token;
        }
        catch(Exception $e)
        {
            header('Content-type: text/plain');
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

}
