<?php


namespace App\Service\Cfdi33\timbrado;


use App\Entity\Pac;
use App\Entity\Timbre\Peticiones;
use App\Service\Utils\XML2Array;
use Doctrine\Persistence\ManagerRegistry;

class Timbrar
{

    /**
     * @param $xml string xml en cadena sin codificicar
     * @param $pacs array array de los pacs que el usuario tiene configurado
     * @param $peticion Peticiones modelo de la peticion
     * @param $infoAccess array informacion de acceso del login del cliente
     */
    public static function timbrar($xml,$pacs,$peticion,$infoAccess,ManagerRegistry $doctrine){

        $PacRepo  = $doctrine->getRepository(Pac::class);

        $respuestaPac = null;

        $contadorPac = 0;
        while($respuestaPac == null){

            $modelPac = $PacRepo->findOneBy(['id'=>$pacs[$contadorPac],'estatus'=>1]);
            if($modelPac == null)
                return [
                    'resultado'=>false,
                    'error'=>'El pac configurado no se encontro favor de reportarlo a soporte: '.$pacs[0]
                ];

            $clasePath = $modelPac->getClase();

            if(class_exists($clasePath)){
                $resultado = $clasePath::timbrar($xml,$peticion);

                if(isset($resultado['xmlData'])){
                    $xmlArray = XML2Array::createArray(base64_decode($resultado['xmlData']));

                    if(isset($xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['UUID'])){
                        $resultado['uuid'] = $xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['UUID'];
                        $resultado['rfcProvCertif'] = $xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['RfcProvCertif'];
                        $resultado['selloCFD'] = $xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['SelloCFD'];
                        $resultado['noCertificadoSAT'] = $xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['NoCertificadoSAT'];
                        $resultado['selloSAT'] = $xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['SelloSAT'];
                        $resultado['FechaTimbrado'] = $xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['FechaTimbrado'];
                    }
                }
                if($resultado != null){
                    $respuestaPac =1;
                    return $resultado;
                }
            }
            else{
                return [
                'resultado'=>false,
                    'error'=>'El pac configurado no se encontra, favor de reportarlo a sistemas: '.$pacs[0]
                ];
            }

        }
    }
}