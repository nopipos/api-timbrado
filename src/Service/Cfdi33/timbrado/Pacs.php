<?php


namespace App\Service\Cfdi33\timbrado;


use App\Entity\Timbre\Peticiones;

interface Pacs
{

    /**
     * @param $xml string sin codificar
     * @param $peticion Peticiones modelo de Peticiones
     * @see  Peticiones
     * @return array | null
     */
    public static function  timbrar($xml, $peticion);

}