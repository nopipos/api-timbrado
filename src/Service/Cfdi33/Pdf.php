<?php


namespace App\Service\Cfdi33;


use App\Entity\Clientes;
use App\Entity\Sucursales;
use App\Entity\Timbre\Pac;
use App\Service\Utils\amazon\S3;
use App\Service\Utils\pdf\GenerarPdf;
use App\Service\Utils\Peticiones;

class Pdf
{
    static function crear($resultaddoTimbre,$doctrine,$resultLogin,$rfcCliente,$cpExpedicion,$fechaDoc){

        $conn = $doctrine->getManager()->getConnection();

        $emisor = $resultLogin['empresa'];
        $SucursalRepo = $doctrine->getRepository(Sucursales::class);
        $pacModel = $SucursalRepo->findOneBy(['cp'=>$cpExpedicion,'empresa'=>$emisor->getId()]);

        $datosParaPDF = [];

        if($pacModel == null){

            $datosParaPDF['emisor']['ExpedidoEn_calle'] = $emisor->getCalle();
            $datosParaPDF['emisor']['ExpedidoEn_noExterior'] = $emisor->getNumExt();
            $datosParaPDF['emisor']['ExpedidoEn_noInterior'] = $emisor->getNumInt();
            $datosParaPDF['emisor']['ExpedidoEn_colonia'] = $emisor->getColonia();
            $datosParaPDF['emisor']['ExpedidoEn_codigoPostal'] = "";
            $datosParaPDF['emisor']['ExpedidoEn_municipio'] = $emisor->getMunicipio();
            $datosParaPDF['emisor']['ExpedidoEn_estado'] = $emisor->getEstado();
        }
        else{
            $datosParaPDF['emisor']['ExpedidoEn_calle'] = $pacModel->getCalle();
            $datosParaPDF['emisor']['ExpedidoEn_noExterior'] = $pacModel->getNumExt();
            $datosParaPDF['emisor']['ExpedidoEn_noInterior'] = $pacModel->getNumInt();
            $datosParaPDF['emisor']['ExpedidoEn_colonia'] = $pacModel->getColonia();
            $datosParaPDF['emisor']['ExpedidoEn_codigoPostal'] = $pacModel->getCp();
            $datosParaPDF['emisor']['ExpedidoEn_municipio'] = $pacModel->getMunicipio();
            $datosParaPDF['emisor']['ExpedidoEn_estado'] = $pacModel->getEstado();
        }


        $datosParaPDF['emisor']['Emisor_calle'] = $emisor->getCalle();
        $datosParaPDF['emisor']['Emisor_noExterior'] = $emisor->getNumExt();
        $datosParaPDF['emisor']['Emisor_noInterior'] = $emisor->getNumInt();
        $datosParaPDF['emisor']['Emisor_colonia'] = $emisor->getColonia();
        $datosParaPDF['emisor']['Emisor_municipio'] = $emisor->getMunicipio();
        $datosParaPDF['emisor']['Emisor_estado'] = $emisor->getEstado();
        $datosParaPDF['emisor']['Emisor_pais'] = $emisor->getPais();


        $clientesRepo = $doctrine->getRepository(Clientes::class);
        $clientesModel = $clientesRepo->findOneBy(['rfc'=>$rfcCliente,'empresa'=>$emisor->getId()]);

        if($clientesModel != null){
            $direccion = $clientesModel->getDirecciones();
            $direccion = $direccion[0];
            $datosParaPDF['receptor']['Receptor_Clave'] = $clientesModel->getClave();
            if($direccion != null){
                $datosParaPDF['receptor']['Residencia_fiscal'] = $direccion->getCp();
                $datosParaPDF['receptor']['Receptor_calle'] = $direccion->getCalle();
                $datosParaPDF['receptor']['Receptor_noExterior'] = $direccion->getNoExt();
                $datosParaPDF['receptor']['Receptor_noInterior'] = $direccion->getNoInt();
                $datosParaPDF['receptor']['Receptor_colonia'] = $direccion->getColonia();
                $datosParaPDF['receptor']['Receptor_municipio'] = $direccion->getMunicipio();
                $datosParaPDF['receptor']['Receptor_estado'] = $direccion->getEstado();
                $datosParaPDF['receptor']['Receptor_pais'] = $direccion->getPais();
                $datosParaPDF['receptor']['Receptor_codigoPostal'] = $direccion->getCp();
                $datosParaPDF['receptor']['Receptor_localidad'] = "";
            }
            
        }

        $datosParaPDF['empresa'] = $emisor->getId();

        $pdfBase64 = GenerarPdf::generarPdf(base64_decode($resultaddoTimbre['xmlData']),$datosParaPDF,$datosParaPDF['empresa'],$conn);

        if(strlen($pdfBase64) > 0){
            #guardo el pdf en amazon
            $rfcEmisor = $emisor->getRfc();

            $fechaDoc = str_replace(" ","T",$fechaDoc);
            $fechaExplo = explode("T", $fechaDoc);
            $fechaExplo = explode("-", $fechaExplo[0]);
            $uuid = $resultaddoTimbre['uuid'];

            $año = $fechaExplo[0];
            $mes = $fechaExplo[1];
            $dia= $fechaExplo[2];

            $rutaMain = "emision/pdf/".$rfcEmisor."/".$año."/".$año."-".$mes."/".$dia."/".$uuid.".pdf";
            //S3::crearFile($pdfBase64,$rutaMain);

        }
        return $pdfBase64;

    }
}