<?php


namespace App\Service\Cfdi33;


class Xsd
{

    public static function addXsd($comprobanteNode,$request){

        if (isset($request['Comprobante']['Complemento']['Nomina'])) {

            $comprobanteNode->setAttribute('xmlns:nomina12', "http://www.sat.gob.mx/nomina12");
            $comprobanteNode->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/nomina12 http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina12.xsd');
        } elseif (isset($request['Comprobante']['Complemento']['Donatarias'])) {

            $comprobanteNode->setAttribute('xmlns:donat', "http://www.sat.gob.mx/donat");
            $comprobanteNode->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/donat http://www.sat.gob.mx/sitio_internet/cfd/donat/donat11.xsd');
        } elseif (isset($request['Comprobante']['Complemento']['ImpuestosLocales']['TotaldeRetenciones'])  &&
            isset($request['Comprobante']['Complemento']['ImpuestosLocales']['TotaldeTraslados'] )) {

            $comprobanteNode->setAttribute('xmlns:implocal', "http://www.sat.gob.mx/implocal");
            $comprobanteNode->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd');
        } elseif (isset($request['Comprobante']['Complemento']['CCE'])) {

            $comprobanteNode->setAttribute('xmlns:cce11', "http://www.sat.gob.mx/ComercioExterior11");
            $comprobanteNode->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/ComercioExterior11 http://www.sat.gob.mx/sitio_internet/cfd/ComercioExterior11/ComercioExterior11.xsd');
        } elseif (isset($request['Comprobante']['Complemento']['Pagos']['Version'])) {

            $comprobanteNode->setAttribute('xmlns:pago10', "http://www.sat.gob.mx/Pagos");
            $comprobanteNode->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd');
        } else {

            $comprobanteNode->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd');
        }

        if (isset($request['Comprobante']['Complemento']['Ine']['TipoProceso']) && strlen($request['Comprobante']['Complemento']['Ine']['TipoProceso']) > 0) {
            $comprobanteNode->setAttribute('xmlns:ine', "http://www.sat.gob.mx/ine");
            $comprobanteNode->setAttribute('xsi:schemaLocation', 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/ine http://www.sat.gob.mx/sitio_internet/cfd/ine/ine11.xsd');
        }

        if (isset($request['Comprobante']['Complemento']['detallista']['documentStructureVersion'])) {
            $comprobanteNode->setAttribute('xmlns:detallista', "http://www.sat.gob.mx/detallista");
            $comprobanteNode->setAttribute('xsi:schemaLocation', "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/detallista http://www.sat.gob.mx/sitio_internet/cfd/detallista/detallista.xsd");
        }

        if (isset($request['Comprobante']['Complemento']['Divisas'])) {
            $comprobanteNode->setAttribute('xmlns:divisas', "http://www.sat.gob.mx/divisas");
            $comprobanteNode->setAttribute('xsi:schemaLocation', "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/divisas http://www.sat.gob.mx/sitio_internet/cfd/divisas/divisas.xsd");
        }

        if (isset($request['Comprobante']['Complemento']['ServiciosParciales']['NumPerLicoAut']) && strlen($request['Comprobante']['Complemento']['ServiciosParciales']['NumPerLicoAut']) > 0) {
            $comprobanteNode->setAttribute('xmlns:servicioparcial', "http://www.sat.gob.mx/servicioparcialconstruccion");
            $comprobanteNode->setAttribute('xsi:schemaLocation', "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/servicioparcialconstruccion http://www.sat.gob.mx/sitio_internet/cfd/servicioparcialconstruccion/servicioparcialconstruccion.xsd");
        }

        if (isset($request['Comprobante']['Complemento']['NotariosPublicos']['Version'])) {
            $comprobanteNode->setAttribute('xmlns:notariospublicos', "http://www.sat.gob.mx/notariospublicos");
            $comprobanteNode->setAttribute('xsi:schemaLocation', "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/notariospublicos http://www.sat.gob.mx/sitio_internet/cfd/notariospublicos/notariospublicos.xsd");
        }

        if(isset($request['Comprobante']['Complemento']['LeyendasFiscales']['textoLeyenda']) && strlen($request['Comprobante']['Complemento']['LeyendasFiscales']['textoLeyenda']) > 0){
            $comprobanteNode->setAttribute('xmlns:leyendasFisc', "http://www.sat.gob.mx/leyendasFiscales");
            $comprobanteNode->setAttribute('xsi:schemaLocation', "http://www.sat.gob.mx/leyendasFiscales http://www.sat.gob.mx/sitio_internet/cfd/leyendasFiscales/leyendasFisc.xsd");

        }

        if (self::existeComplementoIedu($request)) {
            $comprobanteNode->setAttribute('xmlns:iedu', "http://www.sat.gob.mx/iedu");
            $comprobanteNode->setAttribute('xsi:schemaLocation', "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/iedu http://www.sat.gob.mx/sitio_internet/cfd/iedu/iedu.xsd");
        }


        return $comprobanteNode;

    }

    static function existeComplementoIedu($request)
    {

        try {
            for ($i = 0; $i < count($request['Comprobante']['Conceptos']['Concepto']); $i++) {

                if(isset($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['nombreAlumno']))
                if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['nombreAlumno'])) > 0) {
                    return true;
                }
            }
        } catch (\Exception $e) {
            return false;
        }
        return false;
    }
}