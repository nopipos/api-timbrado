<?php

namespace App\Service\Cfdi33\Complementos\impuestosLocales;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of GenerarXML
 *
 * @author Miguel Solis <miguel.solis@clickfactura.mx>
 */
class ImpuestosLocales11 extends Complemento {

    public function ImpuestosLocales11() {
        
    }

    /**
     * Funcion para agregar el complemento de ImpuestosLocales11
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request) {

        if (!self::existeComplemento($request)) {
            return $doc;
        }

//        $file = fopen("./algo.txt", "wr");
//        fwrite($file, print_r($request['Comprobante']['Complemento']['ImpuestosLocales'], true));
//        fclose($file);
//        echo "nooo";
//        exit;


        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        $ImpuestosLocales = $doc->createElement('implocal:ImpuestosLocales');
        $ImpuestosLocales = $ComplementoNode->appendChild($ImpuestosLocales);
        $ImpuestosLocales->setAttribute('version', trim($request['Comprobante']['Complemento']['ImpuestosLocales']['version']));
        $ImpuestosLocales->setAttribute('TotaldeRetenciones', trim($request['Comprobante']['Complemento']['ImpuestosLocales']['TotaldeRetenciones']));
        $ImpuestosLocales->setAttribute('TotaldeTraslados', trim($request['Comprobante']['Complemento']['ImpuestosLocales']['TotaldeTraslados']));


        if (isset($request['Comprobante']['Complemento']['ImpuestosLocales']['TrasladosLocales'][0]['ImpLocTrasladado'])) {

            foreach ($request['Comprobante']['Complemento']['ImpuestosLocales']['TrasladosLocales'] as $datoTrasladoLocal) {
                $TrasladosLocales = $doc->createElement('implocal:TrasladosLocales');
                $TrasladosLocales = $ImpuestosLocales->appendChild($TrasladosLocales);
                $TrasladosLocales->setAttribute('ImpLocTrasladado', trim($datoTrasladoLocal['ImpLocTrasladado']));
                $TrasladosLocales->setAttribute('TasadeTraslado', trim($datoTrasladoLocal['TasadeTraslado']));
                $TrasladosLocales->setAttribute('Importe', trim($datoTrasladoLocal['Importe']));
            }
        }
        if (isset($request['Comprobante']['Complemento']['ImpuestosLocales']['RetencionesLocales'][0]['ImpLocRetenido'])) {

            foreach ($request['Comprobante']['Complemento']['ImpuestosLocales']['RetencionesLocales'] as $datoRetencionLocal) {
                $RetencionesLocales = $doc->createElement('implocal:RetencionesLocales');
                $RetencionesLocales = $ImpuestosLocales->appendChild($RetencionesLocales);
                $RetencionesLocales->setAttribute('ImpLocRetenido', trim($datoRetencionLocal['ImpLocRetenido']));
                $RetencionesLocales->setAttribute('TasadeRetencion', trim($datoRetencionLocal['TasadeRetencion']));
                $RetencionesLocales->setAttribute('Importe', trim($datoRetencionLocal['Importe']));
            }
        }


        return $doc;
    }

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request) {

        if(isset($request['Comprobante']['Complemento']['ImpuestosLocales']['TotaldeRetenciones'])){
            if ($request['Comprobante']['Complemento']['ImpuestosLocales']['TotaldeRetenciones'] >= 0 &&
            $request['Comprobante']['Complemento']['ImpuestosLocales']['TotaldeTraslados'] >= 0
            ) {
                return true;
            }
        }
        return false;
    }

}
