<?php
/**
 * Created by PhpStorm.
 * User: Juan Sotero
 * Date: 17/04/2017
 * Time: 11:05 AM
 */

namespace App\Service\Cfdi33\Complementos\pagos;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of GenerarXML
 *
 * @author Miguel Solis <miguel.solis@clickfactura.mx>
 */
class Pagos10 extends Complemento
{

    public function Pagos10()
    {

    }


    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request)
    {
        if (!isset($request['Comprobante']['Complemento']['Pagos']['Version'])) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para agregar el complemento de pagos 10
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request)
    {


        if (!self::existeComplemento($request)) {
            return $doc;
        }


        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);




        #------------ pago
        if (count($request['Comprobante']['Complemento']['Pagos']['Pago']) > 0) {

            $Pagos = $doc->createElement('pago10:Pagos');
            $Pagos = $ComplementoNode->appendChild($Pagos);


            $comp = $request['Comprobante']['Complemento']['Pagos'];

            $Pagos->setAttribute("Version", $comp['Version']);


            for ($i = 0; $i < count($comp['Pago']); $i++) {


                $pago = null;

                if (strlen($comp['Pago'][$i]['FechaPago']) < 1) {
                    continue;
                } else {
                    $pago = $doc->createElement('pago10:Pago');
                    $pago = $Pagos->appendChild($pago);
                }

                $pago->setAttribute("FechaPago", $comp['Pago'][$i]['FechaPago']);
                $pago->setAttribute("FormaDePagoP", $comp['Pago'][$i]['FormaDePagoP']);
                $pago->setAttribute("MonedaP", $comp['Pago'][$i]['MonedaP']);

                if (strlen($comp['Pago'][$i]['TipoCambioP']) > 0) {
                    $pago->setAttribute("TipoCambioP", $comp['Pago'][$i]['TipoCambioP']);
                }

                $pago->setAttribute("Monto", $comp['Pago'][$i]['Monto']);

                if (strlen($comp['Pago'][$i]['NumOperacion']) > 0) {
                    $pago->setAttribute("NumOperacion", $comp['Pago'][$i]['NumOperacion']);
                }

                if (strlen($comp['Pago'][$i]['RfcEmisorCtaOrd']) > 0) {
                    $pago->setAttribute("RfcEmisorCtaOrd", $comp['Pago'][$i]['RfcEmisorCtaOrd']);
                }

                if (strlen($comp['Pago'][$i]['NomBancoOrdExt']) > 0) {
                    $pago->setAttribute("NomBancoOrdExt", $comp['Pago'][$i]['NomBancoOrdExt']);
                }

                if (strlen($comp['Pago'][$i]['CtaOrdenante']) > 0) {
                    $pago->setAttribute("CtaOrdenante", $comp['Pago'][$i]['CtaOrdenante']);
                }

                if (strlen($comp['Pago'][$i]['RfcEmisorCtaBen']) > 0) {
                    $pago->setAttribute("RfcEmisorCtaBen", $comp['Pago'][$i]['RfcEmisorCtaBen']);
                }

                if (strlen($comp['Pago'][$i]['CtaBeneficiario']) > 0) {
                    $pago->setAttribute("CtaBeneficiario", $comp['Pago'][$i]['CtaBeneficiario']);
                }

                if (strlen($comp['Pago'][$i]['TipoCadPago']) > 0) {
                    $pago->setAttribute("TipoCadPago", $comp['Pago'][$i]['TipoCadPago']);
                }

                if (strlen($comp['Pago'][$i]['CertPago']) > 0) {
                    $pago->setAttribute("CertPago", $comp['Pago'][$i]['CertPago']);
                }

                if (strlen($comp['Pago'][$i]['CadPago']) > 0) {
                    $pago->setAttribute("CadPago", $comp['Pago'][$i]['CadPago']);
                }

                if (strlen($comp['Pago'][$i]['SelloPago']) > 0) {
                    $pago->setAttribute("SelloPago", $comp['Pago'][$i]['SelloPago']);
                }


                #--------------- DoctoRelacionado
                if (count($comp['Pago'][$i]['DoctoRelacionado']) > 0) {

                    for ($j = 0; $j < count($comp['Pago'][$i]['DoctoRelacionado']); $j++) {

                        $doctoRel = null;

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['IdDocumento']) < 1) {
                            continue;
                        } else {
                            $doctoRel = $doc->createElement('pago10:DoctoRelacionado');
                            $doctoRel = $pago->appendChild($doctoRel);
                        }

                        $doctoRel->setAttribute("IdDocumento", $comp['Pago'][$i]['DoctoRelacionado'][$j]['IdDocumento']);

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['Serie']) > 0) {
                            $doctoRel->setAttribute("Serie", $comp['Pago'][$i]['DoctoRelacionado'][$j]['Serie']);
                        }

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['Folio']) > 0) {
                            $doctoRel->setAttribute("Folio", $comp['Pago'][$i]['DoctoRelacionado'][$j]['Folio']);
                        }

                        $doctoRel->setAttribute("MonedaDR", $comp['Pago'][$i]['DoctoRelacionado'][$j]['MonedaDR']);

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['TipoCambioDR']) > 0) {
                            $doctoRel->setAttribute("TipoCambioDR", $comp['Pago'][$i]['DoctoRelacionado'][$j]['TipoCambioDR']);
                        }

                        $doctoRel->setAttribute("MetodoDePagoDR", $comp['Pago'][$i]['DoctoRelacionado'][$j]['MetodoDePagoDR']);

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['NumParcialidad']) > 0) {
                            $doctoRel->setAttribute("NumParcialidad", $comp['Pago'][$i]['DoctoRelacionado'][$j]['NumParcialidad']);
                        }

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpSaldoAnt']) > 0) {
                            $doctoRel->setAttribute("ImpSaldoAnt", $comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpSaldoAnt']);
                        }

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpPagado']) > 0) {
                            $doctoRel->setAttribute("ImpPagado", $comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpPagado']);
                        }

                        if (strlen($comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpSaldoInsoluto']) > 0) {
                            $doctoRel->setAttribute("ImpSaldoInsoluto", $comp['Pago'][$i]['DoctoRelacionado'][$j]['ImpSaldoInsoluto']);
                        }


                    }

                }#--------------- DoctoRelacionado


                #--------------- pago10:Impuestos
                if (count($comp['Pago'][$i]['Impuestos']) > 0) {

                    for ($j = 0; $j < count($comp['Pago'][$i]['Impuestos']); $j++) {

                        $impues = null;

                        if (strlen($comp['Pago'][$i]['Impuestos'][$j]['TotalImpuestosRetenidos']) < 1 && strlen($comp['Pago'][$i]['Impuestos'][$j]['TotalImpuestosTrasladados']) < 1) {
                            continue;
                        } else {
                            $impues = $doc->createElement('pago10:Impuestos');
                            $impues = $pago->appendChild($impues);
                        }

                        if (strlen($comp['Pago'][$i]['Impuestos'][$j]['TotalImpuestosRetenidos']) > 0) {
                            $impues->setAttribute("TotalImpuestosRetenidos", $comp['Pago'][$i]['Impuestos'][$j]['TotalImpuestosRetenidos']);
                        }

                        if (strlen($comp['Pago'][$i]['Impuestos'][$j]['TotalImpuestosTrasladados']) > 0) {
                            $impues->setAttribute("TotalImpuestosTrasladados", $comp['Pago'][$i]['Impuestos'][$j]['TotalImpuestosTrasladados']);
                        }


                        #--------------------


                        if ($comp['Pago'][$i]['Impuestos'][$j]['Retenciones']['Retencion'][0]['Importe'] > 0) {

                            $Retenciones = $doc->createElement('pago10:Retenciones');
                            $Retenciones = $impues->appendChild($Retenciones);

                            for ($k = 0; $k < count($comp['Pago'][$i]['Impuestos'][$j]['Retenciones']['Retencion']); $k++) {

                                $Retencion = $doc->createElement('pago10:Retencion');
                                $Retencion = $Retenciones->appendChild($Retencion);

                                $Retencion->setAttribute('Impuesto', trim($comp['Pago'][$i]['Impuestos'][$j]['Retenciones']['Retencion'][$k]['Impuesto']));//CATALOGO
                                $Retencion->setAttribute('Importe', trim($comp['Pago'][$i]['Impuestos'][$j]['Retenciones']['Retencion'][$k]['Importe']));

                            }
                        }


                        if ($comp['Pago'][$i]['Impuestos'][$j]['Traslados']['Traslado'][0]['Importe'] > 0) {

                            $Traslados = $doc->createElement('cfdi:Traslados');
                            $Traslados = $impues->appendChild($Traslados);

                            for ($k = 0; $k < count($comp['Pago'][$i]['Impuestos'][$j]['Traslados']['Traslado']); $k++) {

                                if ($comp['Pago'][$i]['Impuestos'][$j]['Traslados']['Traslado'][0]['Impuesto'] != "") {

                                    $Traslado = $doc->createElement('pago10:Traslado');
                                    $Traslado = $Traslados->appendChild($Traslado);

                                    $Traslado->setAttribute('Impuesto', trim($comp['Pago'][$i]['Impuestos'][$j]['Traslados']['Traslado'][$k]['Impuesto']));//CATALOGO
                                    $Traslado->setAttribute('TipoFactor', trim($comp['Pago'][$i]['Impuestos'][$j]['Traslados']['Traslado'][$k]['TipoFactor']));//CATALOGO
                                    $Traslado->setAttribute('TasaOCuota', trim($comp['Pago'][$i]['Impuestos'][$j]['Traslados']['Traslado'][$k]['TasaOCuota']));
                                    $Traslado->setAttribute('Importe', trim($comp['Pago'][$i]['Impuestos'][$j]['Traslados']['Traslado'][$k]['Importe']));
                                }
                            }
                        }


                        #--------------------


                    }#for

                }#--------------- pago10:Impuestos

            }#for Pago


        }
        #------------ pago


        return $doc;

    }
}