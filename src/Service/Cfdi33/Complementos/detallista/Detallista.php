<?php

/**
 * Created by PhpStorm.
 * User: Juan Sotero
 * Date: 17/04/2017
 * Time: 11:05 AM
 */

namespace App\Service\Cfdi33\Complementos\detallista;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of GenerarXML
 *
 * @author Miguel Solis <miguel.solis@clickfactura.mx>
 */
class Detallista extends Complemento {

    /**
     * Detallista constructor.
     */
    public function __construct() {
        
    }

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request) {
                if (!isset($request['Comprobante']['Complemento']['detallista']['documentStructureVersion'])) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para agregar el complemento de nomina12
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request) {


        if (!self::existeComplemento($request)) {
            return $doc;
        }


        $detallistaA = $request['Comprobante']['Complemento']['detallista'];
        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        $parent = $doc->createElement('detallista:detallista');
        $parent = $ComplementoNode->appendChild($parent);


        if (strlen($detallistaA['type']) > 0) {
            $parent->setAttribute("type", $detallistaA['type']);
        }
        if (strlen($detallistaA['contentVersion']) > 0) {
            $parent->setAttribute("contentVersion", $detallistaA['contentVersion']);
        }
        $parent->setAttribute("documentStructureVersion", "AMC8.1");
        $parent->setAttribute("documentStatus", $detallistaA['documentStatus']);


        #requestForPaymentIdentification
        if (strlen($detallistaA['requestForPaymentIdentification']['entityType']) > 0) {
            $rfpi = $doc->createElement('detallista:requestForPaymentIdentification');
            $rfpi = $parent->appendChild($rfpi);

            $entityType = $doc->createElement('detallista:entityType', $detallistaA['requestForPaymentIdentification']['entityType']);
            $entityType = $rfpi->appendChild($entityType);
        }
        #requestForPaymentIdentification
        #requestForPaymentIdentification
        foreach ($detallistaA['specialInstruction'] as $data) {

            $si = $doc->createElement('detallista:specialInstruction');
            $si = $parent->appendChild($si);

            if (strlen($data['code']) > 0) {
                $si->setAttribute("code", $data['code']);
            }


            foreach ($data['text'] as $text) {
                if (strlen($text) > 0) {
                    $nText = $doc->createElement('detallista:text', $text);
                    $nText = $si->appendChild($nText);
                }
            }
        }
        #requestForPaymentIdentification
        #requestForPaymentIdentification
        if (count($detallistaA['orderIdentification']['referenceIdentification']) > 0) {
            $ordIden = $doc->createElement('detallista:orderIdentification');
            $ordIden = $parent->appendChild($ordIden);

            foreach ($detallistaA['orderIdentification']['referenceIdentification'] as $data) {

                if ($data['referenceIdentification']) {
                    $refIden = $doc->createElement('detallista:referenceIdentification', $data['referenceIdentification']);
                    $refIden = $ordIden->appendChild($refIden);
                    $refIden->setAttribute("type", $data['type']);
                }
            }


            $ReferenceDate = $doc->createElement('detallista:ReferenceDate', $detallistaA['orderIdentification']['ReferenceDate']);
            $ReferenceDate = $ordIden->appendChild($ReferenceDate);
        }
        #requestForPaymentIdentification
        #AdditionalInformation
        if (count($detallistaA['AdditionalInformation']['referenceIdentification']) > 0) {
            $addInf = $doc->createElement('detallista:AdditionalInformation');
            $addInf = $parent->appendChild($addInf);

            foreach ($detallistaA['AdditionalInformation']['referenceIdentification'] as $data) {

                if ($data['referenceIdentification']) {
                    $refIden = $doc->createElement('detallista:referenceIdentification', $data['referenceIdentification']);
                    $refIden = $addInf->appendChild($refIden);
                    $refIden->setAttribute("type", $data['type']);
                }
            }
        }
        #AdditionalInformation
        #DeliveryNote
        if (count($detallistaA['DeliveryNote']['referenceIdentification']) > 0) {
            $delNot = $doc->createElement('detallista:DeliveryNote');
            $delNot = $parent->appendChild($delNot);

            foreach ($detallistaA['DeliveryNote']['referenceIdentification'] as $data) {

                if ($data['referenceIdentification']) {
                    $refIden = $doc->createElement('detallista:referenceIdentification', $data['referenceIdentification']);
                    $refIden = $delNot->appendChild($refIden);
//                    $refIden->setAttribute("type", $data['type']);
                }
            }


            $ReferenceDate = $doc->createElement('detallista:ReferenceDate', $detallistaA['DeliveryNote']['ReferenceDate']);
            $ReferenceDate = $delNot->appendChild($ReferenceDate);
        }
        #DeliveryNote
        #buyer
        if (strlen($detallistaA['buyer']['gln']) > 0) {

            $data1 = $doc->createElement('detallista:buyer');
            $data1 = $parent->appendChild($data1);

            $data11 = $doc->createElement('detallista:gln', $detallistaA['buyer']['gln']);
            $data11 = $data1->appendChild($data11);


            if (strlen($detallistaA['buyer']['contactInformation']['personOrDepartmentName']['text']) > 0) {
                $data2 = $doc->createElement('detallista:contactInformation');
                $data2 = $data1->appendChild($data2);

                $data3 = $doc->createElement('detallista:personOrDepartmentName');
                $data3 = $data2->appendChild($data3);

                $data4 = $doc->createElement('detallista:text', $detallistaA['buyer']['contactInformation']['personOrDepartmentName']['text']);
                $data4 = $data3->appendChild($data4);
            }
        }
        #buyer
        #seller
        if (strlen($detallistaA['seller']['gln']) > 0) {

            $data1 = $doc->createElement('detallista:seller');
            $data1 = $parent->appendChild($data1);

            $data11 = $doc->createElement('detallista:gln', $detallistaA['seller']['gln']);
            $data11 = $data1->appendChild($data11);


            if (strlen($detallistaA['seller']['alternatePartyIdentification']['alternatePartyIdentification']) > 0) {
                $data2 = $doc->createElement('detallista:alternatePartyIdentification', $detallistaA['seller']['alternatePartyIdentification']['alternatePartyIdentification']);
                $data2 = $data1->appendChild($data2);

                $data2->setAttribute("type", $detallistaA['seller']['alternatePartyIdentification']['type']);
            }
        }
        #seller
        #shipTo
        if (strlen($detallistaA['shipTo']['gln']) > 0 ||
                (strlen($detallistaA['shipTo']['nameAndAddress']['name']) > 0 || strlen($detallistaA['shipTo']['nameAndAddress']['streetAddressOne']) > 0 || strlen($detallistaA['shipTo']['nameAndAddress']['city']) > 0 || strlen($detallistaA['shipTo']['nameAndAddress']['postalCode']) > 0
                )
        ) {

            $data1 = $doc->createElement('detallista:shipTo');
            $data1 = $parent->appendChild($data1);

            if (strlen($detallistaA['shipTo']['gln']) > 0) {
                $data11 = $doc->createElement('detallista:gln', $detallistaA['shipTo']['gln']);
                $data11 = $data1->appendChild($data11);
            }


            if (strlen($detallistaA['shipTo']['nameAndAddress']['name']) > 0 || strlen($detallistaA['shipTo']['nameAndAddress']['streetAddressOne']) > 0 || strlen($detallistaA['shipTo']['nameAndAddress']['city']) > 0 || strlen($detallistaA['shipTo']['nameAndAddress']['postalCode']) > 0
            ) {

                $data2 = $doc->createElement('detallista:nameAndAddress');
                $data2 = $data1->appendChild($data2);


                if (strlen($detallistaA['shipTo']['nameAndAddress']['name']) > 0) {
                    $data3 = $doc->createElement('detallista:name', $detallistaA['shipTo']['nameAndAddress']['name']);
                    $data3 = $data2->appendChild($data3);
                }

                if (strlen($detallistaA['shipTo']['nameAndAddress']['streetAddressOne']) > 0) {
                    $data4 = $doc->createElement('detallista:streetAddressOne', $detallistaA['shipTo']['nameAndAddress']['streetAddressOne']);
                    $data4 = $data2->appendChild($data4);
                }

                if (strlen($detallistaA['shipTo']['nameAndAddress']['city']) > 0) {
                    $data5 = $doc->createElement('detallista:city', $detallistaA['shipTo']['nameAndAddress']['city']);
                    $data5 = $data2->appendChild($data5);
                }

                if (strlen($detallistaA['shipTo']['nameAndAddress']['postalCode']) > 0) {
                    $data6 = $doc->createElement('detallista:postalCode', $detallistaA['shipTo']['nameAndAddress']['postalCode']);
                    $data6 = $data2->appendChild($data6);
                }
            }
        }
        #shipTo
        #InvoiceCreator
        if (
                strlen($detallistaA['InvoiceCreator']['gln']) > 0 ||
                strlen($detallistaA['InvoiceCreator']['alternatePartyIdentification']['alternatePartyIdentification']) > 0 ||
                (strlen($detallistaA['InvoiceCreator']['nameAndAddress']['name']) > 0 || strlen($detallistaA['InvoiceCreator']['nameAndAddress']['streetAddressOne']) > 0 || strlen($detallistaA['InvoiceCreator']['nameAndAddress']['city']) > 0 || strlen($detallistaA['InvoiceCreator']['nameAndAddress']['postalCode']) > 0
                )
        ) {

            $data1 = $doc->createElement('detallista:InvoiceCreator');
            $data1 = $parent->appendChild($data1);

            if (strlen($detallistaA['InvoiceCreator']['gln']) > 0) {
                $data11 = $doc->createElement('detallista:gln', $detallistaA['InvoiceCreator']['gln']);
                $data11 = $data1->appendChild($data11);
            }

            if (strlen($detallistaA['InvoiceCreator']['alternatePartyIdentification']['alternatePartyIdentification']) > 0) {
                $data113 = $doc->createElement('detallista:alternatePartyIdentification', $detallistaA['InvoiceCreator']['alternatePartyIdentification']['alternatePartyIdentification']);
                $data113 = $data1->appendChild($data113);

                $data113->setAttribute("type", $detallistaA['InvoiceCreator']['alternatePartyIdentification']['type']);
            }


            if (strlen($detallistaA['InvoiceCreator']['nameAndAddress']['name']) > 0 || strlen($detallistaA['InvoiceCreator']['nameAndAddress']['streetAddressOne']) > 0 || strlen($detallistaA['InvoiceCreator']['nameAndAddress']['city']) > 0 || strlen($detallistaA['InvoiceCreator']['nameAndAddress']['postalCode']) > 0
            ) {

                $data2 = $doc->createElement('detallista:nameAndAddress');
                $data2 = $data1->appendChild($data2);


                if (strlen($detallistaA['InvoiceCreator']['nameAndAddress']['name']) > 0) {
                    $data3 = $doc->createElement('detallista:name', $detallistaA['InvoiceCreator']['nameAndAddress']['name']);
                    $data3 = $data2->appendChild($data3);
                }

                if (strlen($detallistaA['InvoiceCreator']['nameAndAddress']['streetAddressOne']) > 0) {
                    $data4 = $doc->createElement('detallista:streetAddressOne', $detallistaA['InvoiceCreator']['nameAndAddress']['streetAddressOne']);
                    $data4 = $data2->appendChild($data4);
                }

                if (strlen($detallistaA['InvoiceCreator']['nameAndAddress']['city']) > 0) {
                    $data5 = $doc->createElement('detallista:city', $detallistaA['InvoiceCreator']['nameAndAddress']['city']);
                    $data5 = $data2->appendChild($data5);
                }

                if (strlen($detallistaA['InvoiceCreator']['nameAndAddress']['postalCode']) > 0) {
                    $data6 = $doc->createElement('detallista:postalCode', $detallistaA['InvoiceCreator']['nameAndAddress']['postalCode']);
                    $data6 = $data2->appendChild($data6);
                }
            }
        }
        #InvoiceCreator
        #Customs
        if (count($detallistaA['Customs']) > 0) {

            foreach ($detallistaA['Customs'] as $data) {
                if (strlen($data['gln']) > 0) {
                    $data1 = $doc->createElement('detallista:Customs');
                    $data1 = $parent->appendChild($data1);

                    $data11 = $doc->createElement('detallista:gln', $data['gln']);
                    $data11 = $data1->appendChild($data11);
                }
            }
        }
        #Customs
        #currency
        foreach ($detallistaA['currency'] as $currency) {
            if (
                    strlen($currency['currencyISOCode']) > 0 ||
                    count($currency['currencyFunction']) > 0
            ) {

                $data1 = $doc->createElement('detallista:currency');
                $data1 = $parent->appendChild($data1);

                $data1->setAttribute("currencyISOCode", $currency['currencyISOCode']);


                foreach ($currency['currencyFunction'] as $dato) {
                    $data113 = $doc->createElement('detallista:currencyFunction', $dato);
                    $data113 = $data1->appendChild($data113);
                }

                if (strlen($currency['rateOfChange']) > 0) {
                    $data11 = $doc->createElement('detallista:rateOfChange', $currency['rateOfChange']);
                    $data11 = $data1->appendChild($data11);
                }
            }
        }
        #currency
        #paymentTerms
        if (
                strlen($detallistaA['paymentTerms']['paymentTermsEvent']) > 0 ||
                strlen($detallistaA['paymentTerms']['PaymentTermsRelationTime']) > 0 ||
                strlen($detallistaA['paymentTerms']['netPayment']['netPaymentTermsType']) > 0 ||
                strlen($detallistaA['paymentTerms']['netPayment']['paymentTimePeriod']['timePeriodDue']['timePeriod']) > 0 ||
                strlen($detallistaA['paymentTerms']['discountPayment']['discountType']) > 0
        ) {

            $data1 = $doc->createElement('detallista:paymentTerms');
            $data1 = $parent->appendChild($data1);
            $data1->setAttribute("paymentTermsEvent", $detallistaA['paymentTerms']['paymentTermsEvent']);
            $data1->setAttribute("PaymentTermsRelationTime", $detallistaA['paymentTerms']['PaymentTermsRelationTime']);


            if (strlen($detallistaA['paymentTerms']['netPayment']['netPaymentTermsType']) > 0 ||
                    strlen($detallistaA['paymentTerms']['netPayment']['paymentTimePeriod']['timePeriodDue']['timePeriod']) > 0
            ) {


                $data11 = $doc->createElement('detallista:netPayment');
                $data11 = $data1->appendChild($data11);

                $data11->setAttribute("netPaymentTermsType", $detallistaA['paymentTerms']['netPayment']['netPaymentTermsType']);

                if (strlen($detallistaA['paymentTerms']['netPayment']['paymentTimePeriod']['timePeriodDue']['timePeriod']) > 0) {

                    $data2 = $doc->createElement('detallista:paymentTimePeriod');
                    $data2 = $data11->appendChild($data2);

                    $data3 = $doc->createElement('detallista:timePeriodDue');
                    $data3 = $data2->appendChild($data3);

                    $data3->setAttribute("timePeriod", $detallistaA['paymentTerms']['netPayment']['paymentTimePeriod']['timePeriodDue']['timePeriod']);

                    $data4 = $doc->createElement('detallista:value', $detallistaA['paymentTerms']['netPayment']['paymentTimePeriod']['timePeriodDue']['value']);
                    $data4 = $data3->appendChild($data4);
                }
            }

            if (strlen($detallistaA['paymentTerms']['discountPayment']['discountType']) > 0) {


                $data11 = $doc->createElement('detallista:discountPayment');
                $data11 = $data1->appendChild($data11);

                $data11->setAttribute("discountType", $detallistaA['paymentTerms']['discountPayment']['discountType']);

                $data4 = $doc->createElement('detallista:percentage', $detallistaA['paymentTerms']['discountPayment']['value']);
                $data4 = $data11->appendChild($data4);
            }
        }
        #paymentTerms
        #shipmentDetail
        if (strlen($detallistaA['shipmentDetail']) > 0) {

            $data4 = $doc->createElement('detallista:shipmentDetail', $detallistaA['shipmentDetail']);
            $data4 = $parent->appendChild($data4);
        }
        #shipmentDetail
        #allowanceCharge
        foreach ($detallistaA['allowanceCharge'] as $charge) {

            if (strlen($charge['allowanceChargeType']) > 0) {

                $data1 = $doc->createElement('detallista:allowanceCharge');
                $data1 = $parent->appendChild($data1);
                $data1->setAttribute("allowanceChargeType", $charge['allowanceChargeType']);
                $data1->setAttribute("settlementType", $charge['settlementType']);

                if (strlen($charge['sequenceNumber']) > 0) {
                    $data1->setAttribute("sequenceNumber", $charge['sequenceNumber']);
                }

                if (strlen($charge['specialServicesType']) > 0) {
                    $data11 = $doc->createElement('detallista:specialServicesType', $charge['specialServicesType']);
                    $data11 = $data1->appendChild($data11);
                }

                if (strlen($charge['monetaryAmountOrPercentage']['rate']['base']) > 0) {
                    $data2 = $doc->createElement('detallista:monetaryAmountOrPercentage');
                    $data2 = $data1->appendChild($data2);

                    $data3 = $doc->createElement('detallista:rate');
                    $data3 = $data2->appendChild($data3);

                    $data3->setAttribute("base", $charge['monetaryAmountOrPercentage']['rate']['base']);

                    $data4 = $doc->createElement('detallista:percentage', $charge['monetaryAmountOrPercentage']['rate']['percentage']);
                    $data4 = $data3->appendChild($data4);
                }
            }
        }
        #allowanceCharge
        #lineItem
        foreach ($detallistaA['lineItem'] as $charge) {

            if (strlen($charge['tradeItemIdentification']['gtin']) <= 0) {
                continue;
            }

            $data1 = $doc->createElement('detallista:lineItem');
            $data1 = $parent->appendChild($data1);

            if (strlen($charge['type']) > 0) {
                $data1->setAttribute("type", $charge['type']);
            }

            if (strlen($charge['number']) > 0) {
                $data1->setAttribute("number", $charge['number']);
            }

            #tradeItemIdentification
            $data2 = $doc->createElement('detallista:tradeItemIdentification');
            $data2 = $data1->appendChild($data2);
            $data3 = $doc->createElement('detallista:gtin', $charge['tradeItemIdentification']['gtin']);
            $data3 = $data2->appendChild($data3);
            #tradeItemIdentification
            #alternateTradeItemIdentification
            foreach ($charge['alternateTradeItemIdentification'] as $datoA) {

                if (strlen($datoA['alternateTradeItemIdentification']) > 0) {
                    $data4 = $doc->createElement('detallista:alternateTradeItemIdentification', $datoA['alternateTradeItemIdentification']);
                } else {
                    $data4 = $doc->createElement('detallista:alternateTradeItemIdentification');
                }
                $data4 = $data1->appendChild($data4);
                $data4->setAttribute("type", $datoA['type']);
            }
            #alternateTradeItemIdentification
            #tradeItemDescriptionInformation
            if (strlen($charge['tradeItemDescriptionInformation']['longText']) > 0) {

                $data5 = $doc->createElement('detallista:tradeItemDescriptionInformation');
                $data5 = $data1->appendChild($data5);

                if (strlen($charge['tradeItemDescriptionInformation']['language']) > 0) {
                    $data5->setAttribute("language", $charge['tradeItemDescriptionInformation']['language']);
                }
                $data6 = $doc->createElement('detallista:longText', str_replace("&","&amp;",$charge['tradeItemDescriptionInformation']['longText']) );
                $data6 = $data5->appendChild($data6);
            }
            #tradeItemDescriptionInformation
            #invoicedQuantity
            $data7 = $doc->createElement('detallista:invoicedQuantity', $charge['invoicedQuantity']['invoicedQuantity']);
            $data7 = $data1->appendChild($data7);
            $data7->setAttribute("unitOfMeasure", $charge['invoicedQuantity']['unitOfMeasure']);
            #invoicedQuantity
            #aditionalQuantity
            foreach ($charge['aditionalQuantity'] as $cuanti) {

                if (strlen($cuanti['aditionalQuantity']) > 0) {
                    $data7 = $doc->createElement('detallista:aditionalQuantity', $cuanti['aditionalQuantity']);
                } else {
                    $data7 = $doc->createElement('detallista:aditionalQuantity');
                }


                $data7 = $data1->appendChild($data7);
                $data7->setAttribute("QuantityType", $cuanti['QuantityType']);
            }
            #aditionalQuantity
            #grossPrice
            if (strlen($charge['grossPrice']['Amount']) > 0) {

                $data8 = $doc->createElement('detallista:grossPrice');
                $data8 = $data1->appendChild($data8);

                $data9 = $doc->createElement('detallista:Amount', $charge['grossPrice']['Amount']);
                $data9 = $data8->appendChild($data9);
            }
            #grossPrice
            #netPrice
            if (strlen($charge['netPrice']['Amount']) > 0) {

                $data8 = $doc->createElement('detallista:netPrice');
                $data8 = $data1->appendChild($data8);

                $data9 = $doc->createElement('detallista:Amount', $charge['netPrice']['Amount']);
                $data9 = $data8->appendChild($data9);
            }
            #netPrice
            #AdditionalInformation
            if (strlen($charge['AdditionalInformation']['referenceIdentification']['type']) > 0) {

                $data8 = $doc->createElement('detallista:AdditionalInformation');
                $data8 = $data1->appendChild($data8);

                $data9 = $doc->createElement('detallista:referenceIdentification', $charge['AdditionalInformation']['referenceIdentification']['referenceIdentification']);
                $data9 = $data8->appendChild($data9);

                $data9->setAttribute("type", $charge['AdditionalInformation']['referenceIdentification']['type']);
            }
            #AdditionalInformation
            #Customs
            foreach ($charge['Customs'] as $cust) {


                if (strlen($cust['alternatePartyIdentification']['alternatePartyIdentification']) > 0) {


                    $data8 = $doc->createElement('detallista:Customs');
                    $data8 = $data1->appendChild($data8);


                    if (strlen($cust['gln']) > 0) {
                        $data11 = $doc->createElement('detallista:gln', $cust['gln']);
                        $data11 = $data8->appendChild($data11);
                    }

                    $data2 = $doc->createElement('detallista:alternatePartyIdentification', $cust['alternatePartyIdentification']['alternatePartyIdentification']);
                    $data2 = $data8->appendChild($data2);

                    $data2->setAttribute("type", $cust['alternatePartyIdentification']['type']);


                    $data3 = $doc->createElement('detallista:ReferenceDate', $cust['ReferenceDate']);
                    $data3 = $data8->appendChild($data3);


                    $data4 = $doc->createElement('detallista:nameAndAddress');
                    $data4 = $data8->appendChild($data4);

                    $data5 = $doc->createElement('detallista:name', $cust['nameAndAddress']['name']);
                    $data5 = $data4->appendChild($data5);
                }
            }
            #Customs
            #LogisticUnits
            if (strlen($charge['LogisticUnits']['serialShippingContainerCode']['serialShippingContainerCode']) > 0) {

                $data8 = $doc->createElement('detallista:LogisticUnits');
                $data8 = $data1->appendChild($data8);

                $data9 = $doc->createElement('detallista:serialShippingContainerCode', $charge['LogisticUnits']['serialShippingContainerCode']['serialShippingContainerCode']);
                $data9 = $data8->appendChild($data9);

                $data9->setAttribute("type", $charge['LogisticUnits']['serialShippingContainerCode']['type']);
            }
            #LogisticUnits
            #palletInformation
            if (strlen($charge['palletInformation']['palletQuantity']) > 0) {

                $data8 = $doc->createElement('detallista:palletInformation');
                $data8 = $data1->appendChild($data8);

                $data9 = $doc->createElement('detallista:palletQuantity', $charge['palletInformation']['palletQuantity']);
                $data9 = $data8->appendChild($data9);

                $data4 = $doc->createElement('detallista:description', $charge['palletInformation']['description']['description']);
                $data4 = $data8->appendChild($data4);

                $data4->setAttribute("type", $charge['palletInformation']['description']['type']);

                $data5 = $doc->createElement('detallista:transport');
                $data5 = $data8->appendChild($data5);

                $data6 = $doc->createElement('detallista:methodOfPayment', $charge['palletInformation']['transport']['methodOfPayment']);
                $data6 = $data5->appendChild($data6);
            }
            #palletInformation
            #extendedAttributes
            if (count($charge['extendedAttributes']['lotNumber']) > 0) {

                $data8 = $doc->createElement('detallista:extendedAttributes');
                $data8 = $data1->appendChild($data8);

                foreach ($charge['extendedAttributes']['lotNumber'] as $cust) {

                    if (strlen($cust['lotNumber']) > 0) {

                        $data6 = $doc->createElement('detallista:lotNumber', $cust['lotNumber']);
                        $data6 = $data8->appendChild($data6);

                        $data6->setAttribute("productionDate", $cust['productionDate']);
                    }
                }
            }
            #extendedAttributes
            #allowanceCharge
            foreach ($charge['allowanceCharge'] as $cust) {


                if (strlen($cust['allowanceChargeType']) <= 0) {
                    continue;
                }

                $data8 = $doc->createElement('detallista:allowanceCharge');
                $data8 = $data1->appendChild($data8);

                $data8->setAttribute("allowanceChargeType", $cust['allowanceChargeType']);

                if (strlen($cust['settlementType']) > 0) {
                    $data8->setAttribute("settlementType", $cust['settlementType']);
                }

                if (strlen($cust['sequenceNumber']) > 0) {
                    $data8->setAttribute("sequenceNumber", $cust['sequenceNumber']);
                }


                if (strlen($cust['specialServicesType']) > 0) {
                    $data11 = $doc->createElement('detallista:specialServicesType', $cust['specialServicesType']);
                    $data11 = $data8->appendChild($data11);
                }


                $data2 = $doc->createElement('detallista:monetaryAmountOrPercentage');
                $data2 = $data8->appendChild($data2);

                $data3 = $doc->createElement('detallista:percentagePerUnit', $cust['monetaryAmountOrPercentage']['percentagePerUnit']);
                $data3 = $data2->appendChild($data3);


                if (strlen($cust['monetaryAmountOrPercentage']['ratePerUnit']['amountPerUnit']) > 0) {

                    $data4 = $doc->createElement('detallista:ratePerUnit');
                    $data4 = $data2->appendChild($data4);

                    $data5 = $doc->createElement('detallista:amountPerUnit', $cust['monetaryAmountOrPercentage']['ratePerUnit']['amountPerUnit']);
                    $data5 = $data4->appendChild($data5);
                }
            }
            #allowanceCharge
            #tradeItemTaxInformation
            foreach ($charge['tradeItemTaxInformation'] as $cust) {


                if (strlen($cust['taxTypeDescription']) <= 0) {
                    continue;
                }

                $data8 = $doc->createElement('detallista:tradeItemTaxInformation');
                $data8 = $data1->appendChild($data8);

                $data11 = $doc->createElement('detallista:taxTypeDescription', $cust['taxTypeDescription']);
                $data11 = $data8->appendChild($data11);

                if (strlen($cust['referenceNumber']) > 0) {
                    $data11 = $doc->createElement('detallista:referenceNumber', $cust['referenceNumber']);
                    $data11 = $data8->appendChild($data11);
                }

                if (strlen($cust['taxCategory']) > 0) {
                    $data11 = $doc->createElement('detallista:taxCategory', $cust['taxCategory']);
                    $data11 = $data8->appendChild($data11);
                }


                if (strlen($cust['tradeItemTaxAmount']['taxPercentage']) > 0) {

                    $data4 = $doc->createElement('detallista:tradeItemTaxAmount');
                    $data4 = $data8->appendChild($data4);

                    $data5 = $doc->createElement('detallista:taxPercentage', $cust['tradeItemTaxAmount']['taxPercentage']);
                    $data5 = $data4->appendChild($data5);

                    $data5 = $doc->createElement('detallista:taxAmount', $cust['tradeItemTaxAmount']['taxAmount']);
                    $data5 = $data4->appendChild($data5);
                }
            }
            #tradeItemTaxInformation
            #totalLineAmount
            if (strlen($charge['totalLineAmount']['netAmount']['Amount']) > 0) {

                $data8 = $doc->createElement('detallista:totalLineAmount');
                $data8 = $data1->appendChild($data8);

                if (strlen($charge['totalLineAmount']['grossAmount']['Amount']) > 0) {
                    $data9 = $doc->createElement('detallista:grossAmount');
                    $data9 = $data8->appendChild($data9);

                    $data10 = $doc->createElement('detallista:Amount', $charge['totalLineAmount']['grossAmount']['Amount']);
                    $data10 = $data9->appendChild($data10);
                }

                if (strlen($charge['totalLineAmount']['netAmount']['Amount']) > 0) {
                    $data9 = $doc->createElement('detallista:netAmount');
                    $data9 = $data8->appendChild($data9);

                    $data10 = $doc->createElement('detallista:Amount', $charge['totalLineAmount']['netAmount']['Amount']);
                    $data10 = $data9->appendChild($data10);
                }
            }
            #totalLineAmount
        }#lineItem
        #totalAmount
        if (strlen($detallistaA['totalAmount']['Amount']) > 0) {

            $data4 = $doc->createElement('detallista:totalAmount');
            $data4 = $parent->appendChild($data4);

            $data5 = $doc->createElement('detallista:Amount', $detallistaA['totalAmount']['Amount']);
            $data5 = $data4->appendChild($data5);
        }
        #totalAmount
        #TotalAllowanceCharge
        foreach ($detallistaA['TotalAllowanceCharge'] as $currency) {

            if (strlen($currency['allowanceOrChargeType']) <= 0) {
                continue;
            }


            $data1 = $doc->createElement('detallista:TotalAllowanceCharge');
            $data1 = $parent->appendChild($data1);

            $data1->setAttribute("allowanceOrChargeType", $currency['allowanceOrChargeType']);


            if (strlen($currency['specialServicesType']) > 0) {
                $data11 = $doc->createElement('detallista:specialServicesType', $currency['specialServicesType']);
                $data11 = $data1->appendChild($data11);
            }

            if (strlen($currency['Amount']) > 0) {
                $data11 = $doc->createElement('detallista:Amount', $currency['Amount']);
                $data11 = $data1->appendChild($data11);
            }
        }
        #TotalAllowanceCharge

        return $doc;
    }

}
