<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service\Cfdi33\Complementos\serviciosParciales;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of ServiciosParciales10
 *
 * @author sistemas18
 */
class ServiciosParciales10 extends Complemento {

    /**
     * Funcion para agregar el complemento de ServiciosParciales10
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request) {

        if (!self::existeComplemento($request)) {
            return $doc;
        }

        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        $ServiciosParciales = $doc->createElement('servicioparcial:parcialesconstruccion');
        $ServiciosParciales = $ComplementoNode->appendChild($ServiciosParciales);
        $ServiciosParciales->setAttribute('xmlns:servicioparcial', "http://www.sat.gob.mx/servicioparcialconstruccion");
        $ServiciosParciales->setAttribute('Version', $request['Comprobante']['Complemento']['ServiciosParciales']['Version']);
        $ServiciosParciales->setAttribute('NumPerLicoAut', $request['Comprobante']['Complemento']['ServiciosParciales']['NumPerLicoAut']);

        $Inmueble = $doc->createElement('servicioparcial:Inmueble');
        $Inmueble = $ServiciosParciales->appendChild($Inmueble);

        if (isset($request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Calle']) && $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Calle'] != "") {
            $Inmueble->setAttribute('Calle', $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Calle']);
        }

        if (isset($request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['NoExterior']) && $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['NoExterior'] != "") {
            $Inmueble->setAttribute('NoExterior', $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['NoExterior']);
        }

        if (isset($request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['NoInterior']) && $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['NoInterior'] != "") {
            $Inmueble->setAttribute('NoInterior', $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['NoInterior']);
        }

        if (isset($request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Colonia']) && $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Colonia'] != "") {
            $Inmueble->setAttribute('Colonia', $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Colonia']);
        }

        if (isset($request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Localidad']) && $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Localidad'] != "") {
            $Inmueble->setAttribute('Localidad', $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Localidad']);
        }

        if (isset($request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Referencia']) && $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Referencia'] != "") {
            $Inmueble->setAttribute('Referencia', $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Referencia']);
        }

        if (isset($request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Municipio']) && $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Municipio'] != "") {
            $Inmueble->setAttribute('Municipio', $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Municipio']);
        }

        if (isset($request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Estado']) && $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Estado'] != "") {
            $Inmueble->setAttribute('Estado', $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['Estado']);
        }

        if (isset($request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['CodigoPostal']) && $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['CodigoPostal'] != "") {
            $Inmueble->setAttribute('CodigoPostal', $request['Comprobante']['Complemento']['ServiciosParciales']['Inmueble']['CodigoPostal']);
        }
        
        return $doc;
    }

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request) {


        if(isset($request['Comprobante']['Complemento']['ServiciosParciales']['NumPerLicoAut'])){
            if (strlen($request['Comprobante']['Complemento']['ServiciosParciales']['NumPerLicoAut']) > 0){
                return true;
            }
    
        }

        return false;
    }

}
