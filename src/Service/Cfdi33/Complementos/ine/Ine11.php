<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service\Cfdi33\Complementos\ine;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of Ine11
 *
 * @author Juan Sotero <juan.sotero@clickfactura.mx>
 */
class Ine11 extends Complemento
{
    //put your code here

    /**
     * Funcion para agregar el complemento de ImpuestosLocales11
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request)
    {

        if (!self::existeComplemento($request)) {
            return $doc;
        }

//        $file = fopen("./algo.txt", "wr");
//        fwrite($file, print_r($request['Comprobante']['Complemento']['ImpuestosLocales'], true));
//        fclose($file);
//        echo "nooo";
//        exit;

        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        $ine = $doc->createElement('ine:INE');
        $ine = $ComplementoNode->appendChild($ine);
        $ine->setAttribute('xmlns:ine', "http://www.sat.gob.mx/ine");
        $ine->setAttribute('xsi:schemaLocation', "http://www.sat.gob.mx/ine http://www.sat.gob.mx/sitio_internet/cfd/ine/ine11.xsd");
        $ine->setAttribute('Version', trim($request['Comprobante']['Complemento']['Ine']['Version']));
        $ine->setAttribute('TipoProceso', trim($request['Comprobante']['Complemento']['Ine']['TipoProceso']));

        if (strlen($request['Comprobante']['Complemento']['Ine']['TipoComite']) > 0) {
            $ine->setAttribute('TipoComite', trim($request['Comprobante']['Complemento']['Ine']['TipoComite']));
        }

        if (isset($request['Comprobante']['Complemento']['Ine']['IdContabilidad'])) {
            $ine->setAttribute('IdContabilidad', trim($request['Comprobante']['Complemento']['Ine']['IdContabilidad']));
        }

        if (isset($request['Comprobante']['Complemento']['Ine']['Entidad'][0]['ClaveEntidad'])) {


            foreach ($request['Comprobante']['Complemento']['Ine']['Entidad'] as $entidad) {

                $entidadNode = $doc->createElement('ine:Entidad');
                $entidadNode = $ine->appendChild($entidadNode);
                $entidadNode->setAttribute('ClaveEntidad', trim($entidad['ClaveEntidad']));

                if (isset($entidad['Ambito'])) {
                    $entidadNode->setAttribute('Ambito', trim($entidad['Ambito']));
                }

                if (isset($entidad['IdContabilidad'][0])) {

                    foreach ($entidad['IdContabilidad'] as $conta) {

                        $contabilidad = $doc->createElement('ine:Contabilidad');
                        $contabilidad = $entidadNode->appendChild($contabilidad);
                        $contabilidad->setAttribute('IdContabilidad', trim($conta['IdContabilidad']));
                    }
                }
            }
        }
        return $doc;
    }

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request)
    {


        if(isset($request['Comprobante']['Complemento']['Ine']['TipoProceso'])){
            if (strlen($request['Comprobante']['Complemento']['Ine']['TipoProceso']) > 0)
            return true;
        }
        
        return false;
    }

}
