<?php


namespace App\Service\Cfdi33\Complementos\artes;


use App\Service\Cfdi33\Complementos\Complemento;

class Artes10 extends Complemento
{

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request) {

        if (!isset($request['Comprobante']['Complemento']['Artes'])) {
            return false;
        }
        return true;
    }

    /**
     * Funcion para agregar el complemento de nomina12
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request)
    {
        if (!self::existeComplemento($request)) {
            return $doc;
        }

        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        $obras = $doc->createElement('obrasarte:obrasarteantiguedades');
        $obras = $ComplementoNode->appendChild($obras);

        $art = $request['Comprobante']['Complemento']['Artes'];

        $obras->setAttribute('Version', $art['Version']);

        if(isset($art['TipoBien']) && strlen($art['TipoBien']) > 0)
            $obras->setAttribute('TipoBien', $art['TipoBien']);

        if(isset($art['OtrosTipoBien']) && strlen($art['OtrosTipoBien']) > 0)
            $obras->setAttribute('OtrosTipoBien', $art['OtrosTipoBien']);

        if(isset($art['TituloAdquirido']) && strlen($art['TituloAdquirido']) > 0)
            $obras->setAttribute('TituloAdquirido', $art['TituloAdquirido']);

        if(isset($art['OtrosTituloAdquirido']) && strlen($art['OtrosTituloAdquirido']) > 0)
            $obras->setAttribute('OtrosTituloAdquirido', $art['OtrosTituloAdquirido']);

        if(isset($art['Subtotal']) && strlen($art['Subtotal']) > 0)
            $obras->setAttribute('Subtotal', $art['Subtotal']);

        if(isset($art['IVA']) && strlen($art['IVA']) > 0)
            $obras->setAttribute('IVA', $art['IVA']);

        if(isset($art['FechaAdquisicion']) && strlen($art['FechaAdquisicion']) > 0)
            $obras->setAttribute('FechaAdquisicion', $art['FechaAdquisicion']);

        if(isset($art['CaracterísticasDeObraoPieza']) && strlen($art['CaracterísticasDeObraoPieza']) > 0)
            $obras->setAttribute('CaracterísticasDeObraoPieza', $art['CaracterísticasDeObraoPieza']);

        return $doc;

    }


}