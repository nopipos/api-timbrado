<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service\Cfdi33\Complementos\leyendasFiscales;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of Ine11
 *
 * @author Juan Sotero <juan.sotero@clickfactura.mx>
 */
class LeyendasFiscales extends Complemento
{
    //put your code here

    /**
     * Funcion para agregar el complemento de ImpuestosLocales11
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request)
    {

        if (!self::existeComplemento($request)){
            return $doc;
        }

        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        $ine = $doc->createElement('leyendasFisc:LeyendasFiscales');
        $ine = $ComplementoNode->appendChild($ine);

        $ine->setAttribute('version', trim($request['Comprobante']['Complemento']['LeyendasFiscales']['version']));


        $entidadNode = $doc->createElement('leyendasFisc:Leyenda');
        $entidadNode = $ine->appendChild($entidadNode);

        if(isset($request['Comprobante']['Complemento']['LeyendasFiscales']['disposicionFiscal'])){
            $entidadNode->setAttribute('disposicionFiscal', trim($request['Comprobante']['Complemento']['LeyendasFiscales']['disposicionFiscal']));
        }

        if(isset($request['Comprobante']['Complemento']['LeyendasFiscales']['disposicionFiscal'])){
            $entidadNode->setAttribute('norma', trim($request['Comprobante']['Complemento']['LeyendasFiscales']['norma']));
        }

        $entidadNode->setAttribute('textoLeyenda', trim($request['Comprobante']['Complemento']['LeyendasFiscales']['textoLeyenda']));

        return $doc;

    }

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request)
    {

        if(isset($request['Comprobante']['Complemento']['LeyendasFiscales']['textoLeyenda'])){
            if (strlen($request['Comprobante']['Complemento']['LeyendasFiscales']['textoLeyenda']) > 0)
            return true;

        }


        return false;
    }

}
