<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service\Cfdi33\Complementos\notaria;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of Notaria10
 *
 * @author JUANSOTERO
 */
class Notaria10 extends Complemento {
    //put your code here

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request) {

        if (!isset($request['Comprobante']['Complemento']['NotariosPublicos']['Version'])) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para agregar el complemento de nomina12
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request) {

        if (!self::existeComplemento($request)) {
            return $doc;
        }

        $complementoNotaria = $request['Comprobante']['Complemento']['NotariosPublicos'];

        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);
        $notariospublicos = $doc->createElement('notariospublicos:NotariosPublicos');
        $notariospublicos = $ComplementoNode->appendChild($notariospublicos);
        //$notariospublicos->setAttribute('xmlns:notariospublico', "http://www.sat.gob.mx/notariospublicos");
        $notariospublicos->setAttribute('Version', $complementoNotaria['Version']);

        #datosInmueble
        $DescInmuebles = $doc->createElement('notariospublicos:DescInmuebles');
        $DescInmuebles = $notariospublicos->appendChild($DescInmuebles);

        #esto va en un ciclo
        foreach ($complementoNotaria['DescInmuebles'] as $inmueble) {
            $DescInmueble = $doc->createElement('notariospublicos:DescInmueble');
            $DescInmueble = $DescInmuebles->appendChild($DescInmueble);

            $DescInmueble->setAttribute('TipoInmueble', $inmueble['TipoInmueble']);
            $DescInmueble->setAttribute('Calle', $inmueble['Calle']);
            if (strlen(trim($inmueble['NoExterior'])) > 0)
                $DescInmueble->setAttribute('NoExterior', $inmueble['NoExterior']);#O
            if (strlen(trim($inmueble['NoInterior'])) > 0)
                $DescInmueble->setAttribute('NoInterior', $inmueble['NoInterior']);#O
            if (strlen(trim($inmueble['Colonia'])) > 0)
                $DescInmueble->setAttribute('Colonia', $inmueble['Colonia']);#O
            if (strlen(trim($inmueble['Localidad'])) > 0)
                $DescInmueble->setAttribute('Localidad', $inmueble['Localidad']);#O
            if (strlen(trim($inmueble['Referencia'])) > 0)
                $DescInmueble->setAttribute('Referencia', $inmueble['Referencia']);#O
            $DescInmueble->setAttribute('Municipio', $inmueble['Municipio']);
            $DescInmueble->setAttribute('Estado', $inmueble['Estado']);
            $DescInmueble->setAttribute('Pais', $inmueble['Pais']);
            $DescInmueble->setAttribute('CodigoPostal', $inmueble['CodigoPostal']);
        }


        #datos de la operacion
        $DatosOperacion = $doc->createElement('notariospublicos:DatosOperacion');
        $DatosOperacion = $notariospublicos->appendChild($DatosOperacion);

        $DatosOperacion->setAttribute('NumInstrumentoNotarial', $complementoNotaria['NumInstrumentoNotarial']);
        $DatosOperacion->setAttribute('FechaInstNotarial', $complementoNotaria['FechaInstNotarial']);
        $DatosOperacion->setAttribute('MontoOperacion', $complementoNotaria['MontoOperacion']);
        $DatosOperacion->setAttribute('Subtotal', $complementoNotaria['Subtotal']);
        $DatosOperacion->setAttribute('IVA', $complementoNotaria['IVA']);


        #datosNotario
        $DatosNotario = $doc->createElement('notariospublicos:DatosNotario');
        $DatosNotario = $notariospublicos->appendChild($DatosNotario);

        $DatosNotario->setAttribute('CURP', $complementoNotaria['CURP']);
        $DatosNotario->setAttribute('NumNotaria', $complementoNotaria['NumNotaria']);
        $DatosNotario->setAttribute('EntidadFederativa', $complementoNotaria['EntidadFederativa']);
        if (strlen(trim($complementoNotaria['Adscripcion'])) > 0)
            $DatosNotario->setAttribute('Adscripcion', $complementoNotaria['Adscripcion']);

        #datosEnajenante
        $DatosEnajenante = $doc->createElement('notariospublicos:DatosEnajenante');
        $DatosEnajenante = $notariospublicos->appendChild($DatosEnajenante);

        $DatosEnajenante->setAttribute('CoproSocConyugalE', $complementoNotaria["Enajenante_copropiedad"]);


        if ($complementoNotaria['Enajenante_copropiedad'] == "Si") {
            $DatosEnajenantesCopSC = $doc->createElement('notariospublicos:DatosEnajenantesCopSC');
            $DatosEnajenantesCopSC = $DatosEnajenante->appendChild($DatosEnajenantesCopSC);
        }

        foreach ($complementoNotaria['DatosEnajenante'] as $enajenante) {

            if ($complementoNotaria["Enajenante_copropiedad"] == "No") {

                $DatosUnEnajenante = $doc->createElement('notariospublicos:DatosUnEnajenante');
                $DatosUnEnajenante = $DatosEnajenante->appendChild($DatosUnEnajenante);

                $DatosUnEnajenante->setAttribute('Nombre', $enajenante['Nombre']);
                $DatosUnEnajenante->setAttribute('ApellidoPaterno', $enajenante['ApellidoPaterno']);
                if (strlen(trim($enajenante['ApellidoMaterno'])) > 0)
                    $DatosUnEnajenante->setAttribute('ApellidoMaterno', $enajenante['ApellidoMaterno']);#O
                $DatosUnEnajenante->setAttribute('RFC', $enajenante['RFC']);
                if (strlen(trim($enajenante['CURP'])) > 0)
                    $DatosUnEnajenante->setAttribute('CURP', $enajenante['CURP']);
            } else {

                #esto va en un ciclo
                $DatosEnajenanteCopSC = $doc->createElement('notariospublicos:DatosEnajenanteCopSC');
                $DatosEnajenanteCopSC = $DatosEnajenantesCopSC->appendChild($DatosEnajenanteCopSC);

                $DatosEnajenanteCopSC->setAttribute('Nombre', $enajenante['Nombre']);
                $DatosEnajenanteCopSC->setAttribute('ApellidoPaterno', $enajenante['ApellidoPaterno']);
                if (strlen(trim($enajenante['ApellidoMaterno'])) > 0)
                    $DatosEnajenanteCopSC->setAttribute('ApellidoMaterno', $enajenante['ApellidoMaterno']);#O
                $DatosEnajenanteCopSC->setAttribute('RFC', $enajenante['RFC']);
                if (strlen(trim($enajenante['CURP'])) > 0)
                    $DatosEnajenanteCopSC->setAttribute('CURP', $enajenante['CURP']);
                $DatosEnajenanteCopSC->setAttribute('Porcentaje', $enajenante['Porcentaje']);
            }
        }

        #datosAdquiriente
        $DatosAdquiriente = $doc->createElement('notariospublicos:DatosAdquiriente');
        $DatosAdquiriente = $notariospublicos->appendChild($DatosAdquiriente);

        $DatosAdquiriente->setAttribute('CoproSocConyugalE', $complementoNotaria['Adquiriente_copropiedad']);

        if ($complementoNotaria['Adquiriente_copropiedad'] == "Si") {
            $DatosAdquirientesCopSC = $doc->createElement('notariospublicos:DatosAdquirientesCopSC');
            $DatosAdquirientesCopSC = $DatosAdquiriente->appendChild($DatosAdquirientesCopSC);
        }



        foreach ($complementoNotaria['DatosAdquiriente'] as $adquiriente) {
            if ($complementoNotaria['Adquiriente_copropiedad'] == "No") {

                $DatosUnAdquiriente = $doc->createElement('notariospublicos:DatosUnAdquiriente');
                $DatosUnAdquiriente = $DatosAdquiriente->appendChild($DatosUnAdquiriente);

                $DatosUnAdquiriente->setAttribute('Nombre', $adquiriente['Nombre']);
                if (strlen(trim($adquiriente['ApellidoPaterno'])) > 0)
                    $DatosUnAdquiriente->setAttribute('ApellidoPaterno', $adquiriente['ApellidoPaterno']);#O3
                if (strlen(trim($adquiriente['ApellidoMaterno'])) > 0)
                    $DatosUnAdquiriente->setAttribute('ApellidoMaterno', $adquiriente['ApellidoMaterno']);#O
                $DatosUnAdquiriente->setAttribute('RFC', $adquiriente['RFC']);
                if (strlen(trim($adquiriente['CURP'])) > 0)
                    $DatosUnAdquiriente->setAttribute('CURP', $adquiriente['CURP']);#O
            } else {


                #esto va en un ciclo
                $DatosAdquirienteCopSC = $doc->createElement('notariospublicos:DatosAdquirienteCopSC');
                $DatosAdquirienteCopSC = $DatosAdquirientesCopSC->appendChild($DatosAdquirienteCopSC);

                $DatosAdquirienteCopSC->setAttribute('Nombre', $adquiriente['Nombre']);
                $DatosAdquirienteCopSC->setAttribute('ApellidoPaterno', $adquiriente['ApellidoPaterno']);
                if (strlen(trim($adquiriente['ApellidoMaterno'])) > 0)
                    $DatosAdquirienteCopSC->setAttribute('ApellidoMaterno', $adquiriente['ApellidoMaterno']);#O
                $DatosAdquirienteCopSC->setAttribute('RFC', $adquiriente['RFC']);
                if (strlen(trim($adquiriente['CURP'])) > 0)
                    $DatosAdquirienteCopSC->setAttribute('CURP', $adquiriente['CURP']);
                $DatosAdquirienteCopSC->setAttribute('Porcentaje', $adquiriente['Porcentaje']);
            }
        }
    }

}
