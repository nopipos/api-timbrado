<?php

namespace App\Service\Cfdi33\Complementos\nomina;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of GenerarXML
 *
 * @author Miguel Solis <miguel.solis@clickfactura.mx>
 */
class Nomina12 extends Complemento {

    public function Nomina12() {
        
    }

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request) {

        if (!isset($request['Comprobante']['Complemento']['Nomina'])) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para agregar el complemento de nomina12
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request) {


        if (!self::existeComplemento($request)) {
            return $doc;
        }

        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        $Nomina = $doc->createElement('nomina12:Nomina');
        $Nomina = $ComplementoNode->appendChild($Nomina);
        //$Nomina->setAttribute('xmlns:nomina', "http://www.sat.gob.mx/nomina");
        $Nomina->setAttribute('Version', "1.2");


        $Nomina->setAttribute('TipoNomina', trim($request['Comprobante']['Complemento']['Nomina']['TipoNomina']));
        $Nomina->setAttribute('FechaPago', trim($request['Comprobante']['Complemento']['Nomina']['FechaPago']));
        $Nomina->setAttribute('FechaInicialPago', trim($request['Comprobante']['Complemento']['Nomina']['FechaInicialPago']));
        $Nomina->setAttribute('FechaFinalPago', trim($request['Comprobante']['Complemento']['Nomina']['FechaFinalPago']));
        $Nomina->setAttribute('NumDiasPagados', trim($request['Comprobante']['Complemento']['Nomina']['NumDiasPagados']));

        if (strlen($request['Comprobante']['Complemento']['Nomina']['TotalPercepciones']) > 0) {
            $Nomina->setAttribute('TotalPercepciones', trim($request['Comprobante']['Complemento']['Nomina']['TotalPercepciones']));
        }
        if (strlen($request['Comprobante']['Complemento']['Nomina']['TotalDeducciones']) > 0) {
            $Nomina->setAttribute('TotalDeducciones', trim($request['Comprobante']['Complemento']['Nomina']['TotalDeducciones']));
        }
        if (strlen($request['Comprobante']['Complemento']['Nomina']['TotalOtrosPagos']) > 0) {
            $Nomina->setAttribute('TotalOtrosPagos', trim($request['Comprobante']['Complemento']['Nomina']['TotalOtrosPagos']));
        }


        if (isset($request['Comprobante']['Complemento']['Nomina']['Emisor'])) {

            $nomina_emisor = $doc->createElement('nomina12:Emisor');
            $nomina_emisor = $Nomina->appendChild($nomina_emisor);
            if (strlen($request['Comprobante']['Complemento']['Nomina']['Emisor']['Curp']) > 0)
                $nomina_emisor->setAttribute('Curp', trim($request['Comprobante']['Complemento']['Nomina']['Emisor']['Curp']));

            if (strlen($request['Comprobante']['Complemento']['Nomina']['Emisor']['RegistroPatronal']) > 0)
                $nomina_emisor->setAttribute('RegistroPatronal', trim($request['Comprobante']['Complemento']['Nomina']['Emisor']['RegistroPatronal']));

            if (strlen($request['Comprobante']['Complemento']['Nomina']['Emisor']['RFCPatronOrigen']) > 0)
                $nomina_emisor->setAttribute('RFCPatronOrigen', trim($request['Comprobante']['Complemento']['Nomina']['Emisor']['RFCPatronOrigen']));

            if (isset($request['Comprobante']['Complemento']['Nomina']['Emisor']['EntidadSNCF'])) {

                $nomina_emisor_EntidadSNCF = $doc->createElement('nomina12:EntidadSNCF');
                $nomina_emisor_EntidadSNCF = $nomina_emisor->appendChild($nomina_emisor_EntidadSNCF);

                $nomina_emisor_EntidadSNCF->setAttribute('OrigenRecurso', trim($request['Comprobante']['Complemento']['Nomina']['Emisor']['EntidadSNCF']['OrigenRecurso']));
                if (trim($request['Comprobante']['Complemento']['Nomina']['Emisor']['EntidadSNCF']['MontoRecursoPropio']) > 0) {
                    $nomina_emisor_EntidadSNCF->setAttribute('MontoRecursoPropio', trim($request['Comprobante']['Complemento']['Nomina']['Emisor']['EntidadSNCF']['MontoRecursoPropio']));
                }
            }
        }

        if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor'])) {

            $nomina_receptor = $doc->createElement('nomina12:Receptor');
            $nomina_receptor = $Nomina->appendChild($nomina_receptor);


            if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor']['SubContratacion'])) {

                if (
                        strlen(trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['SubContratacion']['RfcLabora'])) > 0 && strlen(trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['SubContratacion']['PorcentajeTiempo'])) > 0
                ) {
                    $nomina_receptor_SubContratacion = $doc->createElement('nomina12:SubContratacion');
                    $nomina_receptor_SubContratacion = $nomina_receptor->appendChild($nomina_receptor_SubContratacion);
                    $nomina_receptor_SubContratacion->setAttribute('RfcLabora', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['SubContratacion']['RfcLabora']));
                    $nomina_receptor_SubContratacion->setAttribute('PorcentajeTiempo', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['SubContratacion']['PorcentajeTiempo']));
                }
            }

            $nomina_receptor->setAttribute('Curp', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['Curp']));
            if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor']['NumSeguridadSocial']) &&  strlen(trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['NumSeguridadSocial'])) > 0  ) {
                $nomina_receptor->setAttribute('NumSeguridadSocial', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['NumSeguridadSocial']));
            }

            if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor']['FechaInicioRelLaboral']) &&  strlen(trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['FechaInicioRelLaboral'])) > 0  ) {
                $nomina_receptor->setAttribute('FechaInicioRelLaboral', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['FechaInicioRelLaboral']));
            }

            if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor']['Antiguedad']) && strlen(trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['Antiguedad']))) {
                $nomina_receptor->setAttribute('Antigüedad', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['Antiguedad']));
            }

            $nomina_receptor->setAttribute('TipoContrato', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['TipoContrato']));

            if (strlen(trim(($request['Comprobante']['Complemento']['Nomina']['Receptor']['Sindicalizado']))) > 0) {
                $nomina_receptor->setAttribute('Sindicalizado', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['Sindicalizado']));
            }

            if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor']['TipoJornada']) && strlen(trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['TipoJornada']))) {
                $nomina_receptor->setAttribute('TipoJornada', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['TipoJornada']));
            }

            $nomina_receptor->setAttribute('TipoRegimen', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['TipoRegimen']));
            $nomina_receptor->setAttribute('NumEmpleado', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['NumEmpleado']));
            $nomina_receptor->setAttribute('Departamento', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['Departamento']));
            if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['Puesto'])) > 0) {
                $nomina_receptor->setAttribute('Puesto', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['Puesto']));
            }

            if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor']['RiesgoPuesto']) && strlen(trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['RiesgoPuesto']))) {
                $nomina_receptor->setAttribute('RiesgoPuesto', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['RiesgoPuesto']));
            }



            $nomina_receptor->setAttribute('PeriodicidadPago', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['PeriodicidadPago']));
            if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor']['Banco'])) {
                $nomina_receptor->setAttribute('Banco', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['Banco']));
            }

            if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor']['CuentaBancaria'])) {
                $nomina_receptor->setAttribute('CuentaBancaria', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['CuentaBancaria']));
            }

            if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor']['SalarioBaseCotApor']) && strlen(trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['SalarioBaseCotApor']))) {

                $nomina_receptor->setAttribute('SalarioBaseCotApor', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['SalarioBaseCotApor']));
            }


            if (isset($request['Comprobante']['Complemento']['Nomina']['Receptor']['SalarioDiarioIntegrado']) && strlen(trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['SalarioDiarioIntegrado']))) {
                $nomina_receptor->setAttribute('SalarioDiarioIntegrado', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['SalarioDiarioIntegrado']));
            }

            $nomina_receptor->setAttribute('ClaveEntFed', trim($request['Comprobante']['Complemento']['Nomina']['Receptor']['ClaveEntFed']));
        }

        if (isset($request['Comprobante']['Complemento']['Nomina']['Percepciones']['Percepcion'][0])) {
            $Percepciones = $doc->createElement('nomina12:Percepciones');
            $Percepciones = $Nomina->appendChild($Percepciones);
            $Percepciones->setAttribute('TotalGravado', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalGravado']));
            $Percepciones->setAttribute('TotalExento', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalExento']));

            if (isset($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalSueldos'])) {
                $Percepciones->setAttribute('TotalSueldos', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalSueldos']));
            }
            if (isset($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalSeparacionIndemnizacion'])) {
                $Percepciones->setAttribute('TotalSeparacionIndemnizacion', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalSeparacionIndemnizacion']));
            }
            if (isset($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalJubilacionPensionRetiro'])) {
                $Percepciones->setAttribute('TotalJubilacionPensionRetiro', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalJubilacionPensionRetiro']));
            }
            $varTotalImporteGravado = 0;
            $varTotalImporteExento = 0;


            foreach ($request['Comprobante']['Complemento']['Nomina']['Percepciones']['Percepcion'] as $data) {


                if (isset($data['Clave'])) {
                    $Percepcion = $doc->createElement('nomina12:Percepcion');
                    $Percepcion = $Percepciones->appendChild($Percepcion);

                    $data['TipoPercepcion'] = trim($data['TipoPercepcion']);
                    while (strlen($data['TipoPercepcion']) < 3) {
                        $data['TipoPercepcion'] = "0" . $data['TipoPercepcion'];
                    }

                    while (strlen($data['Clave']) < 3) {
                        $data['Clave'] = "0" . $data['Clave'];
                    }


                    $Percepcion->setAttribute('TipoPercepcion', trim($data['TipoPercepcion']));
                    $Percepcion->setAttribute('Clave', trim($data['Clave']));
                    $Percepcion->setAttribute('Concepto', utf8_decode(trim($data['Concepto'])));
                    $Percepcion->setAttribute('ImporteGravado', trim($data['ImporteGravado']));
                    $Percepcion->setAttribute('ImporteExento', trim($data['ImporteExento']));
                    $varTotalImporteGravado += trim($data['ImporteGravado']);
                    $varTotalImporteExento += trim($data['ImporteExento']);


                    if (isset($data['AccionesOTitulos'])) {

                        $Percepciones_AccionesOTitulos = $doc->createElement('nomina12:AccionesOTitulos');
                        $Percepciones_AccionesOTitulos = $Percepcion->appendChild($Percepciones_AccionesOTitulos);

                        $Percepciones_AccionesOTitulos->setAttribute('ValorMercado', trim($data['AccionesOTitulos']['ValorMercado']));
                        $Percepciones_AccionesOTitulos->setAttribute('PrecioAlOtorgarse', trim($data['AccionesOTitulos']['PrecioAlOtorgarse']));
                    }



                    foreach ($data['HorasExtra'] as $dataHoras) {
                        if (trim($dataHoras['ImportePagado']) != "") {

                            $Percepciones_HorasExtra = $doc->createElement('nomina12:HorasExtra');
                            $Percepciones_HorasExtra = $Percepcion->appendChild($Percepciones_HorasExtra);

                            $Percepciones_HorasExtra->setAttribute('Dias', trim($dataHoras['Dias']));
                            $Percepciones_HorasExtra->setAttribute('TipoHoras', trim($dataHoras['TipoHoras']));
                            $Percepciones_HorasExtra->setAttribute('HorasExtra', trim($dataHoras['HorasExtra']));
                            $Percepciones_HorasExtra->setAttribute('ImportePagado', trim($dataHoras['ImportePagado']));
                        }
                    }
                }
            }


            if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['IngresoAcumulable'])) > 0) {

                $Percepcion_JubilacionPensionRetiro = $doc->createElement('nomina12:JubilacionPensionRetiro');
                $Percepcion_JubilacionPensionRetiro = $Percepciones->appendChild($Percepcion_JubilacionPensionRetiro);

                if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['TotalUnaExhibicion'])) > 0) {
                    $Percepcion_JubilacionPensionRetiro->setAttribute('TotalUnaExhibicion', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['TotalUnaExhibicion']));
                }
                if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['TotalParcialidad'])) > 0) {
                    $Percepcion_JubilacionPensionRetiro->setAttribute('TotalParcialidad', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['TotalParcialidad']));
                }
                if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['MontoDiario'])) > 0) {
                    $Percepcion_JubilacionPensionRetiro->setAttribute('MontoDiario', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['MontoDiario']));
                }
                if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['IngresoAcumulable'])) > 0) {
                    $Percepcion_JubilacionPensionRetiro->setAttribute('IngresoAcumulable', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['IngresoAcumulable']));
                }
                if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['IngresoNoAcumulable'])) > 0) {
                    $Percepcion_JubilacionPensionRetiro->setAttribute('IngresoNoAcumulable', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['JubilacionPensionRetiro']['IngresoNoAcumulable']));
                }
            }

            if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['TotalPagado'])) > 0) {

                $Percepcion_SeparacionIndemnizacion = $doc->createElement('nomina12:SeparacionIndemnizacion');
                $Percepcion_SeparacionIndemnizacion = $Percepciones->appendChild($Percepcion_SeparacionIndemnizacion);

                if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['TotalPagado'])) > 0)
                    $Percepcion_SeparacionIndemnizacion->setAttribute('TotalPagado', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['TotalPagado']));
                if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['NumAñosServicio'])) > 0)
                    $Percepcion_SeparacionIndemnizacion->setAttribute('NumAñosServicio', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['NumAñosServicio']));
                if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['UltimoSueldoMensOrd'])) > 0)
                    $Percepcion_SeparacionIndemnizacion->setAttribute('UltimoSueldoMensOrd', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['UltimoSueldoMensOrd']));
                if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['IngresoAcumulable'])) > 0)
                    $Percepcion_SeparacionIndemnizacion->setAttribute('IngresoAcumulable', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['IngresoAcumulable']));
                if (strlen(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['IngresoNoAcumulable'])) > 0)
                    $Percepcion_SeparacionIndemnizacion->setAttribute('IngresoNoAcumulable', trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['SeparacionIndemnizacion']['IngresoNoAcumulable']));
            }


            if (number_format(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalExento']), 0) != number_format($varTotalImporteExento, 0)) {
                //$nominaValidador = false;
                //$errorNomina .= "El total de PERCEPCIONES EXENTO no coincide con su detalle : " .
                trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalExento']) . " != " . $varTotalImporteExento;
            }

            if (number_format(trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalGravado']), 0) != number_format($varTotalImporteGravado, 0)) {
                // $nominaValidador = false;
                //$errorNomina .= "El total de PERCEPCIONES GRAVADO no coincide con su detalle : " .
                trim($request['Comprobante']['Complemento']['Nomina']['Percepciones']['TotalGravado']) . " != " . $varTotalImporteGravado;
            }
        }


        if (isset($request['Comprobante']['Complemento']['Nomina']['Deducciones']['Deduccion'][0])) {
            $Deducciones = $doc->createElement('nomina12:Deducciones');
            $Deducciones = $Nomina->appendChild($Deducciones);
            $Deducciones->setAttribute('TotalOtrasDeducciones', trim($request['Comprobante']['Complemento']['Nomina']['Deducciones']['TotalOtrasDeducciones']));
            if (isset($request['Comprobante']['Complemento']['Nomina']['Deducciones']['TotalImpuestosRetenidos'])) {
                if ($request['Comprobante']['Complemento']['Nomina']['Deducciones']['TotalImpuestosRetenidos'] > 0)
                    $Deducciones->setAttribute('TotalImpuestosRetenidos', trim($request['Comprobante']['Complemento']['Nomina']['Deducciones']['TotalImpuestosRetenidos']));
            }
            $varTotalImporteGravado = 0;
            $varTotalImporteExento = 0;

            foreach ($request['Comprobante']['Complemento']['Nomina']['Deducciones']['Deduccion'] as $data) {
                $Deduccion = $doc->createElement('nomina12:Deduccion');
                $Deduccion = $Deducciones->appendChild($Deduccion);

                $data['TipoDeduccion'] = trim($data['TipoDeduccion']);
                while (strlen($data['TipoDeduccion']) < 3) {
                    $data['TipoDeduccion'] = "0" . $data['TipoDeduccion'];
                }

                while (strlen($data['Clave']) < 3) {
                    $data['Clave'] = "0" . $data['Clave'];
                }

                $Deduccion->setAttribute('TipoDeduccion', trim($data['TipoDeduccion']));
                $Deduccion->setAttribute('Clave', trim($data['Clave']));
                $Deduccion->setAttribute('Concepto', utf8_decode(trim($data['Concepto'])));
                $Deduccion->setAttribute('Importe', trim($data['Importe']));
            }
        }


        if (isset($request['Comprobante']['Complemento']['Nomina']['OtrosPagos'][0])) {

            $OtroPagos = $doc->createElement('nomina12:OtrosPagos');
            $OtroPagos = $Nomina->appendChild($OtroPagos);


            foreach ($request['Comprobante']['Complemento']['Nomina']['OtrosPagos'] as $data) {

                $OtroPago = $doc->createElement('nomina12:OtroPago');
                $OtroPago = $OtroPagos->appendChild($OtroPago);

                $OtroPago->setAttribute('TipoOtroPago', $data['TipoOtroPago']);
                $OtroPago->setAttribute('Clave', trim($data['Clave']));
                $OtroPago->setAttribute('Concepto', trim($data['Concepto']));
                $OtroPago->setAttribute('Importe', trim($data['Importe']));


                if (strlen(trim($data['SubsidioAlEmpleo']['SubsidioCausado'])) > 0) {
//                            echo json_encode(array('status' => 0, 'error_code' => 400, 'message' => ['resultado' => 'false', 'error' => $data['SubsidioAlEmpleo']['SubsidioCausado']]), JSON_PRETTY_PRINT);
//                            exit;
                    $SubsidioAlEmpleo = $doc->createElement('nomina12:SubsidioAlEmpleo');
                    $SubsidioAlEmpleo = $OtroPago->appendChild($SubsidioAlEmpleo);

                    $SubsidioAlEmpleo->setAttribute('SubsidioCausado', $data['SubsidioAlEmpleo']['SubsidioCausado']);
                }

                if (isset($data['CompensacionSaldosAFavor'])) {

                    if (
                            strlen(trim($data['CompensacionSaldosAFavor']['SaldoAFavor'])) > 0 && strlen(trim($data['CompensacionSaldosAFavor']['Año'])) > 0 && strlen(trim($data['CompensacionSaldosAFavor']['RemanenteSalFav'])) > 0
                    ) {

                        $CompensacionSaldosAFavor = $doc->createElement('nomina12:CompensacionSaldosAFavor');
                        $CompensacionSaldosAFavor = $OtroPago->appendChild($CompensacionSaldosAFavor);

                        $CompensacionSaldosAFavor->setAttribute('SaldoAFavor', $data['CompensacionSaldosAFavor']['SaldoAFavor']);
                        $CompensacionSaldosAFavor->setAttribute('Año', $data['CompensacionSaldosAFavor']['Año']);
                        $CompensacionSaldosAFavor->setAttribute('RemanenteSalFav', $data['CompensacionSaldosAFavor']['RemanenteSalFav']);
                    }
                }
            }
        }
        if(isset($request['Comprobante']['Complemento']['Nomina']['Incapacidades']['Incapacidad']))
        if (count($request['Comprobante']['Complemento']['Nomina']['Incapacidades']['Incapacidad']) > 0) {

            $Incapacidades = $doc->createElement('nomina12:Incapacidades');
            $Incapacidades = $Nomina->appendChild($Incapacidades);

            foreach ($request['Comprobante']['Complemento']['Nomina']['Incapacidades']['Incapacidad'] as $data) {

                $Incapacidad = $doc->createElement('nomina12:Incapacidad');
                $Incapacidad = $Incapacidades->appendChild($Incapacidad);

                $Incapacidad->setAttribute('DiasIncapacidad', trim($data['DiasIncapacidad']));
                $Incapacidad->setAttribute('TipoIncapacidad', trim($data['TipoIncapacidad']));
                $Incapacidad->setAttribute('ImporteMonetario', trim($data['ImporteMonetario']));
            }
        }

        return $doc;
    }

}
