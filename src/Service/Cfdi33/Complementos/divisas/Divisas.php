<?php

namespace App\Service\Cfdi33\Complementos\divisas;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of GenerarXML
 *
 * @author Miguel Solis <miguel.solis@clickfactura.mx>
 */
class Divisas extends Complemento
{
    /**
     * Divisas constructor.
     */
    public function __construct()
    {
    }


    /**
     * Funcion para agregar el complemento de donatarias11
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request)
    {


        if (!self::existeComplemento($request)) {
            return $doc;
        }

        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        $Complemento = $doc->createElement('divisas:Divisas');
        $Complemento = $ComplementoNode->appendChild($Complemento);
        $Complemento->setAttribute('version', trim($request['Comprobante']['Complemento']['Divisas']['version']));
        $Complemento->setAttribute('tipoOperacion', trim($request['Comprobante']['Complemento']['Divisas']['tipoOperacion']));


        return $doc;


    }

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request)
    {

        if (!isset($request['Comprobante']['Complemento']['Divisas'])) {
            return false;
        }

        if (strlen($request['Comprobante']['Complemento']['Divisas']['version']) <= 0 || strlen($request['Comprobante']['Complemento']['Divisas']['tipoOperacion']) <= 0) {
            return false;
        }

        return true;
    }
}