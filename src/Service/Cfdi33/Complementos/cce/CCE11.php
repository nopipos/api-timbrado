<?php
/**
 * Created by PhpStorm.
 * User: Juan Sotero
 * Date: 17/04/2017
 * Time: 11:05 AM
 */

namespace App\Service\Cfdi33\Complementos\cce;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of GenerarXML
 *
 * @author Miguel Solis <miguel.solis@clickfactura.mx>
 */
class CCE11 extends Complemento
{

    public function CCE11()
    {

    }


    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request)
    {
        if (!isset($request['Comprobante']['Complemento']['CCE'])) {
            return false;
        }

        return true;
    }

    /**
     * Funcion para agregar el complemento de nomina12
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request)
    {


        if (!self::existeComplemento($request)) {
            return $doc;
        }


        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);


        $ComercioExterior = $doc->createElement('cce11:ComercioExterior');
        $ComercioExterior = $ComplementoNode->appendChild($ComercioExterior);


        $ComercioExterior->setAttribute("Version", "1.1");

        if (strlen($request['Comprobante']['Complemento']['CCE']['MotivoTraslado']) > 0) {
            $ComercioExterior->setAttribute("MotivoTraslado", $request['Comprobante']['Complemento']['CCE']['MotivoTraslado']);
        }

        $ComercioExterior->setAttribute("TipoOperacion", $request['Comprobante']['Complemento']['CCE']['TipoOperacion']);

        if (strlen($request['Comprobante']['Complemento']['CCE']['ClaveDePedimento']) > 0) {
            $ComercioExterior->setAttribute("ClaveDePedimento", $request['Comprobante']['Complemento']['CCE']['ClaveDePedimento']);
        }

        if (strlen($request['Comprobante']['Complemento']['CCE']['CertificadoOrigen']) > 0) {
            $ComercioExterior->setAttribute("CertificadoOrigen", $request['Comprobante']['Complemento']['CCE']['CertificadoOrigen']);
        }

        if (strlen($request['Comprobante']['Complemento']['CCE']['NumCertificadoOrigen']) > 0) {
            $ComercioExterior->setAttribute("NumCertificadoOrigen", $request['Comprobante']['Complemento']['CCE']['NumCertificadoOrigen']);
        }

        if (strlen($request['Comprobante']['Complemento']['CCE']['NumeroExportadorConfiable']) > 0) {
            $ComercioExterior->setAttribute("NumeroExportadorConfiable", $request['Comprobante']['Complemento']['CCE']['NumeroExportadorConfiable']);
        }

        if (strlen($request['Comprobante']['Complemento']['CCE']['Incoterm']) > 0) {
            $ComercioExterior->setAttribute("Incoterm", $request['Comprobante']['Complemento']['CCE']['Incoterm']);
        }

        if (strlen($request['Comprobante']['Complemento']['CCE']['Subdivision']) > 0) {
            $ComercioExterior->setAttribute("Subdivision", $request['Comprobante']['Complemento']['CCE']['Subdivision']);
        }

        if (strlen($request['Comprobante']['Complemento']['CCE']['Observaciones']) > 0) {
            $ComercioExterior->setAttribute("Observaciones", $request['Comprobante']['Complemento']['CCE']['Observaciones']);
        }

        if (strlen($request['Comprobante']['Complemento']['CCE']['TipoCambioUSD']) > 0) {
            $ComercioExterior->setAttribute("TipoCambioUSD", $request['Comprobante']['Complemento']['CCE']['TipoCambioUSD']);
        }

        if (strlen($request['Comprobante']['Complemento']['CCE']['TotalUSD']) > 0) {
            $ComercioExterior->setAttribute("TotalUSD", $request['Comprobante']['Complemento']['CCE']['TotalUSD']);
        }

        #seccion emisor
        if (strlen($request['Comprobante']['Complemento']['CCE']['Emisor']['Curp']) > 0 ||
            strlen($request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Calle']) > 0
        ) {
            $emisor = $doc->createElement('cce11:Emisor');
            $emisor = $ComercioExterior->appendChild($emisor);


            if (strlen($request['Comprobante']['Complemento']['CCE']['Emisor']['Curp']) > 0) {

                $emisor->setAttribute("Curp", $request['Comprobante']['Complemento']['CCE']['Emisor']['Curp']);
            }


            if (strlen($request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Calle']) > 0) {

                $emisorDomicilio = $doc->createElement('cce11:Domicilio');
                $emisorDomicilio = $emisor->appendChild($emisorDomicilio);


                $emisorDomicilio->setAttribute("Calle", $request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Calle']);


                if (strlen($request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['NumeroExterior']) > 0) {
                    $emisorDomicilio->setAttribute("NumeroExterior", $request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['NumeroExterior']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['NumeroInterior']) > 0) {
                    $emisorDomicilio->setAttribute("NumeroInterior", $request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['NumeroInterior']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Colonia']) > 0) {
                    $emisorDomicilio->setAttribute("Colonia", $request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Colonia']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Localidad']) > 0) {
                    $emisorDomicilio->setAttribute("Localidad", $request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Localidad']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Referencia']) > 0) {
                    $emisorDomicilio->setAttribute("Referencia", $request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Referencia']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Municipio']) > 0) {
                    $emisorDomicilio->setAttribute("Municipio", $request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Municipio']);
                }


                $emisorDomicilio->setAttribute("Estado", $request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Estado']);

                $emisorDomicilio->setAttribute("Pais", $request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['Pais']);

                $emisorDomicilio->setAttribute("CodigoPostal", $request['Comprobante']['Complemento']['CCE']['Emisor']['Domicilio']['CodigoPostal']);

            }
        }
        #seccion emisor

        #Propietario
        if (count($request['Comprobante']['Complemento']['CCE']['Propietario']) > 0) {

            for ($i = 0; $i < count($request['Comprobante']['Complemento']['CCE']['Propietario']); $i++) {

                if (strlen($request['Comprobante']['Complemento']['CCE']['Propietario'][$i]['NumRegIdTrib']) > 0 &&
                    strlen($request['Comprobante']['Complemento']['CCE']['Propietario'][$i]['ResidenciaFiscal']) > 0
                ) {


                    $propietario = $doc->createElement('cce11:Propietario');
                    $propietario = $ComercioExterior->appendChild($propietario);

                    $propietario->setAttribute("NumRegIdTrib", $request['Comprobante']['Complemento']['CCE']['Propietario'][$i]['NumRegIdTrib']);
                    $propietario->setAttribute("ResidenciaFiscal", $request['Comprobante']['Complemento']['CCE']['Propietario'][$i]['ResidenciaFiscal']);
                }

            }
        }
        #Propietario

        #seccion receptor
        if (strlen($request['Comprobante']['Complemento']['CCE']['Receptor']['NumRegIdTrib']) > 0 ||
            strlen($request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Calle']) > 0
        ) {
            $receptor = $doc->createElement('cce11:Receptor');
            $receptor = $ComercioExterior->appendChild($receptor);


            if (strlen($request['Comprobante']['Complemento']['CCE']['Receptor']['NumRegIdTrib']) > 0) {
                $receptor->setAttribute("NumRegIdTrib", $request['Comprobante']['Complemento']['CCE']['Receptor']['NumRegIdTrib']);
            }


            if (strlen($request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Calle']) > 0) {

                $receptorDomicilio = $doc->createElement('cce11:Domicilio');
                $receptorDomicilio = $receptor->appendChild($receptorDomicilio);


                $receptorDomicilio->setAttribute("Calle", $request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Calle']);


                if (strlen($request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['NumeroExterior']) > 0) {
                    $receptorDomicilio->setAttribute("NumeroExterior", $request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['NumeroExterior']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['NumeroInterior']) > 0) {
                    $receptorDomicilio->setAttribute("NumeroInterior", $request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['NumeroInterior']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Colonia']) > 0) {
                    $receptorDomicilio->setAttribute("Colonia", $request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Colonia']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Localidad']) > 0) {
                    $receptorDomicilio->setAttribute("Localidad", $request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Localidad']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Referencia']) > 0) {
                    $receptorDomicilio->setAttribute("Referencia", $request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Referencia']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Municipio']) > 0) {
                    $receptorDomicilio->setAttribute("Municipio", $request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Municipio']);
                }


                $receptorDomicilio->setAttribute("Estado", $request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Estado']);

                $receptorDomicilio->setAttribute("Pais", $request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['Pais']);

                $receptorDomicilio->setAttribute("CodigoPostal", $request['Comprobante']['Complemento']['CCE']['Receptor']['Domicilio']['CodigoPostal']);

            }
        }
        #seccion receptor

        #Destinatario
        if ((count($request['Comprobante']['Complemento']['CCE']['Destinatario']) > 0 &&
                (strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][0]['NumRegIdTrib']) > 0 || strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][0]['Nombre']) > 0)) ||

            (count($request['Comprobante']['Complemento']['CCE']['Destinatario'][0]['Domicilio']) > 0 && strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][0]['Domicilio'][0]['Calle']) > 0)
        ) {


            for ($i = 0; $i < count($request['Comprobante']['Complemento']['CCE']['Destinatario']); $i++) {

                $destinatario = $doc->createElement('cce11:Destinatario');
                $destinatario = $ComercioExterior->appendChild($destinatario);

                if (strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['NumRegIdTrib']) > 0) {
                    $destinatario->setAttribute("NumRegIdTrib", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['NumRegIdTrib']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Nombre']) > 0) {
                    $destinatario->setAttribute("Nombre", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Nombre']);
                }

                if(!isset($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][0])){

                    $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][0] = $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'];
                }

                #domicilio
                for ($j = 0; $j < count($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio']); $j++) {

                    if (strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Calle']) > 0) {

                        $domicilio = $doc->createElement('cce11:Domicilio');
                        $domicilio = $destinatario->appendChild($domicilio);

                        $domicilio->setAttribute("Calle", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Calle']);

                        if (strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['NumeroExterior']) > 0) {
                            $domicilio->setAttribute("NumeroExterior", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['NumeroExterior']);
                        }

                        if (strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['NumeroInterior']) > 0) {
                            $domicilio->setAttribute("NumeroInterior", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['NumeroInterior']);
                        }

                        if (strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Colonia']) > 0) {
                            $domicilio->setAttribute("Colonia", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Colonia']);
                        }

                        if (strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Localidad']) > 0) {
                            $domicilio->setAttribute("Localidad", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Localidad']);
                        }

                        if (strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Referencia']) > 0) {
                            $domicilio->setAttribute("Referencia", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Referencia']);
                        }

                        if (strlen($request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Municipio']) > 0) {
                            $domicilio->setAttribute("Municipio", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Municipio']);
                        }


                        $domicilio->setAttribute("Estado", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Estado']);

                        $domicilio->setAttribute("Pais", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['Pais']);

                        $domicilio->setAttribute("CodigoPostal", $request['Comprobante']['Complemento']['CCE']['Destinatario'][$i]['Domicilio'][$j]['CodigoPostal']);

                    }
                }

            }
        }
        #Destinatario


        #Mercancias
        if (count($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia']) > 0 && strlen($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][0]['NoIdentificacion']) > 0) {

            $mercancias = $doc->createElement('cce11:Mercancias');
            $mercancias = $ComercioExterior->appendChild($mercancias);

            for ($i = 0; $i < count($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia']); $i++) {

                $mercancia = $doc->createElement('cce11:Mercancia');
                $mercancia = $mercancias->appendChild($mercancia);

                $mercancia->setAttribute("NoIdentificacion", $request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['NoIdentificacion']);

                if (strlen($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['FraccionArancelaria']) > 0) {
                    $mercancia->setAttribute("FraccionArancelaria", $request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['FraccionArancelaria']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['CantidadAduana']) > 0) {
                    $mercancia->setAttribute("CantidadAduana", $request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['CantidadAduana']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['UnidadAduana']) > 0) {
                    $mercancia->setAttribute("UnidadAduana", $request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['UnidadAduana']);
                }

                if (strlen($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['ValorUnitarioAduana']) > 0) {
                    $mercancia->setAttribute("ValorUnitarioAduana", $request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['ValorUnitarioAduana']);
                }

                $mercancia->setAttribute("ValorDolares", $request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['ValorDolares']);

                if (count($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['DescripcionesEspecificas']) > 0 &&
                    strlen($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['DescripcionesEspecificas'][0]['Marca']) > 0
                ) {

                    for ($j = 0; $j < count($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['DescripcionesEspecificas']); $j++) {

                        $descripcionesEspecificas = $doc->createElement('cce11:DescripcionesEspecificas');
                        $descripcionesEspecificas  = $mercancia->appendChild($descripcionesEspecificas );

                        $descripcionesEspecificas->setAttribute("Marca", $request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['DescripcionesEspecificas'][$j]['Marca']);

                        if (strlen($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['DescripcionesEspecificas'][$j]['Modelo']) > 0) {
                            $descripcionesEspecificas->setAttribute("Modelo", $request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['DescripcionesEspecificas'][$j]['Modelo']);
                        }
                        if (strlen($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['DescripcionesEspecificas'][$j]['SubModelo']) > 0) {
                            $descripcionesEspecificas->setAttribute("SubModelo", $request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['DescripcionesEspecificas'][$j]['SubModelo']);
                        }
                        if (strlen($request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['DescripcionesEspecificas'][$j]['NumeroSerie']) > 0) {
                            $descripcionesEspecificas->setAttribute("NumeroSerie", $request['Comprobante']['Complemento']['CCE']['Mercancias']['Mercancia'][$i]['DescripcionesEspecificas'][$j]['NumeroSerie']);
                        }

                    }

                }

            }
            #Mercancias


        }


        return $doc;

    }
}