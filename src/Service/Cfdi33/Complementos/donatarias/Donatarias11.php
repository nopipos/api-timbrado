<?php

namespace App\Service\Cfdi33\Complementos\donatarias;

use \DomDocument;
use App\Service\Cfdi33\Complementos\Complemento;

/**
 * Description of GenerarXML
 *
 * @author Miguel Solis <miguel.solis@clickfactura.mx>
 */
class Donatarias11 extends Complemento
{

    public function Donatarias11()
    {

    }

    /**
     * Funcion para agregar el complemento de donatarias11
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    public static function agregarComplemento($doc, $ComprobanteNode, $request)
    {


        if (!self::existeComplemento($request)) {
            return $doc;
        }

        $ComplementoNode = parent::getComplemento($doc, $ComprobanteNode);

        $ComplementosDonatarias = $doc->createElement('donat:Donatarias');
        $ComplementosDonatarias = $ComplementoNode->appendChild($ComplementosDonatarias);
        $ComplementosDonatarias->setAttribute('version', trim($request['Comprobante']['Complemento']['Donatarias']['version']));
        $ComplementosDonatarias->setAttribute('noAutorizacion', trim($request['Comprobante']['Complemento']['Donatarias']['noAutorizacion']));
        $ComplementosDonatarias->setAttribute('fechaAutorizacion', trim($request['Comprobante']['Complemento']['Donatarias']['fechaAutorizacion']));
        $ComplementosDonatarias->setAttribute('leyenda', trim($request['Comprobante']['Complemento']['Donatarias']['leyenda']));


        return $doc;


    }

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    public static function existeComplemento($request)
    {

        if (!isset($request['Comprobante']['Complemento']['Donatarias'])) {
            return false;
        }

        if (!isset($request['Comprobante']['Complemento']['Donatarias']['noAutorizacion']) || strlen($request['Comprobante']['Complemento']['Donatarias']['noAutorizacion']) < 3) {
            return false;
        }

        return true;
    }
}