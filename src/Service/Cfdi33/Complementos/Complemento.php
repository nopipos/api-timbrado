<?php

namespace App\Service\Cfdi33\Complementos;

use \DomDocument;
use \DOMXPath;

/**
 * Description of GenerarXML
 *
 * @author Miguel Solis <miguel.solis@clickfactura.mx>
 */
abstract class Complemento
{

    public function Complemento()
    {
    }

    /**
     * Funcion para agregar el node complemento si existe lo tomna, si no existe lo agrega al xml
     *
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante
     * @return  node del complemento
     */
    protected static function getComplemento($doc, $ComprobanteNode)
    {

        $dom = new DOMDocument;
        $dom->loadXml($doc->saveXML());

        $xpath = new DOMXPath($dom);
        $xpath->registerNamespace('ns1', 'http://www.sat.gob.mx/cfd/3');
        $successNodeExists = $xpath->evaluate('boolean(//ns1:Comprobante/ns1:Complemento)');

        $ComplementoNode = null;


        if (!$successNodeExists) {
            $ComplementoNode = $doc->createElement('cfdi:Complemento');
            $ComplementoNode = $ComprobanteNode->appendChild($ComplementoNode);

        } else {
            $ComplementoNode = $ComprobanteNode->getElementsByTagName("cfdi:Complemento")->item(0);

        }

        return $ComplementoNode;


    }


    /**
     * @param $doc DomDocument de la creacion del xml
     * @param $ComprobanteNode nodo del domDocument del cdfi:comprobante, solo para extraer el nodo complemento
     * @param $request request de con los datos a timbrar
     * @return  $doc (DomDocument) con el complemento agregado
     */
    protected abstract static function agregarComplemento($doc, $ComprobanteNode, $request);

    /**
     * Funcion para determinar si existe el complemento
     * @param $request
     * @return true || false
     */
    protected abstract static function existeComplemento($request);
}