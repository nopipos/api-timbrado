<?php


namespace App\Service\Cfdi33;

use App\Entity\SAT\CatCp;
use App\Service\Cfdi33\Complementos\artes\Artes10;
use App\Service\Cfdi33\Complementos\cartaPorte\CP10;
use App\Service\Cfdi33\Complementos\cce\CCE11;
use App\Service\Cfdi33\Complementos\detallista\Detallista;
use App\Service\Cfdi33\Complementos\divisas\Divisas;
use App\Service\Cfdi33\Complementos\donatarias\Donatarias11;
use App\Service\Cfdi33\Complementos\impuestosLocales\ImpuestosLocales11;
use App\Service\Cfdi33\Complementos\ine\Ine11;
use App\Service\Cfdi33\Complementos\leyendasFiscales\LeyendasFiscales;
use App\Service\Cfdi33\Complementos\nomina\Nomina12;
use App\Service\Cfdi33\Complementos\notaria\Notaria10;
use App\Service\Cfdi33\Complementos\pagos\Pagos10;
use App\Service\Cfdi33\Complementos\serviciosParciales\ServiciosParciales10;
use App\Service\Utils\XML2Array;
use Doctrine\Persistence\ManagerRegistry;
use \DomDocument;
use SimpleXMLElement;
use \XSLTProcessor;


class Xml33
{

    /**
     *
     * Funcion para armar el xmlCfdi33a partir de un formato json
     *
     * @param $datos La estructura de datos que envia el cliente
     * @return String El string xml SIN codificar
     */
    public function generarXml($datos, ManagerRegistry $doctrine){
        error_reporting(E_ERROR | E_WARNING | E_PARSE);
        $doc = new DomDocument('1.0', 'utf-8');

        $cfdiComprobante = $doc->createElement('cfdi:Comprobante');
        $cfdiComprobante = $doc->appendChild($cfdiComprobante);

        $cfdiComprobante->setAttribute('xmlns:cfdi', 'http://www.sat.gob.mx/cfd/3');
        $cfdiComprobante->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $cfdiComprobante->setAttribute('xmlns:tfd', 'http://www.sat.gob.mx/TimbreFiscalDigital');

        $cfdiComprobante = Xsd::addXsd($cfdiComprobante,$datos);

        $fechaSinT = explode(" ", $datos['Comprobante']['Fecha']);
        $fechaComprobante = count($fechaSinT) > 1 ? $fechaSinT[0] . "T" . $fechaSinT[1] : $datos['Comprobante']['Fecha'];

        $fechaComprobanteTemp = str_replace("T"," ",$fechaComprobante);

        #saco el CP del lugar de expedicion y verifico su zona horaria.
        $CodigoPostal = $datos['Comprobante']['LugarExpedicion'];


        $CpModel = $doctrine->getRepository(CatCp::class);

        $cpModel = $CpModel->findOneBy(['codigoPostal'=>$CodigoPostal]);

        #hago el cambio de hora segun las zonas
        if($cpModel != null){

            switch ($cpModel->getEstado()){

                #Zona Noroeste  (se restan 2 horas de la hora del centro)
                #Baja California
                case "BCN":
                    $nuevafecha = strtotime('-2 hour' , strtotime($fechaComprobanteTemp ));

                    break;

                /*
            #Zona Sureste  (se le suma una hora de la hora del centro)
               #Quintana Roo
            case "ROO":
                $nuevafecha = strtotime('+1 hour' , strtotime($fechaComprobanteTemp ));
            break;
                */

                #Zona Pacífico   (se restan 1 horas de la hora del centro)
                #Baja California Sur
                case "BCS":
                    $nuevafecha = strtotime('-1 hour' , strtotime($fechaComprobanteTemp ));
                    break;
                #Chihuahua
                case "CHH":
                    $nuevafecha = strtotime('-1 hour' , strtotime($fechaComprobanteTemp ));
                    break;
                #Nayarit
                case "NAY":
                    $nuevafecha = strtotime('-1 hour' , strtotime($fechaComprobanteTemp ));
                    break;
                #Sinaloa
                case "SIN":
                    $nuevafecha = strtotime('-1 hour' , strtotime($fechaComprobanteTemp ));
                    break;
                #Sonora
                case "SON":
                    $nuevafecha = strtotime('-2 hour' , strtotime($fechaComprobanteTemp ));
                    break;

                default:
                    $nuevafecha =strtotime($fechaComprobanteTemp);

            }

            $fechaComprobante = date ( 'Y-m-d H:i:s' , $nuevafecha );
            $fechaComprobante = str_replace(" ","T",$fechaComprobante);

        }

        $cfdiComprobante->setAttribute('Version', '3.3');
        
        if (strlen(trim($datos['Comprobante']['Serie'])) > 0) {
            $cfdiComprobante->setAttribute('Serie', trim($datos['Comprobante']['Serie']));
        }

        if (strlen(trim($datos['Comprobante']['Folio'])) > 0) {
            $cfdiComprobante->setAttribute('Folio', trim($datos['Comprobante']['Folio']));
        }

        #========= PENDIENTE LA LOGICA PARA ASIGNAR FOLIO =========#

        $cfdiComprobante->setAttribute('Fecha', trim($fechaComprobante));

        if(isset($datos['Comprobante']['Sello']))
            $cfdiComprobante->setAttribute('Sello', $datos['Comprobante']['Sello']);
        if(isset($datos['Comprobante']['NoCertificado']))
            $cfdiComprobante->setAttribute('NoCertificado', $datos['Comprobante']['NoCertificado']);
        if(isset($datos['Comprobante']['Certificado']))
            $cfdiComprobante->setAttribute('Certificado', $datos['Comprobante']['Certificado']);

        if (strlen(trim($datos['Comprobante']['FormaPago'])) > 0) {
            $cfdiComprobante->setAttribute('FormaPago', utf8_decode(trim($datos['Comprobante']['FormaPago']))); //CATALOGO
        }


        if (strlen(trim($datos['Comprobante']['CondicionesDePago'])) > 0) {
            $cfdiComprobante->setAttribute('CondicionesDePago', trim($datos['Comprobante']['CondicionesDePago']));
        }

        $cfdiComprobante->setAttribute('SubTotal', trim($datos['Comprobante']['SubTotal']));

        if (strlen(trim($datos['Comprobante']['Descuento'])) > 0) {
            if($datos['Comprobante']['Descuento'] > 0)
                $cfdiComprobante->setAttribute('Descuento', trim($datos['Comprobante']['Descuento']));
        }

        $cfdiComprobante->setAttribute('Moneda', trim($datos['Comprobante']['Moneda'])); //CATALOGO

        if (strlen(trim($datos['Comprobante']['TipoCambio'])) > 0) {
            $cfdiComprobante->setAttribute('TipoCambio', trim($datos['Comprobante']['TipoCambio'])); //CATALOGO, VALIDACION
        }

        $cfdiComprobante->setAttribute('Total', trim($datos['Comprobante']['Total'])); //VALIDACION

        $cfdiComprobante->setAttribute('TipoDeComprobante', trim($datos['Comprobante']['TipoDeComprobante'])); //CATALOGO

        if (strlen(trim($datos['Comprobante']['MetodoPago'])) > 0) {
            $cfdiComprobante->setAttribute('MetodoPago', trim($datos['Comprobante']['MetodoPago'])); //CATALOGO
        }

        $cfdiComprobante->setAttribute('LugarExpedicion', trim($datos['Comprobante']['LugarExpedicion'])); //CATALOGO CODIGO POSTAL

        if (isset($datos['Comprobante']['Confirmacion']) && strlen(trim($datos['Comprobante']['Confirmacion'])) > 0) {
            $cfdiComprobante->setAttribute('Confirmacion', trim($datos['Comprobante']['LugarExpedicion'])); //MAX LENGTH 5
        }


        self::addCfdisRelacionados($doc, $cfdiComprobante, $datos);
        self::addEmisor($doc, $cfdiComprobante, $datos);
        self::addReceptor($doc, $cfdiComprobante, $datos);
        self::addConceptos($doc, $cfdiComprobante, $datos);
        self::addImpuestos($doc, $cfdiComprobante, $datos);
        self::addComplementos($doc, $cfdiComprobante, $datos);

        $xml = $doc->saveXML();


        return $xml;


    }

    public function addComplementos($doc, $cfdiComprobante, $request) {


        if (isset($request['Comprobante']['Complemento'])) {

            if(Nomina12::existeComplemento($request)){
                $doc =Nomina12::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(ImpuestosLocales11::existeComplemento($request)){
                $doc = ImpuestosLocales11::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(Donatarias11::existeComplemento($request)){
                $doc = Donatarias11::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(CCE11::existeComplemento($request)){
                $doc = CCE11::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(Pagos10::existeComplemento($request)){
                $doc = Pagos10::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(Ine11::existeComplemento($request)){
                $doc = Ine11::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(Detallista::existeComplemento($request)){
                $doc = Detallista::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(Divisas::existeComplemento($request)){
                $doc = Divisas::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(ServiciosParciales10::existeComplemento($request)){
                $doc = ServiciosParciales10::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(Notaria10::existeComplemento($request)){
                $doc = Notaria10::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(LeyendasFiscales::existeComplemento($request)){
                $doc = LeyendasFiscales::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(CP10::existeComplemento($request)){
                $doc = CP10::agregarComplemento($doc,$cfdiComprobante,$request);
            }

            if(Artes10::existeComplemento($request)){
                $doc = Artes10::agregarComplemento($doc,$cfdiComprobante,$request);
            }

        }


    }


        /**
     * @param $doc
     * @param $cfdiComprobante
     * @param $request
     */
    public function addCfdisRelacionados($doc, $cfdiComprobante, $request) {

        if (isset($request['Comprobante']['CfdiRelacionados']['TipoRelacion']) && strlen(trim($request['Comprobante']['CfdiRelacionados']['TipoRelacion'])) > 0) {

            $CfdiRelacionados = $doc->createElement('cfdi:CfdiRelacionados');
            $CfdiRelacionados = $cfdiComprobante->appendChild($CfdiRelacionados);

            $CfdiRelacionados->setAttribute('TipoRelacion', trim($request['Comprobante']['CfdiRelacionados']['TipoRelacion'])); //CATALOGO

            for ($i = 0; $i < count($request['Comprobante']['CfdiRelacionados']['CfdiRelacionado']); $i++) {

                $CfdiRelacionado = $doc->createElement('cfdi:CfdiRelacionado');
                $CfdiRelacionado = $CfdiRelacionados->appendChild($CfdiRelacionado);

                $CfdiRelacionado->setAttribute('UUID', trim($request['Comprobante']['CfdiRelacionados']['CfdiRelacionado'][$i]['UUID']));
            }
        }
    }

    /**
     * @param $doc
     * @param $cfdiComprobante
     * @param $request
     */
    public function addEmisor($doc, $cfdiComprobante, $request) {
        $Emisor = $doc->createElement('cfdi:Emisor');
        $Emisor = $cfdiComprobante->appendChild($Emisor);

        $Emisor->setAttribute('Rfc', trim($request['Comprobante']['Emisor']['Rfc']));

        if (strlen(trim($request['Comprobante']['Emisor']['Nombre'])) > 0) {
            $Emisor->setAttribute('Nombre', trim($request['Comprobante']['Emisor']['Nombre']));
        }

        $Emisor->setAttribute('RegimenFiscal', trim($request['Comprobante']['Emisor']['RegimenFiscal'])); //CATALOGO
    }

    /**
     * @param $doc
     * @param $cfdiComprobante
     * @param $request
     */
    public function addReceptor($doc, $cfdiComprobante, $request) {
        $Receptor = $doc->createElement('cfdi:Receptor');
        $Receptor = $cfdiComprobante->appendChild($Receptor);

        $Receptor->setAttribute('Rfc', trim($request['Comprobante']['Receptor']['Rfc']));

        if (strlen(trim($request['Comprobante']['Receptor']['Nombre'])) > 0) {
            $Receptor->setAttribute('Nombre', str_replace("|", "", trim($request['Comprobante']['Receptor']['Nombre'])));
        }


        if (strlen(trim($request['Comprobante']['Receptor']['ResidenciaFiscal'])) > 0) {
            $Receptor->setAttribute('ResidenciaFiscal', trim($request['Comprobante']['Receptor']['ResidenciaFiscal'])); //CATALOGO
        }

        if (strlen(trim($request['Comprobante']['Receptor']['NumRegIdTrib'])) > 0) {
            $Receptor->setAttribute('NumRegIdTrib', trim($request['Comprobante']['Receptor']['NumRegIdTrib']));
        }

        $Receptor->setAttribute('UsoCFDI', trim($request['Comprobante']['Receptor']['UsoCFDI'])); //CATALOGO
    }

    /**
     * @param $doc
     * @param $cfdiComprobante
     * @param $request
     */
    public function addConceptos($doc, $cfdiComprobante, $request) {
        $Conceptos = $doc->createElement('cfdi:Conceptos');
        $Conceptos = $cfdiComprobante->appendChild($Conceptos);


        for ($i = 0; $i < count($request['Comprobante']['Conceptos']['Concepto']); $i++) {

            if (trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ClaveProdServ']) != "" ||
                trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Cantidad']) != "" ||
                trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ClaveUnidad']) != "" ||
                trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Descripcion']) != "" ||
                trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ValorUnitario']) != "" ||
                trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Importe']) != ""
            ) {

                $Concepto = $doc->createElement('cfdi:Concepto');
                $Concepto = $Conceptos->appendChild($Concepto);

                $Concepto->setAttribute('ClaveProdServ', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ClaveProdServ'])); //CATALOGO

                if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['NoIdentificacion'])) > 0) {
                    $Concepto->setAttribute('NoIdentificacion', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['NoIdentificacion']));
                }

                $Concepto->setAttribute('Cantidad', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Cantidad']));
                $Concepto->setAttribute('ClaveUnidad', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ClaveUnidad'])); //CATALOGO

                if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Unidad'])) > 0) {
                    $Concepto->setAttribute('Unidad', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Unidad']));
                }

                $Concepto->setAttribute('Descripcion', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Descripcion']));
                $Concepto->setAttribute('ValorUnitario', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ValorUnitario']));
                $Concepto->setAttribute('Importe', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Importe']));


                if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Descuento'])) > 0) {
                    if($request['Comprobante']['Conceptos']['Concepto'][$i]['Descuento'] > 0)
                        $Concepto->setAttribute('Descuento', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Descuento']));
                }


                //minimo debe tener un traslado o retencion
                #----------------------- IMPUESTOS
                if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][0]['Base'])) > 0 ||
                    strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Retenciones']['Retencion'][0]['Base'])) > 0
                ) {

                    $ImpuestosCon = $doc->createElement('cfdi:Impuestos');
                    $ImpuestosCon = $Concepto->appendChild($ImpuestosCon);

                    if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][0]['Base'])) > 0) {

                        $trasladosCon = $doc->createElement('cfdi:Traslados');
                        $trasladosCon = $ImpuestosCon->appendChild($trasladosCon);


                        for ($j = 0; $j < count($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado']); $j++) {

                            $trasladoCon = $doc->createElement('cfdi:Traslado');
                            $trasladoCon = $trasladosCon->appendChild($trasladoCon);

                            $trasladoCon->setAttribute('Base', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][$j]['Base'])); //VALIDACION

                            $trasladoCon->setAttribute('Impuesto', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][$j]['Impuesto'])); //CATALOGO

                            $trasladoCon->setAttribute('TipoFactor', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][$j]['TipoFactor'])); //CATALOGO

                            if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][0]['TasaOCuota'])) > 0) {

                                $tasaCuota = $request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][0]['TasaOCuota'];
                                $decimalesTasaCuota = explode(".",$tasaCuota);

                                if(strlen($decimalesTasaCuota[1]) < 6)
                                    $trasladoCon->setAttribute('TasaOCuota', number_format(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][$j]['TasaOCuota']),6,".","")); //CATALOGO
                                else
                                    $trasladoCon->setAttribute('TasaOCuota', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][$j]['TasaOCuota'])); //CATALOGO
                            }

                            if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][0]['Importe'])) > 0) {
                                $trasladoCon->setAttribute('Importe', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Traslados']['Traslado'][$j]['Importe']));
                            }
                        }
                    }

                    if (isset($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Retenciones']['Retencion'][0]['Base']) && strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Retenciones']['Retencion'][0]['Base'])) > 0) {

                        $RetencionesCon = $doc->createElement('cfdi:Retenciones');
                        $RetencionesCon = $ImpuestosCon->appendChild($RetencionesCon);

                        for ($j = 0; $j < count($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Retenciones']['Retencion']); $j++) {

                            $retencionCon = $doc->createElement('cfdi:Retencion');
                            $retencionCon = $RetencionesCon->appendChild($retencionCon);

                            $retencionCon->setAttribute('Base', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Retenciones']['Retencion'][$j]['Base'])); //VALIDACION

                            $retencionCon->setAttribute('Impuesto', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Retenciones']['Retencion'][$j]['Impuesto'])); //CATALOGO

                            $retencionCon->setAttribute('TipoFactor', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Retenciones']['Retencion'][$j]['TipoFactor'])); //CATALOGO

                            $retencionCon->setAttribute('TasaOCuota', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Retenciones']['Retencion'][$j]['TasaOCuota'])); //CATALOGO

                            $retencionCon->setAttribute('Importe', $request['Comprobante']['Conceptos']['Concepto'][$i]['Impuestos']['Retenciones']['Retencion'][$j]['Importe']);
                        }
                    }
                }
                #----------------------- IMPUESTOS
                #----------------------- INFORMACION ADUANERA
                if(isset($request['Comprobante']['Conceptos']['Concepto'][$i]['InformacionAduanera']))
                if (count($request['Comprobante']['Conceptos']['Concepto'][$i]['InformacionAduanera']) > 0) {

                    for ($j = 0; $j < count($request['Comprobante']['Conceptos']['Concepto'][$i]['InformacionAduanera']); $j++) {

                        if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['InformacionAduanera'][$j]['NumeroPedimento'])) > 0) {

                            $InfoAduaneraCon = $doc->createElement('cfdi:InformacionAduanera');
                            $InfoAduaneraCon = $Concepto->appendChild($InfoAduaneraCon);

                            $InfoAduaneraCon->setAttribute('NumeroPedimento', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['InformacionAduanera'][$j]['NumeroPedimento']));
                        }
                    }
                }
                #----------------------- INFORMACION ADUANERA
                #----------------------- CUENTA PREDIAL
                if ( isset($request['Comprobante']['Conceptos']['Concepto'][$i]['CuentaPredial']['Numero']) && strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['CuentaPredial']['Numero'])) > 0) {

                    $InfoAduaneraCon = $doc->createElement('cfdi:CuentaPredial');
                    $InfoAduaneraCon = $Concepto->appendChild($InfoAduaneraCon);

                    $InfoAduaneraCon->setAttribute('Numero', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['CuentaPredial']['Numero']));
                }
                #----------------------- CUENTA PREDIAL
                #----------------------- PARTE
                if (isset($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte']) && count($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte']) > 0) {



                    for ($j = 0; $j < count($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte']); $j++) {

                        $parte = $doc->createElement('cfdi:Parte');
                        $parte = $Concepto->appendChild($parte);

                        $parte->setAttribute('ClaveProdServ', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['ClaveProdServ']));

                        if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['NoIdentificacion'])) > 0) {
                            $parte->setAttribute('NoIdentificacion', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['NoIdentificacion']));
                        }

                        $parte->setAttribute('Cantidad', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['Cantidad']));

                        if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['Unidad'])) > 0) {
                            $parte->setAttribute('Unidad', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['Unidad']));
                        }

                        $parte->setAttribute('Descripcion', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['Descripcion']));

                        if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['ValorUnitario'])) > 0) {
                            $parte->setAttribute('ValorUnitario', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['ValorUnitario']));
                        }

                        if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['Importe'])) > 0) {
                            $parte->setAttribute('Importe', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['Importe']));
                        }


                        for ($k = 0; $k < count($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['InformacionAduanera']); $k++) {

                            $parteInfoAduanera = $doc->createElement('cfdi:InformacionAduanera');
                            $parteInfoAduanera = $parte->appendChild($parteInfoAduanera);


                            $parteInfoAduanera->setAttribute('NumeroPedimento', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['Parte'][$j]['InformacionAduanera'][$k]['NumeroPedimento']));
                        }
                    }
                    #----------------------- PARTE


                }

                #----------------------- COMPLEMENTO CONCEPTO

                if ( isset($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['nombreAlumno']) && strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['nombreAlumno'])) > 0) {

                    $complemento = $doc->createElement('cfdi:ComplementoConcepto');
                    $complemento = $Concepto->appendChild($complemento);

                    $iedu = $doc->createElement('iedu:instEducativas');
                    $iedu = $complemento->appendChild($iedu);



                    $iedu->setAttribute('version', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['version']));

                    $iedu->setAttribute('nombreAlumno', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['nombreAlumno']));
                    $iedu->setAttribute('CURP', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['CURP']));
                    $iedu->setAttribute('nivelEducativo', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['nivelEducativo']));
                    $iedu->setAttribute('autRVOE', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['autRVOE']));

                    if (strlen(trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['rfcPago'])) > 0){
                        $iedu->setAttribute('rfcPago', trim($request['Comprobante']['Conceptos']['Concepto'][$i]['ComplementoConcepto']['instEducativas']['rfcPago']));
                    }


                }
                #----------------------- CUENTA PREDIAL
            }//if
        }//for
    }

    /**
     * @param $doc
     * @param $cfdiComprobante
     * @param $request
     */
    public function addImpuestos($doc, $cfdiComprobante, $request) {


        if (isset($request['Comprobante']['Impuestos']['Traslados']['Traslado'][0]['Importe']) || isset($request['Comprobante']['Impuestos']['Retenciones']['Retencion'][0]['Importe'])) {

            $Impuestos = $doc->createElement('cfdi:Impuestos');
            $Impuestos = $cfdiComprobante->appendChild($Impuestos);

            if (isset($request['Comprobante']['Impuestos']['TotalImpuestosTrasladados'])) {
//                if (count($request['Comprobante']['Impuestos']['Traslados']['Traslado']) > 0) {
                $Impuestos->setAttribute('TotalImpuestosTrasladados', trim($request['Comprobante']['Impuestos']['TotalImpuestosTrasladados']));
//                }
            }

            if (strlen($request['Comprobante']['Impuestos']['TotalImpuestosRetenidos']) > 0) {
//                if (count($request['Comprobante']['Impuestos']['Retenciones']['Retencion']) > 0) {
                $Impuestos->setAttribute('TotalImpuestosRetenidos', trim($request['Comprobante']['Impuestos']['TotalImpuestosRetenidos']));
//                }
            }

            if (isset($request['Comprobante']['Impuestos']['Retenciones']['Retencion'][0]['Importe'])) {

                if ($request['Comprobante']['Impuestos']['Retenciones']['Retencion'][0]['Importe'] > 0) {

                    $Retenciones = $doc->createElement('cfdi:Retenciones');
                    $Retenciones = $Impuestos->appendChild($Retenciones);

                    for ($i = 0; $i < count($request['Comprobante']['Impuestos']['Retenciones']['Retencion']); $i++) {
                        $Retencion = $doc->createElement('cfdi:Retencion');
                        $Retencion = $Retenciones->appendChild($Retencion);

                        $Retencion->setAttribute('Impuesto', trim($request['Comprobante']['Impuestos']['Retenciones']['Retencion'][$i]['Impuesto'])); //CATALOGO
                        $Retencion->setAttribute('Importe', trim($request['Comprobante']['Impuestos']['Retenciones']['Retencion'][$i]['Importe']));
                    }
                }
            }


            if (isset($request['Comprobante']['Impuestos']['Traslados']['Traslado'][0]['Importe'])) {

                if (strlen($request['Comprobante']['Impuestos']['Traslados']['Traslado'][0]['Importe']) > 0) {

                    $Traslados = $doc->createElement('cfdi:Traslados');
                    $Traslados = $Impuestos->appendChild($Traslados);

                    for ($i = 0; $i < count($request['Comprobante']['Impuestos']['Traslados']['Traslado']); $i++) {

                        if ($request['Comprobante']['Impuestos']['Traslados']['Traslado'][0]['Impuesto'] != "") {

                            $Traslado = $doc->createElement('cfdi:Traslado');
                            $Traslado = $Traslados->appendChild($Traslado);

                            $Traslado->setAttribute('Impuesto', trim($request['Comprobante']['Impuestos']['Traslados']['Traslado'][$i]['Impuesto'])); //CATALOGO
                            $Traslado->setAttribute('TipoFactor', trim($request['Comprobante']['Impuestos']['Traslados']['Traslado'][$i]['TipoFactor'])); //CATALOGO
                            $Traslado->setAttribute('TasaOCuota', trim($request['Comprobante']['Impuestos']['Traslados']['Traslado'][$i]['TasaOCuota']));
                            $Traslado->setAttribute('Importe', trim($request['Comprobante']['Impuestos']['Traslados']['Traslado'][$i]['Importe']));
                        }
                    }
                }
            }
        }
    }


    public function sellarXml33($peticion, $accesos, $xml){

        $xmlArray = XML2Array::createArray($xml);
        $sello = $xmlArray['cfdi:Comprobante']['@attributes']['Sello'];

        $empresa = $accesos['empresa'];
        $rutaCsd = "./../../empresas/".$empresa->getId()."/csd/";
        $ruta_xslt = "./../../SAT/v3/cadenaoriginal_3_3.xslt";

        #si el xml no viene con el sello lo agregamos
        if($sello == null){

            $numCertificado = '';
            $hex = str_replace('serial=', '', shell_exec('openssl x509 -in ' . $rutaCsd."/".$empresa->getId().".cer.pem" . ' -serial -noout'));
            for ($i = 0; $i < strlen($hex) - 1; $i = $i + 2) {
                $numCertificado .= substr(substr($hex, $i, 2), 1, 2);
            }

            $xmlSellado = new DomDocument("1.0", "UTF-8");
            $xmlSellado->loadXML($xml);
            $Comprobante = $xmlSellado->firstChild;
            $Comprobante->setAttribute('NoCertificado', $numCertificado);

            $xml = $xmlSellado->saveXML();


            $idPeticion = $peticion->getId();
            $archivo_xml = $idPeticion . "a.xml";


            file_put_contents($archivo_xml,$xml);


            // Obtiene la llave privada del Certificado de Sello Digital (CSD),
            //    Ojo , Nunca es la FIEL/FEA
            $pkeyid = openssl_get_privatekey(file_get_contents( $rutaCsd."/".$empresa->getId().".key.pem"));
            openssl_sign(self::satXmlv33GeneraCadenaOriginal($xml, $ruta_xslt), $crypttext, $pkeyid, OPENSSL_ALGO_SHA256);
            openssl_free_key($pkeyid);
            $sello = base64_encode($crypttext);      // lo codifica en formato base64


            $file = $rutaCsd.$empresa->getId().".cer.pem";      // Ruta al archivo de Llave publica


            $datos = file($file);
            $certificado = "";
            $carga = false;
            for ($i = 0; $i < sizeof($datos); $i++) {
                if (strstr($datos[$i], "END CERTIFICATE"))
                    $carga = false;
                if ($carga)
                    $certificado .= trim($datos[$i]);
                if (strstr($datos[$i], "BEGIN CERTIFICATE"))
                    $carga = true;
            }


            $xmlSellado = new DomDocument("1.0", "UTF-8");
            $xmlSellado->loadXML($xml);
            $Comprobante = $xmlSellado->firstChild;
            $Comprobante->setAttribute('Sello', $sello);
            $Comprobante->setAttribute('Certificado', $certificado);


            $xmlString = $xmlSellado->saveXML();

            unlink($archivo_xml);

            return $xmlString;


        }
        else
            return $xml;

    }

    /**
     * Metodo para obtener la cadena orginal del xml
     * @param $xmlString string sin codificar
     * @param $PathFileXslt path del cadenaoriginal_3_3.xslt
     * @return string de la cadena original
     */
    public function satXmlv33GeneraCadenaOriginal($xmlString, $PathFileXslt) {


        $paso = new DOMDocument("1.0", "UTF-8");
        $paso->loadXML($xmlString);
        $xsl = new DOMDocument("1.0", "UTF-8");
        
        $xsl->load($PathFileXslt);

        $proc = new \XSLTProcessor();
        $proc->importStyleSheet($xsl);

        $cadena_original = $proc->transformToXML($paso);

        /*if(!file_exists($PathFileXslt)){
            echo "no existe el archivo";
        }
        print_r($PathFileXslt);
        print_r($xmlString);
        var_dump($cadena_original);
        exit;*/
        return $cadena_original;
//        return '||3.3|a|LF|2017-10-30T11:00:00|01|30001000000300023708|CONTADO|3333.15|0.00|MXN|3756.09|I|PUE|72498|AAA010101AAA|PORTAL DE PRUEBAS|601|XAXX010101000|PRUEBASSoporte3.3|D10|10101703|1|A50|PIEZA|BOTELLA|3333.15|3333.15|3333.15|002|Tasa|0.16|533.30|002|Tasa|0.16|533.30|533.30|1.0|110.36|0|Pruebas|3.31|110.36|AMC8.1|1234567891234|2017-10-26|7504000107903|1234567891234|1234567891234|123|AA|123||';
    }

}