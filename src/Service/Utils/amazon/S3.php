<?php


namespace App\Service\Utils\amazon;


class S3
{

    static function crear($path,$body){


        $params['path']=$path;
        $params['body']=$body;


        $host = "https://uploads.clickfactura.app/index.php?r=app";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            )
        );

        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);

       
        
        $file = self::obtener($path);

        
        if( strlen($file) > 100){
            //if(file_exists($path))
            //    unlink($path);
            return true;
        }
        else
            return false;

    }
    static function obtener($path){

        if(str_starts_with($path,"/"))
            $pathAmazon = substr($path,1);
        else
        $pathAmazon = $path;

        $params['path']=$pathAmazon;
        
        $host = "https://uploads.clickfactura.app/index.php?r=app&path=".$params['path'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            )
        );


       

        
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);

        if( strlen($result['file']) > 100){
            return $result['file'];
        }
        else
            return "";

    }

    static function delete($path){

        $pathAmazon = substr($path,1);

        $params['path']=$pathAmazon;
        
        $host = "https://uploads.clickfactura.app/index.php?r=app&path=".$params['path'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            )
        );
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);

        if( strlen($result['file']) > 100){
            return $result['file'];
        }
        else
            return "";

    }

    
    /**
     * @deprecated funcion que ya no se debe ocupar usar en su lugar crear
     */

    static function crearFile($body,$path){

        $host = "https://api.clickfactura.app/api/amazon/s3/crearFile/";
        $params['body'] = $body;
        $params['path'] = $path;
        $result = self::request($params,$host);

        if($result != null)
            return true;


    }

    /**
     * @deprecated funcion que ya no se debe ocupar
     */

    static function obtenerFile($path){

        $host = "https://api.clickfactura.app/api/amazon/s3/obtener/";
        $params['xml'] = $path;
        $result = self::request($params,$host);

            return $result;

    }
    private function request($params,$host){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            )
        );

        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return $result;

    }
}