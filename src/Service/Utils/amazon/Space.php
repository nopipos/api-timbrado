<?php


namespace App\Service\Utils\amazon;
use Symfony\Component\HttpClient\HttpClient;


class Space
{

    static function crearFile($body,$path,$endPoint,$name){

        $host = $_ENV['URL_API'];
        $client = HttpClient::create(['verify_peer'=>false,'timeout'=>1500]);
        $response = $client->request('POST', $host.$endPoint,
            [
                'headers' => ['Content-Type' => 'application/json',],
                'json' => [
                    "fileName"=>$name,
                    "path"=>$path,
                    "file"=>$body
                ],
            ]
        );

        if($response->getStatusCode() ==200){
            return $response;
        }            
        else{
            file_put_contents("./file.txt",print_r($response,true));
        }
    }

    static function obtenerFile($fileCsd){

        $host = $_ENV['URL_API'];
        $client = HttpClient::create(['verify_peer'=>false,'timeout'=>1500]);
        $response = $client->request('GET', $host."/test/spaces/filebase?file=".$fileCsd,
            [
                'headers' => ['Content-Type' => 'application/json',
                'Accept'=>'*/*'
            ],
                
            ]
        );

        file_put_contents("./fileGet.txt",print_r($response->toArray(),true));
        file_put_contents("./fileGetCode.txt",print_r($response->getStatusCode(),true));

        if($response->getStatusCode() ==200){

            $csdFileInfo = $response->toArray();

            if($csdFileInfo['data']['success']){
                return $csdFileInfo['data']['data']['Body'];
            }
        }

        return null;
        

    }

    static function deleteFile($body,$path,$endPoint,$name){

        $host = $_ENV['URL_API'];
        $client = HttpClient::create(['verify_peer'=>false,'timeout'=>1500]);
        $response = $client->request('DELETE', $host.$endPoint."?file=".$path.$name,
            [
                'headers' => ['Content-Type' => 'application/json',],
                'json' => [
                    "fileName"=>$name,
                    "path"=>$path,
                    "file"=>$path.$name
                ],
            ]
        );

        return $response;

    }


}