<?php

namespace App\Service\Utils\Mail;

class SendMail
{
    /**
     * @param string[] para a quien o quienes se les enviara el mail
     * @param string cuerpo contenido en base64 que es el cuerpo del mail
     * @param string[] copia a quienes va dirigido el mail como copia
     * @param string[] asunto a quienes va dirigido el mail como copia
     * @param string[] archivos los archivos que se van a enviar
     * @param string[] de_para el nombre del remitente
     */
    static function enviarMail($para,$cuerpo,$copia,$asunto,$archivos,$de_nombre){

        $host ="https://www.clickfactura.com.mx/rest/enviacorreo/mail/enviar";

        $config = [
            'host' => 'server.clickfactura.com.mx',
            'port' => '25',
            'usuario' => "facturacion_portal@clickfactura.com.mx",
            //'usuario' => "recepcion@clickfactura.com.mx",
            'password' => base64_encode("m9Ri3t(4TA1)"),
            //'password' => base64_encode("Q(XB(1-?*0cD"),
        ];

        $datos = [
            "token"=>"0F0xlpGTicK3chZKrK569eDNI6",
            'body' => $cuerpo,
            'asunto' => $asunto,
            'from' => base64_encode("facturacion_portal@clickfactura.com.mx"),
            //'from' => base64_encode("recepcion@clickfactura.com.mx"),
            'from_name' => $de_nombre,
            'cc' => $copia,
            'to' => $para,
            'files' => $archivos,
            'config' => $config,
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($datos));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        )
        );

        $result = curl_exec($ch);
        curl_close($ch);
    
        file_put_contents("./mail.txt",print_r($result,true));
        $result = json_decode($result, true);

        if($result['error_code'] == "200")
            return true;
        else
            return false;
    }
}
?>