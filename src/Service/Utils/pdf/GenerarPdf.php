<?php

namespace App\Service\Utils\pdf;
use App\Service\Utils\NumerosToLetras;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Logo\Logo;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use App\Service\Utils\XML2Array;

class GenerarPdf
{

    static function generarPdf($xml, $request,$empresa,$con){

        $rutaXsltCadenaTfd = $_ENV['PATH_MAIN']."SAT/v4/cadenaoriginal_TFD_1_1.xslt";
        $xmlDato = XML2Array::createArray($xml);

        $xmlDato = $xmlDato['cfdi:Comprobante'];
        $timbreFiscal = $xmlDato['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes'];

        $noCertificadoSAT = $timbreFiscal['NoCertificadoSAT'];
        $noCertificado = $xmlDato['@attributes']['NoCertificado'];
        $selloSAT = $timbreFiscal['SelloSAT'];
        $selloCFD = $timbreFiscal['SelloCFD'];
        $fechaTimbrado = $timbreFiscal['FechaTimbrado'];
        $fecha =$xmlDato['@attributes']['Fecha'];
        $lugarExpedicion =$xmlDato['@attributes']['LugarExpedicion'];

        $moneda =$xmlDato['@attributes']['Moneda'];
        $rfcReceptor = $xmlDato['cfdi:Receptor']['@attributes']['Rfc'];
        $nombreReceptor = $xmlDato['cfdi:Receptor']['@attributes']['Nombre'];
        $usoCfdi = $xmlDato['cfdi:Receptor']['@attributes']['UsoCFDI'];
        $rfcEmisor = $xmlDato['cfdi:Emisor']['@attributes']['Rfc'];
        $nombreEmisor = $xmlDato['cfdi:Emisor']['@attributes']['Nombre'];
        $regimenEmisor = $xmlDato['cfdi:Emisor']['@attributes']['RegimenFiscal'];
        $totalXML = $xmlDato['@attributes']['Total'];
        $subtotal = $xmlDato['@attributes']['SubTotal'];
        $descuento = $xmlDato['@attributes']['Descuento'];
        $IVAXML = $xmlDato['cfdi:Impuestos']['@attributes']['TotalImpuestosTrasladados'];
        $tipoDeComprobante = $xmlDato['@attributes']['TipoDeComprobante'];
        $serie = $xmlDato['@attributes']['Serie'];
        $folio = $xmlDato['@attributes']['Folio'];
        $uuid = $timbreFiscal['UUID'];
        $uuid = $timbreFiscal['UUID'];
        $selloEmisor = $xmlDato['@attributes']['Sello'];
        $datosEmisor = $request['emisor'];
        $datosReceptor = $request['receptor'];
        $datosReceptor = $request['receptor'];
        $datosAdicionales = $request['adicionales'];
        $conceptosXml = $xmlDato['cfdi:Conceptos'];

        file_put_contents("con.txt",print_r($conceptosXml,true));
        $numerosToLetras = new NumerosToLetras();

        $valorCampo = self::getCatalogoDes($regimenEmisor,"cat_regimen_fiscal",$con,"cregimen_fiscal");
        $usoCfdiDesc = self::getCatalogoDes($usoCfdi,"cat_uso_cfdi",$con,"cuso_cfdi");

        $totalesLetra = $numerosToLetras->num2letras($totalXML,false,false);

        $writer = new PngWriter();

        $fe = strlen($selloEmisor) > 8 ? substr($selloEmisor,-8):"";
        $sellodigital = "https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?"
            . "&id=" . $uuid
            . "&re=".$rfcEmisor . "&rr="
            . $rfcReceptor . "&tt="
            . number_format($totalXML, 6, ".", "")
            . "&fe=" . $fe;

        // Create QR code
        $qrCode = QrCode::create($sellodigital)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        // Create generic logo
        $logo = Logo::create('./../assets/s.png')
            ->setResizeToWidth(50);

        $result = $writer->write($qrCode, $logo, null)->getString();
        file_put_contents("./../assets/sss.png",$result);

        #condicion para ASA
        $tituloFactura = "";
        if ($tipoDeComprobante == "I") {
            $tituloFactura = "FACTURA";
        } else
            $tituloFactura = "NOTA DE CREDITO";

        if (strtoupper($rfcEmisor) == "AAA010101AAA" || strtoupper($rfcEmisor) == "ASA6506102U9") {
            if ($tipoDeComprobante == "I") {
                $tituloFactura = "INGRESO";
            } else
                $tituloFactura = "EGRESO";
        }

        $datosConceptos = [];
        $contadorConcepto = 0;
        if(isset($conceptosXml[0]))
            $conceptosXml = $conceptosXml;
        else
            $conceptosXml[0] = $conceptosXml;

        foreach ($conceptosXml as $concepto) {
            
            $datosConceptos[$contadorConcepto]['Conceptos_descripcion'] = $concepto['cfdi:Concepto']['@attributes']['Descripcion'];
            $datosConceptos[$contadorConcepto]['Conceptos_cantidad'] = $concepto['cfdi:Concepto']['@attributes']['Cantidad'];
            $datosConceptos[$contadorConcepto]['Conceptos_unidad'] = $concepto['cfdi:Concepto']['@attributes']['Conceptos_unidad'];
            $datosConceptos[$contadorConcepto]['Conceptos_productoClave'] = $concepto['cfdi:Concepto']['@attributes']['NoIdentificacion'];
            $datosConceptos[$contadorConcepto]['Conceptos_valorUnitario'] = $concepto['cfdi:Concepto']['@attributes']['ValorUnitario'];
            $datosConceptos[$contadorConcepto]['Conceptos_importe'] = $concepto['cfdi:Concepto']['@attributes']['Conceptos_importe'];
            $datosConceptos[$contadorConcepto]['Conceptos_Objeto'] = $concepto['cfdi:Concepto']['@attributes']['ObjetoImp'];
            $datosConceptos[$contadorConcepto]['Conceptos_importe'] = $concepto['cfdi:Concepto']['@attributes']['Conceptos_importe'];
            $datosConceptos[$contadorConcepto]['Conceptos_ClaveSat'] = $concepto['cfdi:Concepto']['@attributes']['ClaveProdServ'];
            $datosConceptos[$contadorConcepto]['Conceptos_Claveunidad'] = $concepto['cfdi:Concepto']['@attributes']['ClaveUnidad'];
            $datosConceptos[$contadorConcepto]['Conceptos_Descuento33'] = $concepto['cfdi:Concepto']['@attributes']['Conceptos_Descuento33'];

            $contadorConcepto ++;
        }
        
        $datosxml = Array(
            'Emisor' =>
                [
                    'Emisor_rfc' => strtoupper($rfcEmisor),
                    'Emisor_nombre' => strtoupper($nombreEmisor),
                    'Emisor_calle' => $datosEmisor['Emisor_calle'],
                    'Emisor_noExterior' => $datosEmisor['Emisor_noExterior'],
                    'Emisor_noInterior' => $datosEmisor['Emisor_noInterior'],
                    'Emisor_colonia' => $datosEmisor['Emisor_colonia'],
                    'Emisor_municipio' => $datosEmisor['Emisor_municipio'],
                    'Emisor_estado' => $datosEmisor['Emisor_estado'],
                    'Emisor_pais' => $datosEmisor['Emisor_pais'],
                    'Emisor_codigoPostal' => $xmlDato['@attributes']['LugarExpedicion'],
                    'Emisor_RegimenFiscal' => self::getCatalogoDes($xmlDato['cfdi:Emisor']['@attributes']['RegimenFiscal'],"cat_regimen_fiscal",$con,"cregimen_fiscal"),
                    'Emisor_RegimenFiscal_CYD' => $xmlDato['cfdi:Emisor']['@attributes']['RegimenFiscal'] . ' ' . self::getCatalogoDes($xmlDato['cfdi:Emisor']['@attributes']['RegimenFiscal'],"cat_regimen_fiscal",$con,"cregimen_fiscal"),
                    'Emisor_RegimenFiscal_C' => $xmlDato['cfdi:Emisor']['@attributes']['RegimenFiscal']
                ],

            'factura' => [
                'nombre_alumno'=>"",
                'curp'=>"",
                'RFC_APROV'=>"",
                'nivel_educativo'=>"",
                'aut_rvoe'=>"",
                'rfc_pago'=>"",
                'ISH'=>"",
                'ExpedidoEn_calle'=>$datosEmisor['ExpedidoEn_calle'],
                'ExpedidoEn_noExterior'=>$datosEmisor['ExpedidoEn_noExterior'],
                'ExpedidoEn_noInterior'=>$datosEmisor['ExpedidoEn_noInterior'],
                'ExpedidoEn_colonia'=>$datosEmisor['ExpedidoEn_colonia'],
                'ExpedidoEn_codigoPostal'=>$datosEmisor['ExpedidoEn_codigoPostal'],
                'ExpedidoEn_municipio'=>$datosEmisor['ExpedidoEn_municipio'],
                'ExpedidoEn_estado'=>$datosEmisor['ExpedidoEn_estado'],
                'Conceptos_productoClave_gas'=>"",
                'version' => '4.0',
                'DocumentoNota' => $datosAdicionales['nota'],
                'MotivoCancelado' => $datosAdicionales['cancelado'],
                'noTicket' => $datosAdicionales['ticket'],
                'No_dias' => "",
                'lugarDeExpedicion' => $lugarExpedicion,
                'Tipo_Comprobante' => $xmlDato['@attributes']['TipoDeComprobante'],
                'TipoComprobante_CYD' => $tipoDeComprobanteDb ['c_TipoDeComprobante'] . ' ' . $tipoDeComprobanteDb['Descripcion'],
                'TipoComprobante_C' => $tipoDeComprobanteDb ['Descripcion'],
                'Moneda_CYC' => $moneda. ' ' . $monedaBd['Descripcion'],
                'Moneda_C' => $moneda,
                'CertificadoSAT' => $noCertificadoSAT,
                'CertificadoCSD' => $noCertificado,
                'SelloSAT' => $selloSAT,
                'SelloCFD' => $selloCFD,
                'CertificacionFecha' => $fechaTimbrado,
                'DocumentoFecha' => $fecha,
                'UUID' => $uuid,
                'pathCBB' => $path,
                //'MetodoPago' => strtoupper(str_replace("??", "�", $xmlDato['cfdi:Comprobante']['@attributes']['metodoDePago'])),
                'MetodoPago' => self::getCatalogoDes($xmlDato['@attributes']['MetodoPago'],"cat_metodo_pago",$con,"cmetodo_pago"),
                'MetodoPago_CYD' => $xmlDato['@attributes']['MetodoPago'] . ' ' . self::getCatalogoDes($xmlDato['@attributes']['MetodoPago'],"cat_metodo_pago",$con,"cmetodo_pago"),
                'MetodoPago_C' => $xmlDato['@attributes']['MetodoPago'] ,
                'clavemPago' => $xmlDato['cfdi:Comprobante']['@attributes']['MetodoPago'],
                'metPagoDesc' =>"",
                'formaDePago' => self::getCatalogoDes($xmlDato['@attributes']['FormaPago'],"cat_forma_pago",$con,"cforma_pago"),
                'formaDePago_CYD' => $xmlDato['@attributes']['FormaPago'] . ' ' . self::getCatalogoDes($xmlDato['@attributes']['FormaPago'],"cat_forma_pago",$con,"cforma_pago"),
                'formaDePago_C' => $xmlDato['@attributes']['FormaPago'],
                'TipoCambio' => $datosAdicionales['TipoCambio'],
                'TipoCambioXML' => $tipoCambioXp,
                'TotalProductos' => $TotalProductos,
                'cadenaOriginal' => self::satXmlv33GeneraCadenaOriginalTFD($xml, $rutaXsltCadenaTfd),
                #seccion de complemento de pagos#################################
                'p_FechaPago' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['FechaPago'],
                'P_FormaDePagoP' => "",//$this->getInfoMetodoPago($xmlDato['cfdi:Comprobante']['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['FormaDePagoP']),
                'P_MonedaP' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['MonedaP'],
                'P_TipoCambioP' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['TipoCambioP'],
                'P_Monto' => "$" . number_format($xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['Monto'], '2', '.', ','),
                'P_NumOperacion' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['NumOperacion'],
                'P_RfcEmisorCtaOrd' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['RfcEmisorCtaOrd'],
                'P_NomBancoOrdExt' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['NomBancoOrdExt'],
                'P_CtaOrdenante' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['CtaOrdenante'],
                'P_RfcEmisorCtaBen' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['RfcEmisorCtaBen'],
                'P_CtaBeneficiario' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['CtaBeneficiario'],
                #pagos SPEI
                'P_TipoCadPago' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['TipoCadPago'],
                'P_CerPagoSa' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['CertPago'],
                'P_CadPago' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['CadPago'],
                'P_SelloPago' => $xmlDato['cfdi:Complemento']['pago10:Pagos']['pago10:Pago']['@attributes']['SelloPago'],
                #DocumentosRelacionados
                'DocumentosRelAnterior' => $doctosRel,
                'DocumentosRel' => $doctosRel2,
                'Pagos' => $pagos,
            ],
            'receptor' =>  [
                'Receptor_rfc' => strtoupper(str_replace("&#xA;&#xA;", " ", $rfcReceptor)),
                'Receptor_nombre' => strtoupper(str_replace("&#xA;&#xA;", " ", $nombreReceptor)),
                'Receptor_Clave' => $docModel->cliente->ClienteClave,
                'UsoCFDI' => $usoCfdi,
                'UsoCFDI_CYD' => $usoCfdi . ' ' . $usoCfdiDesc,
                'UsoCFDI_C' => $usoCfdiDesc,
                'Residencia_fiscal' => $residenciaFiscRecXp,
                'NumRegIdTrib' => $numRegIdTribRecXmp,
                'Receptor_calle' => $documentoCilente->ClienteCalle,
                'Receptor_noExterior' => $documentoCilente->ClienteNoExt,
                'Receptor_noInterior' => $documentoCilente->ClienteNoInt,
                'Receptor_colonia' => $documentoCilente->ClienteCol,
                'Receptor_municipio' => $documentoCilente->ClienteMunicipio,
                'Receptor_estado' => "",//$this->claveEstadoCCE($documentoCilente->ClienteEstado),
                'Receptor_pais' => "",//utf8_encode($this->clavePaisCCE($documentoCilente->ClientePais)),
                'Receptor_codigoPostal' => $documentoCilente->ClienteCP,
                'Receptor_localidad' => $documentoCilente->ClienteLocalidad
            ],
            $impuestosLocales,
            'Totales_ivaPorcentaje' => $ivaDocumento,
            'Totales_subtotal' => $subtotal,
            'Totales_subtotal2' => $subTotal - $descuento,
            'Totales_total' => $totalXML,
            'Totales_iva' => $IVAXML,
            'Tasa_Iva' => $tasaIva,
            'totales_SubtotalMenosDescuento' => $subTotal - $descuento,
            'Totales_descuento' => $descuentoXp > 0 ? $descuentoXp : null,
            'Totales_descuentoMoney' => $Totales_descuentoMoney,
            'Totales_Ieps' => "",//$this->getTotalIepsTrasladado($xpath),
            'Totales_retencionIsr' => $docModel->DocumentoRetISR,
            'Totales_retencionIva' => "",//$this->getTotalretIva($xpath, "importe"),
            'Totales_ivaPorcentajeRetencion' => "",//$this->getTotalretIva($xpath, "porcentaje"),
            'Totales_ivaTasaRetencion' => "",//$this->getTotalretIva($xpath, "Tasa"),
            'Totales_DocumentoTipoMoneda' => $monedaXp,
            'Totales_ivaBaseRetencion' => "",//$this->getTotalretIva($xpath, "Base"),
            'Totales_ivaTipoRetencion' => "",//$this->getTotalretIva($xpath, "Tipo"),
            'Totales_ivaPorcentajeRetencion' => $docModel->DocumentoRetIvaPorcentaje,
            'Totales_isrPorcentajeRetencion' => number_format($docModel->DocumentoRetISRPorcentaje, 2),
             #ieps nuevas variables 17/01/218
            'Totales_ieps_general' => $iepsConceptos,
            'IVA_SIN_IEPS' => $IVAXML - $iepsConceptos,
            'Totales_iva_general' => $docModel->DocumentoIva,
            #Documentos relacionados
            'TipoRelacion' => $docModel->tipoRelacion,
            'UUID_Relacion' => "",//$this->UUIDRelacionados($docModel),
            'UUID_Relacion2' => "",//$this->UUIDRelacionadosOnly($docModel),
            'TotalesLetra' => $totalesLetra,//$numerosToLetras->num2letras($precio[0], false, false) . " " . $monedaDescripcion . " " . $precio[1] . "/100 " . $moneda,
            'conceptos' => $datosConceptos,
            'datosExtras' => [
                'referencia' => $docModel->documentosInformacionExtra->Referencia,
                'centroCostos' => $docModel->documentosInformacionExtra->centroCostos,
                'NoOrden' => $docModel->documentosInformacionExtra->NoOrden,
                'AgenteVentas' => $docModel->documentosInformacionExtra->AgenteVentas,
                'periodoReferencia' => $docModel->documentosInformacionExtra->periodoReferencia,
                'rdaut' => $rdauth,

                'origen' => $docModel->documentosInformacionExtra->origen,
                'usuario' => $docModel->documentosInformacionExtra->usuario,
                'email' => $docModel->documentosInformacionExtra->mail,
                'referencia2' => $docModel->documentosInformacionExtra->refeerencia2,
                'Moneda_Tipocambio' => $monedaXp,
                'descripcion_aduanas' => strlen($InformacionAduanera_numero) > 0 && strlen($datosAdicionales['AddBikeInfo']) == 0 ? "PEDIMENTO IMPORTACION:" . $InformacionAduanera_numero : "",
                'logo' => $logo,
                'titulo' => $tituloFactura,//self::getTipoComprobante($docModel->TipoComprobante),
                'Tipo_Comprobante' => $tipoDeComprobante,
                'serie' => $serie,
                'folio' => $folio,
                'numeroArticulos' => $numArt,
                'cancelado' => $documentoCancelado == "1" ? Yii::$app->params['local']['PathLocal'].'csd/cancelado.png' : '',
            ],
            'datosAdicionales' => $datosAdicionales,
            "",//$this->datosAutomaticos($docModel->idSucursal, $docModel->idDocumento),
            "",//$this->cartaPorte($docModel->TipoComprobante, $docModel->idSucursal, $docModel->idDocumento),
            'datosNomina' => $datosNomina,
            'datosPercepciones' => $datosPercepciones,
            'datosDeducciones' => $datosDeducciones,
            'Receptor_tipoRegimen' => $regimen,
            'datosIncapadidades' => $datosIncapacidades,
            'impuestosConceptos' => "",//$this->getImpuestosConceptos($xpath),
            'groupImpuestosConceptos' => "",//$this->getGrupoImpuestosConceptos($xpath, $subTotalXp),
            'tipoRelacion' => $tipoRelacionXp,
            'uuidsRelacionados' => $cfdiRelacionadoArray,
            'INE' => $INE,
            /*pdfcomplementos\cce::generarComplemento($docModel),
            pdfcomplementos\Donataria::generarComplemento($docModel),
            pdfcomplementos\notaria::generarComplemento($docModel),
            'inmueble' => pdfcomplementos\notaria::generarInmuebles($docModel),
            'enajenante' => pdfcomplementos\notaria::generarEnajenantes($docModel),
            'adquiriente' => pdfcomplementos\notaria::generarAdquirientes($docModel),
            'serviciosParciales' => $serviciosParciales,*/
        );

        $host = "https://www.clickfactura.com.mx/api/pdf/generar";

        $params = array(
            "esquema" => "CFDI4",
            "usuario" => "6131",
            "documento" => "FA",
            'datos' => $datosxml,
            'rfc'=>strtoupper($rfcEmisor)
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            )
        );
        $result = curl_exec($ch);

        file_put_contents( "petResul.txt",print_r($result,true));
        file_put_contents( "petRequest.txt",json_encode($params));
        curl_close($ch);
        $datos = json_decode($result, true);

        if($datos['error_code'] == 200){
            return $datos['message'];
        }
        else{
            return "";
        }
    }

    static function generaPdf33Ws($esquema, $idUsuario, $tipoComprobante,$datosxml){

        $params = array(
            "esquema" => $esquema,
            "usuario" => $idUsuario,
            "documento" => $tipoComprobante,
            'datos' => $datosxml,
        );

        $host = "https://www.clickfactura.com.mx/api/pdf/generar";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            )
        );

        $result = curl_exec($ch);
        curl_close($ch);
        $datos = json_decode($result, true);
        $pdf = "";

        if ($datos['status'] == 1 && $datos['error_code'] == 200)
            $pdf = $datos['message'];

        return $pdf;
    }

    static function getCatalogoDes($clave,$tabla,$con,$campo){

        $sql = "SELECT descripcion FROM ".$tabla." WHERE ".$campo." = ?";
        $stmt = $con->prepare($sql);
        $stmt->bindValue(1, $clave);
        $resultado = $stmt->executeQuery();
        
        $res = $resultado->fetchAllAssociative();
        file_put_contents("res_query.txt",print_r($res,true));
        if(isset($res[0]['descripcion'])){
            return $res[0]['descripcion'];
        }
        return "";
    }

    static function satXmlv33GeneraCadenaOriginalTFD($xmlString, $PathFileXslt)
    {

        $string = <<<XML
$xmlString 
XML;

        $xml = new \SimpleXMLElement($string);
        $xml->registerXPathNamespace('ns1', 'http://www.sat.gob.mx/cfd/4');
        $xml->registerXPathNamespace('ns2', 'http://www.sat.gob.mx/TimbreFiscalDigital');

        $nodeTmbre = $xml->xpath("//ns1:Comprobante/ns1:Complemento/ns2:TimbreFiscalDigital");

        
        
        $timbreXml = '';
        foreach ($nodeTmbre as $node) {
            $timbreXml .= $node->asXML();
        }

        if (strpos($timbreXml, 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"') === false) {
            $timbreXml = str_replace("<tfd:TimbreFiscalDigital", '<tfd:TimbreFiscalDigital xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ', $timbreXml);
        }

        if (strpos($timbreXml, 'xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital"') === false) {
            $timbreXml = str_replace("<tfd:TimbreFiscalDigital", '<tfd:TimbreFiscalDigital xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital" ', $timbreXml);
        }


        file_put_contents("timbre.txt",print_r($timbreXml,true));
        $cadena_original = self::satXmlv33GeneraCadenaOriginal($timbreXml, $PathFileXslt);

        return $cadena_original;
    }

    /**
     * Metodo para obtener la cadena orginal del xml
     * @param $xmlString string sin codificar
     * @param $PathFileXslt path del cadenaoriginal_3_3.xslt
     * @return string de la cadena original
     */
    static function satXmlv33GeneraCadenaOriginal($xmlString, $PathFileXslt)
    {


        $paso = new \DOMDocument("1.0", "UTF-8");
        $paso->loadXML($xmlString);
        $xsl = new \DOMDocument("1.0", "UTF-8");
        $xsl->load($PathFileXslt);
        $proc = new \XSLTProcessor();
        $proc->importStyleSheet($xsl);
        $cadena_original = $proc->transformToXML($paso);


        return $cadena_original;
    }
}