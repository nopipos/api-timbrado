<?php

namespace App\Service\Utils;

use App\Entity\Pac;
use App\Entity\Empresas;
use App\Entity\Facturas;
use App\Entity\FacturasTimbre;
use App\Service\Utils\amazon\S3;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpClient\HttpClient;

class Peticiones
{

    public static function guardarPeticion($xml,ManagerRegistry $doctrine,$request,$resultLogin){

        $em = $doctrine->getManager();
        $xmlArray = XML2Array::createArray($xml);

        $pathFactura = "/clickfactura/".$resultLogin['empresa']->getRfc()."/emision/";
        $tipoComprobante = $xmlArray['cfdi:Comprobante']['@attributes']['TipoDeComprobante'];

        $fechaArray = explode("T",$xmlArray['cfdi:Comprobante']['@attributes']['Fecha']);
        $fechaArray = explode("-",$fechaArray[0]);
        
        if($tipoComprobante == "FA")
            $pathFactura = $pathFactura."factura/";
        elseif($tipoComprobante == "NC")
            $pathFactura = $pathFactura."notaCredito/";
        else
            $pathFactura = $pathFactura."factura/";

        $pathFactura = $pathFactura.$fechaArray[0]."/".$fechaArray[1]."/".$fechaArray[2]."/";
        $fechaExplo = explode("T", $xmlArray['cfdi:Comprobante']['@attributes']['Fecha']);
        $fechaExplo = explode("-", $fechaExplo[0]);

        #cuando el usuario tiene el pass de SuperPruebas o superPruebas
        if($request['password'] == "5e5b8d64174619cbc64bb0aa17a14f2c44c3e54a" || $request['password'] == "f316100a77ef8bccd0f70c506c324046ae71e06d"){
            $resultLogin['empresa']->setPac1("15");
            $resultLogin['empresa']->setPac2("19");
        }
        $pacs = [];
        $pacs[] = $resultLogin['empresa']->getPac1();
        $pacs[] = $resultLogin['empresa']->getPac2();

        $PacRepo = $doctrine->getRepository(Pac::class);
        $pacModel = $PacRepo->findOneBy(['id'=>$resultLogin['empresa']->getPac1()]);

        $peticion = null;
        if( isset($request['id-factura'])){

            $DocumentoRepo = $doctrine->getRepository(Facturas::class);
            $documentoModel = $DocumentoRepo->findOneBy(['id'=>$request['id-factura'],'empresa'=>$resultLogin['empresa']->getId()]);
        }

        $peticion = new FacturasTimbre();
        $uuid = uniqid("PET-");
        $peticion->setRutaTimbre($pathFactura.$uuid.".xml");

        $peticion->setFactura($documentoModel == null? null:$documentoModel);
        $peticion->setEmpresa($resultLogin['empresa']);
        $peticion->setFechaPeticion(\DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s')));
        $peticion->setPac($pacModel);
        
        $em->persist($peticion);
        $em->flush();
        S3::crear($pathFactura.$uuid.".xml",base64_encode($xml));
        return array('peticion' => $peticion, 'resultLogin' => $resultLogin, 'pacs' => $pacs);

    }
    
    public static function guardarPeticionTimbre($resultadoTimbre, FacturasTimbre  $peticion,$segundos,$doctrine,$pdf = null){

        $xmlArray = XML2Array::createArray(base64_decode($resultadoTimbre['xmlData']));

        $peticion->setCodigo($resultadoTimbre['codigo']);
        $peticion->setMensaje($resultadoTimbre['mensaje']);
        $em = $doctrine->getManager();
        $em->persist($peticion);
        $em->flush();

        if($resultadoTimbre['codigo'] == "200"){

            $pathFactura = "clickfactura/".$peticion->getEmpresa()->getRfc()."/emision/";
            $tipoComprobante = $xmlArray['cfdi:Comprobante']['@attributes']['TipoDeComprobante'];
            $fechaArray = explode("T",$xmlArray['cfdi:Comprobante']['@attributes']['Fecha']);
            $fechaArray = explode("-",$fechaArray[0]);
            
            if($tipoComprobante == "I")
                $pathFactura = $pathFactura."factura/";
            elseif($tipoComprobante == "E")
                $pathFactura = $pathFactura."notaCredito/";
            else
                $pathFactura = $pathFactura."factura/";
    
            $pathFactura = $pathFactura.$fechaArray[0]."/".$fechaArray[1]."/".$fechaArray[2]."/";

            S3::delete($pathFactura.$peticion->getRutaTimbre());

            $fechaTimbrado = $xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['FechaTimbrado'];
            $fechaTimbrado = str_replace("T"," ",$fechaTimbrado);

            $uuid = $xmlArray['cfdi:Comprobante']['cfdi:Complemento']['tfd:TimbreFiscalDigital']['@attributes']['UUID'];
            $peticion->setFolioFiscal($uuid);
            $peticion->setFechaTimbrado(\DateTime::createFromFormat('Y-m-d H:i:s', $fechaTimbrado ));
            $peticion->setRutaTimbre($pathFactura.$uuid.".xml");
            $peticion->setFolioFiscal($uuid);

            $em->persist($peticion);
            $em->flush();

            #guardo el xml en S3
            S3::crear($pathFactura.$uuid.".xml",$resultadoTimbre['xmlData']);
            S3::crear($pathFactura.$uuid.".pdf",$pdf);
        }
        return ['peticion'=>$peticion,'path'=>$pathFactura.$uuid];
    }

    public static function getPacsEmpresa($empresaId, $empresaRfc, ManagerRegistry $doctrine){
        #region get empresa
        $empresasRepo = $doctrine->getRepository(Empresas::class);
        $infoEmpresa = NULL;
        if($empresaId != NULL){
            $infoEmpresa = $empresasRepo->find($empresaId);
        }

        if($infoEmpresa == NULL && $empresaRfc != NULL){
            $infoEmpresa = $empresasRepo->findOneBy(['rfc'=> $empresaRfc]);
        }

        if($infoEmpresa == NULL){
            return ['success' => false, 'msg' => 'La empresa no existe en nuestra base de datos'];
        }
        #endregion

        #region response
        $pacs = [];
        $pacs[] = $infoEmpresa->getPac1();
        $pacs[] = $infoEmpresa->getPac2();
        $pacs[] = $infoEmpresa->getPac3();

        return ['success' => true, 'msg' => $pacs];
        #endregion
    }
}
