<?php


namespace App\Service\Utils;


class Catalogos
{

    static function tipoDocumento($clave){

        $catalogos['FA'] = "I";
        $catalogos['NC'] = "E";
        $catalogos['FP'] = "P";

        return $catalogos[$clave];
    }
}