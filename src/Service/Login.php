<?php


namespace App\Service;


use App\Entity\Empresas;
use App\Entity\Usuarios;
use App\Entity\Pac;
use App\Entity\UsuariosEmpresas;
use Doctrine\Persistence\ManagerRegistry;

class Login
{
    public static function logueo( ManagerRegistry $repo,$datos){

        error_reporting(E_ERROR | E_WARNING | E_PARSE);

        $respuesta['estatus'] = null;

        $usuariosRepo = $repo->getRepository(Usuarios::class);

        #aqui se meten todas las condiciones para los login de los diferentes clientes,
        # hay q tratar de que no se mueva mucho la logica

        if(isset($datos['esPortal']) &&  $datos['esPortal']==true)
            $modelUsuario = $usuariosRepo->findOneBy(['email'=>$datos['usuario'],'estatus'=>'1']);
        else
            $modelUsuario = $usuariosRepo->findOneBy(['email'=>$datos['usuario'],'password'=>sha1($datos['password']),'estatus'=>'1']);

        if($modelUsuario == null){
            $respuesta['estatus'] = 0;
        }
        else{

            $respuesta['usuario'] = $modelUsuario;
            $respuesta['estatus'] = 1;

            #busco la empresa por su mail
            $usuariosEmpresasRepo = $repo->getRepository(UsuariosEmpresas::class);
            $modelUsuarioEmpresas = $usuariosEmpresasRepo->findOneBy(['usuario'=>$modelUsuario->getId()]);

            if($modelUsuarioEmpresas != null){
                $empresasRepo = $repo->getRepository(Empresas::class);
                $modelEmpresa = $empresasRepo->findOneBy(['id'=>$modelUsuarioEmpresas->getEmpresa()]);
                
                $respuesta['empresa'] = $modelEmpresa;
            }

            if($modelEmpresa == null){
                #no existe la empresa que viene en el json
                $respuesta['estatus'] = 2;

            }else{

                #verifico   que ya tenga sus CSD cargados
                if($modelEmpresa->getCer() == null){
                    $respuesta['estatus'] = 4;
                }
                else{
                    $patMain = $_ENV['PATH_MAIN'];
                    if(!is_file($patMain."/empresas/".$modelEmpresa->getId()."/temp/".$modelEmpresa->getCer().".pem")){
                        $respuesta['estatus'] = 4;
                    }
                }

                #verifico que tenga pac asignado
                if($modelEmpresa->getPac1() == null || $modelEmpresa->getPac1() == "" )
                $respuesta['estatus'] = 5;

                #verifico que el pac asignado exista en la BD
                $PacRepo = $repo->getRepository(Pac::class);
                $pacModel = $PacRepo->findOneBy(['id'=>$modelEmpresa->getPac1()]);

                if($pacModel == null){
                    $respuesta['estatus'] = 6;
                }
            }
            
            #si es distinto de 3 entonces quiere decir que es un usuario al que se le deben verificar las empresas
            if($modelUsuario->getTipoLogin() != 3){

                #busco las empresas que estan relacionadas para este usuario para saber si tiene permisos
                $usuariosEmpresasRepo = $repo->getRepository(UsuariosEmpresas::class);
                $modelUsuarioEmpresas = $usuariosEmpresasRepo->findOneBy(['empresa'=>$modelEmpresa->getId(),'usuario'=>$modelUsuario->getId()]);

                if($modelUsuarioEmpresas == null){
                    #Existe la empresa pero no esta relacionada a este login
                    $respuesta['estatus'] = 3;

                }
                $respuesta['empresa'] = $modelEmpresa;
                
            }
            
        }

        return $respuesta;
    }
}